// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_COMMAND
#define RCUXX_SHELL_COMMAND
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace Rcuxx
{
  namespace Shell 
  {
    //____________________________________________________________________
    /** 
     * A command in the shell 
     */
    struct Command 
    {
      /** Type of argument list */
      typedef std::vector<std::string> arg_list;
      /** 
       * Constructor 
       * 
       * @param name      Name of command 
       * @param need_args Whether we need arguments 
       */
      Command(const char* name, bool need_args=true) 
	: _name(name),  _need_args(need_args)
      {}
      /** 
       * Destructor
       */
      virtual ~Command() {}
      /** 
       * Get the name of the command
       * 
       * @return name of the command 
       */      
      const std::string& Name() const { return _name; }
      /** 
       * Whether we need arguments or not 
       * 
       * @return {@code true} if we need arguments 
       */
      bool NeedArguments() const { return _need_args; }
      /** 
       * Description of the command 
       * 
       * @return String description 
       */
      virtual const char* Description() const { return "DOCUMENT ME"; }
      /** 
       * Show the usage description 
       * 
       */
      virtual void ShowUsage() const 
      { 
	std::stringstream cmd;
	cmd << Name() << (_need_args ? " ARGS"  : "");
	std::cout << std::left << std::setw(20) << cmd.str() 
		  << std::right << Description() << std::endl;
      }
      virtual void ShowHelp() const { ShowUsage(); }
      /** 
       * Exectue the command with no arguments
       * 
       * @return {@code true} on success. 
       */
      virtual bool operator()()
      {
	arg_list args;
	return operator()(args);
      }
      /** 
       * Exectue the command with arguments
       * 
       * @param args Arguments
       *
       * @return {@code true} on success. 
       */
      virtual bool operator()(const arg_list& args) 
      {
	bool miss_args = (args.size() <= 0 && _need_args);
	bool do_help   = (args.size() > 0 && 
			  (args[0] == "-h" || args[0] == "--help"));
	if (do_help) { 
	  ShowHelp();
	  return true;
	}
	if (miss_args) { 
	  ShowUsage();
	  return miss_args;
	}
	return Execute(args);
      }
      /** 
       * Execute the command. Concrete classes must implement this. 
       * 
       * @param args Arguments
       * 
       * @return {@code true} on success. 
       */
      virtual bool Execute(const arg_list& args) = 0;
      virtual bool IgnoreReturn() const { return false; }
      /** Name of command */
      std::string _name;
      /** Whether we need arguments */
      bool        _need_args;
    };
  }
}
#endif
//
// EOF
//

    

