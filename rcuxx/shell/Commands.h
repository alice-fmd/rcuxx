// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_COMMANDS
#define RCUXX_SHELL_COMMANDS
#include <rcuxx/shell/Command.h>
#include <rcuxx/shell/Sh.h>
#include <rcuxx/RegisterGuard.h>
#include <stack>

namespace Rcuxx 
{
  namespace Shell 
  {
    //____________________________________________________________________
    struct Help : public Command
    {
      Help(Sh& shell) : Command("help", false), _shell(shell) {}
      const char* Description() const
      {
	return "Show available commands or help on a single command/node";
      }
      bool Execute(const arg_list& args) 
      { 
	if (args.size() <= 0) { 
	  _shell.ListCommands();
	  return true;
	}
	for (arg_list::const_iterator i = args.begin(); i != args.end(); i++) {
	  Command* c = _shell.FindCommand(*i);
	  if (c) { 
	    c->ShowHelp();
	    return true;;
	  }
	  const Node* n = _shell.FileSystem().FindNode(*i);
	  if (n) { 
	    n->Show(n->Name().size() + 2, true);
	    return true;
	  }
	  std::cout << *i << ": No such command or node" << std::endl;
	}
	return true;
      }
      Sh& _shell;
    };

    //____________________________________________________________________
    struct Alias : public Command
    {
      Alias(const char* name, Command* command) 
	: Command(name, command->NeedArguments()), 
	  _command(command)
      {}
      const char* Description() const { return _command->Description(); }
      bool Execute(const arg_list& a) { return _command->Execute(a); }
      virtual bool IgnoreReturn() const { return _command->IgnoreReturn(); }
      Command* _command;
    };

    
    //____________________________________________________________________
    struct Pwd : public Command
    {
      Pwd(Fs& fs) : Command("pwd", false), _fs(fs) {}
      const char* Description() const 
      { 
	return "Print working directory";
      }
      bool Execute(const arg_list&) 
      { 
	std::cout << _fs.Pwd() << std::endl;
	return true;
      }
      Fs& _fs;
    };
    //____________________________________________________________________
    struct Exit : public Command
    {
      Exit(Sh& shell) : Command("exit", false), _shell(shell) {}
      const char* Description() const
      {
	return "Exit shell";
      }
      bool Execute(const arg_list&) { 
	_shell.Quit();
	return true;
      }
      bool IgnoreReturn() const { return true; }
      Sh& _shell;
    };

    //____________________________________________________________________
    struct Ls : public Command
    {
      Ls(Fs& fs) : Command("ls", false), _fs(fs) {}
      const char* Description() const 
      {
	return "List current directory or directory/file given";
      }
      void ShowHelp() const 
      {
	std::cout << Name() << " [OPTIONS] [ARGS]\n\n"  
		  << Description() << "\n\n"
		  << "Options:\n"
		  << "\t-l          Long listing\n"
		  << "\t-r          Recursive listing\n"
		  << std::endl;
      }
      bool Execute(const arg_list& args) 
      { 
	bool recursive = false;
	bool l         = false;
	std::string which;
	for (Command::arg_list::const_iterator i=args.begin(); 
	     i!=args.end(); ++i) {
	  if ((*i)[0] == '-') { 
	    switch((*i)[1]) { 
	    case 'r': recursive = true; break;
	    case 'l': l         = true; break;
	    default: 
	      std::cerr << "ls: unknown option " << (*i) << std::endl;
	      return false;
	    }
	    continue;
	  }
	  which = *i;
	}
    
	return _fs.Ls(which, recursive, l);
	// return true;
      }
      Fs& _fs;
    };
    //____________________________________________________________________
    struct Cd : public Command
    {
      Cd(Fs&  fs) : Command("cd", false), _fs(fs), _last("/") {}
      const char* Description() const 
      {
	return "Change directory";
      }
      void ShowHelp() const 
      {
	ShowUsage();
	std::cout << "\n"
		  << "If no argument is specified, change directory to the\n" 
		  << "root.  If the argument is '-' then change directory to\n"
		  << "the last visited directory"
		  << std::endl;
      }
      bool Execute(const arg_list& args) 
      { 
	std::string t = _last; 
	_last         = _fs.Pwd();
	if (args.size() == 0)  return _fs.Cd("/");
	if (args[0] == "-")    return _fs.Cd(t);
	return                        _fs.Cd(args[0]);
      }
      Fs& _fs;
      std::string _last;
  
    };

    //____________________________________________________________________
    struct Cat : public Command
    {
      Cat(Fs& fs) : Command("cat", true), _fs(fs) {}
      const char* Description() const 
      {
	return "Display contents of a file or set of files";
      }
      bool Execute(const arg_list& args) 
      {
	for (arg_list::const_iterator i = args.begin(); i != args.end(); ++i) { 
	  Node* node = const_cast<Node*>(_fs.FindNode(*i));
	  if (!node) { 
	    std::cout << *i << ": no such file" << std::endl;
	    return false;
	  }
	  if (!node->Read()) return false;
	}
	return true;
      }
      Fs& _fs;
    };

    //____________________________________________________________________
    struct Sleep : public Command
    {
      Sleep() : Command("sleep", true) {}
      const char* Description() const 
      {
	return "Sleep for specified number of seconds";
      }
      bool Execute(const arg_list& args) 
      {
	int dur = 1;
	if (args.size() > 0) {
	  std::stringstream sd(args[0]);
	  sd >> dur;
	}
	std::cout << "Sleeping for " << dur << " seconds ... " << std::flush;
	sleep(dur);
	std::cout << "done";
	return true;
      }
    };

    //____________________________________________________________________
    struct Echo : public Command
    {
      Echo() : Command("echo", true) {}
      const char* Description() const 
      {
	return "Echo string(s) to output";
      }
      bool Execute(const arg_list& args) 
      {
	bool first = true;
	for (arg_list::const_iterator i = args.begin(); i != args.end(); ++i) { 
	  if (first) std::cout << " ";
	  std::cout << *i << std::flush;
	}
	std::cout << std::endl;
	return true;
      }
    };


    //____________________________________________________________________
    struct Set : public Command
    {
      Set(Fs& fs) : Command("set", true), _fs(fs) {}
      const char* Description() const 
      {
	return "Set the content of a file (register, memory)";
      }
      void ShowHelp() const 
      {
	std::cout << Name() << "  NODE VALUE\n" 
		  << "NODE = VALUE\n"
		  << Name() << "  NODE ADDRESS VALUE\n"
		  << "NODE[ADDRESS] = VALUE\n\n"
		  << "The first two forms are for a single register, while\n"
		  << "the last two forms are for nodes which can be\n"
		  << "sub-address (e.g., memories)" << std::endl;
      }
      bool Execute(const arg_list& args) 
      {
	if (args.size() < 2) { 
	  std::cout << Name() 
		    << ": I need at least 2 arguments, register and value"
		    << std::endl;
	  return false;
	}
	Node* node = const_cast<Node*>(_fs.FindNode(args[0]));
	if (!node) { 
	  std::cout << args[0] << ": no such file" << std::endl;
	  	  return false;
	}
    
	unsigned int addr = 0, value = str2val(args[1]);
	if (args.size() > 2) { 
	  addr  = value;
	  value = str2val(args[2]);
	}
	return node->Write(addr, value);
      }
      Fs& _fs;
    };
    //____________________________________________________________________
    struct Sync : public Command
    {
      Sync(Fs& fs) : Command("sync", false), _fs(fs) {}
      const char* Description() const 
      {
	return "Syncronise to hardware";
      }
      bool Execute(const arg_list&) 
      {
	std::string cur = _fs.Pwd();
	_fs.Cd("/");
	if (!_fs.Update()) { 
	  std::cout << Name() << ": Failed!" << std::endl;
	  return false;
	}
	if (!_fs.FindNode(cur)) return true;
	return _fs.Cd(cur);
      }
      Fs& _fs;
    };
    //____________________________________________________________________
    struct Option : public Command
    {
      Option(Sh& sh) : Command("option", true), _sh(sh)
      {}
      const char* Description() const 
      {
	return "Set shell options";
      }
      void ShowHelp() const
      {
	std::cout << Name() << " [SWITCHES]\n\n"
		  << "Switches:\n"
		  << "    -e/+e        Turn on/off exit on failure\n"
		  << std::endl;
      }
      bool Execute(const arg_list& args) 
      {
	for (arg_list::const_iterator i = args.begin(); i!= args.end(); ++i) {
	  bool on = true;
	  if ((*i)[0] == '+') on = false;

	  switch ((*i)[1]) { 
	  case 'e': _sh._strict = on; break;
	  }
	}
	return true;
      }
      Sh& _sh;
    };
    //____________________________________________________________________
    struct Find : public Command
    {
      Find(Fs& fs) : Command("find", true), _fs(fs) {}
      const char* Description() const 
      {
	return "Find a node (recursive)";
      }
      void ShowHelp() const 
      {
	std::cout << Name() << " NAME\n"
		  << Name() << " NODE NAME\n\n"
		  << "The first form starts the search at the currrent node,\n"
		  << "while the second form starts the search at the\n"
		  << "specified node.\n\n"
		  << "In both cases the full path of possible matches is shown"
		  << std::endl;
      }
      bool Execute(const arg_list& args) 
      {
	std::string path=".";
	std::string name=args[0];
	if (args.size() > 1) { 
	  path = args[0];
	  name = args[1];
	}
	std::list<Node*> nodes;
	if (!_fs.FindNodes(nodes, name, path)) return true;
	for (std::list<Node*>::const_iterator i = nodes.begin(); 
	     i != nodes.end(); ++i) 
	  std::cout << (*i)->Path() << std::endl;
	return true;
      }
      Fs& _fs;
    };
    //____________________________________________________________________
    struct Push : public Command
    {
      Push(Fs& fs) : Command("push", true), _fs(fs) {}
      const char* Description() const 
      {
	return "Push a register on restore stack";
      }
      void ShowHelp() const 
      {
	std::cout << Name() << " NAME\n\n"
		  << "Save the value of register NAME (full path)\n"
		  << "for later restore using \"pop\""
		  << std::endl;
      }
      bool Execute(const arg_list& args)
      {
	for (arg_list::const_iterator i = args.begin(); i != args.end(); ++i) { 
	  Node* node = const_cast<Node*>(_fs.FindNode(*i));
	  if (!node) { 
	    std::cout << *i << ": no such file" << std::endl;
	    return false;
	  }
	  if (!node->IsWritable()) {
	    std::cout << *i << ": not writeable" << std::endl;
	    return false;
	  }
	  RegisterNode* reg = dynamic_cast<RegisterNode*>(node);
	  if (!reg) {
	    std::cout << *i << ": is not a register" << std::endl;
	    return false;
	  }
	  Node* pwd = _fs._cur;
	  if (reg->Parent() && reg->Parent() != &_fs._top)
	    reg->Parent()->Cd();
	  _guards.push(new Rcuxx::RegisterGuard(reg->_register));
	  std::cout << reg->Path() << " pushed on guard stack" << std::endl;
	}
      }
      Fs& _fs;
      /** The stack of saved registers */
      typedef std::stack<Rcuxx::RegisterGuard*> guard_stack;
      /** Our guards */
      guard_stack _guards;
    };
    //____________________________________________________________________
    struct Pop : public Command
    {
      typedef Push::guard_stack guard_stack;
      guard_stack& _guards;
      Pop(Push& p) : Command("pop", false), _guards(p._guards) {}
      const char* Description() const 
      {
	return "Pop all registers on restore stack";
      }
      void ShowHelp() const 
      {
	std::cout << Name() << "\n\n"
		  << "Restore all saved registers (using \"push\")"
		  << std::endl;
      }
      bool Execute(const arg_list&) 
      {
	while (!_guards.empty()) {
	  Rcuxx::RegisterGuard* g  = _guards.top();
	  _guards.pop();
	  delete g;
	  g = 0;
	}
      }
    };
    
  }
}
#endif
//
// EOF
//
