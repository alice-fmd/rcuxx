// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_NODE
#define RCUXX_SHELL_NODE
#include <list>
#include <string>

namespace Rcuxx
{
  class Rcu;

  namespace Shell 
  {
    struct Indent;

    //------------------------------------------------------------------
    /** 
     * A node in the file system 
     */
    struct Node 
    {
      enum { 
	executable = 1, 
	writable   = 2, 
	readable   = 4
      };
	
      /** 
       * Constructor 
       * 
       * @param parent Parent node
       * @param name   Name of node 
       */      
      Node(Node* parent, const std::string& name); 
      /** 
       * Copy constrotr
       * 
       * @param other Node to copy from
       */
      Node(const Node& other);
      /** 
       * Destructor 
       */
      virtual ~Node() {  Clear(); }
      /** 
       * Remove all children
       * 
       */
      void Clear(); 
      /** 
       * Remove a node from this node 
       * 
       * @param n Node to remove 
       * 
       * @return @c true of the note is found and removed
       */
      bool RemoveNode(Node*& n);
      /** 
       * Get the name of the node 
       * 
       * @return Name of node
       */
      const std::string& Name() const  { return _name; }
      /** 
       * Get the description of the node 
       * 
       * @return Description of node
       */
      virtual const char* Description() const  { return _name.c_str(); }
      /** 
       * Get the permissions for this node
       * 
       * @return Permissions of the node
       */
      virtual unsigned short Permissions() const { return 0; }
      virtual unsigned short SortOrder() const = 0; // { return -1; }
      /** 
       * Get parent node
       * 
       * @return Pointer to parent node, if any.
       */
      Node* Parent() const { return _parent; }
      /** 
       * Change directory to node 
       * 
       * @return @c true on success 
       */
      virtual bool Cd();
      /** 
       * Update contents
       * 
       * @return @c true on success 
       */
      virtual bool Update(Rcuxx::Rcu*) { return true; }
      /** 
       * Get the foreground color
       * 
       * @return Foreground colour
       */
      virtual int Fg() const; 
      /** 
       * Get the Background color
       * 
       * @return Background colour
       */
      virtual int Bg() const { return -1; }
      /** 
       * Get the effect
       * 
       * @return Effect
       */
      virtual int Fx() const; 
      /** 
       * Check if this is a directory
       * 
       * @return @c true if this is a directory
       */
      virtual bool IsDir() const { return false; /*Type()>=folder_type;*/ }
      /** 
       * Return @c true if we can execute this node
       * 
       * 
       * @return Whether we can execute the node
       */
      virtual bool IsExecutable() const { return Permissions() & executable; }
      /** 
       * Whether we can write to this node 
       * 
       * @return @c true if this node is writeable
       */
      virtual bool IsWritable() const { return Permissions() & writable; }
      /** 
       * Whether we can read from this node 
       * 
       * @return @c true if this node is writeable
       */
      virtual bool IsReadable() const { return Permissions() & readable; }
      /** 
       * Whether we can write to this node 
       * 
       * @return @c true if this node is writeable
       */
      virtual bool IsReadonly() const { return !IsWritable() ; }
      /** 
       * Read content of node from HW
       * 
       * @return @c true on success
       */
      virtual bool Read();         
      /** 
       * Write content of node to HW
       * 
       * @return @c true on success
       */
      virtual bool Write(unsigned int);
      /** 
       * Write content of node to HW
       * 
       * @return @c true on success
       */
      virtual bool Write(unsigned int, unsigned int);        
      /** 
       * Execute node
       * 
       * @return @c true on success
       */
      virtual bool Exec();
      /** 
       * Find a node under this node
       * 
       * @param name Name of the name 
       * 
       * @return Pointer to node if found
       */
      virtual Node* FindNode(const std::string& name) const;
      /** 
       * Find nodes that has the name @a name.  All possible nodes are
       * returned in @a ret. 
       * 
       * @param name Name of nodes to look for
       * @param ret  Return list
       * 
       * @return @c true if any nodes where found. 
       */
      virtual bool FindNodes(const std::string& name, 
			     std::list<Node*>& ret) const; 
      /** 
       * Find a node under this nodes tree
       * 
       * @param name Name of node
       * 
       * @return Pointer to node if found
       */
      virtual Node* Find(const std::string& name) const;
      /** 
       * Get the full path of this node
       * 
       * @return Full path as a string 
       */      
      std::string Path() const; 
      /** 
       * Compare this node to another node
       * 
       * @param other  Other node to compare to
       * @param byName if @c true compare names. 
       * 
       * @return -1 if this is smaller than @c other, 0 if they
       * are the same, and +1 if other is smaller than this. 
       */
      int Compare(const Node& other, bool byName=true) const; 
      /** 
       * Show this node
       * 
       * @param n Width available
       * @param l Wheter to show long format or not
       */
      virtual void Show(int n, bool l=false) const; 
      /** 
       * Find the widest name of the sub-nodes
       * 
       * @return Width of name of all sub-nodes
       */
      int FindWidest() const; 
      /** 
       * Sort the content of this node
       * 
       */
      virtual void Sort();
      /** 
       * List this node
       * 
       * @param recursive if @c true list all sub-nodes
       */
      virtual void Ls(Indent& indent, bool recursive, bool l) const; 
      /**  List of nodes */
      typedef std::list<Node*> node_list;
      /** Name of node */
      std::string      _name;
      /** Parent node, if any */
      Node*            _parent;
      /** Sub-nodes */
      node_list        _nodes;
    };
    /** 
     * Compare nodes 
     * 
     * @param lhs Left-hand side
     * @param rhs Right-hand side
     * 
     * @return @c true if @a lhs is @e smaller than @a rhs
     */
    inline bool 
    operator<(const Node& lhs, const Node& rhs)
    {
      return lhs.Compare(rhs) < 0;
    }
  }
}
#endif
//
// EOF
//
