// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_ALTRONODE
#define RCUXX_SHELL_ALTRONODE
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/Node.h>
#include <rcuxx/shell/RegisterNode.h>
#include <rcuxx/shell/CommandNode.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Altro.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/altro/AltroCommand.h>
#include <rcuxx/altro/AltroRegister.h>
#include <iostream>

namespace Rcuxx
{
  namespace Shell 
  {
    //------------------------------------------------------------------
    struct AltroNode : public Node 
    { 
      /** 
       * Constructor 
       * 
       * @param parent   Parent node
       * @param altro    Altro interface 
       * @param board    Board number 
       * @param chip     Chip number 
       * @param channel  Channel number
       */
      AltroNode(Node* parent, Rcuxx::Altro* altro, int board, int chip,
		int channel=-1) 
	: Node(parent, MakeName(board, chip, channel)), 
	  _altro(altro), 
	  _board(board), 
	  _chip(chip),
	  _channel(channel)
      {
	std::stringstream s;
	s << "ALTRO # " << chip; 
	if (channel >= 0) s << " channel # " << channel;
	_title = s.str();
      }
      const char* Description() const { return _title.c_str(); }
      /** 
       * This is a directory 
       * 
       * @return {@code true}
       */
      bool IsDir() const { return true; }
      unsigned short SortOrder() const { return 1; }
      /** 
       * Update from hardware 
       * 
       * @param rcu RCU HW interface 
       * 
       * @return {@code true} on success 
       */      
      bool Update(Rcuxx::Rcu* rcu)
      {
	Rcuxx::RcuACL* acl = rcu->ACL();
	if (_channel < 0) { 
	  int minC = (_board >= 0 ? 0  : -1);
	  int maxC = (_board >= 0 ? 16 : 0);
	  
	  for (int c=minC; c < maxC; c++) { 
	    std::string nc = AltroNode::MakeName(_board, _chip, 
						 (_board < 0 ? 0 : c));
	    Node*       ch = FindNode(nc);
	    if (c >= 0 && acl->CheckChannel(_board, _chip, c) == 0) { 
	      if (ch) RemoveNode(ch); 
	      continue;
	    }
	    if (!ch)    ch = new AltroNode(this, _altro, _board, _chip, 
					   (_board < 0 ? 0 : c));
	    ch->Update(rcu);
	  }
	}
	typedef Rcuxx::Altro::Registers Registers;
	const Registers& r = _altro->RegisterList();
	for (Registers::const_iterator i = r.begin(); i != r.end(); ++i) { 
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  bool bcast = (*i)->IsBroadcastable();
	  if (_chip < 0 && bcast || ((bcast && _channel < 0) || 
				      (!bcast && _channel >= 0))) 
	    new RegisterNode(this, *i);
	}
	if (_channel >= 0) return true;

	typedef Rcuxx::Altro::Commands Commands;
	const Commands& c = _altro->CommandList();
	for (Commands::const_iterator i = c.begin(); i != c.end(); ++i) { 
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  new CommandNode(this, *i, rcu);
	}
	Sort();
	return true;
      }
      /** 
       * Change directory to this FEC.  sets the address
       * 
       * @return {@code true} on success
       */
      bool Cd()
      {
	if (_chip >= 0) 
	  _altro->SetAddress(_board, _chip, _channel);
	else 
	  _altro->SetBroadcast();
	Sort();
	return true;
      }
      /** 
       * Make a name 
       * 
       * @param board Board number  
       * 
       * @return String with name
       */
      static std::string MakeName(int board, int chip, int channel=-1) 
      {
	std::string ret((channel < 0 ? "altro" : "channel"));
	if (board < 0) { 
	  ret.append("s");
	  return ret;
	}
	std::stringstream s;
	s << ret << PRINT_HEX((channel < 0 ? chip : channel), 1);
	ret = s.str();
	return ret;
      }
      /** HW interface */
      Rcuxx::Altro* _altro;
      /** Board # */
      int           _board;
      /** Chip # */
      int           _chip;
      /** Channel # */
      int           _channel;
      std::string _title;
    };
  }
}
#endif
//
// EOF
//
