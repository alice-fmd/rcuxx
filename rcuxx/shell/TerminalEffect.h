// -*- mode: C++ -*-
/*
 * Terminal effects 
 */
#ifndef RCUXX_SHELL_TERMINALEFFECT
#define RCUXX_SHELL_TERMINALEFFECT
#include <iostream>
#include <cstdio>

namespace Rcuxx
{
  namespace Shell 
  {
    /** 
     * Terminal effects 
     */
    namespace Effect {
      /**
       * Types of effects 
       */
      enum { 
	/** No effect */
	none      = 0, 
	/** Bright */
	bright    = 1, 
	/** Dimmed */
	dim       = 2,
	/** Underlined */
	underline = 3, 
	/** Blinking */
	blink     = 4, 
	/** Reverse fore-/background colour */
	reverse   = 7, 
	/** Not shown */
	hidden    = 8
      };
    }
    //____________________________________________________________________
    /** 
     * Colour effects 
     */
    namespace Color { 
      enum { 
	/** Black */
	black     = 0, 
	/** Red */
	red       = 1, 
	/** Green */
	green     = 2, 
	/** Yellow */
	yellow    = 3, 
	/** Blue */
	blue      = 4, 
	/** Magenta */
	magenta   = 5,
	/** Cyan */
	cyan      = 6, 
	/** White */
	white     = 7
      };
    }
    //____________________________________________________________________
    /** 
     * A terminal effect 
     */
    struct _TerminalEffect
    {
      /** The effect */
      int  _effect;
      /** Foreground colour */
      int  _fg;
      /** Background colour */
      int  _bg;
      /** 
       * Constructor 
       */
      _TerminalEffect() : _effect(-1),  _fg(-1), _bg(-1) {}
      /** 
       * Set the foreground color 
       */
      void SetFg(int fg)    { _fg     = fg; }
      /** 
       * Set the background color 
       */
      void SetBg(int bg)    { _bg     = bg; }
      /**
       * Set the effect 
       */
      void SetEffect(int e) { _effect = e; }
      /** 
       * Write escape sequence to output stream 
       * 
       * @param o Stream to manipulate 
       */
      std::ostream& Apply(std::ostream& o)
      {
	char buf[13];
	int n  = sprintf(buf, "%c[", char(0x1B));
	int n1 = n;
	if (_effect >= 0) n += sprintf(&buf[n], "%d;", _effect);
	if (_fg     >= 0) n += sprintf(&buf[n], "%d;", (_fg+30));
	if (_bg     >= 0) n += sprintf(&buf[n], "%d;", (_bg+40));
	buf[n-1] = 'm';
	buf[n]   = '\0';
	return o << buf << std::flush;
      }
    };

    /** 
     * Output streamer of a terminal effect 
     *
     * @param o Output stream
     * @param t Terminal effect 
     *
     * @return o after manipulation 
     */
    inline std::ostream&
    operator<<(std::ostream& o, _TerminalEffect t) 
    {
      return t.Apply(o);
    }

    /** 
     * Convenience function to do IO manipulator operations 
     * 
     * @param fg Foreground color 
     * @return a Terminal effect object 
     */
    inline _TerminalEffect setfg(int fg)
    {
      _TerminalEffect t;
      t.SetFg(fg);
      return t;
    }
    /** 
     * Convenience function to do IO manipulator operations 
     * 
     * @param bg Background color 
     * @return a Terminal effect object 
     */
    inline _TerminalEffect setbg(int bg)
    {
      _TerminalEffect t;
      t.SetBg(bg);
      return t;
    }
    /** 
     * Convenience function to do IO manipulator operations 
     * 
     * @param e Effect 
     * @return a Terminal effect object 
     */
    inline _TerminalEffect seteffect(int e)
    {
      _TerminalEffect t;
      t.SetEffect(e);
      return t;
    }
  }
}
#endif
//
// EOF
//     
    
