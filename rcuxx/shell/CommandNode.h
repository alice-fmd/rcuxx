// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_COMMANDNODE
#define RCUXX_SHELL_COMMANDNODE
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/Command.h>
#include <rcuxx/shell/Node.h>
#include <rcuxx/Command.h>
#include <iostream>

namespace Rcuxx
{
  class Rcu;

  namespace Shell 
  {
    //------------------------------------------------------------------
    struct CommandNode : public Node, public Command
    {
      CommandNode(Node* parent, Rcuxx::Command* c, Rcuxx::Rcu* rcu) 
	: Node(parent, c->Name()), 
	  Command(c->Name().c_str(), false),
	  _command(c),
	  _rcu(rcu)
      {}
      const char* Description() const { return _command->Title().c_str(); }
      void ShowHelp() const 
      {
	std::cout << Command::Name() << "\n\n"
		  << "Executes the command " << Description() << std::endl;
      }
      /** 
       * Get the permissions 
       * 
       * @return Permissions mask
       */
      unsigned short Permissions()  const { return Node::executable; }
      unsigned short SortOrder() const { return 3; }
      /** 
       * List (not applicable)
       * 
       */
      void Ls(bool) const { }
      /** 
       * Execute this command 
       * 
       * @param value Argument value 
       * 
       * @return {@code true} on success 
       */
      bool Exec(unsigned int value)
      {
	unsigned int ret = _command->Commit();
	if (ret == 0) return true;
	std::cerr << "Error: " << Node::Name() << ": " 
		  << _rcu->ErrorString(ret) 
		  << std::endl;
	return false;
      }
      /** 
       * Execute this command 
       * 
       * @param a Argument list
       * 
       * @return {@code true} on success 
       */
      bool Execute(const arg_list& a) 
      {
	return Exec(0);
      }
      /** Pointer to HW interface */
      Rcuxx::Command* _command;
      Rcuxx::Rcu*     _rcu;
    };
  }
}
#endif 
//
// EOF
//
