// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_RCUNODE
#define RCUXX_SHELL_RCUNODE
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/Node.h>
#include <rcuxx/shell/BcNode.h>
#include <rcuxx/shell/MemoryNode.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/Altro.h>
#include <rcuxx/Bc.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuACL.h>
#include <iostream>

namespace Rcuxx
{
  class Rcu;

  namespace Shell 
  {
    //------------------------------------------------------------------
    struct RcuNode : public Node 
    { 
      /** 
       * Constructor 
       * 
       * @param rcu   HW interface
       * @param bc    BC HW interface
       * @param altro ALTRO HW interface 
       */
      RcuNode(Rcuxx::Rcu* rcu, Rcuxx::Bc* bc, Rcuxx::Altro* altro) 
	: Node(0, "rcu"), 
	  _rcu(rcu), 
	  _bc(bc),
	  _altro(altro)
      {}
      /** 
       * This is a directory
       * 
       * @return {@code true}
       */
      bool IsDir() const { return true; }
      virtual unsigned short SortOrder() const { return 0; }
      /** 
       * Update this node 
       * 
       * @return {@code true} on success
       */
      bool Update(Rcuxx::Rcu*) 
      {
	Rcuxx::RcuACTFEC* actfec = _rcu->ACTFEC();
	Rcuxx::RcuACL*    acl    = _rcu->ACL();
      
	unsigned int ret = 0;
	if ((ret = actfec->Update())) { 
	  std::cout << "Failed to update active front-end card list:\n\t" 
		    << _rcu->ErrorString(ret) << std::endl;
	  return false;
	}
	if ((ret = acl->Update())) { 
	  std::cout << "Failed to update active channel list:\n\t" 
		    << _rcu->ErrorString(ret) << std::endl;
	  return false;
	}
	do { 
	  std::string nbs = BcNode::MakeName(-1);
	  Node*       bcs = FindNode(nbs);
	  if (actfec->Value() == 0) { 
	    if (bcs) RemoveNode(bcs);
	    continue;
	  }
	  if (!bcs)   bcs = new BcNode(this, _bc, _altro, -1);
	  bcs->Update(_rcu);
	} while (false);

	for (int b = 0; b < 32; b++) { 
	  std::string nb = BcNode::MakeName(b);
	  Node*       bc = FindNode(nb);
	  if (!actfec->IsOn(b)) { 
	    if (bc) RemoveNode(bc);
	    continue;
	  }
	  if (!bc) bc = new BcNode(this, _bc, _altro, b);
	  bc->Update(_rcu);
	}
	typedef Rcuxx::Rcu::Registers Registers;
	const Registers& r = _rcu->RegisterList();
	for (Registers::const_iterator i = r.begin(); i != r.end(); ++i) {
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  new RegisterNode(this, *i);
	}
	typedef Rcuxx::Rcu::Commands Commands;
	const Commands& c = _rcu->CommandList();
	for (Commands::const_iterator i = c.begin(); i != c.end(); ++i) {
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  new CommandNode(this, *i, _rcu);
	}
	typedef Rcuxx::Rcu::Memories Memories;
	const Memories& m = _rcu->MemoryList();
	for (Memories::const_iterator i = m.begin(); i != m.end(); ++i) {
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  new MemoryNode(this, *i);
	}
	Sort();
	return true;
      }
      /** Reference to  HW interface */
      Rcuxx::Rcu*   _rcu;
      /** Reference to  HW interface */
      Rcuxx::Bc*    _bc;
      /** Reference to  HW interface */
      Rcuxx::Altro* _altro;
    };
  }
}
#endif
//
// EOF
//
