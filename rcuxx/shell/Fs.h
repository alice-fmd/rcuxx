// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_FS
#define RCUXX_SHELL_FS
#include <rcuxx/shell/RcuNode.h>

namespace Rcuxx
{
  namespace Shell 
  {
    struct Indent;

    //____________________________________________________________________
    struct Fs
    {
      /** 
       * Constructor 
       * 
       * @param rcu   HW interface 
       * @param bc    HW interface 
       * @param altro HW interface 
       */
      Fs(Rcuxx::Rcu&   rcu,  
	 Rcuxx::Bc&    bc, 
	 Rcuxx::Altro& altro, 
	 Indent&       indent);
      /** 
       * Update from hardware 
       * 
       * @return {@code true} on success
       */
      bool Update();
      /** 
       * Print current path
       * 
       * @return Current path
       */
      std::string Pwd() const  { return _cur->Path();  }
      /** 
       * List a directory 
       * 
       * @param which      Which directory to list
       * @param recursive  Wether to list recursively
       * @param l          Whehter to do long listing
       * 
       * @return true on success 
       */
      bool Ls(const std::string& which, bool recursive, bool l) const;
      /** 
       * Find a node
       * 
       * @param name     What to look for 
       * @param verbose  Whether to be verbose 
       * 
       * @return Node found or null
       */
      const Node* FindNode(const std::string& name, bool verbose=true) const;
      /** 
       * Find a node 
       * 
       * @param name     Node to look for 
       * @param path     Starting path 
       * @param verbose  Whether to be verbose 
       * 
       * @return Found node or null
       */
      Node* Find(const std::string& name, const std::string& path=".", 
		 bool verbose=true) const;
      /** 
       * Find all possible nodes that match @a name
       * 
       * @param ret     Return list
       * @param name    Name to look for 
       * @param verbose Wehter to be verbose
       * 
       * @return {@code true} id any node was found
       */
      bool FindNodes(std::list<Node*>& ret,
		     const std::string& name, 
		     const std::string& path=".",
		     bool verbose=true) const;
      /** 
       * Change directory to @a name 
       * 
       * @param name Where to go
       * 
       * @return {@code true} on success
       */    
      bool Cd(const std::string& name);
      /** Top node */
      RcuNode        _top;
      /** current node */
      Node*          _cur;
      /** HW interface */
      Rcuxx::Rcu&    _rcu;
      /** HW interface */
      Rcuxx::Bc&     _bc;
      /** HW interface */
      Rcuxx::Altro&  _altro;
      /** Indention */
      Indent& _indent;
    };
  }
}
#endif
//
// EOF
//
