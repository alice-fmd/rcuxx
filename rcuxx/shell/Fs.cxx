#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <rcuxx/shell/Fs.h>
#include <rcuxx/shell/Indent.h>

namespace Rcuxx
{
  namespace Shell 
  {
    //________________________________________________________________
    Fs::Fs(Rcuxx::Rcu&   rcu,  
	   Rcuxx::Bc&    bc, 
	   Rcuxx::Altro& altro, 
	   Indent&       indent) 
      : _top(&rcu, &bc, &altro), 
	_cur(&_top), 
	_rcu(rcu),
	_bc(bc),
	_altro(altro), 
	_indent(indent)
    {
      // _top.Update(&_rcu);
      Update();
    }
    //________________________________________________________________
    bool Fs::Update()
    {
      // actfec->Print();
      bool ret = _top.Update(_top._rcu);
      if (ret) _top.Sort();
      return ret;
    }
    //________________________________________________________________
    bool Fs::Ls(const std::string& which, bool recursive, bool l) const 
    {
      _indent.Reset();
      if (which.empty()) { 
	_cur->Ls(_indent, recursive, l);
	return true;
      }
      const Node* n = FindNode(which);
      if (!n) { 
	std::cout << which << ": no such node" << std::endl;
	return false;
      }
      if (n->IsDir()) const_cast<Node*>(n)->Update(_top._rcu);
      else            n->Show(n->Name().size()+2, true);
      n->Ls(_indent, recursive, l);
      return true;
    }
    //________________________________________________________________
    const Node* Fs::FindNode(const std::string& name, bool verbose) const
    {
      if (name == ".") return _cur;
      
      const Node* ret = _cur;
    
      std::string path(name);
      if (path.substr(0,4).compare("/rcu") == 0) { 
	ret = &_top;
	path.erase(0, 4);
      }
      else if (path[0] == '/') { 
	path.erase(0, 1);
	ret = &_top;
      }
    
      std::stringstream s(path);
      while (!s.eof()) { 
	std::string d;
	std::getline(s, d, '/');

	if (d.empty()) continue;

	if (d == ".." && ret != &_top) { 
	  ret = ret->Parent();
	  continue;
	}
      
	Node* next = ret->FindNode(d);
	if (!next) { 
	  if (verbose)
	    std::cout << d << ": no such directory in " << ret->Name() 
		      << std::endl;
	  ret = 0;
	  break;
	}
	ret = next;
      }
      return ret;
    }
    //________________________________________________________________
    Node* Fs::Find(const std::string& name, const std::string& path, 
		   bool verbose) const
    {
      const Node* top = FindNode(path, verbose);
      if (!top) { 
	std::cout << setfg(Color::red) << seteffect(Effect::bright)
		  << "Invalid path: " << path << setfg(Color::black) 
		  << seteffect(Effect::none) << std::endl;
	  return 0;
      }
      return top->Find(name);
    }
    //________________________________________________________________
    bool Fs::FindNodes(std::list<Node*>& ret,
		       const std::string& name, 
		       const std::string& path, 
		       bool verbose) const 
    {
      const Node* top = FindNode(path, verbose);
      if (!top) { 
	std::cout << setfg(Color::red) << seteffect(Effect::bright)
		  << "Invalid path: " << path << setfg(Color::black) 
		  << seteffect(Effect::none) << std::endl;
	return 0;
      }
      return top->FindNodes(name, ret);
    }
    //________________________________________________________________
    bool Fs::Cd(const std::string& name)
    {
      if (name == ".") return true;
    
      if (!Update()) { 
	std::cout << "Failed to update FS" << std::endl;
	return false;
      }

      Node* t = const_cast<Node*>(FindNode(name));
      if (!t) { 
	std::cout << name << ": no such directory" << std::endl;
	return false;
      }
    
      if (!t->Cd()) return false;

      _cur = t;
      return true;
    }
  }
}
//____________________________________________________________________
//
// EOF
//

    
