#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "rcuxx/Rcu.h"
#include "rcuxx/Altro.h"
#include "rcuxx/Bc.h"
#include "rcuxx/Fmd.h"
#include "rcuxx/Url.h"
#include "rcuxx/shell/Sh.h"
#include "rcuxx/shell/Commands.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <stdexcept>
#include <cstdio>
#include <cstring>
#include "Options.h"


int main(int argc, char** argv)
{
  std::string node("echo:?fwvers=0x280210");

  Option<bool> hOpt('h', "help",     "This help", false, false); 
  Option<bool> HOpt('H', "long-help","This help", false, false); 
  Option<bool> vOpt('v', "version",  "Show version",false,false);
  Option<bool> dOpt('d', "debug",    "Turn on debug messages",false,false);
  Option<bool> eOpt('e', "emulation","Emulation",false,false);
  Option<bool> fOpt('f', "fmd",      "Use FMD",true,false);
  Option<bool> rOpt('r', "restore",  "Restore IMEM on exit", false, false);
  Option<bool> sOpt('s', "strict",   "Return error on failures",false,false);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(HOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(eOpt);
  cl.Add(fOpt);
  cl.Add(rOpt);
  cl.Add(sOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { cl.Help(); return 0; }
  if (HOpt.IsSet()) { cl.Help(); Rcuxx::Rcu::PrintHelp(std::cout); return 0; }
  if (vOpt.IsSet()) {
    std::cout << "rcuxx version " << VERSION << std::endl;
    return 0;
  }

  if (cl.Remain().size() > 0) node = cl.Remain()[0];
  bool emulate = eOpt.IsSet();
  bool debug   = dOpt.IsSet();
  bool fmd     = fOpt.IsSet();
  bool success = false;
  bool restore = rOpt.IsSet();
  bool strict  = sOpt.IsSet();

  std::cout << "Running RCU++ shell " << PACKAGE_VERSION << "-" << RELEASE 
	    << "\n\n"
	    << "\tEmulation: " << emulate << "\n"
	    << "\tFMD:       " << fmd     << "\n"
	    << "\tDebug:     " << debug   << "\n"
	    << "\tNode:      " << node    << "\n"
	    << "\tRestore:   " << restore << "\n"
	    << "\tStrict:    " << strict  << "\n"
	    << std::endl;
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(node.c_str(), emulate, debug);  
  if (!rcu) {
    std::cerr << "Failed to open connection to RCU on "
	      << node << " - exiting" << std::endl;
    return 1;
  }
  if (debug) rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
  try {
    Rcuxx::Altro   altro(*rcu);
    Rcuxx::Bc*     bc  = (fmd ? new Rcuxx::Fmd(*rcu) : new Rcuxx::Bc(*rcu));

    if (emulate) { 
      Rcuxx::RcuACTFEC* actfec = rcu->ACTFEC();
      actfec->SetValue(0x30003);
      actfec->Commit();
      Rcuxx::RcuACL*    acl    = rcu->ACL();
      for (int b = 0; b < 32; b++) { 
	if (!actfec->IsOn(b)) continue;
	for (int a = 0; a < 3; a++) { 
	  unsigned int mask = (a == 1 ? 0xFF : 0xFFFF);
	  acl->EnableChip(b, a, mask);
	}
      }
      acl->Commit();
    }
    Rcuxx::RcuIMEM::RestoreGuard rg(rcu->IMEM(), restore, false);

    Rcuxx::Shell::Sh       sh(*rcu, *bc, altro);
    Rcuxx::Shell::Command* exit = new Rcuxx::Shell::Exit(sh);
    Rcuxx::Shell::Command* ls   = new Rcuxx::Shell::Ls(sh.FileSystem());
    Rcuxx::Shell::Command* cat  = new Rcuxx::Shell::Cat(sh.FileSystem());
    Rcuxx::Shell::Push*    push = new Rcuxx::Shell::Push(sh.FileSystem());
    sh.AddCommand(new Rcuxx::Shell::Help(sh));
    sh.AddCommand(new Rcuxx::Shell::Pwd(sh.FileSystem()));
    sh.AddCommand(new Rcuxx::Shell::Cd(sh.FileSystem()));
    sh.AddCommand(new Rcuxx::Shell::Set(sh.FileSystem()));
    sh.AddCommand(new Rcuxx::Shell::Sync(sh.FileSystem()));
    sh.AddCommand(new Rcuxx::Shell::Find(sh.FileSystem()));
    sh.AddCommand(new Rcuxx::Shell::Sleep());
    sh.AddCommand(exit);
    sh.AddCommand(new Rcuxx::Shell::Alias("quit", exit));
    sh.AddCommand(ls);
    sh.AddCommand(new Rcuxx::Shell::Alias("dir", ls));
    sh.AddCommand(cat);
    sh.AddCommand(new Rcuxx::Shell::Echo());
    sh.AddCommand(new Rcuxx::Shell::Option(sh));
    sh.AddCommand(push);
    sh.AddCommand(new Rcuxx::Shell::Pop(*push));
    sh._strict = strict;
    if (sh._strict)
      std::cout << "Strict (failure checking) more enabled" << std::endl;
    success = sh.Run();

  }
  catch (unsigned int ret) {
    std::cerr << "Failed to update <" << ret 
	      << ">: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  return success ? 0 : 1;
}
//
// EOF
//

