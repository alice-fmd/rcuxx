#include <rcuxx/shell/Node.h>
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/TerminalEffect.h>
#include <rcuxx/shell/Indent.h>
#include <iostream>
#include <iomanip>
#include <typeinfo>

namespace std 
{ 
  /** Less-than comparator */
  template <>
  struct less<Rcuxx::Shell::Node*>
  {
    bool 
    operator()(const Rcuxx::Shell::Node* lhs, 
	       const Rcuxx::Shell::Node* rhs) const
    {
      if (!lhs && !rhs) return true;
      if (!lhs) return false;
      if (!rhs) return true;
      int cmp = lhs->Compare(*rhs);
      return cmp < 0;
    }
  };
}
namespace Rcuxx
{
  namespace Shell 
  {
    //________________________________________________________________
    Node::Node(Node* parent, const std::string& name) 
      : _name(name), 
	_parent(parent)
    {
      _name = trim(_name);
      if (!_parent) return;
      _parent->_nodes.push_back(this);
    }
    //________________________________________________________________
    Node::Node(const Node& other) 
      : _name(other._name), 
	_parent(other._parent)
    {}
    //________________________________________________________________
    void Node::Clear() 
    {
      for (node_list::iterator i = _nodes.begin(); i != _nodes.end(); ++i) 
	delete *i;
      _nodes.clear();
    }	
    //________________________________________________________________
    bool Node::RemoveNode(Node*& n)
    {
      bool ret = false;
      for (node_list::iterator i = _nodes.begin(); i != _nodes.end(); ++i) {
	if (*i == n) { 
	  delete n;
	  n = 0;
	  _nodes.erase(i);
	  ret = true;
	  break;
	}
      }
      return ret;
    }
    //________________________________________________________________
    bool Node::Cd()                
    { 
      if (!IsDir()) { 
	std::cout << Name() << ": cannot cd here" << std::endl;
	return false;
      }
      return true; 
    }
    //________________________________________________________________
    int Node::Fg() const 
    { 
      if (IsDir())        return Color::blue; 
      if (IsExecutable()) return Color::green;
      if (IsReadonly())   return Color::cyan;
      return -1; 
    }
    //________________________________________________________________
    int Node::Fx() const 
    { 
      if (IsDir())        return Effect::bright;
      if (IsExecutable()) return Effect::bright;
      if (IsReadonly())   return Effect::dim;
      return Effect::none; 
    }
    //________________________________________________________________
    bool Node::Read()         
    { 
      std::cout << Name() << ": cannot read from this node" << std::endl;
      return false; 
    }
    //________________________________________________________________
    bool Node::Write(unsigned int)        
    { 
      if (IsReadonly()) {
	std::cout << Name() << ": read only" << std::endl;
	return false; 
      }
      return true;
    }
    //________________________________________________________________
    bool Node::Write(unsigned int, unsigned int)        
    { 
      if (IsReadonly()) {
	std::cout << Name() << ": read only" << std::endl;
	return false; 
      }
      return true;
    }
    //________________________________________________________________
    bool Node::Exec()
    {
      if (!IsExecutable()) {
	std::cout << Name() << ": cannot execute" << std::endl;
	return false;
      }
      return true;
    }
    //________________________________________________________________
    Node* Node::FindNode(const std::string& name) const
    {
      for (node_list::const_iterator i =_nodes.begin(); 
	   i != _nodes.end(); ++i) 
	if ((*i)->Name() == name) return *i;
      return 0;
    }
    //________________________________________________________________
    bool Node::FindNodes(const std::string& name, std::list<Node*>& ret) const 
    {
      for (node_list::const_iterator i = _nodes.begin(); 
	   i != _nodes.end(); ++i) { 
	if ((*i)->Name() == name) ret.push_back(*i);
	(*i)->FindNodes(name, ret);
      }
      return ret.size() > 0;
    }
    //________________________________________________________________
    Node* Node::Find(const std::string& name) const
    {
      Node* ret = 0;
      for (node_list::const_iterator i =_nodes.begin(); 
	   i != _nodes.end(); ++i){
	if ((*i)->Name() == name) { 
	  ret = *i;
	  break;
	}
	if ((ret = (*i)->Find(name))) break;
      }
      return ret;
    }
    //________________________________________________________________
    std::string Node::Path() const 
    {
      const Node* p = this;
      typedef std::list<std::string> string_list;
      string_list l;
      while (p) { 
	l.push_front(p->Name());
	p = p->Parent();
      }
      std::stringstream s;
      for (string_list::const_iterator i = l.begin(); i != l.end(); i++) 
	s << "/" << *i;
      return s.str();
    }
    //________________________________________________________________
    int Node::Compare(const Node& other, bool byName) const 
    {
      if (SortOrder() < other.SortOrder()) return -1;
      if (SortOrder() > other.SortOrder()) return  1;
      // if (IsDir()        && !other.IsDir())        return -1;
      // if (IsExecutable() && !other.IsExecutable()) return -1;
      // if (IsWritable()   && !other.IsWritable())   return -1;
      if (_name == other.Name())                   return 0;
      return (_name < other.Name() ? -1 : 1);
    }
    //________________________________________________________________
    void Node::Show(int n, bool l) const 
    {
      if (l) { 
	if (IsDir()) std::cout << "d--x";
	else 
	  std::cout << '-' 
		    << (IsReadable()   ? 'r' : '-') 
		    << (IsWritable()   ? 'w' : '-') 
		    << (IsExecutable() ? 'x' : '-');
	std::cout << "  " << std::oct << Permissions() << std::dec 
		  << "  " << SortOrder() << "  ";
      }
      std::cout << setfg(Fg()) << setbg(Bg()) << seteffect(Fx())
		<< std::left << std::setw(n) << Name() << std::right
		<< setfg(Color::black) << setbg(Color::white) 
		<< seteffect(Effect::none) << std::flush;
      if (l) 
	std::cout << "  " << std::left << Description() << std::right
		  << std::endl;;
    }
    //________________________________________________________________
    int Node::FindWidest() const 
    {
      int n = 0;
      for (node_list::const_iterator i =_nodes.begin(); 
	   i != _nodes.end();++i)
	if ((*i)->Name().size() > n) n = (*i)->Name().size();
      return n;
    }
    //________________________________________________________________
    void Node::Sort()
    {
      _nodes.sort(std::less<Node*>()); 
    }
    //________________________________________________________________
    void Node::Ls(Indent& indent, bool recursive, bool l) const 
    {
      int                       widest = FindWidest();
      node_list::const_iterator iter   = _nodes.begin();
      
      if (!l) { 
	widest += 2;
	int avail  = 78 - indent._cur;
	int cols   = avail / widest;
	if (cols < 1) cols = 1;
	int n      = _nodes.size();
	int rows   = n / cols + 1;
	if (rows < 1) rows = n;
	
	for (int r = 0; r < rows; r++) { 
	  for (int c = 0; c < cols; c++) { 
	    int   i = r * cols + c;
	    if (i >= n) continue;
	    Node* t = *iter++;
	    if (!t) continue;
	    t->Show(widest, l);
	  }
	  std::cout << std::endl;
	}
      }
      else { 
	while (iter != _nodes.end()) { 
	  Node* t = *iter++;
	  if (!t) continue;
	  t->Show(widest, l);
	}
      }
      if (!recursive) return; 
      indent++;
      for (node_list::const_iterator i =_nodes.begin(); 
	   i != _nodes.end(); ++i){
	std::cout << indent;
	(*i)->Ls(indent, recursive, l);
	// if (recursive) (*i)->Ls(recursive);
      }
      indent--;
    }
  }
}
//
// EOF
//
