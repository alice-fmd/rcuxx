// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_UTIL
#define RCUXX_SHELL_UTIL
#include <string>
#include <sstream>
#define PRINT_HEX(X,S)					\
  std::hex << std::setfill('0') << std::setw(S) << X	\
  << std::dec << std::setfill(' ')

namespace Rcuxx
{
  namespace Shell 
  {
    //____________________________________________________________________
    /** 
     * Trim away leading and trailing white space of a string
     * 
     * @param s String to trim (not modified)
     * 
     * @return A trimmed version of @a s
     */    
    inline std::string 
    trim(const std::string& s)
    {
      std::string r(s);
      // trim leading whitespace
      std::string::size_type  notwhite = r.find_first_not_of(" \t\n");
      r.erase(0,notwhite);
  
      // trim trailing whitespace
      notwhite = r.find_last_not_of(" \t\n"); 
      r.erase(notwhite+1); 
      
      return r;
    }
    //____________________________________________________________________
    inline unsigned int
    str2val(const std::string& str)
    {
      std::stringstream s(str);
      if (str[0] == '0') { 
	if (str[1] == 'x' || str[1] == 'X') 
	  s << std::hex;
	else 
	  s << std::oct;
      }
      unsigned int ret;
      s >> ret;
      return ret;
    }
  }
}
#endif
//
// EOF
//
