// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_REGISTERNODE
#define RCUXX_SHELL_REGISTERNODE
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/Node.h>
#include <rcuxx/Register.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/altro/AltroRegister.h>
#include <iostream>
#include <typeinfo>

namespace Rcuxx
{
  class Rcu;

  namespace Shell 
  {
    //------------------------------------------------------------------
    /**
     * A register in one of the components (RCU, ALTRO, BC)
     */
    struct RegisterNode : public Node
    {
      /** 
       * Constructor 
       * 
       * @param parent  Parent node
       * @param r       Pointer to register interface 
       * @param n       Name of register 
       */
      RegisterNode(Node* parent, Rcuxx::Register* r, 
		   const std::string& n=std::string()) 
	: Node(parent, n.empty() ? r->Name() : n), 
	  _register(r), 
	  _permissions(Node::readable)
      {
	if (_register->IsSubmitable()) _permissions |= Node::writable;
      }
      /** 
       * Get the permissions
       * 
       * @return Permissions mask 
       */
      unsigned short Permissions() const { return _permissions; }
      const char* Description() const { return _register->Title().c_str(); }
      unsigned short SortOrder() const { return 4 + (IsReadonly() ? 0 : 1); }
      /** 
       * List (not applicable)
       * 
       */
      void Ls(Indent&,bool,bool) const {}
      /** 
       * Update from the hardware 
       * 
       * @param rcu Harware interface
       * 
       * @return {@code true} on succcess. 
       */
      bool Update(Rcuxx::Rcu* rcu)
      {
	unsigned int ret = 0;
	if ((ret = _register->Update())) { 
	  std::cout << "Failed to update register " << std::endl;
	  return false;
	}
	Sort();
	return true;
      }
      /** 
       * Read and display the register
       * 
       * @return {@code true} on success. 
       */
      bool Read() 
      {
	if (!Update(0)) return false;
	_register->Print();
	return true;
      }
      /** 
       * Write values to the hardware 
       * 
       * @param int   Not used
       * @param value Value to write 
       * 
       * @return {@code true} on success
       */
      bool Write(unsigned int, unsigned int value)
      {
	return Write(value);
      }
      /** 
       * Write values to the hardware 
       * 
       * @param value Value to write 
       * 
       * @return {@code true} on success
       */
      bool Write(unsigned int value)
      {
	if (!_register->IsSubmitable()) { 
	  std::cout << Name() << " is read only" << std::endl;
	  return false;
	}
	_register->Decode(value);
	unsigned int ret = 0;
	if ((ret = _register->Commit())) { 
	  std::cout << "Failed to commit register " << _name << std::endl;
	  return false;
	}
	return true;
      }
      /** The register interface */
      Rcuxx::Register* _register;
      /** Permissions */
      unsigned int _permissions;
    };
  }
}
#endif
//
// EOF
//
