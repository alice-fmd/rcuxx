// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_SH
#define RCUXX_SHELL_SH
#include <rcuxx/shell/Fs.h>
#include <rcuxx/shell/Indent.h>
#include <map>
#include <vector>
#include <cstring>

namespace Rcuxx
{
  namespace Shell 
  {
    //____________________________________________________________________
    struct Sh
    {
      /** Map of commands */
      typedef std::map<std::string,Command*> command_list;
      /** Command history */
      typedef std::vector<std::string> history_list;
      
      /** 
       * Constructor 
       * 
       * @param rcu 
       * @param bc 
       * @param altro 
       */
      Sh(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro);
      /** 
       * Add a command 
       * 
       * @param command Command to add  
       */
      void AddCommand(Command* command);
      /** 
       * Find a command 
       * 
       * @param str Name of command
       * 
       * @return Pointer to command or null 
       */
      Command* FindCommand(const std::string& str) const;
      /** 
       * Handle an input line 
       * 
       * @param l 
       * 
       * @return 
       */
      bool HandleLine(const std::string& l, bool oldRet);
      /** 
       * List available commands
       * 
       */
      void ListCommands() const;
      /** 
       * Get file system pointer
       * 
       * @return FS reference
       */
      Fs& FileSystem() { return _fs; }
      /** 
       * Get RCU pointer
       * 
       * @return RCU reference 
       */
      Rcuxx::Rcu&   Rcu()   { return _rcu; }
      /** 
       * Get BC pointer
       * 
       * @return BC reference 
       */
      Rcuxx::Bc&    Bc()    { return _bc; }
      /** 
       * Get ALTRO pointer
       * 
       * @return ALTRO reference 
       */
      Rcuxx::Altro& Altro() { return _altro; }
      /** 
       * Exit shell 
       * 
       */
      void Quit() { _done = true; }
      /** 
       * Set- up the terminal 
       * 
       */      
      void SetupTerminal();
      /** 
       * Get a single character from standard input 
       * 
       * 
       * @return Rad character
       */
      int GetChar()
      {
	return std::cin.get();
      }
      /** 
       * Read in a line 
       * 
       * @param prompt Prompt to display 
       * @param line   Line read
       * 
       * @return {@code true} on succecss 
       */
      bool GetLine(const std::string& prompt, std::string& line);
      /** 
       * Make the command prompt
       * 
       * @param p 
       */    
      void MakePrompt(std::string& p);
      /** 
       * Run this shell 
       * 
       * @return {@code true} on success
       */
      bool Run();
      /** REfernce to RCU */
      Rcuxx::Rcu&    _rcu;
      /** REfernce to RCU */
      Rcuxx::Bc&     _bc;
      /** REfernce to RCU */
      Rcuxx::Altro&  _altro;
      /** Indentor */
      Indent _indent;
      /** File system */
      Fs             _fs;
      /** Whether we're done */
      bool          _done;
      /** Strict */
      bool          _strict;
      /** List of commands */
      command_list  _commands;
      /** History */
      history_list  _history;
      /** singletong */
      static Sh* _instance;
      /** 
       * Get the singleton
       * 
       * @return Singleton
       */
      static Sh& Instance() { return *_instance; }
      /** 
       * Completion function 
       * 
       * @param l Possible things to match
       * 
       * @return List of possible mathces
       */
      char** MakeMatches(const std::vector<std::string>& l) const;
      /** 
       * Complate a command 
       * 
       * @param text Command 
       * @param n    charactors 
       * 
       * @return List of possible commands 
       */
      char** CompleteCommand(const char* text, size_t n) const; 
      /** 
       * Complete a node
       * 
       * @param text Node to complete 
       * @param n    Count
       * 
       * @return Possible completions 
       */
      char** CompleteNode(const char* text,size_t n) const; 
    };
  }
}
#endif
//
// EOF
//


