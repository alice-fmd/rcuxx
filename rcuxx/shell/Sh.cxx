#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <rcuxx/shell/Sh.h>
#ifdef HAVE_READLINE_READLINE_H
# include <readline/readline.h>
# include <readline/history.h>
#endif

#if defined(HAVE_READLINE_READLINE_H) && defined(HAVE_LIBREADLINE)
namespace {
  int save_end;
  char** 
  my_completion(const char* text, int start, int end)
  {
    char** matches = 0;
    save_end = rl_end;

    size_t l = end - start;
    if (start == 0) 
      // Command completion 
      matches = Rcuxx::Shell::Sh::Instance().CompleteCommand(text, l);
    else 
      matches = Rcuxx::Shell::Sh::Instance().CompleteNode(text, l);
    return matches;
  }

  void 
  my_display(char **matches, int num_matches, int max_length)
  {
    // std::cout << "possible matches: " << std::endl;
    // std::cout << "Line buffer is " << rl_line_buffer << std::endl;
    // rl_delete_text(save_end, rl_end);
    // rl_end = save_end;
    int widest = max_length + 2;
    int avail  = 78 - Rcuxx::Shell::Sh::Instance()._indent._cur;
    int cols   = avail / widest;
    if (cols < 1) cols = 1;
    int rows   = num_matches / cols + 1;
    int i      = 0;
    int j      = 0;
    if (rows < 1) rows = num_matches;
    std::cout << std::left << std::endl;
    for (int r = 0; r < rows; r++) { 
      for (int c = 0; c < cols; c++) { 
	i = r * cols + c;
	if (i >= num_matches) continue;
	j++;
	Rcuxx::Shell::Node* node = 
	  Rcuxx::Shell::Sh::Instance()._fs._cur->FindNode(matches[i]);
	if (node) node->Show(widest);
	else 
	  std::cout << std::setw(widest) 
		    << matches[i]  << std::flush;
      }
      std::cout << std::endl;
    }
    std::cout << std::right;
    if (j != num_matches) { 
      std::cerr << "Only showed " << j << " out of " 
		<< num_matches << " possible matches" 
		<< std::endl;
    }
    // rl_display_match_list(matches, num_matches, max_length);
    rl_forced_update_display();
  }
}
#endif

namespace Rcuxx 
{
  namespace Shell 
  {
    Sh* Sh::_instance = 0;

    Sh::Sh(Rcuxx::Rcu& rcu, Rcuxx::Bc& bc, Rcuxx::Altro& altro)
      : _rcu(rcu), 
	_bc(bc), 
	_altro(altro), 
	_indent(),
	_fs(rcu, bc, altro, _indent), 
	_strict(false)
    {
      _instance = this;
    }
    //________________________________________________________________
    void Sh::AddCommand(Command* command) 
    {
      if (FindCommand(command->Name())) return;
      _commands[command->Name()] = command;
    }
    //________________________________________________________________
    Command* Sh::FindCommand(const std::string& str) const 
    {
      command_list::const_iterator i = _commands.find(str);
      if (i == _commands.end()) { 
	const Node* n = _fs.FindNode(str, false);
	if (!n) return 0;
	if (!n->IsExecutable()) return 0;
	CommandNode* c = dynamic_cast<CommandNode*>(const_cast<Node*>(n));
	return c;
      }
      return i->second;
    }
    //________________________________________________________________
    bool Sh::HandleLine(const std::string& l, bool oldRet) 
    {
      if (l.empty()) return true;
      if (l[0] == '#') return true;
      std::string ll(l);

      size_t eq = l.find('=');
      if (eq != std::string::npos) { 
	std::string  name  = trim(l.substr(0, eq));
	std::string  value = trim(l.substr(eq+1));
	std::string  addr  = "";
	size_t       lb    = name.find('[');
	if (lb != std::string::npos) { 
	  size_t rb = name.find(']');
	  if (rb == std::string::npos) { 
	    std::cerr << "Malformed line: " << l << std::endl;
	    return false;
	  }
	  addr = trim(name.substr(lb+1, rb-lb-1));
	  name = trim(name.substr(0,lb));
	}
	ll = "set ";
	ll += name + " ";
	ll += addr + " ";
	ll += value;
      }
	  
      std::stringstream s(ll);
      int               i  = 0;
      Command*          c  = 0;
      Command::arg_list args;
      while (!s.eof()) {
	std::string token;
	s >> token;
	  
	if (i == 0) { 
	  c = FindCommand(token);
	  if (!c) { 
	    std::cout << "Unknown command: " << token << std::endl;
	    return false;
	  }
	  i++;
	  continue;
	}
	while (token[0] == '"' && 
	       token[token.size()-1] != '"') { 
	  std::string t;
	  s >> t;
	  token.append(" ");
	  token.append(t);
	}
	if (token[0] == '"')            token=token.substr(1,token.size()-1);
	if (token[token.size()-1]=='"') token=token.substr(0,token.size()-1);

	if (token.empty()) continue;
	args.push_back(token);
	i++;
      }
      if (!c) return false;
      bool ret = c->operator()(args);
      if (c->IgnoreReturn()) return oldRet;
      return ret;
    }
    //________________________________________________________________
    void Sh::ListCommands() const 
    {
      std::cout << "Available commands:" << std::endl;
      for (command_list::const_iterator i = _commands.begin(); 
	   i != _commands.end(); ++i) { 
	std::cout << "  "  << std::flush;
	i->second->ShowUsage();
      }
    }
    //________________________________________________________________
    void Sh::MakePrompt(std::string& p)
    {
      std::stringstream s;
      s << seteffect(Effect::bright) << "rcuxx:" << _fs.Pwd()
	<< seteffect(Effect::none)  << "> ";
      p = s.str();
    }
    //________________________________________________________________
    bool Sh::Run()
    {
      SetupTerminal();
      _done    = false;
      bool ret = true;
      while (!_done) {
	std::string l;
	std::string p;
	MakePrompt(p);
	if (!GetLine(p, l)) { 
	  std::cout << "exit" << std::endl;
	  return true;
	} 
	ret = HandleLine(l, ret);
	if (_strict && !ret) break;
      }
      return ret;
    }
    //________________________________________________________________
    char** Sh::MakeMatches(const std::vector<std::string>& l) const
    {
      size_t m       = l.size();
      // std::cout << "Got " << m << " matches" << std::endl;
      if (m <= 0) return 0;
      char** matches = (char**)malloc((m+1)*sizeof(char*));
      for (size_t i = 0; i < m; i++) { 
	const std::string& s = l[i];
	size_t             n = s.size();
	matches[i]           = (char*)malloc(n + 1);
	strcpy(matches[i], s.c_str());
	matches[i][n] = '\0';
      }
      matches[m] = 0;
      return matches;
    }    
    //________________________________________________________________
    char** Sh::CompleteCommand(const char* text, size_t n) const 
    {
      std::string w(text);
      std::vector<std::string> l;
      typedef command_list::const_iterator iter;
      for (iter i = _commands.begin(); i != _commands.end(); ++i) { 
	std::string other = i->first.substr(0, n);
	if (w.empty() || w == other) l.push_back(i->first);
      }
      return MakeMatches(l);
    }
    //________________________________________________________________
    char** Sh::CompleteNode(const char* text,size_t n) const 
    {
      std::string w(text);
      std::vector<std::string> l;
      typedef Node::node_list::const_iterator iter;
      Node::node_list& nodes = _fs._cur->_nodes;
      for (iter i = nodes.begin(); i != nodes.end(); ++i) { 
	std::string other = (*i)->Name().substr(0, n);
	if (w.empty() || w == other) l.push_back((*i)->Name());
      }
      return MakeMatches(l);
    }
    //________________________________________________________________
    bool Sh::GetLine(const std::string& prompt, std::string& line)
    {
#if defined(HAVE_READLINE_READLINE_H) && defined(HAVE_LIBREADLINE)
      line = "";
      char* l = readline(prompt.c_str());
      // printf("Read line '%s' [%d]\n",l, int(*l));
      if (!l) return false;
	
      while (isspace(*l)) l++;
      if (*l == 0) return true;
	
      char* t = l + strlen(l) - 1;
      while (t > l && isspace(*t)) t--;
      *++t = '\0';
	
      line = l;
      add_history(l);
      free(l);
      return true;
#else 
      std::cout << "[no-rl]" << prompt << std::flush;
      std::getline(std::cin, line);
      _history.push_back(line);
      return true;
#endif  
    }
    void 
    Sh::SetupTerminal()
    {
#if defined(HAVE_READLINE_READLINE_H) && defined(HAVE_LIBREADLINE)
      rl_readline_name                   = "Rcu++";
      rl_attempted_completion_function   = my_completion;
      rl_completion_display_matches_hook = my_display;
      rl_variable_bind("show-all-if-ambiguous", "1");
      // rl_variable_bind("show-all-if-unmodified", "1");
#endif
    }
  }
}
//
// EOF
//
