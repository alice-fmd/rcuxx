// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_BCNODE
#define RCUXX_SHELL_BCNODE
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/Node.h>
#include <rcuxx/shell/AltroNode.h>
#include <rcuxx/shell/RegisterNode.h>
#include <rcuxx/shell/CommandNode.h>
#include <rcuxx/Rcu.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Altro.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/bc/BcCommand.h>
#include <rcuxx/bc/BcRegister.h>
#include <iostream>

namespace Rcuxx
{
  namespace Shell 
  {
    //------------------------------------------------------------------
    struct BcNode : public Node 
    { 
      /** 
       * Constructor 
       * 
       * @param parent  Parent node
       * @param bc      HW interface 
       * @param altro   HW interface 
       * @param board   Board number
       */
      BcNode(Node* parent, Rcuxx::Bc* bc, Rcuxx::Altro* altro, int board) 
	: Node(parent, MakeName(board)), 
	  _bc(bc), 
	  _altro(altro),
	  _board(board)
      {
	std::stringstream s;
	s << "Front-end Card # " << board;
	_title = s.str();
      }
      const char* Description() const { return _title.c_str(); }
      /** 
       * This is a directory
       * 
       * @return {@code true}
       */
      bool IsDir() const { return true; }
      unsigned short SortOrder() const { return 0; }
      /** 
       * Update from hardware 
       * 
       * @param rcu RCU HW interface 
       * 
       * @return {@code true} on success 
       */      
      bool Update(Rcuxx::Rcu* rcu) 
      {
	Rcuxx::RcuACL* acl = rcu->ACL();

	int startA = (_board >= 0 ? 0 : -1);
	int maxA   = (_board >= 0 ? 8 : 0);
	for (int a = startA; a < maxA; a++) { 
	  std::string na = AltroNode::MakeName(_board, a);
	  Node*       al = FindNode(na);
	  if ((a >= 0 && acl->CheckChip(_board, a) == 0)) { 
	    if (al) RemoveNode(al);
	    continue;
	  }
	  if (!al)    al = new AltroNode(this, _altro, _board, a);
	  al->Update(rcu);
	}

	typedef Rcuxx::Bc::Registers Registers;
	const Registers& r = _bc->RegisterList();
	for (Registers::const_iterator i = r.begin(); i != r.end(); ++i) {
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  new RegisterNode(this, *i);
	}
	typedef Rcuxx::Bc::Commands Commands;
	const Commands& c = _bc->CommandList();
	for (Commands::const_iterator i = c.begin(); i != c.end(); ++i) {
	  if (!*i) continue;
	  if (FindNode(trim((*i)->Name()))) continue;
	  new CommandNode(this, *i, rcu);
	}
	Sort();
	return true;
      }
      /** 
       * Change directory to this FEC.  sets the address
       * 
       * @return {@code true} on success
       */
      bool Cd()
      {
	if (_board >= 0) 
	  _bc->SetAddress(_board);
	else 
	  _bc->SetBroadcast();
	Sort();
	return true;
      }
      /** 
       * Make a name 
       * 
       * @param board Board number  
       * 
       * @return String with name
       */
      static std::string MakeName(int board) 
      {
	std::string ret("fec");
	if (board < 0) { 
	  ret.append("s");
	  return ret;
	}
	std::stringstream s;
	s << ret << PRINT_HEX(board, 2);
	ret = s.str();
	return ret;
      }
      /** HW interface */
      Rcuxx::Bc*    _bc;
      /** HW interface */
      Rcuxx::Altro* _altro;
      /** Board number */
      int        _board;
      std::string _title;
    };
  }
}
#endif
//
// EOF
//
