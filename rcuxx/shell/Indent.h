// -*- mode: C++ -*-
/*
 * Terminal effects 
 */
#ifndef RCUXX_SHELL_INDENT
#define RCUXX_SHELL_INDENT
#include <iostream>
#include <iomanip>

namespace Rcuxx
{
  namespace Shell 
  {
    /** 
     * Persistent indention 
     */
    struct Indent
    {
      /** 
       * Constructor 
       * 
       * @param n Indent step
       */
      Indent(size_t n=2) : _cur(0), _d(n) {}
      /** 
       * Post increment operator 
       * 
       * @return Reference to this object 
       */
      Indent& operator++(int) { _cur += _d; return *this; }
      /** 
       * Post decrement operator 
       * 
       * @return Reference to this object 
       */
      Indent& operator--(int) { if (_cur >= _d) _cur -= _d; return *this; }
      /** 
       * Pre increment operator 
       * 
       * @return Reference to this object 
       */
      Indent& operator++()    { _cur += _d; return *this; }
      /** 
       * Pre decrement operator 
       * 
       * @return Reference to this object 
       */
      Indent& operator--()    { if (_cur >= _d) _cur -= _d; return *this; }
      /** 
       * Reset the indentor 
       */
      void    Reset() { _cur = 0; }
      /** Current indention */
      size_t _cur;
      /** The indention step size */
      size_t _d;
    };

    /** 
     * Output streamer of an indentor.  Indents current line with the
     * current indention level 
     * 
     * @param o Output stream
     * @param i Indention object 
     * 
     * @return Reference to @a o after manipulation 
     */
    inline std::ostream&
    operator<<(std::ostream& o, const Indent& i)
    {
      if (i._cur <= 0) return o;
      return o << std::setw(i._cur) << ' ';
    }
  }
}
#endif
//
// EOF
//
