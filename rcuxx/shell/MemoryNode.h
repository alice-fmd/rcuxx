// -*- mode: C++ -*-
#ifndef RCUXX_SHELL_MEMORYNODE
#define RCUXX_SHELL_MEMORYNODE
#include <rcuxx/shell/Util.h>
#include <rcuxx/shell/Node.h>
#include <rcuxx/shell/TerminalEffect.h>
#include <rcuxx/rcu/RcuMemory.h>
#include <iostream>
#include <iomanip>

namespace Rcuxx
{
  class Rcu;

  namespace Shell 
  {
    //------------------------------------------------------------------
    struct MemoryNode : public Node
    {
      /** 
       * Constructor
       * 
       * @param parent Parent node
       * @param m      Interface pointer
       */
      MemoryNode(Node* parent, Rcuxx::RcuMemory* m) 
	: Node(parent, m->Name()), 
	  _memory(m), 
	  _permissions(Node::readable)
      {
	if (_memory->IsWriteable())  _permissions |= Node::writable;
	if (_memory->IsExecutable()) _permissions |= Node::executable;
      }
      const char* Description() const { return _memory->Title().c_str(); }
      unsigned short Permissions() const { return _permissions; }
      unsigned short SortOrder() const { return 2; }
      /** 
       * List (not applicable)
       * 
       */
      void Ls(Indent&,bool,bool) const {}
      /** 
       * Update from the hardware 
       * 
       * @param rcu Harware interface
       * 
       * @return {@code true} on succcess. 
       */
      bool Update(Rcuxx::Rcu*) 
      {
	unsigned int ret = 0;
	if ((ret = _memory->Update())) { 
	  std::cout << "Failed to update memory " << _name << std::endl;
	  return false;
	}
	Sort();
	return true;
      }
      /** 
       * Read and display the register
       * 
       * @return {@code true} on success. 
       */
      bool Read() 
      {
	if (!Update(0)) return false;
	_memory->Print();
	return true;
      }
      /** 
       * Write values to the hardware 
       * 
       * @param addr  Offset in memory
       * @param value Value to write 
       * 
       * @return {@code true} on success
       */
      bool Write(unsigned int addr, unsigned int value)
      {
	_memory->Set(addr, 1, &value);
	unsigned int ret = 0;
	if ((ret = _memory->Commit())) { 
	  std::cout << "Failed to commit memory " << _name << std::endl;
	  return false;
	}
	return true;
      }
      /** 
       * Get the foreground color
       * 
       * @return Foreground colour
       */
      virtual int Fg() const { return Color::magenta; }
      /** 
       * Get the effect
       * 
       * @return Effect
       */
      virtual int Fx() const 
      { 
	return Effect::bright; 
      }
      /** Interface to memory */
      Rcuxx::RcuMemory* _memory;
      /** Permissions */
      unsigned int _permissions;
    };
  }
}
#endif
//
// EOF
//

