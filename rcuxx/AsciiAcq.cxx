//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    AsciiAcq.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:19:17 2006
    @brief   Implementation of ASCII data acquisition 
*/
#ifndef RCUXX_ASCIIACQ_H
# include <rcuxx/AsciiAcq.h>
#endif
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <sstream>
#include "DebugGuard.h"

namespace 
{
  bool fDebug = false;
}

//____________________________________________________________________
void
Rcuxx::AsciiAcq::CloseCurrent()
{
  if (!fFile) return;
  fFile->close();
  delete fFile;
  fFile = 0;
}
  
//____________________________________________________________________
unsigned int
Rcuxx::AsciiAcq::Run() 
{
  if (fStop) return 1;

  unsigned int ret = 0;
  // Call base class member function 
  ret = Rcuxx::Acq::Run();
  fOutName.erase();
  CloseCurrent();
  return ret;
}

//____________________________________________________________________
int
Rcuxx::AsciiAcq::StartEvent(unsigned int evno) 
{
  Rcuxx::DebugGuard g(fDebug, "AsciiAcq::StartEvent(%d)", evno);
  Rcuxx::Acq::StartEvent(evno);
  CloseCurrent();
  std::stringstream s;
  if (!fOutName.empty()) s << fOutName << '/';
  s << std::setw(8) << std::setfill('0') << evno << ".dat";
  fFile = new std::ofstream(s.str().c_str());
  if (!fFile || !(*fFile)) {
    std::cerr << "Failed to open file " << s.str() << std::endl;
    return kStopped;
  }
  return 0;
}

//____________________________________________________________________
int
Rcuxx::AsciiAcq::EndEvent(unsigned int evno) 
{
  Rcuxx::DebugGuard g(fDebug, "AsciiAcq::EndEvent(%d)", evno);
  CloseCurrent();
  return 0;
}

//____________________________________________________________________
int
Rcuxx::AsciiAcq::ProcessChannel(const std::vector<unsigned int>& data, 
				 unsigned int size) 
{
  Rcuxx::DebugGuard g(fDebug, "AsciiAcq::ProcessChannel(%p,%d)", 
		      &(data[0]), size);
  if (!fFile) return 0;
  
  // We need to pack the 10bit words into 40bit words, and write out
  // as 5 byte words in raw format. 
  typedef long long w40_t;
  for (size_t i = 0; i < size; i++) {
    w40_t dat = (((w40_t(data[i*4+0]) & 0x3ff) << 30) +
		 ((w40_t(data[i*4+1]) & 0x3ff) << 20) +
		 ((w40_t(data[i*4+2]) & 0x3ff) << 10) +
		 ((w40_t(data[i*4+3]) & 0x3ff)));
    for (size_t j = 4; j > 0; j--) {
      char cdat = (dat >> j * 8) & 0xff;
      fFile->put(cdat);
    }
  }  

  return 0;
}

//____________________________________________________________________
//
// EOF
// 
