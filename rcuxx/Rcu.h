// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Rcu.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCU_H
#define RCUXX_RCU_H
#include <rcuxx/RcuLayout.h>
#include <rcuxx/Url.h>
#include <string>
#include <vector>
#include <list>

/** @defgroup rcuxx Abstract interface of the RCU communications.
 */
/** @namespace Rcuxx Namespace for Abstract interface 
    @ingroup rcuxx 
*/
namespace Rcuxx
{
  /** @defgroup rcuxx_rcu RCU interface classes  
      @ingroup rcuxx 
  */
  
  //____________________________________________________________________
  // Forward declaration 
  class Rcu;
  class Register;
  class RcuMemory;
  class RcuCommand;
  class RcuRegister;
  
  //====================================================================
  struct RcuERRST;     // RCU I & U2F:    Error and status
  struct RcuTRCNT;     // RCU I & U2F:    Trigger counter
  struct RcuLWADD;     // U2F:            Last word address
  struct RcuIRADD;     // RCU I & U2F:    Last IMEM address
  struct RcuIRDAT;     // RCU I & U2F:    Last IMEM data
  struct RcuEVWORD;    // U2F:            Event word 
  struct RcuACTFEC;    // RCU I+II & U2F: Active front-ends 
  struct RcuRDOFEC;    // RCU I:          Read-out cards
  struct RcuTRCFG1;    // RCU I:          Trigger configuration
  struct RcuTRCFG2;    // U2F:            Trigger configuration mode
  struct RcuFMIREG;    // U2F:            FW input register
  struct RcuFMOREG;    // U2F:            FW output register
  struct RcuPMCFG;     // RCU I & U2F:    PMEM configuration
  struct RcuCHADD;     // RCU I & U2F:    Last read channel
  struct RcuINTREG;    // RCU I+II:       I2C interrupt
  struct RcuRESREG;    // RCU I+II:       I2C result
  struct RcuERRREG;    // RCU I+II:       I2C error 
  struct RcuFWVERS;    // RCU I+II:       FW version
  struct RcuALTROIF;   // RCU II:         Altro config
  struct RcuTRGCONF;   // RCU II:         Trigger configuration 
  struct RcuRDOMOD;    // RCU II:         Read-out mode
  struct RcuL1Timeout; // Rcu II:         L1 timeout and window
  struct RcuL2Timeout; // Rcu II:         L1 timeout and window
  struct RcuL1MsgTimeout; // Rcu II:         L1 timeout and window
  struct RcuRCUBUS;    // RCU II:	  Bus grant
  struct RcuALTROCFG1; // RCU II:         ALTRO config
  struct RcuALTROCFG2; // RCU II:         ALTRO config
  struct RcuBPVERS;    // RCU II:         Back-plane version
  struct RcuRCUID;     // RCU II:         Back-plane version
  struct RcuFECERR;    // RCU II:         FEC error (A & B)
  struct RcuRDOERR;    // RCU II:         Read-out error
  struct RcuCounter;   // RCU II:         trigger counter (SW,L2A,L2R,AUX)
  struct RcuINTMOD;    // RCU I+II:       I2C Interrupt mode
  struct RcuSCADD;     // RCU I+II:       I2C address 
  struct RcuSCDAT;     // RCU I+II:       I2C data
  struct RcuSCCLK;     // RCU II:         I2C bus speed
  struct RcuINT_TYPE;  // 
  struct RcuSMEM_STATUS; 
  struct RcuMEBSTCNT;  // RCU II - multibuffer counter
  struct RcuBusStatus; 

  struct RcuTTCControl;		// Trigger control register (0x4000)
  struct RcuROIConfig1;		// Region of interest configuration (0x4002)
  struct RcuROIConfig2;		// Region of interest configuration (0x4003)
  struct RcuL1Timeout;		// L1 timeout and window (0x4006)
  struct RcuL2Timeout;		// L2 time-out (0x4007)
  struct RcuRoiTimeout;		// ROI message timeout (0x4009)
  struct RcuL1MsgTimeout;	// L1 message timeout (0x400A)
  struct RcuTTCEventInfo;	// Event information (0x4028)
  struct RcuTTCEventError;	// Event information (0x4029)

  struct RcuABDFSMST;
  struct RcuRDOFSMST;
  struct RcuINSSEQST;
  struct RcuEVMNGST;

  struct RcuIMEM;
  struct RcuPMEM;
  struct RcuRMEM;
  struct RcuACL;
  struct RcuDMEM;
  struct RcuHEADER;
  struct RcuSTATUS;
  struct RcuHITLIST;
  struct RcuCDH;

  // Forward declaration 
  class Url;
  
  //====================================================================
  /** @class Rcu rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Abstract interface to the RCU communications.
      @ingroup rcuxx_rcu
  */
  class Rcu
  {
  public:
    /** Static function to create interface 
	@param url URL.  This can have the forms 
	@verbatim 
	U2F interface:

        	usb:DEVICE
        	u2f:DEVICE

	where DEVICE is something like '/dev/altro0'

	FEE client interface:

        	fee://HOST/FEE_SERVER

  	where HOST is the address of the DIM DNS, and SERVER is the
       	FeeServer name. 

	FED client interface:

        	fed://HOST/FED_SERVER#FEE_SERVER

  	where HOST is the address of the DIM DNS, and FED_SERVER is the
       	FedServer name, and FEE_SERVER is the FedServers name for a
       	FeeServer. 

	RORC client interface: 

		rorc://MINOR:CHANNEL
		ddl://MINOR:CHANNEL

	where MINOR is the minor device number (0 to 8) and CHANNEL is
	the channel number on that device (0 to 8). 
	@endverbatim
	@param emul Software emulation? 
	@param debug Debug of back-end
	@return Pointer to newly allocated interface, or 0 on error */
    static Rcuxx::Rcu* Open(const char* url, bool emul=false, bool debug=false);
    static Rcuxx::Rcu* Open(const Url& url, bool emul=false, bool debug=false);

    /** Error codes */
    enum {
      /** Didn't get enough data back */
      kNotEnough = 1024, 
      /** Inconsistence sizes from DMEM 1 and 2 */
      kInconsistentSizes, 
      /** Unknown memory block */
      kUnknownMemory,
      /** Got wrong instruction code back */
      kInstructionMismatch, 
      /** Got wrong address back */
      kAddressMismatch, 
      /** Got wrong data back */
      kDataMismatch,
      /** Non-writeable register */
      kNotWriteable,
      /** cannot broadcast this */
      kCannotBroadcast, 
      /** No acknowledge */
      kNoAcknowledge, 
      /** Not active */
      kNotActive,
      /** Result memory error */ 
      kRMemError = 2048
    };
    
    
    /** Component numbers */
    enum {
      /** RCU */
      kRcu = 10,
      /** RCU backend */
      kBackend, 
      /** ALTRO */
      kAltro, 
      /** Board controller */
      kBc
    };

    /** List of registers */ 
    typedef std::list<RcuRegister*> Registers;
    /** List of commands */ 
    typedef std::list<RcuMemory*> Memories;
    /** List of  */ 
    typedef std::list<RcuCommand*> Commands;

    /** Print to output stream @a out the possible device options
	@param out Stream to write on. */
    static void PrintHelp(std::ostream& out);
    /** @return Get package version number as a string */
    static const std::string& Version();
    /** @return Get package short name as a string */
    static const std::string& Package();
    /** @return Get package string as a string */
    static const std::string& PackageString();

    /** Get the list of registers */
    const Registers& RegisterList() const { return fRegisters; }
    /** Get the list of commands */
    const Commands& CommandList()   const { return fCommands; }
    /** Get the list of memories */
    const Memories& MemoryList()   const { return fMemories; }

    /** Type of data return */ 
    typedef std::vector<long> Block_t;
    /** Begin a data block.  This can be used with a command coder
	backend like  
	@code 
        class CommandCoder : public CommandCoderBase
	{
	  CommandCoder() 
          {
	    fRcu   = Rcuxx::Rcu::Open("coder:");
  	    fAltro = new Rcuxx::Altro(*rcu);
	    fBc    = new Rcuxx::Bc(*rcu);
          }
	  int createDataBlock(const char* t, int w)
	  {
	    // Start the block 
	    fRcu->StartBlock();
	    // Set the various registers in the RCU, ALTRO, and BC 
	    setRegisters(t, w);
	    // Finish the block 
	    return rcu->EndBlock();
          }
	  long int* getDataBlock() 
	  {
	    // Get the block of code to send 
	    const Rcuxx::Rcu::Block_t& block = rcu->GetBlock();
	    return block;
          }
          void setRegisters(const char* t int w)
	  {
	    // Should get config data from DB, set register 
	    // values, etc.  and then call the appropriate Commit 
	    // member functions 
	  }
	  std::vector<std::string> getError() 
	  {
	    // Get error strings - should really be by reference 
	  }
        };
        @endcode */
    virtual void StartBlock() {}
    /** End a data block.  
	@return the size of the created block 
	@see StartBlock */
    virtual int EndBlock() { return 0; }
    /** Get the data block created 
	@return the data block created */
    virtual const Block_t& GetBlock() const { return fBlock; }
	
    /** Read a data file 
	@param name File name 
	@param data On return, the read data 
	@return @c true on success */ 
    static bool ReadFile(const std::string& name, 
			 std::vector<unsigned int>& data);
    /** Read a data file 
	@param stream Input stream
	@param data On return, the read data 
	@return @c true on success */ 
    static bool ReadFile(std::istream& stream, 
			 std::vector<unsigned int>& data);
    
    /** Destructor */
    virtual ~Rcu();
    /** Get the connection URL */
    const Url& GetUrl() const { return fUrl; }
    /** Update (some of) the register caches */
    virtual unsigned int Update();
    /** Print contents of all registers (and memories) to standard out */
    virtual void Print() const;
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    virtual std::string ErrorString(unsigned int code);
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Execute a command 
	@param cmd Command to execute 
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd,
				     unsigned int data=0) = 0;
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data) = 0;
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data) = 0;
    /** Read a data memory block 
	@param lbase Base address for low bits
	@param hbase Base address for high bits
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadDataMemory(unsigned int lbase, 
					unsigned int hbase, 
					unsigned int offset, 
					unsigned int& size, 
					unsigned int* data);
    /** Write a data memory block 
	@param lbase Base address for low bits
	@param hbase Base address for high bits
	@param offset Where to start writing 
	@param size How many words to write 
	@param data Array of data to write
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteDataMemory(unsigned int lbase, 
					 unsigned int hbase, 
					 unsigned int offset, 
					 unsigned int& size, 
					 unsigned int* data);

    /** @{
	@name Error and status  */
    /** @return ERRor and STatus register.   */
    RcuERRST*	ERRST()		{ return fERRST; }
    /** @return TRigger CouNTers.   */
    RcuTRCNT*	TRCNT()		{ return fTRCNT; }
    /** @return ReaDOut FrontEnd Card register.   */
    RcuRDOFEC*	RDOFEC()	{ return fRDOFEC; }
    /** @return CHannel Address register  */
    RcuCHADD*	CHADD()		{ return fCHADD; }
    /** @return FirmWare VERSion */
    RcuFWVERS*	FWVERS()	{ return fFWVERS; }
    /** @return Back-plane version (RCU II) */
    RcuBPVERS*  BPVERS() 	{ return fBPVERS; }
    /** @return Identifier (RCU II) */
    RcuRCUID*   RCUID() 	{ return fRCUID; }
    /** @return ERRors on branch A FEC bus */
    RcuFECERR*  FECERRA() 	{ return fFECERRA; }
    /** @return ERRors on branch B FEC bus */
    RcuFECERR*  FECERRB() 	{ return fFECERRB; }
    /** @return ReaD-Out errors */
    RcuRDOERR*  RDOERR()	{ return fRDOERR; }
    /** @return Bus status */
    RcuBusStatus* ALTBUS_STATUS() { return fALTBUS_STATUS; }
    /** @return Bus status */
    RcuBusStatus* ALTBUS_TRSF() { return fALTBUS_TRSF; }
    /** @return Bus grand */ 
    RcuRCUBUS*  RCUBUS()	{ return fRCUBUS; } 
    /** @return ReSet STATUS command */
    RcuCommand* RS_STATUS()	{ return fRS_STATUS; } 
    /** @return Clear RDRX error register */
    RcuCommand* CLEARRDRXREG()	{ return fCLEARRDRXREG; }
    /** @return Align the phases of the readout clocks */
    RcuCommand* ARM_ASYNCH()    { return fARM_ASYNCH; }
       
    /** @} */

    /** @{ 
	@name U2F Specific registers */
    /** @return Last Written ADDress in the RcuDMEM1 and RcuDMEM2.   */
    RcuLWADD*	LWADD()		{ return fLWADD; }
    /** @return EVent WORD register.   */
    RcuEVWORD*	EVWORD()	{ return fEVWORD; }
    /** @return ReaD-Out MODe register.   */
    RcuTRCFG2*	TRCFG2()	{ return fTRCFG2; }
    /** @return FirMware Input ReGister. */
    RcuFMIREG*	FMIREG()	{ return fFMIREG; }
    /** @return FirMware Output ReGister  */
    RcuFMOREG*	FMOREG()	{ return fFMOREG; }
    /** @return CLeaR EVent TAG command */
    RcuCommand* CLR_EVTAG()	{ return fCLR_EVTAG; } 
    /** @return FirMware command */
    RcuCommand* RDFM()		{ return fRDFM; } 
    /** @return TRiGger CLeaR command */
    RcuCommand* TRG_CLR()	{ return fTRG_CLR; } 
    /** @return WRite FirMware command */
    RcuCommand* WRFM()		{ return fWRFM; } 
    /** @} */

    /** @{ 
	@name Instruction coder */
    /** @return Last executed ALTRO InstRuction ADDress.   */
    RcuIRADD*	IRADD()		{ return fIRADD; }
    /** @return Last executed ALTRO InstRuction DATa. */
    RcuIRDAT*	IRDAT()		{ return fIRDAT; }
    /** @return RCU instruction memory */
    RcuIMEM*	IMEM()		{ return fIMEM; } 
    /** @return RCU result memory */
    RcuRMEM*	RMEM()		{ return fRMEM; } 
    /** @return ABORT instruction execution  */
    RcuCommand* ABORT()		{ return fABORT; } 
    /** @return EXECute instructions command */
    RcuCommand* EXEC()		{ return fEXEC; } 
    /** @} */

    
    /** @{ 
	@name Configuration registers */
    /** @return ACTive FrontEnd Card.   */
    RcuACTFEC*	ACTFEC()	{ return fACTFEC; }
    /** @return Pedestal Memory ConFiGuration */
    RcuPMCFG*	PMCFG()		{ return fPMCFG; }
    /** @return Altro config (RCU II) */
    RcuALTROIF* ALTROIF()	{ return fALTROIF; }
    /** @return ALTRO config (RCU II)  */
    RcuALTROCFG1* ALTROCFG1()	{ return fALTROCFG1; }
    /** @return ALTRO config (RCU II)  */
    RcuALTROCFG2* ALTROCFG2()	{ return fALTROCFG2; }
    /** @return RCU pedestal memory */
    RcuPMEM*	PMEM()		{ return fPMEM; } 
    /** @return DCS OwN bus command */
    RcuCommand* DCS_ON()	{ return fDCS_ON; } 
    /** @return DDL OwN bus command */
    RcuCommand* DDL_ON()	{ return fDDL_ON; } 
    /** @return Front-End Card ReSeT command */
    RcuCommand* FECRST()	{ return fFECRST; } 
    /** @return GLoBal RESET command */
    RcuCommand* GLB_RESET()	{ return fGLB_RESET; } 
    /** @return RCU RESET command */
    RcuCommand* RCU_RESET()	{ return fRCU_RESET; } 
    /** @return Re-program FEC BC */
    RcuCommand* CONFFEC()	{ return fCONFFEC; }
    /** @return Request DCS interrupt */
    RcuCommand* ARBITERIRQ()	{ return fARBITERIRQ; }
    /** @} */

    /** @{ 
	@name Monitoring and safety registers */
    /** @return INTerrupt REGister  */
    RcuINTREG*	INTREG()	{ return fINTREG; }
    /** @return RESult REGister  */
    RcuRESREG*	RESREG()	{ return fRESREG; }
    /** @return ERRor REGister  */
    RcuERRREG*	ERRREG()	{ return fERRREG; }
    /** @return INTerrupt MODe register  */
    RcuINTMOD*	INTMOD()	{ return fINTMOD; }
    /** @return FirmWare VERSion */
    RcuSCADD*	SCADD()		{ return fSCADD; }
    /** @return FirmWare VERSion */
    RcuSCDAT*	SCDAT()		{ return fSCDAT; }
    /** @return FirmWare VERSion */
    RcuSCCLK*	SCCLK()		{ return fSCCLK; }
    /** @return ReSet ERRor REGister */
    RcuCommand* RS_ERRREG()     { return fRS_ERRREG; }
    /** @return ReSet ERRor REGister */
    RcuCommand* RS_RESREG()     { return fRS_RESREG; }
    /** @return ReSet ERRor REGister */
    RcuCommand* SCEXEC()        { return fSCEXEC; }
    /** @return ReSTore Active Front-end card List */
    RcuCommand* RST_AFL()       { return fRST_AFL; }
    /** @return ReSTore ReaD-Out front-end card list */
    RcuCommand* RST_RDO()       { return fRST_RDO; }
    /** @return Clear status memory ready bits */
    RcuCommand* RS_SMEM()       { return fRS_SMEM; }
    /** @return Status memory */
    RcuSTATUS*  STATUS() 	{ return fSTATUS; }
    /** @return Slow Control COMMAND command */
    RcuCommand* SCCOMMAND()	{ return fSCCOMMAND; } 
    /** @} */

    /** @{ 
	@name Trigger & event interface */
    /** @return TRigger ConFiG register.   */
    RcuTRCFG1*	TRCFG1()	{ return fTRCFG1; }
    /** @return TRigger ConFiG register.   */
    RcuTRGCONF*	TRGCONF()	{ return fTRGCONF; }
    /** @return ReaD-Out mode (RCU II) */
    RcuRDOMOD*  RDOMOD()	{ return fRDOMOD; }
    /** @} */

    /** @{ 
	@name TTC interface */
    /** @return Trigger control register */
    RcuTTCControl*	TTCControl() 	{ return fTTCControl; }
    /** @return TTC interface reset command */
    RcuCommand*	TTCReset() 	{ return fTTCReset; }
    /** @return Region of interest configuration */
    RcuROIConfig1*	ROIConfig1() 	{ return fROIConfig1; }
    /** @return Region of interest configuration */
    RcuROIConfig2*	ROIConfig2() 	{ return fROIConfig2; }
    /** @return TTC interface reset counters command */
    RcuCommand*	TTCResetCnt() 	{ return fTTCResetCnt; }
    /** @return TTC interface test mode command */
    RcuCommand*	TTCTestmode() 	{ return fTTCTestmode; }
    /** @return L1 timeout and window */
    RcuL1Timeout*	L1Timeout() 	{ return fL1Timeout; }
    /** @return L2 time-out */
    RcuL2Timeout*	L2Timeout() 	{ return fL2Timeout; }
    /** @return ROI message timeout */
    RcuRoiTimeout*	RoiTimeout() 	{ return fRoiTimeout; }
    /** @return L1 message timeout */
    RcuL1MsgTimeout*	L1MsgTimeout() 	{ return fL1MsgTimeout; }
    /** @return Prepulser counter */
    RcuCounter*	PrePulseCnt() 	{ return fPrePulseCnt; }
    /** @return Bunch-crossing counter */
    RcuCounter*	BCId() 	{ return fBCId; }
    /** @return L0 counter */
    RcuCounter*	L0CNT() 	{ return fL0CNT; }
    /** @return L1 counter */
    RcuCounter*	L1CNT() 	{ return fL1CNT; }
    /** @return L1 message counter */
    RcuCounter*	L1MsgCNT() 	{ return fL1MsgCNT; }
    /** @return L2A counter */
    RcuCounter*	L2ACNT() 	{ return fL2ACNT; }
    /** @return L2R counter */
    RcuCounter*	L2RCNT() 	{ return fL2RCNT; }
    /** @return ROI counter */
    RcuCounter*	ROICNT() 	{ return fROICNT; }
    /** @return Hamming coding errors */
    RcuCounter*	TTCHammingCnt() 	{ return fTTCHammingCnt; }
    /** @return Decode errors */
    RcuCounter*	TTCDecodeCnt() 	{ return fTTCDecodeCnt; }
    /** @return # of buffered events */
    RcuCounter*	BufferedCnt() 	{ return fBufferedCnt; }
    /** @return Event information */
    RcuTTCEventInfo*	TTCEventInfo() 	{ return fTTCEventInfo; }
    /** @return Event information */
    RcuTTCEventError*	TTCEventError() 	{ return fTTCEventError; }
    /** @} */

    /** @{ 
	@name Foo */
    /** @return SoftWare TRiGger CouNTer */
    RcuCounter* SWTRGCNT()	{ return fSWTRGCNT; }
    /** @return AUXilary TRiGger CouNTer */
    RcuCounter* AUXTRGCNT()	{ return fAUXTRGCNT; }
    /** @return TTC L2A trigger CouNTer */
    RcuCounter* TTCL2ACNT()	{ return fTTCL2ACNT; }
    /** @return TTC L2R trigger CouNTer */
    RcuCounter* TTCL2RCNT()	{ return fTTCL2RCNT; }
    /** Data strobe counter for branch A - kO_MEBSTCNT */
    RcuMEBSTCNT* MEBSTCNT()	{ return fMEBSTCNT; }
    /** Data strobe counter for branch A - kO_DSTBACNT */
    RcuCounter* DSTBACNT()	{ return fDSTBACNT; }
    /** Data strobe counter for branch B - kO_DSTBBCNT */
    RcuCounter* DSTBBCNT()	{ return fDSTBBCNT; }
    /** Transfer assert counter for branch A - kO_TRSFACNT */
    RcuCounter* TRSFACNT()	{ return fTRSFACNT; }
    /** Transfer assert counter for branch B - kO_TRSFBCNT */
    RcuCounter* TRSFBCNT()	{ return fTRSFBCNT; }
    /** Acknowledge assert counter for branch A - kO_ACKACNT */
    RcuCounter* ACKACNT()	{ return fACKACNT; }
    /** Acknowledge assert counter for branch B - kO_ACKBCNT */
    RcuCounter* ACKBCNT()	{ return fACKBCNT; }
    /** Control strobe counter for branch A - kO_CSTBACNT */
    RcuCounter* CSTBACNT()	{ return fCSTBACNT; }
    /** Control strobe counter for branch B - kO_CSTBBCNT */
    RcuCounter* CSTBBCNT()	{ return fCSTBBCNT; }
    /** Last transfer data strobe counter for branch A - kO_DSTBNUMA */
    RcuCounter* DSTBNUMA()	{ return fDSTBNUMA; }
    /** Last transfer data strobe counter for branch B - kO_DSTBNUMB */
    RcuCounter* DSTBNUMB()	{ return fDSTBNUMB; }
    /** @return RCU data memory */
    RcuDMEM*	DM1()		{ return fDM1; } 
    /** @return RCU data memory */
    RcuDMEM*	DM2()		{ return fDM2; } 
    /** @return RCU Active Channel List memory */
    RcuACL*	ACL()		{ return fACL; } 
    /** @return RCU Active Channel List memory */
    RcuHITLIST*	HITLIST()	{ return fHITLIST; } 
    /** @return RCU event HEADER memory */
    RcuHEADER*	HEADER()	{ return fHEADER; } 
    /** @return L1-enable via CoMmanD command */
    RcuCommand* L1_CMD()	{ return fL1_CMD; } 
    /** @return L1-enable via I2C command */
    RcuCommand* L1_I2C()	{ return fL1_I2C; } 
    /** @return L1-enable via TTC command */
    RcuCommand* L1_TTC()	{ return fL1_TTC; } 
    /** @return ReaD-out ABORT command */
    RcuCommand* RDABORT()	{ return fRDABORT; } 
    /** @return ReSet DMEM1 command */
    RcuCommand* RS_DMEM1()	{ return fRS_DMEM1; } 
    /** @return ReSet DMEM2 command */
    RcuCommand* RS_DMEM2()	{ return fRS_DMEM2; } 
    /** @return ReSet TRigger ConFiGuration command */
    RcuCommand* RS_TRCFG()	{ return fRS_TRCFG; } 
    /** @return ReSet TRigger CouNTer command */
    RcuCommand* RS_TRCNT()	{ return fRS_TRCNT; } 
    /** @return SoftWare TRiGger command */
    RcuCommand* SWTRG()		{ return fSWTRG; } 
    /** CDH memory in TTCrx */
    RcuCDH*  CDH() { return fCDH; }
    /** @} */

    /** @{ 
	@name States */
    /** @return  FSM state */
    RcuABDFSMST* ABDFSMST()	{ return fABDFSMST; }
    /** @return  FSM state */
    RcuRDOFSMST* RDOFSMST()	{ return fRDOFSMST; }
    /** @return  FSM state */
    RcuINSSEQST* INSSEQST()	{ return fINSSEQST; }
    /** @return  FSM state */
    RcuEVMNGST* EVMNGST()	{ return fEVMNGST; }
    /** @} */

    /** Get the register, memory, and command layout */
    const RcuLayout& Layout() const { return fLayout; }
  protected:
    /** Create the RCU interface */
    Rcu();
    /** Create the RCU interface */
    Rcu(const Rcu& other);
    /** Assignment 
	@param o Other */
    Rcu& operator=(const Rcu& o);
    /** Initialize the object 
	@return 0 on success, error code otherwise */
    virtual int Init(const Url& url);
    /** 
     * Handle an option passed in the URI
     * 
     * @param opt Option
     * @param arg Option argument
     * 
     * @return @c true on success, @c false otherwise 
     */    
    virtual bool HandleOption(const std::string& opt, const std::string& arg);
    /** Get the firmware version 
	@return 0 on success, error code otherwise */
    virtual int GetFWVersion() { return 0; }
    /** Set up registers, command, and memories based on layout */
    virtual void SetupRegisters();
    /** Set up registers, command, and memories based on layout */
    virtual void SetupMemories();
    /** Set up registers, command, and memories based on layout */
    virtual void SetupCommands();
    /** Get the decoded firmware version */ 
    virtual unsigned int FirmwareVersion() const 
    { 
      return fFirmwareVersionOrdered; 
    }
    /** 
     * Check if this can access SC partition 
     *
     * @return false 
     */
    virtual bool SCAccess() const { return true; }

    /** Layout of of the RCU */ 
    RcuLayout fLayout;
    /** Possible data block */ 
    Block_t fBlock;
    /** The list of registers */
    Registers fRegisters;
    /** The list of memories */
    Memories fMemories;
    /** The list of commands */
    Commands fCommands;

    
    RcuMemory* fREGS;
    RcuMemory* fMONS;
    
    /** @{
	@name Error and status  */
    /** @return ERRor and STatus register.   */
    RcuERRST*	fERRST;
    /** @return TRigger CouNTers.   */
    RcuTRCNT*	fTRCNT;
    /** @return ReaDOut FrontEnd Card register.   */
    RcuRDOFEC*	fRDOFEC;
    /** @return CHannel Address register  */
    RcuCHADD*	fCHADD;
    /** @return FirmWare VERSion */
    RcuFWVERS*	fFWVERS;
    /** @return Back-plane version (RCU II) */
    RcuBPVERS*	fBPVERS;
    /** @return Rcu Identifier (RCU II) */
    RcuRCUID*	fRCUID;
    /** @return ERRors on branch A FEC bus */
    RcuFECERR*	fFECERRA;
    /** @return ERRors on branch B FEC bus */
    RcuFECERR*	fFECERRB;
    /** @return ReaD-Out errors */
    RcuRDOERR*	fRDOERR;
    /** @return Bus status */
    RcuBusStatus* fALTBUS_STATUS;
    /** @return Bus status */
    RcuBusStatus* fALTBUS_TRSF;
    /** @return Bus grand */ 
    RcuRCUBUS*	fRCUBUS;
    /** @return ReSet STATUS command */
    RcuCommand*	fRS_STATUS; 
    /** @return Clear RDRX error register */
    RcuCommand*	fCLEARRDRXREG;
    /** @return Align the phases of the readout clocks */
    RcuCommand* fARM_ASYNCH;
    /** @} */

    /** @{ 
	@name U2F Specific registers */
    /** @return Last Written ADDress in the RcuDMEM1 and RcuDMEM2.   */
    RcuLWADD*	fLWADD;
    /** @return EVent WORD register.   */
    RcuEVWORD*	fEVWORD;
    /** @return ReaD-Out MODe register.   */
    RcuTRCFG2*	fTRCFG2;
    /** @return FirMware Input ReGister. */
    RcuFMIREG*	fFMIREG;
    /** @return FirMware Output ReGister  */
    RcuFMOREG*	fFMOREG;
    /** @return CLeaR EVent TAG command */
    RcuCommand*	fCLR_EVTAG; 
    /** @return FirMware command */
    RcuCommand*	fRDFM; 
    /** @return TRiGger CLeaR command */
    RcuCommand*	fTRG_CLR; 
    /** @return WRite FirMware command */
    RcuCommand*	fWRFM; 
    /** @} */

    /** @{ 
	@name Instruction coder */
    /** @return Last executed ALTRO InstRuction ADDress.   */
    RcuIRADD*	fIRADD;
    /** @return Last executed ALTRO InstRuction DATa. */
    RcuIRDAT*	fIRDAT;
    /** @return RCU instruction memory */
    RcuIMEM*	fIMEM; 
    /** @return RCU result memory */
    RcuRMEM*	fRMEM; 
    /** @return ABORT instruction execution  */
    RcuCommand*	fABORT; 
    /** @return EXECute instructions command */
    RcuCommand*	fEXEC; 
    /** @} */

    
    /** @{ 
	@name Configuration registers */
    /** @return ACTive FrontEnd Card.   */
    RcuACTFEC*	fACTFEC;
    /** @return Pedestal Memory ConFiGuration */
    RcuPMCFG*	fPMCFG;
    /** @return Altro config (RCU II) */
    RcuALTROIF*	fALTROIF;
    /** @return ALTRO config (RCU II)  */
    RcuALTROCFG1*	fALTROCFG1;
    /** @return ALTRO config (RCU II)  */
    RcuALTROCFG2*	fALTROCFG2;
    /** @return RCU pedestal memory */
    RcuPMEM*	fPMEM; 
    /** @return DCS OwN bus command */
    RcuCommand*	fDCS_ON; 
    /** @return DDL OwN bus command */
    RcuCommand*	fDDL_ON; 
    /** @return Front-End Card ReSeT command */
    RcuCommand*	fFECRST; 
    /** @return GLoBal RESET command */
    RcuCommand*	fGLB_RESET; 
    /** @return RCU RESET command */
    RcuCommand*	fRCU_RESET; 
    /** @return Re-program FEC BC */
    RcuCommand*	fCONFFEC;
    /** @return Request DCS interrupt */
    RcuCommand*	fARBITERIRQ;
    /** @} */

    /** @{ 
	@name Monitoring and safety registers */
    /** @return INTerrupt REGister  */
    RcuINTREG*	fINTREG;
    /** @return RESult REGister  */
    RcuRESREG*	fRESREG;
    /** @return ERRor REGister  */
    RcuERRREG*	fERRREG;
    /** @return INTerrupt MODe register  */
    RcuINTMOD*	fINTMOD;
    /** @return FirmWare VERSion */
    RcuSCADD*	fSCADD;
    /** @return FirmWare VERSion */
    RcuSCDAT*	fSCDAT;
    /** @return FirmWare VERSion */
    RcuSCCLK*	fSCCLK;
    /** @return ReSet ERRor REGister */
    RcuCommand*	fRS_ERRREG;
    /** @return ReSet ERRor REGister */
    RcuCommand*	fRS_RESREG;
    /** @return ReSet ERRor REGister */
    RcuCommand*	fSCEXEC;
    /** @return Interrtupt types */
    RcuINT_TYPE* fINT_TYPE;
    /** @return Status memory status */
    RcuSMEM_STATUS* fSMEM_STATUS;
    /** @return ReSTore Active Front-end card List */
    RcuCommand* fRST_AFL;
    /** @return ReSTore ReaD-Out front-end card list */
    RcuCommand* fRST_RDO;
    /** @return Clear status memory ready bits */
    RcuCommand* fRS_SMEM; 
    /** @return Status memory */
    RcuSTATUS*	fSTATUS;
    /** @return Slow Control COMMAND command */
    RcuCommand*	fSCCOMMAND; 
    /** @} */

    /** @{ 
	@name Trigger & event interface */
    /** @return TRigger ConFiG register.   */
    RcuTRCFG1*	fTRCFG1;
    /** @return TRigger ConFiG register.   */
    RcuTRGCONF*	fTRGCONF;
    /** @return ReaD-Out mode (RCU II) */
    RcuRDOMOD*	fRDOMOD;
    /** @return SoftWare TRiGger CouNTer */
    RcuCounter*	fSWTRGCNT;
    /** @return AUXilary TRiGger CouNTer */
    RcuCounter*	fAUXTRGCNT;
    /** @return TTC L2A trigger CouNTer */
    RcuCounter*	fTTCL2ACNT;
    /** @return TTC L2R trigger CouNTer */
    RcuCounter*	fTTCL2RCNT;
    /** Data strobe counter for branch A - kO_MEBSTCNT */
    RcuMEBSTCNT* fMEBSTCNT;
    /** Data strobe counter for branch A - kO_DSTBACNT */
    RcuCounter* fDSTBACNT;
    /** Data strobe counter for branch B - kO_DSTBBCNT */
    RcuCounter* fDSTBBCNT;
    /** Transfer assert counter for branch A - kO_TRSFACNT */
    RcuCounter* fTRSFACNT;
    /** Transfer assert counter for branch B - kO_TRSFBCNT */
    RcuCounter* fTRSFBCNT;
    /** Acknowledge assert counter for branch A - kO_ACKACNT */
    RcuCounter* fACKACNT;
    /** Acknowledge assert counter for branch B - kO_ACKBCNT */
    RcuCounter* fACKBCNT;
    /** Control strobe counter for branch A - kO_CSTBACNT */
    RcuCounter* fCSTBACNT;
    /** Control strobe counter for branch B - kO_CSTBBCNT */
    RcuCounter* fCSTBBCNT;
    /** Last transfer data strobe counter for branch A - kO_DSTBNUMA */
    RcuCounter* fDSTBNUMA;
    /** Last transfer data strobe counter for branch B - kO_DSTBNUMB */
    RcuCounter* fDSTBNUMB;
    /** @} */

    /** @{ 
	@name TTC interface */
    /** Trigger control register (0x4000) */
    RcuTTCControl*	fTTCControl;
    /** TTC interface reset command (0x4001) */
    RcuCommand*		fTTCReset;
    /** Region of interest configuration (0x4002) */
    RcuROIConfig1*	fROIConfig1;
    /** Region of interest configuration (0x4003) */
    RcuROIConfig2*	fROIConfig2;
    /** TTC interface reset counters command (0x4004) */
    RcuCommand*		fTTCResetCnt;
    /** TTC interface test mode command (0x4005) */
    RcuCommand*		fTTCTestmode;
    /** L1 timeout and window (0x4006) */
    RcuL1Timeout*	fL1Timeout;
    /** L2 time-out (0x4007) */
    RcuL2Timeout*	fL2Timeout;
    /** ROI message timeout (0x4009) */
    RcuRoiTimeout*	fRoiTimeout;
    /** L1 message timeout (0x400A) */
    RcuL1MsgTimeout*	fL1MsgTimeout;
    /** Prepulser counter (0x400B) */
    RcuCounter*	fPrePulseCnt;
    /** Bunch-crossing counter (0x400C) */
    RcuCounter*		fBCId;
    /** L0 counter (0x400D) */
    RcuCounter*		fL0CNT;
    /** L1 counter (0x400E) */
    RcuCounter*		fL1CNT;
    /** L1 message counter (0x400F) */
    RcuCounter*	fL1MsgCNT;
    /** L2A counter (0x4010) */
    RcuCounter*		fL2ACNT;
    /** L2R counter (0x4011) */
    RcuCounter*		fL2RCNT;
    /** ROI counter (0x4012) */
    RcuCounter*		fROICNT;
    /** Hamming coding errors (0x4016) */
    RcuCounter*	fTTCHammingCnt;
    /** Decode errors (0x4017) */
    RcuCounter*	fTTCDecodeCnt;
    /** # of buffered events (0x4020) */
    RcuCounter*	fBufferedCnt;
    /** Event information (0x4028) */
    RcuTTCEventInfo*	fTTCEventInfo;
    /** Event information (0x4029) */
    RcuTTCEventError*	fTTCEventError;
    /** @} */

    /** @{ 
	@name Stuff */
    /** @return RCU data memory */
    RcuDMEM*	fDM1; 
    /** @return RCU data memory */
    RcuDMEM*	fDM2; 
    /** @return RCU Active Channel List memory */
    RcuACL*	fACL; 
    /** @return RCU Active Channel List memory */
    RcuHITLIST*	fHITLIST; 
    /** @return RCU event HEADER memory */
    RcuHEADER*	fHEADER; 
    /** @return L1-enable via CoMmanD command */
    RcuCommand*	fL1_CMD; 
    /** @return L1-enable via I2C command */
    RcuCommand*	fL1_I2C; 
    /** @return L1-enable via TTC command */
    RcuCommand*	fL1_TTC; 
    /** @return ReaD-out ABORT command */
    RcuCommand*	fRDABORT; 
    /** @return ReSet DMEM1 command */
    RcuCommand*	fRS_DMEM1; 
    /** @return ReSet DMEM2 command */
    RcuCommand*	fRS_DMEM2; 
    /** @return ReSet TRigger ConFiGuration command */
    RcuCommand*	fRS_TRCFG; 
    /** @return ReSet TRigger CouNTer command */
    RcuCommand*	fRS_TRCNT; 
    /** @return SoftWare TRiGger command */
    RcuCommand*	fSWTRG; 
    /** CDH memory in TTCrx */
    RcuCDH*  fCDH;
    /** @} */

    /** @{ 
	@name States */
    /** @return Arbitrator FSM state */
    RcuABDFSMST* fABDFSMST;
    /** @return read-out FSM state */
    RcuRDOFSMST* fRDOFSMST; 
    /** @return instruction sequencer FSM state */
    RcuINSSEQST* fINSSEQST; 
    /** @return event manager FSM state */
    RcuEVMNGST*  fEVMNGST; 
    /** @} */

    /** The firmware version */
    unsigned int fFirmwareVersion;
    /** The firmware version */
    unsigned int fFirmwareVersionOrdered;
    /** Url */
    Url fUrl;
  };
}

#endif
//====================================================================
//
// EOF
//
