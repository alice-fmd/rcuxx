//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/RcuLayout.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_RcuLayout
# include <rcuxx/RcuLayout.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif


//____________________________________________________________________
namespace Rcuxx 
{
  RcuLayout::RcuLayout(int vers)
    : kA_RMEM(		kInvalid), 
      kA_ACL(		kInvalid), 
      kA_PMEM(		kInvalid), 
      kA_IMEM(		kInvalid), 
      kA_DML1(		kInvalid), 
      kA_DMH1(		kInvalid), 
      kA_REGISTERS(	kInvalid), 
      kA_DML2(		kInvalid), 
      kA_DMH2(		kInvalid), 
      kA_DM2HH(		kInvalid), 
      kA_MONITOR(	kInvalid), // kInvalid), 
      kA_STATUS(	kInvalid), 
      kA_HEADER(	kInvalid), 
      kA_TTCRX(         kInvalid),
      kA_HITLIST(       kInvalid),
      // Masks 
      kM_IMEM(		kInvalid), 
      kM_RMEM(		kInvalid),
      kM_ACL(		kInvalid),
      kA_CDH(		kInvalid),
      
      // Sizes of memories 
      kS_IMEM(		0), 
      kS_PMEM(		0), 
      kB_PMEM(		0), 
      kS_RMEM(		0), 
      kS_ACL(		0), 
      kS_DM(		0), 
      kS_REGISTERS(	0), // 8), 
      kS_MONITOR(	0), // 101), 
      kS_HEADER(	0), 
      kS_STATUS(	0), 
      kS_TTCRX(         0),
      kS_HITLIST(       0),
      kS_CDH(		0),
      
      // Registers 
      kO_ERRST(		kInvalid), 
      kO_TRCFG1(	kInvalid), 
      kO_TRGCONF(       kInvalid), 
      kO_TRCNT(		kInvalid), 
      kO_LWADD(		kInvalid), 
      kO_IRADD(		kInvalid), 
      kO_IRDAT(		kInvalid), 
      kO_PMCFG(		kInvalid), 
      kO_CHADD(		kInvalid), 
      // Monitor commands 
      kO_ACTFEC(	kInvalid), 
      kO_RDOFEC(	kInvalid), 
      kO_INTREG(	kInvalid), 
      kO_RESREG(	kInvalid),  // 4 in 100506
      kO_ERRREG(	kInvalid),  // 5 in 100506
      kO_INTMOD(	kInvalid),  // 6 in 100506
      kO_SCADD(		kInvalid),  // kInvalid in 100506
      kO_SCDAT(		kInvalid),  // kInvalid in 100506
      kO_INT_TYPE(	kInvalid), 
      kO_SMEM_STATUS(	kInvalid), 
      kO_SCAFL(         kInvalid),  // 
      kO_FWVERS(	kInvalid),  // 6 in 060506 - one 24 bit word
      kO_SCCLK(		kInvalid), 
      kO_EVWORD(	kInvalid), 
      kO_FMIREG(	kInvalid), 
      kO_FMOREG(	kInvalid), 
      kO_TRCFG2(	kInvalid),      
      // RCU II registers
      kO_ALTROIF(	kInvalid),	//  RCU II specific - kO_ALTROIF 
      kO_RDOMOD(	kInvalid),	//  RCU II specific - kO_RDOMOD 
      kO_ALTROCFG1(	kInvalid),	//  RCU II specific - kO_ALTROCFG1 
      kO_ALTROCFG2(	kInvalid),	//  RCU II specific - kO_ALTROCFG2 
      kO_FECERRA(	kInvalid),	//  RCU II specific - kO_FECERRA 
      kO_FECERRB(	kInvalid),	//  RCU II specific - kO_FECERRB 
      kO_RDOERR(	kInvalid),	//  RCU II specific - kO_RDOERR 
      kO_ALTBUS_STATUS( kInvalid), 
      kO_ALTBUS_TRSF(   kInvalid),
      kO_RCUBUS(	kInvalid),	//  RCU II specific - kO_RCUBUS 
      kO_SWTRCNT(	kInvalid),	//  RCU II specific - kO_SWTRCNT 
      kO_AUXTRGCNT(	kInvalid),	//  RCU II specific - kO_AUXTRGCNT 
      kO_TTCL2ACNT(	kInvalid),	//  RCU II specific - kO_TTCL2ACNT 
      kO_TTCL2RCNT(	kInvalid),	//  RCU II specific - kO_TTCL2RCNT 
      kO_MEBSTCNT(	kInvalid),	//  Data strobe counter for branch A
      kO_DSTBACNT(	kInvalid),	//  Data strobe counter for branch A
      kO_DSTBBCNT(	kInvalid),	//  Data strobe counter for branch B
      kO_TRSFACNT(	kInvalid),	//  Transfer counter for branch A
      kO_TRSFBCNT(	kInvalid),	//  Transfer counter for branch B 
      kO_ACKACNT(	kInvalid),	//  Acknowledge counter for branch A
      kO_ACKBCNT(	kInvalid),	//  Acknowledge counter for branch B
      kO_CSTBACNT(	kInvalid),	//  Control strobe counter for branch A
      kO_CSTBBCNT(	kInvalid),	//  Control strobe counter for branch B
      kO_DSTBNUMA(	kInvalid),	//  Last transfer DSTB counter 
      kO_DSTBNUMB(	kInvalid),	//  Last transfer DSTB counter
      kO_BPVERS(	kInvalid),	//  RCU II specific - kO_BPVERS 
      kO_RCUID(		kInvalid),	//  RCU II specific - kO_BPVERS 
      // TTC interface 
      kO_TTCControl(	kInvalid),	// Trigger control register
      kC_TTCReset(	kInvalid),	// TTC interface reset command
      kO_ROIConfig1(	kInvalid),	// Region of interest configuration
      kO_ROIConfig2(	kInvalid),	// Region of interest configuration
      kC_TTCResetCnt(	kInvalid),	// TTC interface reset counters command
      kC_TTCTestmode(	kInvalid),	// TTC interface test mode command
      kO_L1Timeout(	kInvalid),	// L1 timeout and window
      kO_L2Timeout(	kInvalid),	// L2 time-out
      kO_RoiTimeout(	kInvalid),	// ROI message timeout
      kO_L1MsgTimeout(	kInvalid),	// L1 message timeout
      kO_PrePulseCnt(	kInvalid),	// Prepulser counter
      kO_BCId(		kInvalid),	// Bunch-crossing counter
      kO_L0CNT(		kInvalid),	// L0 counter
      kO_L1CNT(		kInvalid),	// L1 counter
      kO_L1MsgCNT(	kInvalid),	// L1 message counter
      kO_L2ACNT(	kInvalid),	// L2A counter
      kO_L2RCNT(	kInvalid),	// L2R counter
      kO_ROICNT(	kInvalid),	// ROI counter
      kO_TTCHammingCnt(	kInvalid),	// Hamming coding errors
      kO_TTCDecodeCnt(	kInvalid),	// Decode errors
      kO_BufferedCnt(	kInvalid),	// # of buffered events
      kO_TTCEventInfo(	kInvalid),	// Event information
      kO_TTCEventError(	kInvalid),	// Event information
      // Commands 
      kC_RS_STATUS(	kInvalid), 
      kC_RS_TRCFG(	kInvalid), 
      kC_RS_TRCNT(	kInvalid), 
      kC_EXEC(		kInvalid), 
      kC_ABORT(		kInvalid), 
      kC_FECRST(	kInvalid), 
      kC_SWTRG(		kInvalid), 
      kC_RS_DMEM1(	kInvalid), 
      kC_RS_DMEM2(	kInvalid), 
      kC_DCS_ON(	kInvalid), 
      kC_DDL_ON(	kInvalid), 
      kC_L1_TTC(	kInvalid), 
      kC_L1_I2C(	kInvalid), 
      kC_L1_CMD(	kInvalid), 
      kC_GLB_RESET(	kInvalid), 
      kC_RCU_RESET(	kInvalid), 
      kC_SCEXEC(        kInvalid), // Invalid in 100506
      kC_RS_ERRREG(	kInvalid),  // kInvalid in 100506
      kC_RS_RESREG(	kInvalid), // in 100506
      kC_RST_AFL(	kInvalid), 
      kC_RST_RDO(	kInvalid), 
      kC_RS_SMEM(	kInvalid), 
      kC_TRG_CLR(	kInvalid), 
      kC_WRFM(		kInvalid), 
      kC_RDFM(		kInvalid), 
      kC_RDABORT(	kInvalid), 
      kC_CLR_EVTAG(	kInvalid), 
      kC_SCCOMMAND(	kInvalid),  // Alternatively, do kInvalidxxx - where
				  // xxx 
      kC_CLEARRDRXREG(	kInvalid),
      kC_ARBITERIRQ(    kInvalid),
      kC_ARM_ASYNCH(    kInvalid),
      kC_CONFFEC(       kInvalid),

      kO_ABDFSMST(	kInvalid),	// Arbitrator FSM state
      kO_RDOFSMST(	kInvalid),      // readout FSM state 
      kO_INSSEQST(	kInvalid),      // Instruction sequencer FSM state 
      kO_EVMNGST(	kInvalid),      // event mananger FSM state 

      kIMEM_RCU_CMD(	kInvalid),	//  RCU II specific - kIMEM_RCU_CMD 
      kIMEM_STOP(	kInvalid),	//  RCU II specific - kIMEM_STOP 
      kIMEM_END(	kInvalid),	//  RCU II specific - kIMEM_END 
      kIMEM_FEC_RD1(	kInvalid),	//  RCU II specific - kIMEM_FEC_RD1 
      kIMEM_FEC_WR1(	kInvalid),	//  RCU II specific - kIMEM_FEC_WR1 
      kIMEM_FEC_CMD1(	kInvalid),	//  RCU II specific - kIMEM_FEC_CMD 
      kIMEM_FEC_RD2(	kInvalid),	//  RCU II specific - kIMEM_FEC_RD2 
      kIMEM_FEC_WR2(	kInvalid),	//  RCU II specific - kIMEM_FEC_WR2 
      kIMEM_FEC_CMD2(	kInvalid),	//  RCU II specific - kIMEM_FEC_CMD 
      kRMEM_ERROR(	kInvalid),	//  RCU II specific - kRMEM_ERROR 
      kRMEM_RCU_STOP(	kInvalid),	//  RCU II specific - kRMEM_RCU_STOP 
      kRMEM_RCU_END(	kInvalid),	//  RCU II specific - kRMEM_RCU_END 
      kRMEM_FEC_WR(	kInvalid),	//  RCU II specific - kRMEM_FEC_WR 
      kRMEM_FEC_RD(	kInvalid),	//  RCU II specific - kRMEM_FEC_RD 
      kRMEM_FEC_CMD(	kInvalid),	//  RCU II specific - kRMEM_FEC_CMD 
      fTriggerMapFlipped(false), 
      fFullFMDD(true)
  {
    Setup(vers);
  }

  //====================================================================
  unsigned int RcuLayout::Hw2Date(unsigned int hwvers) 
  {
    if (hwvers == 0xffffffff) return hwvers;
    unsigned int day   = 0;
    unsigned int month = 0;
    unsigned int year  = 0;
    if (hwvers >  0xffffff) { 
      day   = ((hwvers >> 24) & 0xFF);
      month = ((hwvers >> 16) & 0xFF);
      year  = ((hwvers >>  8) & 0xFF);
    }
    else {
      day   = ((hwvers >> 16) & 0xFF);
      month = ((hwvers >>  8) & 0xFF);
      year  = ((hwvers >>  0) & 0xFF);
    }
    unsigned int decoded = (year << 16) | (month << 8) | day;
    return decoded;
  }
  //====================================================================
  int RcuLayout::Decode(const unsigned int fw_version, 
			unsigned int& decoded) const
  {
#if 1
    std::cout << "Decoding FW version 0x" << std::hex
	      << std::setfill('0') << std::setw(6) << fw_version;
#endif

    decoded = Hw2Date(fw_version); 
    if (decoded == 0xffffffff) return kU2F;

#if 1
    // (year << 16) | (month << 8) | day;
    std::cout << " -> 0x" << std::setw(6) << decoded << std::dec
	      << std::setfill(' ') << std::endl;
#endif

    if (decoded >= 0x141119) return k191114;
    if (decoded >= 0x100525) return k250510;
    if (decoded >= 0x100226) return k260210;
    if (decoded >= 0x080616) return k160608;
    if (decoded >= 0x080400) return k000408;
    if (decoded >= 0x061218) return k181206;
    if (decoded >= 0x061106) return k061106;
    if (decoded >= 0x060627) return k270606;
    if (decoded >= 0x060619) return k190606;
    if (decoded >= 0x060510) return k100506;
    return kAncient;
  }
  //====================================================================
  void RcuLayout::SetupDefault(bool debug)
  {
    if (debug) std::cout << " Default" << std::flush;
    kA_RMEM		= 0x6000; 
    kA_ACL		= 0x6400; 
    kA_PMEM		= 0x6800; 
    kA_IMEM		= 0x7000; 
    kA_DML1		= 0x7400; 
    kA_DMH1		= 0x7500; 
    kA_REGISTERS	= 0; // 0x7800), 
    kA_DML2		= 0x7C00; 
    kA_DMH2		= 0x7D00; 
    kA_DM2HH		= 0xFD00; 
    kA_STATUS		= 0x8100; 
    kA_HEADER		= 0x4001; 
    // Masks 
    kM_IMEM		= 0xffffff; 
    kM_RMEM		= 0xfffff;
    kM_ACL		= 0xffff;
      
    // Sizes of memories 
    kS_IMEM		= 256; 
    kS_PMEM		= 1024; 
    kB_PMEM		= 256; 
    kS_RMEM		= 128; 
    kS_ACL		= 256; 
    kS_DM		= 256; 
    kS_REGISTERS	= 0x8200; // 8), 
    kS_HEADER		= 8; 
    kS_STATUS		= 32; 
      
    // Registers 
    kO_ERRST		= 0x7800; 
    kO_TRCFG1		= 0x7801; 
    kO_TRCNT		= 0x7802; 
    kO_LWADD		= 0x7803; 
    kO_IRADD		= 0x7804; 
    kO_IRDAT		= 0x7805; 
    kO_PMCFG		= 0x7806; 
    kO_CHADD		= 0x7807; 
    // Monitor commands 
    kO_ACTFEC		= 0x8000; 
    kO_RDOFEC		= 0x8001; 
    kO_INTREG		= 0x8100; 
    kO_RESREG		= 0x8002;  // 4 in 100506
    kO_ERRREG		= 0x8003;  // 5 in 100506
    kO_INTMOD		= 0x8004;  // 6 in 100506
    kO_SCADD		= 0x8005;  // kInvalid in 100506
    kO_SCDAT		= 0x8006;  // kInvalid in 100506
    kO_SCAFL          	= 0x8000;  // 
    kO_FWVERS		= 0x8008;  // 6 in 060506 - one 24 bit word
    // Commands 
    kC_RS_STATUS	= 0x6c01; 
    kC_RS_TRCFG		= 0x6c02; 
    kC_RS_TRCNT		= 0x6c03; 
    kC_EXEC		= 0x0000; 
    kC_ABORT		= 0x0800; 
    kC_FECRST		= 0x2001; 
    kC_SWTRG		= 0xD000; 
    kC_RS_DMEM1		= 0x6C04; 
    kC_RS_DMEM2		= 0x6C05; 
    kC_DCS_ON		= 0xE000; 
    kC_DDL_ON		= 0xF000; 
    kC_L1_TTC		= 0xE800; 
    kC_L1_I2C		= 0xF800; 
    kC_L1_CMD		= 0xD800; 
    kC_GLB_RESET	= 0x2000; 
    kC_RCU_RESET	= 0x2002; 
    kC_SCEXEC         	= 0x8010; // Invalid in 100506
    kC_RS_ERRREG	= 0x8011;  // 0x8011 in 100506
    kC_RS_RESREG	= 0x8012; // in 100506
    kC_SCCOMMAND	= 0xC000;  // Alternatively, do 0xCxxx - where
    kIMEM_STOP		= 0x390000;	//  RCU II specific - kIMEM_STOP 
    kIMEM_FEC_RD1	= 0x400000;	//  RCU II specific - kIMEM_FEC_RD1 
    kIMEM_FEC_WR1	= 0x600000;	//  RCU II specific - kIMEM_FEC_WR1 
    kIMEM_FEC_CMD1	= 0x600000;	//  RCU II specific - kIMEM_FEC_CMD 
    kIMEM_FEC_RD2	= 0x500000;	//  RCU II specific - kIMEM_FEC_RD2 
    kIMEM_FEC_WR2	= 0x700000;	//  RCU II specific - kIMEM_FEC_WR2 
    kIMEM_FEC_CMD2	= 0x700000;	//  RCU II specific - kIMEM_FEC_CMD 
  }
  void RcuLayout::SetupU2F(bool debug)
  {
    // Load the default
    SetupDefault(debug);
    
    if (debug) std::cout << " U2F" << std::flush;
    // Memories 
    kA_STATUS		= RcuLayout::kInvalid; // 0x8100;
    kA_HEADER		= RcuLayout::kInvalid;
    // Sizes 
    // kS_REGISTERS	= 15;
    kS_HEADER		= 0;
    kS_STATUS		= 0;
    // Registers 
    kO_EVWORD		= 0x7806;
    kO_FMIREG		= 0x7808;
    kO_FMOREG		= 0x7809;
    kO_ACTFEC		= 0x7807;
    kO_TRCFG2		= 0x7810;
    kO_RESREG		= 0x7811;
    kO_ERRREG		= 0x7812;
    kO_INTMOD		= 0x7813;
    kO_INTREG		= 0x7814;
    kO_PMCFG		= RcuLayout::kInvalid;
    kO_CHADD		= RcuLayout::kInvalid;
    kO_RDOFEC		= RcuLayout::kInvalid;
    // Commands 
    kC_DCS_ON		= RcuLayout::kInvalid;
    kC_DDL_ON		= RcuLayout::kInvalid;
    kC_L1_TTC		= RcuLayout::kInvalid;
    kC_L1_I2C		= RcuLayout::kInvalid;
    kC_L1_CMD		= RcuLayout::kInvalid;
    kC_GLB_RESET	= RcuLayout::kInvalid;
    kC_RCU_RESET	= RcuLayout::kInvalid;
    kC_RS_ERRREG	= RcuLayout::kInvalid;
    kC_RS_DMEM1		= 0xD001;
    kC_RS_DMEM2		= 0xD002;
    kC_TRG_CLR		= 0xD003;
    kC_WRFM		= 0xD004;
    kC_RDFM		= 0xD005;
    kC_RDABORT		= 0xD006;
    kC_CLR_EVTAG	= 0xD007;
  }
  void RcuLayout::SetupAncient(bool debug)
  {
    // Load base definition 
    Setup20060510(debug);
    // Overrides
    if (debug) std::cout << " Ancient" << std::flush;
    kO_FWVERS	= 0x8006;
  }
    
  void RcuLayout::Setup20060510(bool debug)
  {
    // Load defaults
    SetupDefault(debug);
    // Overrides 
    if (debug) std::cout << " 20060510" << std::flush;
    kO_RESREG		= 0x8004; 
    kO_ERRREG		= 0x8005; 
    kO_INTMOD		= 0x8006; 
    kO_SCADD		= kInvalid;
    kO_SCDAT		= kInvalid;
    kC_SCEXEC		= kInvalid;
  }
  void RcuLayout::Setup20141119(bool debug)
  {
    // Load base
    Setup20100525(debug);
    // Overridess
    if (debug) std::cout << " 20141119" << std::flush;
    kO_TTCEventInfo	= 0x402B;	// Event information
    kO_TTCEventError	= 0x402C;	// Event information
    kS_CDH		= 10;
  }
  void RcuLayout::Setup20100525(bool debug)
  {
    // Load base
    Setup20080616(debug);
    // Overrides 
    if (debug) std::cout << " 20100525" << std::flush;
    kO_SCCLK		= 0x800C;       // I2C bus speed
    kO_INT_TYPE		= 0x8007;	// Interrupt types 
    kO_SMEM_STATUS	= 0x8009;	// Status memory status
    kC_RST_AFL		= 0x8013;
    kC_RST_RDO		= 0x8014;
    kC_RS_SMEM		= 0x8015;
    kA_CDH		= 0x4021;
    kS_CDH		= 7;
    kA_PMEM		= kInvalid;
    kS_PMEM		= kInvalid;
    kA_DML1		= kInvalid;
    kA_DMH1		= kInvalid;
    kA_DML2		= kInvalid;
    kA_DMH2		= kInvalid;
    kS_DM		= kInvalid;
    kC_RS_DMEM1		= kInvalid;
    kC_RS_DMEM2		= kInvalid;
    kA_STATUS		= kInvalid;
    kS_STATUS		= kInvalid;
  };
  void RcuLayout::Setup20080616(bool debug)
  {
    // Load base
    Setup20080400(debug);
    // Overrides 
    if (debug) std::cout << " 20080616" << std::flush;
    kO_ABDFSMST		= 0x512C;	// Arbitrator FSM state
    kO_RDOFSMST		= 0x512D;      	// readout FSM state 
    kO_INSSEQST		= 0x512E;      	// Instruction sequencer FSM state 
    kO_EVMNGST		= 0x512F;      	// event mananger FSM state 
    kO_ALTBUS_STATUS  	= 0x5114; 
    kO_ALTBUS_TRSF    	= 0x5115; 
  }
  void RcuLayout::Setup20080400(bool debug)
  {
    // Load base
    SetupDefault(debug);
    // Overrides
    if (debug) std::cout << " 20080400" << std::flush;
    // Memories
    // kA_REGISTERS	= 0x5100;
    kA_IMEM		= 0x0000;
    kA_ACL		= 0x1000;
    kA_RMEM		= 0x2000; // 0x2001;
    kA_TTCRX		= 0x4000;
    kA_HITLIST		= 0x5000;
    kA_DML1		= 0x5800;
    kA_DMH1		= 0x5800;
    kA_DML2		= 0x5900;
    kA_DMH2		= 0x5900;
    kM_IMEM		= 0x3FFFFF;
    kM_RMEM		= 0x1FFFFFF;
    kM_ACL		= 0xFFF;
      
    // Sizes 
    // kS_REGISTERS	= 0x0208;
    kS_IMEM		= 4096; // 0x1000 - kA_IMEM    + 1;
    kS_ACL		= 4096; // 0x2000 - kA_ACL     + 1;
    kS_RMEM		= 6144; // 0x4000 - kA_RMEM    + 1;
    kS_TTCRX		= 128;  // 0x5000 - kA_TTCRX   + 1;
    kS_HITLIST		= 128;  // 0x5080 - kA_HITLIST + 1;
    kS_DM		= 256;  // (0x6000 - kA_DML1    + 1)/2;
    // Commands
    kC_RS_STATUS      	= 0x5307;
    kC_RS_TRCFG       	= kInvalid;
    kC_RS_TRCNT 	= 0x5309;
    kC_EXEC		= 0x5304; 
    kC_ABORT		= 0x5305; 
    kC_FECRST		= 0x5301; 
    kC_SWTRG		= 0x5306; 
    kC_RS_DMEM1		= kInvalid; 
    kC_RS_DMEM2		= kInvalid; 
    kC_DCS_ON		= 0xE000; // kInvalid; 
    kC_DDL_ON		= 0xF000; // kInvalid; 
    kC_L1_TTC		= kInvalid; 
    kC_L1_I2C		= kInvalid; 
    kC_L1_CMD		= kInvalid; 
    kC_GLB_RESET	= 0x5300; 
    kC_RCU_RESET	= 0x5302; 
    kC_TRG_CLR		= kInvalid; 
    kC_WRFM		= kInvalid; 
    kC_RDFM		= kInvalid; 
    kC_RDABORT		= kInvalid; 
    kC_CLR_EVTAG	= kInvalid; 
    kC_CONFFEC		= 0x5303;
    kC_CLEARRDRXREG	= 0x5308;
    kC_ARM_ASYNCH     	= 0x530A;
    kC_ARBITERIRQ	= 0x5310;
    // Registers 
    kO_ACTFEC		= 0x5100; 
    kO_ALTROIF        	= 0x5101;
    kO_TRGCONF		= 0x5102; 
    kO_RDOMOD		= 0x5103;
    kO_ALTROCFG1	= 0x5104;
    kO_ALTROCFG2	= 0x5105;
    kO_FWVERS		= 0x5106;  // 6 in 060506 - one 24 bit word
    kO_BPVERS		= 0x5107;
    kO_RCUID		= 0x5108;
    kO_FECERRA		= 0x5110;
    kO_FECERRB		= 0x5111;
    kO_RDOERR		= 0x5112;
    kO_RCUBUS		= 0x5113;
    kO_MEBSTCNT		= 0x511B;	//  Data strobe counter for branch A
    kO_SWTRCNT		= 0x511C;
    kO_AUXTRGCNT	= 0x511D;
    kO_TTCL2ACNT	= 0x511E;
    kO_TTCL2RCNT	= 0x511F;
    kO_DSTBACNT		= 0x5120;	//  Data strobe counter for branch A
    kO_DSTBBCNT		= 0x5121;	//  Data strobe counter for branch B
    kO_TRSFACNT		= 0x5122;	//  Transfer counter for branch A
    kO_TRSFBCNT		= 0x5123;	//  Transfer counter for branch B 
    kO_ACKACNT		= 0x5124;	//  Acknowledge counter for branch A
    kO_ACKBCNT		= 0x5125;	//  Acknowledge counter for branch B
    kO_CSTBACNT		= 0x5126;	//  Control strobe counter for branch A
    kO_CSTBBCNT		= 0x5127;	//  Control strobe counter for branch B
    kO_DSTBNUMA		= 0x5128;	//  Last transfer DSTB counter 
    kO_DSTBNUMB		= 0x5129;	//  Last transfer DSTB counter

    // TTC interface
    kO_TTCControl	= 0x4000;	// Trigger control register
    kC_TTCReset		= 0x4001;	// TTC interface reset command
    kO_ROIConfig1	= 0x4002;	// Region of interest configuration
    kO_ROIConfig2	= 0x4003;	// Region of interest configuration
    kC_TTCResetCnt	= 0x4004;	// TTC interface reset counters command
    kC_TTCTestmode	= 0x4005;	// TTC interface test mode command
    kO_L1Timeout	= 0x4006;	// L1 timeout and window
    kO_L2Timeout	= 0x4007;	// L2 time-out
    kO_RoiTimeout	= 0x4009;	// ROI message timeout
    kO_L1MsgTimeout	= 0x400A;	// L1 message timeout
    kO_PrePulseCnt	= 0x400B;	// Prepulser counter
    kO_BCId		= 0x400C;	// Bunch-crossing counter
    kO_L0CNT		= 0x400D;	// L0 counter
    kO_L1CNT		= 0x400E;	// L1 counter
    kO_L1MsgCNT		= 0x400F;	// L1 message counter
    kO_L2ACNT		= 0x4010;	// L2A counter
    kO_L2RCNT		= 0x4011;	// L2R counter
    kO_ROICNT		= 0x4012;	// ROI counter
    kO_TTCHammingCnt	= 0x4016;	// Hamming coding errors
    kO_TTCDecodeCnt	= 0x4017;	// Decode errors
    kO_BufferedCnt	= 0x4020;	// # of buffered events
    kO_TTCEventInfo	= 0x4028;	// Event information
    kO_TTCEventError	= 0x4029;	// Event information

    kO_ERRST		= kInvalid; 
    kO_TRCFG1		= kInvalid;
    kO_TRCNT		= kInvalid; 
    kO_LWADD		= kInvalid; 
    kO_IRADD		= kInvalid; 
    kO_IRDAT		= kInvalid; 
    kO_PMCFG		= kInvalid; 
    kO_CHADD		= kInvalid; 
    kO_RDOFEC		= kInvalid; 
    kO_INTREG		= kInvalid;

    // IMEM constants 
    kIMEM_RCU_CMD	= (0x3 << 20);
    kIMEM_STOP		= kIMEM_RCU_CMD | (0x8 << 16);
    kIMEM_END		= kIMEM_RCU_CMD | (0xF << 16);
    kIMEM_FEC_WR1	= (0x2 << 20);
    kIMEM_FEC_WR2       = kIMEM_FEC_WR1;
    kIMEM_FEC_RD1	= (0x0 << 20);
    kIMEM_FEC_RD2       = kInvalid;
    kIMEM_FEC_CMD1	= (0x1 << 20);
    kIMEM_FEC_CMD2	= kInvalid;
    // RMEM contants 
    kRMEM_ERROR		= (0x1 << 20);
    kRMEM_RCU_STOP	= (0x6 << 21);
    kRMEM_RCU_END	= (0x7 << 21);
    kRMEM_FEC_WR	= kIMEM_FEC_WR1  << 1;
    kRMEM_FEC_RD	= kIMEM_FEC_RD1  << 1;
    kRMEM_FEC_CMD	= kIMEM_FEC_CMD1 << 1;
    
  }
  //====================================================================
  void RcuLayout::Setup(int vers, bool debug)
  {
    if (debug) {
      std::cout << "Setting up code-book for " << vers << " (";
      switch (vers) {
      case kU2F:        std::cout << "U2F";    break;
      case kAncient:    std::cout << "Ancient";break;
      case k191114:     std::cout << "191114"; break;
      case k250510:     std::cout << "250510"; break;
      case k260210:     std::cout << "260210"; break;
      case k160608:     std::cout << "160608"; break;
      case k000408:	std::cout << "000408"; break;
      case k181206:	std::cout << "181206"; break;
      case k061106:	std::cout << "061106"; break;
      case k270606:	std::cout << "270606"; break;
      case k190606:	std::cout << "190606"; break;
      case k100506:	std::cout << "100506"; break;
      default:          std::cout << "default"; break;
      }
      std::cout << ")\n" 
		<< "Loading values for " << std::endl;
    }

    fVersion = vers;
    switch (fVersion) {
    case kDefault:	SetupDefault(debug);	break;
    case kU2F:		SetupU2F(debug);	break;
    case kAncient:	SetupAncient(debug); 	break;
      // Fall through 
    case k100506:	Setup20060510(debug);	break;
    case k190606: 	// Fall through
    case k270606: 	// Fall through
    case k061106: 	// Fall through
    case k181206: 	SetupDefault(debug);	break;
    case k191114: 	Setup20141119(debug);	break;
    case k250510: 	// Fall through
    case k260210: 	Setup20100525(debug);	break;
    case k160608:	Setup20080616(debug);	break;
    case k000408:	Setup20080400(debug);	break;
    default:
      fVersion = kDefault;
      SetupDefault(debug);
      break;
    }
    if (debug) std::cout << std::endl;
    if (fVersion == k000408) {
      fTriggerMapFlipped = true;
      fFullFMDD          = false;
    }
  }
}

#define ADDR(X) \
  std::left  << std::setfill('.') << std::setw(16) << #X << \
  std::right << std::setfill('0') << " 0x" << std::setw(4) << X << "\n"

#define SIZE(X) \
  std::left  << std::setfill('.') << std::setw(16) << #X << \
  std::right << std::setfill(' ') <<  " " << std::setw(4) << X << "\n"
#define OFF(X)  \
  std::left  << std::setfill('.') << std::setw(16) << #X << \
  std::right << std::setfill('0') << " 0x" << std::setw(4) << X << "\n"

//____________________________________________________________________
void
Rcuxx::RcuLayout::Print() const 
{
  std::cout << "Rcu layout: " << std::flush;
  switch (fVersion) {
  case kDefault: std::cout << "The default "; 				break;
  case kU2F:	 std::cout << "U2F test card  "; 			break;
  case kAncient: std::cout << "Before May 10, 2006"; 			break;
  case k100506:	 std::cout << "Firmware version from May 10, 2006"; 	break;
  case k190606:	 std::cout << "Firmware version from June 19, 2006"; 	break;
  case k270606:	 std::cout << "Firmware version from June 27, 2006"; 	break;
  case k061106:	 std::cout << "Firmware version from November 6, 2006"; break;
  case k181206:	 std::cout << "Firmware version from December 12, 2006";break;
  case k000408:  std::cout << "Firmware II from April 2008"; 		break;
  case k160608:	 std::cout << "Firmware II from June 16, 2008";		break;
  case k260210:	 std::cout << "Firmware II february 26, 2010";		break;
  case k250510:	 std::cout << "Firmware II may 25, 2010";		break;
  case k191114:	 std::cout << "Firmware II november 19, 2014 - CDHv3 ";	break;
  default:       std::cout << "Unknown";
  }
  
  std::cout 
    << "\n" << std::hex << std::setfill('0') 
    << "  Addresses of memories:\n" 
    << "    Result memory:\t\t\t"           		<< ADDR(kA_RMEM)
    << "    Active Channel List:\t\t"       		<< ADDR(kA_ACL) 
    << "    Pattern memory:\t\t\t"          		<< ADDR(kA_PMEM)
    << "    Instruction memory:\t\t\t"      		<< ADDR(kA_IMEM)
    << "    Data memory 1 - low bits:\t\t"  		<< ADDR(kA_DML1)
    << "    Data memory 1 - high bits:\t\t" 		<< ADDR(kA_DMH1)
    << "    Base address for registers:\t\t"		<< ADDR(kA_REGISTERS)
    << "    Data memory 1 - low bits:\t\t"  		<< ADDR(kA_DML2)
    << "    Data memory 2 - high bits:\t\t" 		<< ADDR(kA_DMH2)
    << "    Data memory 2 - high bits:\t\t" 		<< ADDR(kA_DM2HH)
    << "    Base address for monitor registers:\t" 	<< ADDR(kA_MONITOR)
    << "    Status memory:\t\t\t"           		<< ADDR(kA_STATUS)
    << "    Event header words:\t\t\t"      		<< ADDR(kA_HEADER)
    << "  Sizes of memories:\n" << std::dec << std::setfill(' ')
    << "    Size of instruction memory:\t\t"  		<< SIZE(kS_IMEM)
    << "    Size of pattern memory:\t\t"      		<< SIZE(kS_PMEM)
    << "    Size of pattern memory:\t\t"      		<< SIZE(kB_PMEM)
    << "    Size of result memory:\t\t"       		<< SIZE(kS_RMEM)
    << "    Size of active channel memory:\t" 		<< SIZE(kS_ACL)
    << "    Size of partial data memory:\t"   		<< SIZE(kS_DM)
    << "    Size of register memory:\t\t"       	<< SIZE(kS_REGISTERS)
    << "    Size of monitor memory:\t\t"        	<< SIZE(kS_MONITOR)
    << "    Size of header memory:\t\t"	        	<< SIZE(kS_HEADER)
    << "    Size of status memory:\t\t"         	<< SIZE(kS_STATUS)
    << "  Registers:\n" << std::hex << std::setfill('0')
    << "    Error and status:\t\t\t" 			<< OFF(kO_ERRST)
    << "    Trigger configuration:\t\t" 		<< OFF(kO_TRCFG1)
    << "    Trigger counter:\t\t\t" 			<< OFF(kO_TRCNT)
    << "    Last words in data buffers:\t\t"		<< OFF(kO_LWADD)
    << "    Last instruction address:\t\t" 		<< OFF(kO_IRADD)
    << "    Last instruction data:\t\t" 		<< OFF(kO_IRDAT)
    << "    Pedestal memory configuration:\t" 		<< OFF(kO_PMCFG)
    << "    Last channel address:\t\t" 			<< OFF(kO_CHADD)
    << "    Active FEC bit mask:\t\t" 			<< OFF(kO_ACTFEC)
    << "    Read-out FEC bit mask:\t\t" 		<< OFF(kO_RDOFEC)
    << "    Return BC register to DCS:\t\t" 		<< OFF(kO_RESREG)
    << "    Error type register:\t\t" 			<< OFF(kO_ERRREG)
    << "    Interrupt mask for branches:\t" 		<< OFF(kO_INTMOD)
    << "    Slow control address:\t\t"                  << OFF(kO_SCADD)
    << "    Slow control data:\t\t\t"                   << OFF(kO_SCDAT)
    << "    Slow control clock:\t\t\t"                  << OFF(kO_SCCLK)
    << "    Interrupt types:\t\t\t"                     << OFF(kO_INT_TYPE)
    << "    Status memory status:\t\t\t"                << OFF(kO_SMEM_STATUS)
    << "    Firmware version:\t\t\t"                  	<< OFF(kO_FWVERS)
    << "    Interrupt information:\t\t" 		<< OFF(kO_INTREG)
    << "    Event word register:\t\t" 			<< OFF(kO_EVWORD)
    << "    Firmware input register:\t\t" 		<< OFF(kO_FMIREG)
    << "    Firmware output register:\t\t" 		<< OFF(kO_FMOREG)
    << "    Trigger config 2 register:\t\t" 		<< OFF(kO_TRCFG2)
    << "  Commands:\n"
    << "    EXECute instructions:\t\t" 			<< ADDR(kC_EXEC)
    << "    ABORT instruction execution:\t" 		<< ADDR(kC_ABORT)
    << "    ReSet STATUS:\t\t\t" 			<< ADDR(kC_RS_STATUS)
    << "    ReSet TRigger ConFiGuration:\t"		<< ADDR(kC_RS_TRCFG)
    << "    ReSet TRigger CouNTer:\t\t" 		<< ADDR(kC_RS_TRCNT)
    << "    Front-End Card ReSeT:\t\t"		 	<< ADDR(kC_FECRST)
    << "    SoftWare TRiGger:\t\t\t"	 		<< ADDR(kC_SWTRG)
    << "    ReSet DMEM1:\t\t\t" 			<< ADDR(kC_RS_DMEM1)
    << "    ReSet DMEM2:\t\t\t" 			<< ADDR(kC_RS_DMEM2)
    << "    DCS OwN bus:n\t\t\t" 			<< ADDR(kC_DCS_ON)
    << "    DDL OwN bus:\t\t\t" 			<< ADDR(kC_DDL_ON)
    << "    L1-enable via TTC:\t\t\t" 			<< ADDR(kC_L1_TTC)
    << "    L1-enable via I2C:\t\t\t" 			<< ADDR(kC_L1_I2C)
    << "    L1-enable via CoMmanD:\t\t" 		<< ADDR(kC_L1_CMD)
    << "    GLoBal RESET:\t\t\t" 			<< ADDR(kC_GLB_RESET)
    << "    RCU RESET:\t\t\t\t" 			<< ADDR(kC_RCU_RESET)
    << "    ReSet ERRor REGister:\t\t" 			<< ADDR(kC_RS_ERRREG)
    << "    ReSet RESult REGister:\t\t" 		<< ADDR(kC_RS_RESREG)
    << "    Exeute slow control:\t\t"			<< ADDR(kC_SCEXEC)
    << "    Restore Active card list:\t\t"		<< ADDR(kC_RST_AFL)
    << "    Restore read-out card list:\t\t"		<< ADDR(kC_RST_RDO)
    << "    Clear status memory bits:\t\t"		<< ADDR(kC_RS_SMEM)
    << "    TRiGger CLeaR:\t\t\t" 			<< ADDR(kC_TRG_CLR)
    << "    WRite FirMware:\t\t\t" 			<< ADDR(kC_WRFM)
    << "    ReaD FirMware:\t\t\t" 			<< ADDR(kC_RDFM)
    << "    ReaD-out ABORT:\t\t\t" 			<< ADDR(kC_RDABORT)
    << "    CLeaR EVent TAG:\t\t\t" 			<< ADDR(kC_CLR_EVTAG)
    << "    Slow Control COMMAND:\t\t" 			<< ADDR(kC_SCCOMMAND)
    << std::dec << std::setfill(' ') << std::flush;
}

//
// EOF
//

  
    
