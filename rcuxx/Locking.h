// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Locking.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration locking structures 
*/
#ifndef RCUXX_LOCKING
# define RCUXX_LOCKING
# ifndef _WIN32 
#  include <pthread.h>
# else 
#  include <windows.h>
# endif

namespace Rcuxx 
{
  /** @class Lock 
      @brief A mutually exclusive lock / Critical section lock
   */
  struct Lock 
  {
    /** Construct the lock.  It's initialized to the default type. */
    Lock()
    {
#ifndef _WIN32 
      pthread_mutex_init(&fMutex, NULL); //  = ::PTHREAD_MUTEX_INITIALIZER;
#else 
      InitializeCriticalSection(&fCS); 
#endif
    }
    /** Destruct the lock */
    ~Lock() 
    { 
#ifdef _WIN32
      DeleteCriticalSection(&fCS); 
#endif
    }
    /** Lock the critical section, or wait until it's free and then
	lock it. */
    void LockIt() 
    {
#ifndef _WIN32
      pthread_mutex_lock(&fMutex);
#else
      EnterCriticalSection(&fCS);
#endif
    }
    /** Unlock the critical section */
    void UnlockIt() 
    {
#ifndef _WIN32
      pthread_mutex_unlock(&fMutex);
#else 
      LeaveCriticalSection(&fCS);
#endif
    }
#ifndef _WIN32 
    /** Mutually exclusive lock */
    pthread_mutex_t fMutex;
#else
    /** Critical section */
    CRITICAL_SECTION fCS;
#endif
  };
  
  /** @class LockGuard 
      @brief Utility structure to do automatic locking and unlocking
      in a scope.  On construction the passed lock is locked, and on
      destruction the lock is unlocked.  For example 
      @code 
      struct Foo 
      { 
        Lock lock;
	int DoIt() 
	{
	  LockGuard g(lock); // Lock mutex 
	  // ...
	  return 0; // Automatically unlock mutex
	}
      };
      @endcode
   */
  struct LockGuard 
  {
    /** Construct the guard, and lock the passed mutex. 
	@param l Mutex to lock.  */
    LockGuard(Lock& l) : fLock(l) { fLock.LockIt(); }
    /** Destruct the guard, and unlock the mutex referenced.  */
    ~LockGuard() { fLock.UnlockIt(); }
    /** The mutex to manage */
    Lock& fLock;
  };
}
#endif
//____________________________________________________________________
//
// EOF
//

  
    
    
  
        
