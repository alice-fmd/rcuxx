#include <rcuxx/Rcu.h>
#include <rcuxx/Altro.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/altro/AltroPMDTA.h>
#include <rcuxx/altro/AltroPMADD.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <ctime>
#include <unistd.h>
/*
 * Compile this program with 
 * 
 *   g++ read_patterns.cxx -o read_patterns  `rcuxx-config --cppflags --ldflags` 
 *
 * Run it as a 
 *
 *   ./read_patterns <URL> [-U USEC_DELAY]
 *
 * The output of the program is a comma-separated-values table of all
 * the pattern memory fields of all enabled ALTRO channels. This
 * information can easily be imported into a spread-sheet or the like.
 */
 

//____________________________________________________________________
void usage(const char* progname, bool all) 
{
  std::cout << "Usage: " << progname << " [OPTIONS] NODE\n\n" 
            << "\t-h\t\tThis help\n" 
            << "\t-H\t\tLong help\n"
            << "\t-u USEC\t\tSleep between updates\n"
            << std::endl;
  if (!all) return;
  Rcuxx::Rcu::PrintHelp(std::cout);
}

//____________________________________________________________________
template <typename T>
void
str2val(const char* str, T& val) 
{
  std::stringstream s(str);
  s >> val;
}

//____________________________________________________________________
int
main(int argc, char** argv) 
{
  std::string  node  = argv[1]; 
  unsigned int delay = 1; 
  bool         emul  = false;
  bool         debug = false;
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0],false); return 0;
      case 'H': usage(argv[0],true); return 0;
      case 'U': str2val(argv[++i], delay); break;
      case 'e': emul = true; break;
      case 'd': debug = true; break;
      }
    }
    else {
      node = argv[i];
    }
  }

  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(node.c_str(), false, false);
  if (!rcu) return 1;
  try {
    Rcuxx::Altro        altro(*rcu);
    Rcuxx::RcuACTFEC*   actfec = rcu->ACTFEC();
    Rcuxx::RcuACL*      acl    = rcu->ACL();
    Rcuxx::AltroPMADD*  pmadd  = altro->PMADD();
    Rcuxx::AltroPMDAT*  pmdat  = altro->PMDAT();

    // Get the currently active cards 
    unsigned int ret = 0;
    if ((ret = actfec->Update())) throw ret;
    if ((ret = acl->Update())) throw ret;

    // Print out headers
    std::cout << "# board,altro,channel,timebin,value" << std::endl;

    // Loop over cards 
    for (unsigned int b = 0; b < 32; b++) {
      if (!actfec->IsOn(b)) continue;

      // Loop over ALTROs 
      for (unsigned int a = 0; a < 8; a++) {
	if (!acl->CheckChip(b, a)) continue;

	// Loop over channels
	for (unsigned int c = 0; c < 16; c++) {
	  if (!acl->CheckChannel(b, a, c)) continue;

	  // Set the channel address 
	  altro->SetAddress(b, a, c);

	  // Loop over all time-bins 
	  for (unsigned int t = 0; t < 1024; t++) {
	    // First set the time-bin address 
	    pmadd->SetPMADD(t);
	    if ((ret = pmadd->Commit())) throw ret;

	    // Then update and read the data register 
	    if ((ret = pmdat->Update())) throw ret;
	    unsigned int p = pmdat->PMDTA();
	    
	    // Print a line
	    std::cout << b << ',' 
		      << a << ','
		      << c << ','
		      << t << ',' 
		      << p << std::endl;
	  } // for t ... 
	} // for c 
      } // for a 
    } // for b
  } 
  catch (unsigned int ret) {
    std::cerr << "\nFailed to update <" << ret 
              << ">: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  return 0;
}
// 
// EOF
//
