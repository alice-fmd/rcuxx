#include <rcuxx/RcuLayout.h>
#include <sstream>
#include <iostream>

int 
main(int argc, char** argv) 
{
  int version = 0;
  if (argc > 1) {
    std::stringstream s(argv[1]);
    s >> version;
  }
  if (version < 0 || version > Rcuxx::RcuLayout::k191114) {
    std::cerr << "Invalid version: " << version 
	      << " (must be in the range [0," << Rcuxx::RcuLayout::k191114
	      << "])" << std::endl;
    return 1;
  }
  Rcuxx::RcuLayout l(version);
  l.Print();
  return 0;
}
