//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcuxx/Altro.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/bc/BcCommand.h>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
  
// 

void usage(const char* progname, bool all) 
{
  std::cout << "Usage: " << progname << " [OPTIONS] NODE\n\n" 
	    << "\t-h\t\tThis help\n" 
	    << "\t-H\t\tLong help\n"
	    << "\t-e\t\tEmulate mode (no hardware)\n" 
	    << "\t-d\t\tEnable debug output\n" 
	    << std::endl;
  if (!all) return;
  Rcuxx::Rcu::PrintHelp(std::cout);
}

int main(int argc, char** argv)
{
  std::string node("echo:?fwvers=0x010109");
  bool        emulate = false ,debug = false;
  bool        fmd     = false;
  
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0],false); return 0;
      case 'H': usage(argv[0],true); return 0;
      case 'e': emulate = true; break;
      case 'd': debug = true; break;
      case 'f': fmd = true; break;
      case 't': fmd = false; break;
      }
    }
    else {
      node = argv[i];
    }
  }

  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(node.c_str(), emulate, debug);  
  if (!rcu) return 1;
  if (debug) rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
  try {
    Rcuxx::Altro   altro(*rcu);
    Rcuxx::Bc*     bc  = (fmd ? new Rcuxx::Fmd(*rcu) : new Rcuxx::Bc(*rcu));
    Rcuxx::RcuACL* acl = rcu->ACL();

    unsigned short  boards[]   = { 0x0, 0x1, 0x10, 0x11, 0xFF };
    unsigned short  chips[]    = { 0x0, 0x1, 0x2, 0xFF };
    unsigned short  channels[] = { 0x0, 0x1, 0x2, 0x3, 0x4, 0xFF };
    unsigned short* pboard     = boards;
    
    while (*pboard < 0xFF) { 
      unsigned short* pchip      = chips;
      while (*pchip < 0xFF) { 
	unsigned short* pchannel   = channels;
	while (*pchannel < 0xFF) { 
	  acl->EnableChannel(*pboard, *pchip, *pchannel);
	  pchannel++;
	  if (*pboard < 0x10) break;
	}
	pchip++;
      }
      pboard++;
    }
    acl->Print();
    acl->Optimize();
    acl->Print();
  }
  catch (unsigned int ret) {
    std::cerr << "Failed to update <" << ret 
	      << ">: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  return 0;
}

//____________________________________________________________________
//
// EOF
//
