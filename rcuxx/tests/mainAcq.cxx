//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
# include <rcuxx/Rcu.h>
# include <rcuxx/Altro.h>
# include <rcuxx/Bc.h>
# include <rcuxx/Acq.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuTRCFG1.h>
#ifdef HAVE_CONFIG_H
# include "config.h"
#else 
# ifndef VERSION
#  define VERSION "?.?"
# endif
#endif
#include "Options.h"
#include <stdexcept>

int
main(int argc, char** argv)
{
  Option<bool>        hOpt('h', "help",          "\tThis help", false, false); 
  Option<bool>        vOpt('v', "version",       "\tShow version",false,false);
  Option<long>        nOpt('n', "events",        "\tNumber of events", 10);
  Option<std::string> dOpt('d', "debug",         "\tTurn on debug messages");
  Option<unsigned>    rOpt('r', "run",           "\tRun number\t");
  Option<std::string> tOpt('t', "trigger",       "Trigger\t\t","External");
  Option<unsigned>    sOpt('s', "samples",       "Timebins\t", 255);
  Option<bool>        eOpt('e', "emulation",     "\tEmulation",false,false);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(nOpt);
  cl.Add(dOpt);
  cl.Add(tOpt);
  cl.Add(sOpt);
  cl.Add(eOpt);
  cl.Add(rOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "fmddaq version " << VERSION << std::endl;
    return 0;
  }
  std::string device = (cl.Remain().size() > 0 ? 
			cl.Remain()[0] : "/dev/altro0");
  Rcuxx::Acq::Trigger_t mode = (tOpt->find("software") != std::string::npos ? 
				Rcuxx::Acq::kSoftwareTrigger : 
				Rcuxx::Acq::kExternalTrigger);
  // Rcuxx::DebugGuard::fDebug = level;
  Rcuxx::Acq::InstallSignalHandler();
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), eOpt.IsSet());
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    if (dOpt->find("rcuxx")!=std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
    if (dOpt->find("backend")!=std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kBackend,1);
    Rcuxx::Altro altro(*rcu);
    Rcuxx::Bc    bc(*rcu);
    Rcuxx::Acq   acq(*rcu);
    altro.SetDebug(dOpt->find("altro") != std::string::npos);
    bc.SetDebug(dOpt->find("bc") != std::string::npos);
    acq.SetDebug(dOpt->find("acq") != std::string::npos);
    
    rcu->ACTFEC()->SetValue(0x1);
    unsigned int imem[] = { 0x64000a, 0x700000, 0x390000 };
    imem[1]          += sOpt;
    rcu->IMEM()->Set(0, 3, imem);
    unsigned int acl[] = { 0x3, 0 };
    rcu->ACL()->EnableChannel(0, 0, 0);
    
    rcu->TRCFG1()->SetPop(true);
    rcu->TRCFG1()->SetBMD(Rcuxx::RcuTRCFG1::k4Buffers);
    rcu->TRCFG1()->SetMode(Rcuxx::RcuTRCFG1::kDerivedL2);
    rcu->TRCFG1()->SetTwv(4*(sOpt+1));

    unsigned int ret;
    acq.Setup(rOpt, nOpt, mode);
    if ((ret = acq.Run())) throw ret;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  
  return 0;
}
//____________________________________________________________________
//
// EOF
// 
