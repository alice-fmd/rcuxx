//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    testfile.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:48 2006
    @brief   Program to interactively execute instructions in the RCU
    IMEM 
*/
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif
#ifndef RCUXX_BC_H
# include <rcuxx/Bc.h>
#endif
#include <rcuxx/rcu/RcuTRCFG1.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/bc/BcCSR2.h>
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <fstream>

/** Test program 
    @return 0 on success */
int
main()
{
  // std::string device = "pipe:out:";
  std::string device ="pipe:";
  

  // Rcuxx::DebugGuard::fDebug = rcuxxdebug;
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), false);
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    Rcuxx::Altro altro(*rcu);
    Rcuxx::Bc    bc(*rcu);

    std::vector<unsigned int> imem;
    imem.push_back(0x64000a);
    imem.push_back(0x7003a0);
    imem.push_back(0x660011);
    imem.push_back(0x70037f);
    imem.push_back(0x390000);

    // Write to IMEM and execute it 
    unsigned int offset = 0;
    unsigned int ret = 0;
    rcu->IMEM()->Set(0, imem.size(), &(imem[0]));
    // if ((ret = rcu->IMEM()->Commit())) throw ret;
    // if ((ret = rcu->IMEM()->Exec(offset))) throw ret;
    // if ((ret = rcu->IMEM()->Update())) throw ret;

    // Write to TRCFG
    Rcuxx::RcuTRCFG1* trcfg = rcu->TRCFG1();
    if ((ret = trcfg->Update())) throw ret;
    std::cerr << trcfg->Twv() << std::endl;
    trcfg->SetTwv(4096);
    trcfg->SetPop(true);
    trcfg->SetMode(Rcuxx::RcuTRCFG1::kDerivedL2);
    if ((ret = trcfg->Commit())) throw ret;
    if ((ret = trcfg->Update())) throw ret;
    std::cerr << trcfg->Twv() << std::endl;
    // trcfg->Print();
    
    Rcuxx::BcCSR2* csr2 = bc.CSR2();
    csr2->Update();
    
    
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  
  return 0;
}

// EOF

