//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif
#ifndef RCUXX_BC_H
# include <rcuxx/Bc.h>
#endif
#include <rcuxx/rcu/RcuIMEM.h>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <string>

void usage(const char* progname) 
{
  std::cout << "Usage: " << progname << " [OPTIONS] NODE\n\n" 
	    << "\t-h\t\tThis help\n" 
	    << "\t-e\t\tEmulate mode (no hardware)\n" 
	    << "\t-d\t\tEnable debug output\n" 
	    << std::endl;
  Rcuxx::Rcu::PrintHelp(std::cout);
}

int main(int argc, char** argv)
{
  std::string node("u2f:/dev/altro0");
  bool        emulate = false ,debug = false;
  
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 'e': emulate = true; break;
      case 'd': debug = true; break;
      }
    }
    else {
      node = argv[i];
    }
  }

  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(node.c_str(), emulate, debug);  
  if (!rcu) return 1;
  if (debug) rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
  if (debug) rcu->SetDebug(Rcuxx::Rcu::kBackend, 1);
  try {
    // Rcuxx::Altro altro(*rcu);
    unsigned int ret;
    Rcuxx::RcuIMEM* imem = rcu->IMEM();
    if (!imem) throw std::runtime_error("No IMEM!");
    std::cout << "BEFORE UPDATE" << std::endl;
    imem->Print();
    if ((ret = imem->Update())) throw ret;
    std::cout << "AFTER UPDATE" << std::endl;
    imem->Print();
    

    /* 
    Rcuxx::RcuTRCFG1* trcfg = rcu->TRCFG1();
    std::cout << "BEFORE UPDATE" << std::endl;
    trcfg->Print();
    trcfg->Clear();
    if ((ret = trcfg->Update())) throw ret;
    std::cout << "AFTER UPDATE" << std::endl;
    trcfg->Print();
    trcfg->SetTwv(4096);
    trcfg->SetPop(true);
    trcfg->SetMode(Rcuxx::RcuTRCFG1::kDerivedL2);
    if ((ret = trcfg->Commit())) throw ret;
    std::cout << "AFTER COMMIT" << std::endl;
    if ((ret = trcfg->Update())) throw ret;
    trcfg->Print();
    */
  }
  catch (unsigned int ret) {
    std::cerr << "Failed to update <" << ret 
	      << ">: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

//____________________________________________________________________
//
// EOF
//
