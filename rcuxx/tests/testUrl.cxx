#include <iostream>
#include <iomanip>
#include "Url.h"

typedef Rcuxx::Url::StringVector StringVector;
typedef Rcuxx::Url::OptionMap OptionMap;

void
testUrl(const std::string& s)
{
  Rcuxx::Url url(s);
  std::cout << "Original: "    << s              << "\n" 
	    << std::left
	    << "  Scheme:  "   << url.Scheme()   << "\n" 
	    << "  User:    "   << url.User()     << "\n" 
	    << "  Password:"   << url.Password() << "\n" 
	    << "  Host:    "   << url.Host()     << "\n" 
	    << "  Port:    "   << url.Port()     << "\n"
	    << "  Path:    "   << url.Path()     << "\n" 
	    << "  Query:   "   << url.Query()    << "\n"  
	    << "  Anchor:  "   << url.Anchor()   << "\n"
	    << " Options:"     << std::endl;
  const StringVector& v = url.Options();
  for (StringVector::const_iterator i = v.begin(); i != v.end(); ++i) 
    std::cout << "  " << *i << std::endl;

  std::cout << " Map of options:" << std::endl;
  const OptionMap& m = url.Map();
  for (OptionMap::const_iterator i = m.begin(); i != m.end(); ++i) 
    std::cout << "  " << std::setw(32) << i->first << ": " 
	      << i->second << std::endl;
  std::cout << std::right << std::flush;
}

  
void
usage(const std::string& prog)
{
  std::cout << "Usage: " << prog << " [-i] TEST ..." << std::endl;
}

int
main(int argc, char** argv) 
{
  StringVector tests;
  bool interactive = false;

  for (size_t i = 1; i < argc; i++) {
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'h': usage(argv[0]); return 1; break;
      case 'i': interactive = true; break;
      }
    }
    else 
      tests.push_back(argv[i]);
  }
  if (tests.empty() && !interactive) {
    tests.push_back("usb:/dev/altro0");
    tests.push_back("fee://localhost/FMD-FEE_0_0_0");
    tests.push_back("fed://localhost/ztt_dcsfed_server:FMD-FEE_0_0_0");
    tests.push_back("fed://localhost/ztt_dcsfed_server?FMD-FEE_0_0_0");
    tests.push_back("pipe:/dev/stdin:/dev/stdout");
    tests.push_back("pipe:/dev/stdin");
    tests.push_back("coder:");
    tests.push_back("ddl:0.0");
    tests.push_back("ddl:0");
    tests.push_back("rorc://4:3002:0");
  }

  for (StringVector::const_iterator i = tests.begin(); i != tests.end(); ++i) 
    testUrl(*i);

  while (interactive) {
    std::cout << "Please input a URL: " << std::flush;
    std::string u;
    std::getline(std::cin, u);
    if (u == "q") break;
    testUrl(u);
  }
  return 0;
}

    
