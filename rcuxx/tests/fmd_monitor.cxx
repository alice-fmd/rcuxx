#include <rcuxx/Rcu.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/fmd/FmdT1.h>
#include <rcuxx/fmd/FmdFLASH_I.h>
#include <rcuxx/fmd/FmdAL_DIG_I.h>
#include <rcuxx/fmd/FmdAL_ANA_I.h>
#include <rcuxx/fmd/FmdVA_REC_IP.h>
#include <rcuxx/fmd/FmdT2.h>
#include <rcuxx/fmd/FmdVA_SUP_IP.h>
#include <rcuxx/fmd/FmdVA_REC_IM.h>
#include <rcuxx/fmd/FmdVA_SUP_IM.h>
#include <rcuxx/fmd/FmdGTL_U.h>
#include <rcuxx/fmd/FmdT3.h>
#include <rcuxx/fmd/FmdT1SENS.h>
#include <rcuxx/fmd/FmdT2SENS.h>
#include <rcuxx/fmd/FmdAL_DIG_U.h>
#include <rcuxx/fmd/FmdAL_ANA_U.h>
#include <rcuxx/fmd/FmdT4.h>
#include <rcuxx/fmd/FmdVA_REC_UP.h>
#include <rcuxx/fmd/FmdVA_SUP_UP.h>
#include <rcuxx/fmd/FmdVA_SUP_UM.h>
#include <rcuxx/fmd/FmdVA_REC_UM.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <ctime>
#include <unistd.h>
/*
 * Compile this program with 
 * 
 *   g++ monitor.cxx -o monitor `rcuxx-config --cppflags --ldflags` 
 *
 * Run it as a 
 *
 *   ./monitor <URL> [-U USEC_DELAY]
 *
 * The output of the program is a comma-separated-values table of 
 * all the monitored values (ADC counts and in 'natural' units), 
 * index with a front-end card number and the current time.  This
 * information can easily be imported into a spread-sheet or the
 * like. 
 */
 

//____________________________________________________________________
void usage(const char* progname, bool all) 
{
  std::cout << "Usage: " << progname << " [OPTIONS] NODE\n\n" 
            << "\t-h\t\tThis help\n" 
            << "\t-H\t\tLong help\n"
            << "\t-u USEC\t\tSleep between updates\n"
            << std::endl;
  if (!all) return;
  Rcuxx::Rcu::PrintHelp(std::cout);
}

//____________________________________________________________________
template <typename T>
void
str2val(const char* str, T& val) 
{
  std::stringstream s(str);
  s >> val;
}

//____________________________________________________________________
int
main(int argc, char** argv) 
{
  std::string  node  = argv[1]; 
  unsigned int delay = 1; 
  bool         emul  = false;
  bool         debug = false;
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0],false); return 0;
      case 'H': usage(argv[0],true); return 0;
      case 'U': str2val(argv[++i], delay); break;
      case 'e': emul = true; break;
      case 'd': debug = true; break;
      }
    }
    else {
      node = argv[i];
    }
  }

  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(node.c_str(), false, false);
  if (!rcu) return 1;
  try {
    Rcuxx::Fmd          fmd(*rcu);
    Rcuxx::RcuACTFEC*   actfec = rcu->ACTFEC();
    Rcuxx::BcMonitored* monitors[] = {
      fmd.T1(),			// T1
      fmd.FLASH_I(),		// FLASH_I
      fmd.AL_DIG_I(),		// AL_DIG_I
      fmd.AL_ANA_I(),		// AL_ANA_I
      fmd.VA_REC_IP(),		// VA_REC_IP
      fmd.T2(),			// T2
      fmd.VA_SUP_IP(),		// VA_SUP_IP
      fmd.VA_REC_IM(),		// VA_REC_IM
      fmd.VA_SUP_IM(),		// VA_SUP_IM
      fmd.GTL_U(),		// GTL_U
      fmd.T3(),			// T3
      fmd.T1SENS(),		// T1SENS
      fmd.T2SENS(),		// T2SENS
      fmd.AL_DIG_U(),		// AL_DIG_U
      fmd.AL_ANA_U(),		// AL_ANA_U
      fmd.T4(),			// T4
      fmd.VA_REC_UP(),		// VA_REC_UP
      fmd.VA_SUP_UP(),		// VA_SUP_UP
      fmd.VA_SUP_UM(),		// VA_SUP_UM
      fmd.VA_REC_UM(),		// VA_REC_UM
      0 };
    // Print out column headers. 
    std::cout << "\"Time\",\"FEC\"";
    Rcuxx::BcMonitored** imon = monitors;
    Rcuxx::BcMonitored*  pmon = 0;
    while ((pmon = *imon++)) { 
      std::cout << ",\"" << pmon->Name() << " ADC\""
		<< ",\"" << pmon->Name() << " Value (" << pmon->Unit() << ")\"";
    }
    std::cout << std::endl;
    
    // Loop forever
    do {
      // Get the currently active cards 
      unsigned int ret = 0;
      if ((ret = actfec->Update())) throw ret;
      
      // Loop over all possible the cards
      for (unsigned int i = 0; i < 32; i++) { 
	// Check if card is active 
	if (!actfec->IsOn(i)) continue;

	// Get current time 
	time_t now = time(NULL);
	std::cout << asctime(localtime(&now)) << "," << std::flush;

	// Set current address 
	fmd.SetAddress(i);
	std::cout << i << std::flush;

	// Loop over al monitored values
	imon = monitors;
	while ((pmon = *imon++)) { 
	  if ((ret = pmon->Update())) throw ret;

	  std::cout << "," << pmon->Current() 
		    << "," << pmon->CurrentNatural()
		    << std::flush;
	} // while (*mon)
      } // for (unsigned int i...)
      usleep(delay);
    } while (true); // Loop forever
  } 
  catch (unsigned int ret) {
    std::cerr << "\nFailed to update <" << ret 
              << ">: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  return 0;
}
// 
// EOF
//
