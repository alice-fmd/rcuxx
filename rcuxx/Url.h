// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Url.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Declaration of abstract URL interface 
*/
#ifndef RCUXX_URL_H
#define RCUXX_URL_H
#ifndef __STRING__
# include <string>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __MAP__
# include <map>
#endif

namespace Rcuxx 
{
  /** @class Url 
      @brief Parse and store URL elements. 
      
      A URL is of the form 
      @verbatim
      http://user:pass@example.com:992/animal/bird?species=seagull#wings
      \___/  \________/\_________/\__/\__________/\______________/\____/
        |        |          |       |       |             |          |
      scheme   login      hosts   port    path          query  anchor/fragment
      @endverbatim 
      @ingroup Utils
  */
  class Url 
  {
  public:
    /** type of string vector */
    typedef std::vector<std::string> StringVector;
    /** Type of string,string map */
    typedef std::map<std::string,std::string> OptionMap;

    /** Construtor 
	@param url Url
    */
    Url(const std::string& url) { Parse(url); }
    /** Assignment operator. 
	@param url String to assign from 
	@return  Parsed object. */
    Url& operator=(const std::string& url) { Parse(url); return *this; }
    /** Print to standard out.  */
    void Print() const;
    /** @return The URL as give by user */
    const std::string& Raw() const { return fUrl; }
    /** @return  Port number (-1 if not specified) */
    int Port() const { return fPort; }
    /** @return Host name  */
    const std::string& Host() const { return fHost; }
    /** @return Scheme (e.g., @c http, @c ftp, or @c foo  */
    const std::string& Scheme() const { return fScheme; }
    /** @return Path part */
    const std::string& Path() const { return fPath; }
    /** @return Query part */
    const std::string& Query() const { return fQuery; }
    /** @return Acnhor part */
    const std::string& Anchor() const { return fAnchor; }
    /** @return User name part */
    const std::string& User() const { return fUser; }
    /** @return Password part */
    const std::string& Password() const { return fPassword; }
    /** @return list of options */
    const StringVector& Options() const;
    /** @return list of options */
    const OptionMap& Map() const;
    /** @return list of options */
    const std::string& Option(const std::string& opt) const;

    /** @param p  Port number (-1 if not specified) */
    void SetPort(int  p) { fPort = p; }
    /** @param h Host name  */
    void SetHost(const std::string& h) { fHost = h; }
    /** @param s Scheme (e.g., @c http, @c ftp, or @c foo  */
    void SetScheme(const std::string& s) { fScheme = s; }
    /** @param p Path part */
    void SetPath(const std::string& p) { fPath = p; }
    /** @param q Query part */
    void SetQuery(const std::string& q) { fQuery = q; }
    /** @param a Anchor part */
    void SetAnchor(const std::string& a) { fAnchor = a; }
    /** @param u User name part */
    void SetUser(const std::string& u) { fUser = u; }
    /** @param p Password part */
    void SetPassword(const std::string& p) { fPassword = p; }
  protected:
    /** Parse the string @a url, and store parts 
	@param url String to parse  */
    void Parse(const std::string& url);
    /** Original url cached. */
    std::string fUrl;
    /** Port number */
    int         fPort;
    /** Host name */
    std::string fHost;
    /** Scheme */
    std::string fScheme;
    /** Path */
    std::string fPath;
    /** Query */
    std::string fQuery;
    /** Anchor */
    std::string fAnchor;
    /** User */
    std::string fUser;
    /** Password */
    std::string fPassword;
    /** Options */ 
    mutable StringVector fOptions;
    /** Options */ 
    mutable OptionMap fMap;
  };
}

#endif
//
// EOF
//


  
      
    
    
      
