// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file   
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDCOMMAND_H
#define RCUXX_FMDCOMMAND_H
#ifndef RCUXX_BCCOMMAND_H
# include <rcuxx/bc/BcCommand.h>
#endif
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuRMEM.h>

namespace Rcuxx 
{  
  /** @defgroup fmdxx FMD Interace classes 
   */
  //====================================================================
  /** @struct FmdCommand rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Command
   *  @ingroup fmdxx
   */
  struct FmdCommand : public BcCommand
  {
    /** Register sub-address*/
    unsigned int  fSub;
    /** Constructor 
	@param name Name of commad 
	@param title Help string 
	@param cmd Command code
	@param rcu Reference to RCU interface 
	@param bcast Whether to allow broadcast  */
    FmdCommand(const char* name, const char* title, 
	       unsigned int cmd, Rcu& rcu, bool bcast=true)
      : BcCommand(name, title, add_fmdd_cmd, rcu, bcast), 
	fReg(rcu, cmd, bcast)
    { 
#ifndef NEW_ADD
      fCmd += (0xa << 5); 
#endif
    }
    /** Write to instruction memory, code to write (or execute) this
	command */ 
    unsigned int WriteInstructions() { return fReg.WriteInstructions(); }
    /** Commit to IMEM, and execute */
    unsigned int Commit() { return fReg.Commit();  }
    /** Execute via I2C bus */
    unsigned int I2CCommit() { return fReg.Commit(); }
    /** Whether to use the I2C bus or not */
    void SetUseI2C(bool use=true) { fReg.SetUseI2C(use); }
    /** Whether we use the I2C bus or not */
    bool UseI2C() const { return fReg.UseI2C(); }
    /** Set the target address */
    void SetAddress(unsigned int board, unsigned int =0, unsigned int =0)
    {
      fReg.SetAddress(board, 0, 0);
    }
    /** Write (execute) in broadcast */
    void SetBroadcast() { fReg.SetBroadcast(); }
  protected:
    /** Dummy implementation of the FMDD command register */
    struct CommandRegister : public BcRegister 
    {
      /** constructor 
	  @param rcu Rcu interface to use 
	  @param cmd Command bit number 
	  @param bcast Whether we can broadcast */
      CommandRegister(Rcu& rcu, unsigned int cmd, bool bcast) 
	: BcRegister("COMMAND", "Commands", 
		     add_fmdd_cmd, rcu, false, true, false, bcast), 
	  fCmd(1 << cmd)
      {}
      /** Set the command data */
      unsigned int Encode() const { return fCmd; }
      /** Encoded command bit */
      const unsigned int fCmd;
    };
    /** The dummy command register */
    CommandRegister fReg;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
