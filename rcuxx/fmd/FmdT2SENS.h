// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDT2SENS_H
#define RCUXX_FMDT2SENS_H
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/bc/BcThreshold.h>
#include <rcuxx/fmd/FmdConversions.h>
#include <rcuxx/fmd/FmdConstants.h>

namespace Rcuxx 
{  
  //__________________________________________________________________
  /** @struct FmdT2SENS Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to T2SENS monitor register */
  struct FmdT2SENS : public BcMonitored
  {
    /** Constructor 
	@param rcu Reference to RCU interface 
	@param csr3 Reference to status register interface
	@param start Reference to start command interface */
    FmdT2SENS(Rcu& rcu, BcCSR3& csr3, AltroCommand& start)
      : BcMonitored("T2SENS", "Temperature Sensor 2", add_t2sens, rcu, csr3, start) 
    {}
    /** Get threshold value in natural units 
	@return The threshold value */
    float Convert2Natural(unsigned int v) const { return adc2TSense(v); }
    float Centigrades() const { return CurrentNatural(); }
    /** Get the unit name */
    const char* Unit() const { return "C"; }
  };
  //__________________________________________________________________
  /** @struct FmdT2SENS_TH Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to T2SENS_TH monitor register */
  struct FmdT2SENS_TH : public BcThreshold
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdT2SENS_TH(Rcu& rcu)
      : BcThreshold("T2SENS_TH", "Temperature Sensor 2 Threshold", add_t2sens_th, rcu) 
    {}
    /** Set threshold in natural units 
	@param x Threshold */
    unsigned int Convert2ADC(float v) const { return tSense2Adc(v); }
    void SetCentigrade(float v) { SetThreshold(v); }
    float Convert2Natural(unsigned int v) const { return adc2TSense(v); }
    /** Get the unit name */
    const char* Unit() const { return "C"; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
