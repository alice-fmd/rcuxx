// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDCALITER_H
#define RCUXX_FMDCALITER_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdCalIter rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Base class for interfaces to FMD clock register 
   *  @ingroup fmdxx
   */
  struct FmdCalIter : public FmdRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdCalIter(Rcu& rcu)
      : FmdRegister("CAL_ITER", "Calibration iterations", add_cal_iter, rcu, 
		    false, true)
    {}
    /** Decode read value */
    void Decode(unsigned int data) { fValue = data & 0xffff;   }
    /** Encode set value */
    unsigned int Encode() const { return (fValue & 0xffff); }
    /** @return division factor */
    unsigned int Value() const { return fValue; }
    /** Set value factor */
    void SetValue(unsigned int val) { fValue = val; }
  protected:
    /** Number of iterations */ 
    unsigned int fValue;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
