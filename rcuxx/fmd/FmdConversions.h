// -*- mode: C++ -*-
#ifndef RCUXX_FMDCONVERSIONS_H
# define RCUXX_FMDCONVERSIONS_H

namespace Rcuxx 
{
  inline float adc2T(unsigned int v)
  {
    return 0.25 * v - (v >= 512 ? 256 : 0);
  }
  inline unsigned int t2Adc(float v) 
  {
    return unsigned(4 * v);
  }
  inline float adc2TSense(unsigned v, unsigned o)
  {
    return (100 *  0.442401 * v /
	    (0.3851 * (2.2 * o - 0.221301 * v)));
  }
  inline float adc2TSense(unsigned v) 
  {
    return adc2TSense(v, 512);
  }
  inline unsigned int tSense2Adc(float t) 
  {
    return unsigned(t / 
		    (100 * 0.442401) * 0.3851 * (2.2 * 512 - 0.221301 * 512));
  }
  inline float adc2V(unsigned int v) 
  {
    return float(5 * v) / 1023; 
  }
  inline unsigned int v2Adc(float v) 
  {
    return unsigned(1023 * v / 5);
  }
  inline float adc2A(unsigned int v)
  {
    return float(5.25 * v) / 1023; 
  }
  inline unsigned int a2Adc(float v)
  {
    return unsigned(1023 * v / 5.25);
  }
  inline float adc2MV(unsigned int v) 
  {
    if (v >= 512) return float(5 * (v-512)) / 1023;
    return float(-5 * (512-v)) / 1023;
  }
  inline unsigned int mv2Adc(float v) 
  {
    return unsigned(1023 * v / 5) + 512;
  }
}
#endif
//
// EOF
//

