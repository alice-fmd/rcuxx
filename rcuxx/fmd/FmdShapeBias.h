// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDSHAPEBIAS_H
#define RCUXX_FMDSHAPEBIAS_H
#include <rcuxx/fmd/FmdTopBottom.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct FmdShapeBias rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Shape Bias
   *  @ingroup fmdxx
   */
  struct FmdShapeBias : public FmdTopBottom
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdShapeBias(Rcu& rcu)
      : FmdTopBottom("SHA_BIAS", "Shape bias", add_shape_bias, rcu)
    {}

  };

}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
