// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDRANGE_H
#define RCUXX_FMDRANGE_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdRange rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Range
   *  @ingroup fmdxx
   */
  struct FmdRange : public FmdRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdRange(Rcu& rcu)
      : FmdRegister("RANGE", "Strip range", add_strips, rcu, false, true),
	fMin(0), fMax(0)
    {}
    /** Decode read data */
    void Decode(unsigned int data) 
    { 
      fMax = (data >> 8) & 0xff; 
      fMin = data & 0xff; 
    }
    /** Encode read data */
    unsigned int Encode() const 
    { 
      return (((fMax & 0xff) << 8) + (fMin & 0xff)); 
    }
    /** @return minimum strip number */
    unsigned int Min() const { return fMin; }
    /** Set minimum strip number */
    void SetMin(unsigned int val){ fMin = val; }
    /** @return maximum strip number */
    unsigned int Max() const { return fMax; }
    /** Set maximum strip number */
    void SetMax(unsigned int val){ fMax = val; }
    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tRange:\t\t\t\t" << fMin << " - " << fMax << std::endl;
    }
  protected:
    /** Minimum strip number */
    unsigned int fMin;
    /** Maximum strip number */
    unsigned int fMax;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
