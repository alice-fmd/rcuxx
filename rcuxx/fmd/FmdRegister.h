// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDREGISTER_H
#define RCUXX_FMDREGISTER_H
#include <rcuxx/bc/BcRegister.h>
#include <rcuxx/fmd/FmdConstants.h>

namespace Rcuxx 
{  
  struct RcuIMEM;
  struct RcuRMEM;
  
  //====================================================================
  /** @struct FmdRegister rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Register
   *  @ingroup fmdxx
   */
  struct FmdRegister : public BcRegister 
  {
    /** Constructor
	@param name Name or register 
	@param instr Instruction code
	@param rcu Reference to RCU interface 
	@param clear Whether to enable a clear command 
	@param submit Whether to enable a submit command 
	@param bcast Whether to allow broadcast */
    FmdRegister(const char* name, const char* title, unsigned int instr, 
		Rcu& rcu, bool clear, bool submit, 
		bool bcast=true)
      : BcRegister(name, title, instr, rcu, clear, submit, true, bcast)
    {}

    // virtual unsigned int Read(unsigned int& val);
    // virtual unsigned int Write(unsigned int val);
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
