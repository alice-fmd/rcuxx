// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDTOPBOTTOM_H
#define RCUXX_FMDTOPBOTTOM_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //====================================================================
  /** @struct FmdTopBottom rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Base class for interfaces to FMD register with top/bottom
   *  partition 
   *  @ingroup fmdxx
   */
  struct FmdTopBottom : public FmdRegister 
  {
    /** Constructor 
	@param name Register name 
	@param addr Register address
	@param rcu Reference to RCU interface */
    FmdTopBottom(const char* name, const char* title, unsigned int addr, 
		 Rcu& rcu)
      : FmdRegister(name, title, addr, rcu, false, true), 
	fTop(0), fBottom(0)
    {}

    /** Decode read data */ 
    void Decode(unsigned int data) 
    { 
      fTop = data & 0xff; 
      fBottom = (data >> 8) & 0xff; 
    }
    /** Encode set data */ 
    unsigned int Encode() const 
    { 
      return ((fTop & 0xff) + ((fBottom & 0xff) << 8)); 
    }
    /** @return value of top 4 chips */
    virtual unsigned int Top() const { return fTop; }
    /** @return value of bottom 4 chips */
    virtual unsigned int Bottom() const { return fBottom; }
    /** Set top value */ 
    virtual void SetTop(unsigned int val) { fTop = val; }
    /** Set bottom value */ 
    virtual void SetBottom(unsigned int val) { fBottom = val; }
    /** Set both values 
	@param top Top value, if @a bot is negative, also bottom value
	@param bot If not negative, bottom value, otherwise use @a top */
    void Set(unsigned int top, int bot=-1)
    { 
      fTop    = top; 
      fBottom = (bot < 0 ? fTop : bot);
    }

    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tTop:\t\t\t\t" << fTop << "\n" 
		<< "\tBottom:\t\t\t\t" << fBottom << std::endl; 
    }

  protected:
    /** Top value */
    unsigned int fTop;
    /** Bottom value */
    unsigned int fBottom;
  };    
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
