// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDT4_H
#define RCUXX_FMDT4_H
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/bc/BcThreshold.h>
#include <rcuxx/fmd/FmdConversions.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //__________________________________________________________________
  /** @struct FmdT4 Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to T4 monitor register */
  struct FmdT4 : public BcMonitored
  {
    /** Constructor 
	@param rcu Reference to RCU interface 
	@param csr3 Reference to status register interface
	@param start Reference to start command interface */
    FmdT4(Rcu& rcu, BcCSR3& csr3, AltroCommand& start)
      : BcMonitored("T4", "Temperature 4", add_t4, rcu, csr3, start) 
    {}
    /** Get threshold value in natural units 
        @return The threshold value */
    float Convert2Natural(unsigned int v) const { return adc2T(v); }
    float Centigrades() const { return CurrentNatural(); }
    /** Get the unit name */
    const char* Unit() const { return "C"; }
  };
  //__________________________________________________________________
  /** @struct FmdT4_TH Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to T4_TH monitor register */
  struct FmdT4_TH : public BcThreshold
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdT4_TH(Rcu& rcu)
      : BcThreshold("T4_TH", "Temperature 4 Threshold", add_t4_th, rcu) 
    {}
    /** Set threshold in natural units 
	@param x Threshold */
    unsigned int Convert2ADC(float v) const { return t2Adc(v); }
    void SetCentigrade(float v) { SetThreshold(v); }
    float Convert2Natural(unsigned int v) const { return adc2T(v); }
    /** Get the unit name */
    const char* Unit() const { return "C"; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
