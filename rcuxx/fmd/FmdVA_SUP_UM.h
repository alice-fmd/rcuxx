// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDVA_SUP_UM_H
#define RCUXX_FMDVA_SUP_UM_H
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/bc/BcThreshold.h>
#include <rcuxx/fmd/FmdConversions.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //__________________________________________________________________
  /** @struct FmdVA_SUP_UM Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to VA_SUP_UM monitor register */
  struct FmdVA_SUP_UM : public BcMonitored
  {
    /** Constructor 
	@param rcu Reference to RCU interface 
	@param csr3 Reference to status register interface
	@param start Reference to start command interface */
    FmdVA_SUP_UM(Rcu& rcu,BcCSR3& csr3,AltroCommand& start)
      : BcMonitored("VA_SUP_UM", "VA Negative Supply Voltage", add_va_sup_um, 
		    rcu, csr3, start) 
    {}
    /** Get threshold value in natural units 
	@return The threshold value */
    float Convert2Natural(unsigned int v) const { return adc2MV(v); }
    float MiliVolts() const { return CurrentNatural(); }
    /** Get the unit name */
    const char* Unit() const { return "mV"; }
  };
  //__________________________________________________________________
  /** @struct FmdVA_SUP_UM_TH Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to VA_SUP_UM_TH monitor register */
  struct FmdVA_SUP_UM_TH : public BcThreshold
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdVA_SUP_UM_TH(Rcu& rcu)
      : BcThreshold("VA_SUP_UM_TH", "VA Negative Supply Voltage Threshold", 
		    add_va_sup_um_th, rcu) 
    {}
    /** Set threshold in natural units 
	@param x Threshold */
    unsigned int Convert2ADC(float v) const { return mv2Adc(v); }
    void SetMiliVolts(float v) { SetThreshold(v); }
    /** Get threshold value in natural units 
	@return The threshold value */
    float Convert2Natural(unsigned int v) const { return adc2MV(v); }
    float MiliVolts() const { return ThresholdNatural(); }
    /** Get the unit name */
    const char* Unit() const { return "mV"; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
