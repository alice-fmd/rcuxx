#ifndef RCUXX_FMDCONSTANTS_H
#define RCUXX_FMDCONSTANTS_H
#define NEW_ADD
namespace
{
  bool fDebug = false;
#ifdef NEW_ADD
  enum {
    add_free            = 0x15,         // Version number
    add_fmdd_stat  	= 0x20,		// FMDD status
    add_l0cnt      	= 0x21,		// L0 counters
    add_l1cnt      	= 0x0B,		// L0 counters
    add_hold_wait  	= 0x22,		// FMD: Wait to hold
    add_l1_timeout 	= 0x23,		// FMD: L1 timeout
    add_l2_timeout 	= 0x24,		// FMD: L2 timeout
    add_shift_div  	= 0x25,		// FMD: Shift clk
    add_strips     	= 0x26,		// FMD: Strips
    add_cal_level  	= 0x27,		// FMD: Cal pulse
    add_shape_bias 	= 0x28,		// FMD: Shape bias
    add_vfs        	= 0x29,		// FMD: Shape ref
    add_vfp        	= 0x2A,		// FMD: Preamp ref
    add_sample_div 	= 0x2B,		// FMD: Sample clk
    add_fmdd_cmd   	= 0x2C,		// FMD: Commands
    add_t1_th		= 0x01,		// First ADC T           
    add_flash_i_th	= 0x02,		// I  3.3 V              
    add_al_dig_i_th	= 0x03,		// I  2.5 V altro digital
    add_al_ana_i_th	= 0x04,		// I  2.5 V altro analog 
    add_va_rec_ip_th	= 0x05,		// I  2.5 V VA           
    add_t1		= 0x06,		// First ADC T           
    add_flash_i		= 0x07,		// I  3.3 V              
    add_al_dig_i	= 0x08,		// I  2.5 V altro digital
    add_al_ana_i	= 0x09,		// I  2.5 V altro analog 
    add_va_rec_ip	= 0x0A,		// I  2.5 V VA           
    add_t2_th		= 0x2D,		// Second ADC T           
    add_va_sup_ip_th	= 0x2E,		// I  1.5 V VA            
    add_va_rec_im_th	= 0x2F,		// I -2.0 V               
    add_va_sup_im_th	= 0x30,		// I -2.0 V VA            
    add_gtl_u_th	= 0x31,		//    2.5 V Digital driver
    add_t2		= 0x32,		// Second ADC T           
    add_va_sup_ip	= 0x33,		// I  1.5 V VA            
    add_va_rec_im	= 0x34,		// I -2.0 V               
    add_va_sup_im	= 0x35,		// I -2.0 V VA            
    add_gtl_u		= 0x36,		//    2.5 V Digital driver
    add_t3_th		= 0x37,		// Third ADC T             
    add_t1sens_th	= 0x38,		// Temperature sens. 1     
    add_t2sens_th	= 0x39,		// Temperature sens. 2     
    add_al_dig_u_th	= 0x3A,		// U  2.5 altro digital (m)
    add_al_ana_u_th	= 0x3B,		// U  2.5 altro analog (m) 
    add_t3		= 0x3C,		// Third ADC T             
    add_t1sens		= 0x3D,		// Temperature sens. 1     
    add_t2sens		= 0x3E,		// Temperature sens. 2     
    add_al_dig_u	= 0x3F,		// U  2.5 altro digital (m)
    add_al_ana_u	= 0x40,		// U  2.5 altro analog (m) 
    add_t4_th		= 0x41,		// Forth ADC T  
    add_va_rec_up_th	= 0x42,		// U  2.5 VA (m)
    add_va_sup_up_th	= 0x43,		// U  1.5 VA (m)
    add_va_sup_um_th	= 0x44,		// U -2.0 VA (m)
    add_va_rec_um_th	= 0x45,		// U -2.0 (m)   
    add_t4		= 0x46,		// Forth ADC T  
    add_va_rec_up	= 0x47,		// U  2.5 VA (m)
    add_va_sup_up	= 0x48,		// U  1.5 VA (m)
    add_va_sup_um	= 0x49,		// U -2.0 VA (m)
    add_va_rec_um	= 0x4A,		// U -2.0 (m)   
    add_cal_iter        = 0x4B,	        // Calibration events
    add_meb             = 0x4C          // MEB
  };
#else
  enum {
    add_fmdd_stat  = 0xD;	// FMDD status
    add_l0cnt      = 0xC,	// L0 counters
    add_l1cnt      = 0xD,	// L0 counters
    add_hold_wait  = 0x6,	// FMD: Wait to hold
    add_l1_timeout = 0x7,	// FMD: L1 timeout
    add_l2_timeout = 0x8,	// FMD: L2 timeout
    add_shift_div  = 0x4,	// FMD: Shift clk
    add_strips     = 0x9,	// FMD: Strips
    add_cal_level  = 0x3,	// FMD: Cal pulse
    add_shape_bias = 0x0,	// FMD: Shape bias
    add_vfs        = 0x2,	// FMD: Shape ref
    add_vfp        = 0x1,	// FMD: Preamp ref
    add_sample_div = 0x5,	// FMD: Sample clk
    add_fmdd_cmd   = 0xF,	// FMD: Commands
    add_t1_th		= 0x0, // 0x01,		// First ADC T           
    add_flash_i_th	= 0x0, // 0x02,		// I  3.3 V              
    add_al_dig_i_th	= 0x0, // 0x03,		// I  2.5 V altro digital
    add_al_ana_i_th	= 0x0, // 0x04,		// I  2.5 V altro analog 
    add_va_rec_ip_th	= 0x0, // 0x05,		// I  2.5 V VA           
    add_t1		= 0x0, // 0x06,		// First ADC T           
    add_flash_i		= 0x0, // 0x07,		// I  3.3 V              
    add_al_dig_i	= 0x0, // 0x08,		// I  2.5 V altro digital
    add_al_ana_i	= 0x0, // 0x09,		// I  2.5 V altro analog 
    add_va_rec_ip	= 0x0, // 0x0A,		// I  2.5 V VA           
    add_t2_th		= 0x0, // 0x2D,		// Second ADC T           
    add_va_sup_ip_th	= 0x0, // 0x2E,		// I  1.5 V VA            
    add_va_rec_im_th	= 0x0, // 0x2F,		// I -2.0 V               
    add_va_sup_im_th	= 0x0, // 0x30,		// I -2.0 V VA            
    add_gtl_u_th	= 0x0, // 0x31,		//    2.5 V Digital driver
    add_t2		= 0x0, // 0x32,		// Second ADC T           
    add_va_sup_ip	= 0x0, // 0x33,		// I  1.5 V VA            
    add_va_rec_im	= 0x0, // 0x34,		// I -2.0 V               
    add_va_sup_im	= 0x0, // 0x35,		// I -2.0 V VA            
    add_gtl_u		= 0x0, // 0x36,		//    2.5 V Digital driver
    add_t3_th		= 0x0, // 0x37,		// Third ADC T             
    add_t1sens_th	= 0x0, // 0x38,		// Temperature sens. 1     
    add_t2sens_th	= 0x0, // 0x39,		// Temperature sens. 2     
    add_al_dig_u_th	= 0x0, // 0x3A,		// U  2.5 altro digital (m)
    add_al_ana_u_th	= 0x0, // 0x3B,		// U  2.5 altro analog (m) 
    add_t3		= 0x0, // 0x3C,		// Third ADC T             
    add_t1sens		= 0x0, // 0x3D,		// Temperature sens. 1     
    add_t2sens		= 0x0, // 0x3E,		// Temperature sens. 2     
    add_al_dig_u	= 0x0, // 0x3F,		// U  2.5 altro digital (m)
    add_al_ana_u	= 0x0, // 0x40,		// U  2.5 altro analog (m) 
    add_t4_th		= 0x0, // 0x41,		// Forth ADC T  
    add_va_rec_up_th	= 0x0, // 0x42,		// U  2.5 VA (m)
    add_va_sup_up_th	= 0x0, // 0x43,		// U  1.5 VA (m)
    add_va_sup_um_th	= 0x0, // 0x44,		// U -2.0 VA (m)
    add_va_rec_um_th	= 0x0, // 0x45,		// U -2.0 (m)   
    add_t4		= 0x0, // 0x46,		// Forth ADC T  
    add_va_rec_up	= 0x0, // 0x47,		// U  2.5 VA (m)
    add_va_sup_up	= 0x0, // 0x48,		// U  1.5 VA (m)
    add_va_sup_um	= 0x0, // 0x49,		// U -2.0 VA (m)
    add_va_rec_um	= 0x0, // 0x4A,		// U -2.0 (m)   
    add_cal_iter        = 0x0  // 0x4B	        // Calibration events
  };
#endif
  enum {
    cmd_change_dac = 0x0,	// change dacs
    cmd_trigger    = 0x1,	// Make trigger sequence
    cmd_l0         = 0x2,	// Make L0 trigger
    cmd_reset      = 0x3,	// Reset
    cmd_calib_on   = 0x4,	// Turn on calibration mode
    cmd_calib_off  = 0x5,	// Turn off calibration mode
    cmd_test_on    = 0x6,	// Turn on test mode
    cmd_test_off   = 0x7,	// Turn off test mode
    cmd_cal_man    = 0x8        // Turn on calibration manager
  };  
}
#endif
