// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDPULSER_H
#define RCUXX_FMDPULSER_H
#include <rcuxx/fmd/FmdTopBottom.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdPulser rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Pulser
   *  @ingroup fmdxx
   */
  struct FmdPulser : public FmdTopBottom
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdPulser(Rcu& rcu)
      : FmdTopBottom("PULS", "Pulser", add_cal_level, rcu)
    {}

    /** @return value of top 4 chips */
    unsigned int Value() const { return fTop; }
    /** @return value of bottom 4 chips */
    unsigned int Test() const { return fBottom; }
    /** Set top value */ 
    void SetValue(unsigned int val) { fTop = val; }
    /** Set bottom value */ 
    void SetTest(unsigned int val) { fBottom = val; }
  protected:
    /** @return Top value */
    unsigned int Top() const { return fTop; }
    /** @return Bottom value */
    unsigned int Bottom() const { return fTop; }
    /** @param v Top value */
    void SetTop(unsigned int v)  { fTop = v; }
    /** @param v Bottom value */
    void SetBottom(unsigned int v) { fBottom = v; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
