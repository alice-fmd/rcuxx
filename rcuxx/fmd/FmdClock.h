// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDCLOCK_H
#define RCUXX_FMDCLOCK_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdClock rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Base class for interfaces to FMD clock register 
   *  @ingroup fmdxx
   */
  struct FmdClock : public FmdRegister 
  {
    /** Constructor 
	@param name  Name of register 
	@param title Help string 
	@param addr  Address of register 
	@param rcu  Reference to RCU interface */
    FmdClock(const char* name, const char* title, unsigned int addr, Rcu& rcu)
      : FmdRegister(name, title, addr, rcu, false, true),
	fDivision(0),
	fPhase(0)
    {}
    /** Decode read value */
    void Decode(unsigned int data) 
    { 
      fPhase    = data & 0xff; 
      fDivision = (data >> 8) & 0xff;  
    }
    /** Encode set value */
    unsigned int Encode() const 
    { 
      return ((fPhase & 0xff) + ((fDivision & 0xff) << 8)); 
    }
    /** @return division factor */
    unsigned int Division() const { return fDivision; }
    /** Set division factor */
    void SetDivision(unsigned int val) { fDivision = val; }
    /** @return phase */
    unsigned int Phase() const { return fPhase; }
    /** Set the phase shift */
    void SetPhase(unsigned int val) { fPhase = val; }
    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tDivider:\t\t\t" << fDivision << "\n" 
		<< "\tPhase:\t\t\t\t" << fPhase << std::endl;
    }

  protected:
    /** division factor */
    unsigned int fDivision;
    /**  phase */
    unsigned int fPhase;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
