// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDMEB_H
#define RCUXX_FMDMEB_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdMeb rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Base class for interfaces to FMD clock register 
   *  @ingroup fmdxx
   */
  struct FmdMeb : public FmdRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdMeb(Rcu& rcu)
      : FmdRegister("MEB", "Multi-event buffer", add_meb, rcu, false, true), 
	fMax(4), 
	fCount(4),
	fEnable(true)
    {}
    /** Decode read value */
    void Decode(unsigned int data) 
    { 
      fCount  = (data >> 0) & 0xf;  
      fMax    = (data >> 4) & 0xf; 
      fEnable = (data >> 8) & 0x1; 
    }
    /** Encode set value */
    unsigned int Encode() const 
    { 
      return (((fCount  & 0xf) << 0) | 
	      ((fMax    & 0xf) << 4) |
	      ((fEnable & 0x1) << 8));
    }
    /** @return Enabled bit */
    bool IsEnabled() const { return fEnable; }
    /** @return Counter */
    unsigned int Counter() const { return fCount; }
    /** @return Maximum */
    unsigned int Maximum() const { return fMax; }
    /** Set Enabled bit */
    void SetEnabled(bool use=true) { fEnable = use; }
    /** Set value factor */
    void SetMaximum(unsigned int val) { fMax = val; }
    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tEnabled:\t\t\t" << std::boolalpha << fEnable 
		<< std::noboolalpha << "\n"
		<< "\tCount:\t\t\t" << fCount << "\n" 
		<< "\tMax:\t\t\t\t" << fMax << std::endl;
      
    }
  protected:
    /** Maximum */ 
    unsigned int fMax;
    /** Count */ 
    unsigned int fCount;
    /** Enable */ 
    bool fEnable;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
