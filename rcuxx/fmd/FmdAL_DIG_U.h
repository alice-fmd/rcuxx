// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDAL_DIG_U_H
#define RCUXX_FMDAL_DIG_U_H
#include <rcuxx/bc/BcMonitored.h>
#include <rcuxx/bc/BcThreshold.h>
#include <rcuxx/fmd/FmdConversions.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //__________________________________________________________________
  /** @struct FmdAL_DIG_U Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to AL_DIG_U monitor register */
  struct FmdAL_DIG_U : public BcMonitored
  {
    /** Constructor 
	@param rcu Reference to RCU interface 
	@param csr3 Reference to status register interface
	@param start Reference to start command interface */
    FmdAL_DIG_U(Rcu& rcu,BcCSR3& csr3,AltroCommand& start)
      : BcMonitored("AL_DIG_U", "ALTRO Digital Voltage", add_al_dig_u, 
		    rcu, csr3, start) 
    {}
    /** Get threshold value in natural units 
	@return The threshold value */
    float Convert2Natural(unsigned int v) const { return adc2V(v); }
    float MiliVolts() const { return CurrentNatural(); }
    /** Get the unit name */
    const char* Unit() const { return "mV"; }
  };
  //__________________________________________________________________
  /** @struct FmdAL_DIG_U_TH Fmd.h <rcuxx/Fmd.h>
      @ingroup fmdxx
      @brief Interface to AL_DIG_U_TH monitor register */
  struct FmdAL_DIG_U_TH : public BcThreshold
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdAL_DIG_U_TH(Rcu& rcu)
      : BcThreshold("AL_DIG_U_TH", "ALTRO Digital Voltage Threshold",add_al_dig_u_th, rcu) 
    {}
    /** Set threshold in natural units 
	@param v Threshold */
    unsigned int Convert2ADC(float v) const { return v2Adc(v); }
    void SetMiliVolts(float v) { SetThreshold(v); }
    /** Get threshold value in natural units 
	@param v Threshold
	@return The threshold value */
    float Convert2Natural(unsigned int v) const { return adc2V(v); }
    float MiliVolts() const { return ThresholdNatural(); }
    /** Get the unit name */
    const char* Unit() const { return "mV"; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
