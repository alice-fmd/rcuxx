// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDL0TIMEOUT_H
#define RCUXX_FMDL0TIMEOUT_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>

#define NEW_ADD

namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdL0Timeout rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register L0Timeout
   *  @ingroup fmdxx
   */
  struct FmdL0Timeout : public FmdRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdL0Timeout(Rcu& rcu)
      : FmdRegister("L1_TO", "L1 timeout", add_l1_timeout, rcu, false, true)
    {}
    /** Decode read data */
    void Decode(unsigned int data) { fData = data & 0xffff; }
    /** Encode set data */
    unsigned int Encode() const { return fData & 0xffff; }
    /** @return number of clock cycles for time-out */
    unsigned int Clocks() const { return fData; }
    /** Set number of clock cycles for time-out */
    void SetClocks(unsigned int val) { fData = val; }
    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\t# of 25ns clock cycles:\t\t" << fData << std::endl;
    }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
