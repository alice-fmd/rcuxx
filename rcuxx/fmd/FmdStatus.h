// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDSTATUS_H
#define RCUXX_FMDSTATUS_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdStatus rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Status
   *  @ingroup fmdxx
   */
  struct FmdStatus : public FmdRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdStatus(Rcu& rcu)
      : FmdRegister("STATUS", "Status", add_fmdd_stat, rcu, false, false),
	fCalOn(false),
	fTriggerBusy(false),
	fDacBusy(false), 
	fReadoutBusy(false),
	fBoxBusy(false),
	fIncompleteRo(false),
	fTriggerOverlap(false),
	fTriggerTimeout(false),
	fTestOn(false),
	fCalManOn(false)
    {
    }
    
    /** Decode read valies */
    void Decode(unsigned int data)
    { 
      fCalOn          = data & (1 << 0); 
      fTriggerBusy    = data & (1 << 1);
      fDacBusy        = data & (1 << 2);
      fReadoutBusy    = data & (1 << 3);
      fBoxBusy        = data & (1 << 4);
      fIncompleteRo   = data & (1 << 5);
      fTriggerOverlap = data & (1 << 6);
      fTriggerTimeout = data & (1 << 7); 
      fTestOn         = data & (1 << 8);
      fCalManOn       = data & (1 << 9);
    }

    /** @return @c true if calibration pulse is on */
    bool IsCalOn() const { return fCalOn; };
    /** @return @c true if trigger FSM is busy */
    bool IsTriggerBusy() const { return fTriggerBusy; };
    /** @return @c true if DAC FSM is busy */
    bool IsDacBusy() const { return fDacBusy; };
    /** @return @c true if readout FSM is busy */
    bool IsReadoutBusy() const { return fReadoutBusy; };
    /** @return @c true if trigger box is busy */
    bool IsBoxBusy() const { return fBoxBusy; };
    /** @return @c true if the read-out was incomplete */
    bool IsIncompleteRo() const { return fIncompleteRo; };
    /** @return @c true if there was an overlap of triggers */
    bool IsTriggerOverlap() const { return fTriggerOverlap; };
    /** @return @c true if trigger(s) timed out */
    bool IsTriggerTimeout() const { return fTriggerTimeout; };
    /** @return @c true if test mode is on */
    bool IsTestOn() const { return fTestOn; };
    /** @return @c true if calibration manager mode is on */
    bool IsCalManOn() const { return fCalManOn; };
    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha 
		<< "\tCalibration pulse is on :\t\t" << fCalOn << "\n"
		<< "\tTrigger FSM is busy :\t\t" << fTriggerBusy << "\n"
		<< "\tDAC FSM is busy :\t\t" << fDacBusy << "\n"
		<< "\tReadout FSM is busy :\t\t" << fReadoutBusy << "\n"
		<< "\tTrigger box is busy :\t\t" << fBoxBusy << "\n"
		<< "\tRead-out was incomplete :\t\t" << fIncompleteRo << "\n"
		<< "\tOverlap of triggers :\t\t" << fTriggerOverlap << "\n"
		<< "\tTrigger(s) timed out :\t\t" << fTriggerTimeout << "\n"
		<< "\tTest mode is on :\t\t" << fTestOn << "\n" 
		<< "\tCal. Manager is on:\t\t" << fCalManOn << std::endl;
    }
  protected:
    /** Whether cal pulse is on */
    bool fCalOn;
    /** Whether trigger FSM is busy */
    bool fTriggerBusy;
    /** Whether DAC FSM is busy */
    bool fDacBusy;
    /** Whether readout FSM is busy */
    bool fReadoutBusy;
    /** Whether trigger box is busy */
    bool fBoxBusy;
    /** Whether the read-out was incomplete */
    bool fIncompleteRo;
    /** Whether there was an overlap of triggers */
    bool fTriggerOverlap;
    /** Whether Trigger(s) timed out */
    bool fTriggerTimeout;
    /** Whether Test mode ison */
    bool fTestOn;
    /** Whether Calibration manager mode is on */
    bool fCalManOn;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
