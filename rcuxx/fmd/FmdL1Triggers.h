// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMDL1TRIGGERS_H
#define RCUXX_FMDL1TRIGGERS_H
#include <rcuxx/fmd/FmdRegister.h>
#include <rcuxx/fmd/FmdConstants.h>


namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct FmdL1Triggers rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register L1Triggers
   *  @ingroup fmdxx
   */
  struct FmdL1Triggers : public FmdRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    FmdL1Triggers(Rcu& rcu)
      : FmdRegister("L1CNT", "L1 Triggers", add_l1cnt, rcu, false, false)
    {}
    /** Decode read data */
    void Decode(unsigned int data) { fData = data & 0xfffff; }
    /** @return number of triggers */
    unsigned int Recieved() const { return fData; }
    /** Print contents of register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\t# of L1 triggers recieved:\t\t" << fData << std::endl;
    }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
