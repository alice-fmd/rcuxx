//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Altro.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:18 2006
    @brief   Implementation of ALTRO interface 
*/
#include <rcuxx/Altro.h>
#include "DebugGuard.h"
#include <iostream>
#include <iomanip>
#include "altro/AltroADEVL.h"
#include "altro/AltroBCTHR.h"
#include "altro/AltroCommand.h"
#include "altro/AltroDPCF2.h"
#include "altro/AltroDPCFG.h"
#include "altro/AltroERSTR.h"
#include "altro/AltroKLCoeffs.h"
#include "altro/AltroPMADD.h"
#include "altro/AltroPMDTA.h"
#include "altro/AltroRegister.h"
#include "altro/AltroTRCFG.h"
#include "altro/AltroTRCNT.h"
#include "altro/AltroVFPED.h"
#include "altro/AltroZSTHR.h"

namespace 
{
  bool fDebug;
}

//====================================================================
Rcuxx::Altro::Altro(Rcu& rcu) 
  : fRcu(rcu), 
    fBoardAddress(0), 
    fChipAddress(0), 
    fChannelAddress(0)
{
  // 
  RcuIMEM& imem = *(rcu.IMEM());
  RcuRMEM& rmem = *(rcu.RMEM());

  fWPINC = new AltroCommand("WPINC", "Write pointer++", 0x18, rcu);
  fRPINC = new AltroCommand("RPINC", "Read pointer++",  0x19, rcu);
  fCHRDO = new AltroCommand("CHRDO", "Channel readout (channel specific)", 
			    0x1A, rcu, false);
  fSWTRG = new AltroCommand("SWTRG", "Make trigger",    0x1B, rcu);
  fTRCLR = new AltroCommand("TRCLR", "Clear triggers",  0x1C, rcu);
  fERCLR = new AltroCommand("ERCLR", "Clear errors",    0x1D, rcu);
  fCommands.push_back(fWPINC);
  fCommands.push_back(fRPINC);
  fCommands.push_back(fCHRDO);
  fCommands.push_back(fSWTRG);
  fCommands.push_back(fTRCLR);
  fCommands.push_back(fERCLR);

  fERSTR   = new AltroERSTR(rcu);
  fTRCFG   = new AltroTRCFG(rcu);
  fDPCFG   = new AltroDPCFG(rcu);
  fDPCF2   = new AltroDPCF2(rcu);
  fVFPED   = new AltroVFPED(rcu);
  fPMDTA   = new AltroPMDTA(rcu);
  fPMADD   = new AltroPMADD(rcu);
  fZSTHR   = new AltroZSTHR(rcu);
  fBCTHR   = new AltroBCTHR(rcu);
  fK1      = new AltroKLCoeffs('K', 1, rcu);
  fK2      = new AltroKLCoeffs('K', 2, rcu);
  fK3      = new AltroKLCoeffs('K', 3, rcu);
  fL1      = new AltroKLCoeffs('L', 1, rcu);
  fL2      = new AltroKLCoeffs('L', 2, rcu);
  fL3      = new AltroKLCoeffs('L', 3, rcu);
  fADEVL   = new AltroADEVL(rcu);
  fTRCNT   = new AltroTRCNT(rcu);

  fRegisters.push_back(fERSTR);
  fRegisters.push_back(fTRCFG);
  fRegisters.push_back(fDPCFG);
  fRegisters.push_back(fDPCF2);
  fRegisters.push_back(fVFPED);
  fRegisters.push_back(fPMDTA);
  fRegisters.push_back(fPMADD);
  fRegisters.push_back(fZSTHR);
  fRegisters.push_back(fBCTHR);
  fRegisters.push_back(fK1);
  fRegisters.push_back(fK2);
  fRegisters.push_back(fK3);
  fRegisters.push_back(fL1);
  fRegisters.push_back(fL2);
  fRegisters.push_back(fL3);
  fRegisters.push_back(fADEVL);
  fRegisters.push_back(fTRCNT);

  SetAddress(0);
}

//____________________________________________________________________
void 
Rcuxx::Altro::SetBroadcast() 
{
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetBroadcast();
  }
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetBroadcast();
  }
  fBoardAddress   = -1;
  fChipAddress    = -1;
  fChannelAddress = -1;
}

//____________________________________________________________________
void
Rcuxx::Altro::SetAddress(unsigned int board, unsigned int chip, 
			 unsigned int channel) 
{
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetAddress(board, chip, channel);
  }
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetAddress(board, chip, channel);
  }
  fBoardAddress   = board;
  fChipAddress    = chip;
  fChannelAddress = channel;
}



//____________________________________________________________________
unsigned int 
Rcuxx::Altro::Commit()
{
  unsigned int ret;
  size_t j = 0;
  for (size_t i = 0; i < fRegisters.size(); i++) {
    if (!fRegisters[i]) continue;
    fRegisters[i]->Set();
    if (fRegisters[i]->WriteInstructions()) j += 2;
  }
  ret     = fRcu.IMEM()->Execute();
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Altro::Update()
{
  unsigned int ret;
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    ret = (*i)->Update();
    if (ret) break;
  }
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Altro::Print() const
{
  for (Registers::const_iterator i = fRegisters.begin(); 
       i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->Print();
  }
}

//____________________________________________________________________
void
Rcuxx::Altro::SetDebug(bool debug)
{
  fDebug = debug;
}
  
  
  
//____________________________________________________________________
//
// EOF
//
