// -*- Mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROADEVL_H
#define RCUXX_ALTROADEVL_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroADEVL rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register ADEVL.  The 8bit channel
   *  address can be read from AD[15-8]. The Event Length for a given
   *  channel is coded in the lower 8 bit of the address space.
   *  @note Read only
   *  @ingroup rcuxx_altro
   */
  struct AltroADEVL : public AltroRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroADEVL(Rcu& rcu )
      : AltroRegister("ADEVL", "Chip addres and event length", 0x11, rcu,
		      false, false), fHADD(0), fEVL(0)
    {}
  
    /** Get the values from internal cache after update */
    void Decode(unsigned int data) 
    { 
      fHADD = (data >> 8) & 0xff; 
      fEVL = data & 0xff; 
    }
    /** @return Hardware address */
    unsigned int HADD() const { return fHADD; }
    /** @return Event length */
    unsigned int EVL() const { return fEVL; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tHarware address:\t\t0x" << std::hex << fHADD << "\n" 
		<< "\tEvent length:\t\t\t" << std::dec << fEVL << std::endl;
    }
  protected:
    /** Hardware address */
    unsigned int fHADD;
    /** Event length */
    unsigned int fEVL;
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
