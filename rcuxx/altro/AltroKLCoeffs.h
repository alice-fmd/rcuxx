// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Altro.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROKLCOEFFS_H
#define RCUXX_ALTROKLCOEFFS_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  
  //====================================================================
  /** @struct AltroKLCoeffs rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO K or L registers.  There is a set of 6
   *  16bit registers for each channel independently. The @f$ K_i@f$ 
   *  correspond to the poles of each stage of the tail Cancellation
   *  Filter and the @f$ L_i@f$ are similarly the zeros of the
   *  Filter. A broadcast of the Coefficients will give the same Tail  
   *  Cancellation Filter settings to all the channels and indeed all
   *  the ALTROs.    The parameters @f$ K_i, L_i@f$ should be in the
   *  range @f$ [0,1]@f$, so the floating point value from a given
   *  integer input is given by e.g., @f$ K_1=K_{1i}/65535@f$, or the
   *  integer input needed for a given floating point number is e.g., 
   *  @f$ K_{1i} = K_1 \times 65335@f$
   *  @note This is definend per channel 
   *  @ingroup rcuxx_altro
   */
  struct AltroKLCoeffs : public AltroRegister 
  {
    /** Constructor 
	@param which Which (K or L) 
	@param n Which (1-3)
	@param rcu Reference to RCU interface*/
    AltroKLCoeffs(char which, size_t n, Rcu& rcu);
    /** Get the values from internal cache after update */
    void Decode(unsigned int data) { fValue = data; }
    /** Set the internal cache before commit */
    unsigned int Encode() const { return fValue; }
    /** K or L coefficent */
    unsigned int Value() const { return fValue; }
    /** K or L coefficent */
    void SetValue(unsigned int val) { fValue = val; }
    /** K or L coefficent */
    void SetValue(float val) { fValue = (unsigned int)(val*65535); }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Current value */
    unsigned int fValue;
  };
  //====================================================================
  inline AltroKLCoeffs::AltroKLCoeffs(char which, size_t n, Rcu& rcu)
    : AltroRegister("  ", "Filter parameters", 
		    (which == 'k' || which == 'K' ? 0 : 3) + n - 1,
		    rcu, false, true), 
    fValue(0)
  {
    char i = '\0';
    fName.insert(0, 1, which); 
    switch (n) {
    case 1: i = '1'; break;
    case 2: i = '2'; break;
    case 3: i = '3'; break;
    }
    fName.insert(1, 1, i);
  }
    
  //____________________________________________________________________
  inline void
  AltroKLCoeffs::Print() const 
  {
    AltroRegister::Print();
    std::cout << "\tValue:\t\t\t\t" << fValue << std::endl;
  }
}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
