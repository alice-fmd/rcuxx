// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROVFPED_H
#define RCUXX_ALTROVFPED_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroVFPED rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register VFPED - the variable and
   *  fixed pedestals used in the baseline unit (First baseline
   *  correction unit). If the correct mode is used, the input signal
   *  coming from the ADC will be subtracted to FPD or VPD. Both are
   *  coded in 10bit each. The variable pedestal can only be read, the 
   *  fixed one can be both read and written.
   *  @note This is definend per channel 
   *  @ingroup rcuxx_altro
   */
  struct AltroVFPED : public AltroRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroVFPED(Rcu& rcu)
      : AltroRegister("VFPED", "Pedestals", 0x6, rcu, false, true), 
	fVP(0), fFP(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data) 
    { 
      fVP = (data >> 10) & 0x3ff; 
      fFP = data & 0x3ff; 
    }
    /** Set the internal cache before commit */
    unsigned int Encode() const { return fFP & 0x3ff; }
    /** Compare against value */ 
    bool Compare(unsigned int val) const { return Encode() == (val & 0x3FF); }
    /** Variable pedestal */
    unsigned int VP() const { return fVP; }
    /** Fixed pedestal */
    unsigned int FP() const { return fFP; }
    /** Fixed pedestal */
    void SetFP(unsigned int val) { fFP = val; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tVariable:\t\t\t" << fVP << "\n"
		<< "\tFixed:\t\t\t\t" << fFP << std::endl;
    }
  protected:  
    /** Variable pedestal */
    unsigned int fVP;
    /** Fixed pedestal */
    unsigned int fFP;
  
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
