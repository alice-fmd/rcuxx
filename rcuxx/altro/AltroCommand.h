// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROCOMMAND_H
#define RCUXX_ALTROCOMMAND_H
#include <rcuxx/Command.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuRMEM.h>

namespace Rcuxx 
{
  /** @defgroup rcuxx_altro ALTRO interface classes
      @ingroup rcuxx
   */
  struct AltroRegister;
  struct AltroCommand;
  
  //====================================================================
  /** @struct AltroCommand rcuxx/Altro.h <rcuxx/Altro.h> 
   *  @brief Interface to an ALTRO (bus) command.
   *  Note, that the access is implemented via the RcuIMEM
   *  interface, so this interface, and it's derived classes are
   *  completly general and does not need to be specialised for a
   *  particular back-end. 
   *  @ingroup rcuxx_altro
   */
  struct AltroCommand : public Command
  {
    /** Constructor 
	@param name Name of command 
	@param title Help string 
	@param cmd Command code
	@param rcu Reference to RCU interface 
	@param bcast Whether to allow broadcast 
	@return  */
    AltroCommand(const char* name, const char* title, unsigned int cmd, 
		 Rcu& rcu, bool bcast=true);
    /** Set to broadcast, if allowed. */
    virtual void   SetBroadcast();
    /** Set target address
	@param board   Board number   (0x1f)
	@param chip    Chip number    (0x7)
	@param channel Channel number (0xf) */
    virtual void   SetAddress(unsigned int board, unsigned int chip, 
			      unsigned int channel=0);
    /** Make write instructions and store them in @a imem
	@return @c false if the operation is not allowed in the
	current mode (broadcast). */
    virtual unsigned int WriteInstructions(Rcuxx::RcuIMEM::Cache_t& cache, 
					   size_t& marker);
    /** Make write instructions and store them in @a imem
	@return @c false if the operation is not allowed in the
	current mode (broadcast). */
    virtual unsigned int WriteInstructions();
    /** Commit stored values to hardware 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
  protected:
    /** Reference to RCU */
    Rcu& fRcu;
    /** Address */
    unsigned int fAddress;
    /** Whether to allow broad cast */
    bool fAllowBroadcast;
    /** Data */ 
    unsigned int fData;
  };

  //====================================================================
  inline 
  AltroCommand::AltroCommand(const char* name, const char* title, 
			     unsigned int cmd, Rcu& rcu, bool bcast) 
    : Command(name, title, cmd), 
      fRcu(rcu),
      // fIMEM(imem),
      // fRMEM(rmem),
      fAddress(0), 
      fAllowBroadcast(bcast),
      fData(0) 
  {}
  
  //____________________________________________________________________
  inline void
  AltroCommand::SetBroadcast()
  { 
    if (!fAllowBroadcast) return;
    fAddress = (1 << 18); 
  }
  
  //____________________________________________________________________
  inline void
  AltroCommand::SetAddress(unsigned int board, unsigned int chip, 
			   unsigned int channel) 
  {
    unsigned int addr = (((board & 0x1f) << 7) + ((chip & 0x7) << 4) + 
			 (fAllowBroadcast ? 0 : (channel & 0xf)));
    fAddress = addr << 5;
  }
  //____________________________________________________________________
  inline unsigned int
  AltroCommand::WriteInstructions(Rcuxx::RcuIMEM::Cache_t& cache, 
				  size_t& marker)
  {
    if (!fAllowBroadcast && (fAddress & (1 << 18))) 
      return Rcu::kCannotBroadcast;
    return fRcu.IMEM()->FecCommand(fAddress + fCmd, cache, marker);
  }
  //____________________________________________________________________
  inline unsigned int
  AltroCommand::WriteInstructions()
  {
    return WriteInstructions(fRcu.IMEM()->Data(), fRcu.IMEM()->Marker());
  }
  //____________________________________________________________________
  inline unsigned int
  AltroCommand::Commit()
  {
    // DebugGuard g(fDebug, "AltroCommand::Commit() <%s,0x%x>", 
    // fName.c_str(), fCmd); 
    unsigned int ret = 0;
    if ((ret = WriteInstructions())) return ret;
    if ((ret = fRcu.IMEM()->Execute())) return ret;
    if ((ret = fRcu.RMEM()->FecCommand(fAddress + fCmd, 0))) return ret;
    return ret;
  }
}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
