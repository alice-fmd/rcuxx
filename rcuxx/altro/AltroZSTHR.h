// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROZSTHR_H
#define RCUXX_ALTROZSTHR_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroZSTHR rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register ZSTHR - the zero
   *  suppression threshold.  The Offset (higher 10bit) enables the
   *  signal to sit in a higher baseline so that negative variations
   *  can be seen. The samples below the threshold (lower 10bit) are
   *  suppressed. This register has a direct influence on the amount
   *  of data transmitted and the number of samples / event stored in
   *  the memory. 
   *  @ingroup rcuxx_altro
   */
  struct AltroZSTHR : public AltroRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroZSTHR(Rcu& rcu)
      : AltroRegister("ZSTHR", "Zero supression threshold", 
		      0x8, rcu, false, true, true), fOffset(0), fZS_THR(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data)
    { 
      fOffset = (data >> 10) & 0x3ff; 
      fZS_THR = data & 0x3ff;
    }

    /** Set the internal cache before commit */
    unsigned int Encode() const 
    { 
      return ((fOffset & 0x3ff) << 10)+(fZS_THR & 0x3ff); 
    }
    /** Zero suppresson offset */
    unsigned int Offset() const { return fOffset; }
    /** Zero suppresson offset */
    void SetOffset(unsigned int val) { fOffset = val; }
    /** Zero suppresson threshold */
    unsigned int ZS_THR() const { return fZS_THR; }
    /** Zero suppresson threshold */
    void SetZS_THR(unsigned int val) { fZS_THR = val; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tOffset:\t\t\t\t" << fOffset << "\n" 
		<< "\tThreshold:\t\t\t"<< fZS_THR << std::endl;
    }
  protected:
    /** Offset */
    unsigned int fOffset;
    /** Threshold */
    unsigned int fZS_THR;
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
