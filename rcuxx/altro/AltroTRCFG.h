// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROTRCFG_H
#define RCUXX_ALTROTRCFG_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroTRCFG rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register TRCFG - trigger
   *  configuration.    The higher 10bit code for the trigger
   *  delay. The delay between the global trigger and the arrival of
   *  data to the pads depends on the position of the pads themselves
   *  in the chamber. For specific chips the delay can be adjusted in
   *  order to compensate for this. Number of samples/event codes the
   *  number of samples / event to be processed and it ranges from 0
   *  to 1000. 
   * 
   *  @note ACQ_START must be less or equal than ACQ_END. When
   *  Pretrigger is used, ACQ_START is ignored. Pretrigger and
   *  ACQ_START are mutually exclusive features. To avoid overflowing
   *  the data memory when it is divided in 8 buffers, ACQ_END should
   *  not exceed 506 (1FA). 
   *  @ingroup rcuxx_altro
   */
  struct AltroTRCFG : public AltroRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroTRCFG(Rcu& rcu)
      : AltroRegister("TRCFG", "Acquisition window", 0xA, rcu, 
		      false, true, true), fACQ_START(0), fACQ_END(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data)
    {
      fACQ_START = (data >> 10) & 0x3ff;
      fACQ_END   = data & 0x3ff;
    }

    /** Set the internal cache before commit */
    unsigned int Encode() const
    {
      return ((fACQ_START & 0x3ff) << 10) + (fACQ_END & 0x3ff);
    }

    /** Acquisition start (in time-bins) */
    unsigned int ACQ_START() const { return fACQ_START; }
    /** Acquisition start (in time-bins) */
    void SetACQ_START(unsigned int val) { fACQ_START = val; }
    /** Acquisition end (in time-bins) */
    unsigned int ACQ_END() const { return fACQ_END; }
    /** Acquisition end (in time-bins) */
    void SetACQ_END(unsigned int val) { fACQ_END = val; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tStart:\t\t\t\t" << fACQ_START << "\n" 
		<< "\tEnd:\t\t\t\t"<< fACQ_END << std::endl;
    }

  protected:
    /** Acquisition start (in time-bins) */
    unsigned int fACQ_START;
    /** Acquisition end (in time-bins) */
    unsigned int fACQ_END;
  };


}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
