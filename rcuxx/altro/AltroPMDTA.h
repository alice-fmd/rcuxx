// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROPMDTA_H
#define RCUXX_ALTROPMDTA_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroPMDTA rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register PMDTA - The pedestal memory
   *  data.   Data to be stored in the Pedestal Memory of each one of
   *  the 16 channels. The memories are cuts of 1K x 10bit and can be
   *  used in different modes, see register AltroDPCFG. 
   * 
   *  @note Data written to or read from this register is routed
   *  to/from the Pedestal Memory of the corresponding channel at the
   *  address specified in the global register PMADD. PMADD is common
   *  for all the channels. Therefore, the strategy to fill up the
   *  Pedestal Memories is first to write the address and then the
   *  data for that address across all the 16 channels. The procedure
   *  is repeated again for each address. 
   *
   *  Before writing or reading the Pedestal Memory, make sure that
   *  the First Baseline Correction is in a mode that does not access
   *  the memory, otherwise data will be corrupted. The recommended
   *  operation mode is @b din-fpd. 
   *  
   *  @note This is definend per channel.  
   *  @ingroup rcuxx_altro
   */
  struct AltroPMDTA : public AltroRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    AltroPMDTA(Rcu& rcu)
      : AltroRegister("PMDTA", "Pedestal memory data", 0x7, rcu, false, true),
	fPMDTA(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data) { fPMDTA = data; }
    /** Set the internal cache before commit */
    unsigned int Encode() const { return fPMDTA & 0x3ff; }
    /** @return Pedestal memory data address */
    unsigned int PMDTA() const { return fPMDTA; }
    /** Pedestal memory data address */
    void SetPMDTA(unsigned int val) { fPMDTA = val; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tPedestal data:\t\t\t" << fPMDTA << std::endl;
    }

  protected:
    /** Pedestal data */
    unsigned int fPMDTA;  
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
