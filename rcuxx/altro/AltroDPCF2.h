// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTRODPCF2_H
#define RCUXX_ALTRODPCF2_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroDPCF2 rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register AltroDPCF2
   *  @note The Power Save bit may reduce the power consumption
   *  dramatically under certain data path configurations.
   *  @ingroup rcuxx_altro
   */
  struct AltroDPCF2 : public AltroRegister 
  {
    /** Types */
    enum BUF_t {
      /** 4 buffers */
      k4Buffers, 
      /** 8 buffers */
      k8Buffers
    }; 
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroDPCF2(Rcu& rcu)
      : AltroRegister("DPCF2", "Data path configuration 2", 0xC, rcu /*imem, rmem*/, 
		      false, true, true),
	fPTRG(0), fBUF(k4Buffers), fFLT_EN(0), fPWSV(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data)
    {
      fPTRG   = (data & 0xf);
      fFLT_EN = data & (1 << 5);
      fPWSV   = data & (1 << 6);
      fBUF    = (data & (1 << 4) ? k4Buffers : k8Buffers);
    }

    /** Set the internal cache before commit */
    unsigned int Encode() const
    {
      return ((fPTRG & 0xf) + 
	      (fBUF == k4Buffers ? 0 : 0x10) +
	      (fFLT_EN ? 0x20 : 0) + 
	      (fPWSV ? 0x40 : 0));
    } 

    /** Pre trigger */
    unsigned int PTRG() const { return fPTRG; }
    /** Set the pre trigger value 
	@param val Number of sample before trigger */
    void SetPTRG(unsigned int val) { fPTRG = val; } 
    /** # buffers */
    BUF_t BUF() const { return fBUF; }
    /** # buffers */
    void SetBUF(BUF_t buf) { fBUF = buf; }
    /** Is Digital filter enabled */
    bool IsFLT_EN() const { return fFLT_EN; }
    /** @param on Set digital filter to on or off */
    void SetFLT_EN(bool on=true) { fFLT_EN = on; } 
    /** Is power save enabled */
    bool IsPWSV() const { return fPWSV; }
    /** @param on Set power save to on or off */
    void SetPWSV(bool on=true) { fPWSV = on; }   
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha
		<< "\tPre-trigger:\t\t\t" << fPTRG << "\n" 
		<< "\tDigitial filter:\t\t" << fFLT_EN << "\n" 
		<< "\tPower save:\t\t\t" <<  fPWSV << "\n" 
		<< "\t# Buffers:\t\t\t"  << (fBUF == k4Buffers ? 4 : 8)
		<< std::endl;
    }

  protected:
    /** Pre trigger */
    unsigned int fPTRG; 
    /** # buffers */
    BUF_t fBUF; 
    /** Digital filter */
    bool fFLT_EN; 
    /** Power save */
    bool fPWSV;   
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
