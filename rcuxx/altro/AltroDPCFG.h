// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTRODPCFG_H
#define RCUXX_ALTRODPCFG_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroDPCFG rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register DPCFG - data path
   *  configuration. Register containing configuration parameters for
   *  the BSU, MAU and ZSU. The table below shows in detail the
   *  function of each bit. 
   *  
   *  @ingroup rcuxx_altro
   */
  struct AltroDPCFG : public AltroRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroDPCFG(Rcu& rcu)
      : AltroRegister("DPCFG", "Data path configuration 1", 0xB, rcu /*imem, rmem*/, 
		      false, true, true), f1stBMode(0), f1stBPol(0), 
	f2ndBPre(0), f2ndBPost(0), f2ndBEnable(0), fZSPre(0), 
	fZSPost(0), fZSGlitch(0), fZSEnable(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data)
    {
      f1stBMode   = (data >>  0) & 0xf;
      f1stBPol    = (data >>  4) & 0x1;
      f2ndBPre    = (data >>  5) & 0x3;
      f2ndBPost   = (data >>  7) & 0xf;
      f2ndBEnable = (data >> 11) & 0x1;
      fZSGlitch   = (data >> 12) & 0x3;
      fZSPost     = (data >> 14) & 0x7;
      fZSPre      = (data >> 17) & 0x3;
      fZSEnable   = (data >> 19) & 0x1;
    }

    /** Set the internal cache before commit */
    unsigned int Encode() const
    {
      unsigned int data = 0;
      data = (((f1stBMode & 0xf) << 0) | 
	      ((f1stBPol  & 0x1) << 5) |
	      (f2ndBEnable ? ((f2ndBPre  & 0x3) << 5) : 0) |
	      (f2ndBEnable ? ((f2ndBPost & 0xf) << 7) : 0) |
	      ((f2ndBEnable & 0x1) << 11) |
	      (fZSEnable   ? ((fZSGlitch & 0x3) << 12) : 0) |
	      (fZSEnable   ? ((fZSPost   & 0x7) << 14) : 0) |
	      (fZSEnable   ? ((fZSPre    & 0x3) << 17) : 0) |
	      (fZSEnable & 0x1) << 19);
      return data;
    } 

    /**
     * The various kinds of data paths.
     * 
     */
    enum { 
      kDinFpd		= 0x0,	/// data minus fixed pedestal
      kDinFt		= 0x1,  /// data minus time-dep. pedestal
      kDinFdin		= 0x2,  /// data minus data-dep. pedestal
      kDinFdinvpd	= 0x3,  /// Data minus reduced data-dep. pedestal
      kDinVpdFpd	= 0x4,  /// Data minux fixed and variable pedestal
      kDinVpdFt		= 0x5,  /// Data minux variable and time-dep. pedestal
      kDinVpdFdin	= 0x6,  /// Data minux variable and data-dep. pedestal
      kDinVpdFdinvpd	= 0x7,  /// Data minux variable and red. data-dep. ped.
      kFdinFpd		= 0x8,  /// Data lookup minus fixed pedestal
      kFdinvpdFpd	= 0x9,  /// Reduced data lookup minus fixed pedestal
      kFtFPd		= 0xA,  /// Time-dep. pattern minus fixed pedestal
      kFtFt		= 0xB,  /// Time-dep. pattern minus it self
      kFdinFdin		= 0xC,  /// Data-dep. pattern minus it self
      kFdinvpdkFdinvpd	= 0xD,  /// Reduced data-dep. pattern minus it self
      kDinFpd1		= 0xE,	/// data minus fixed pedestal
      kDinFpd2		= 0xF	/// data minus fixed pedestal
    };
      
    /** 1st baseline mode */
    unsigned int FirstBMode() const { return f1stBMode; }
    /** 1st baseline mode 
	Valid modes are 
	<table> 
	<tr><th>bits</th><th>hex</th><th>Meaning</th></tr>
	<tr><td>0000</td><td>0x0</td><td>din-fpd</td></tr>
	<tr><td>0001</td><td>0x1</td><td>din-f(t)</td></tr>
	<tr><td>0010</td><td>0x2</td><td>din-f(din)</td></tr>
	<tr><td>0011</td><td>0x3</td><td>din-f(din-vpd)</td></tr>
	<tr><td>0100</td><td>0x4</td><td>din-vpd-fpd</td></tr>
	<tr><td>0101</td><td>0x5</td><td>din-vpd-f(t)</td></tr>
	<tr><td>0110</td><td>0x6</td><td>din-vpd-f(din)</td></tr>
	<tr><td>0111</td><td>0x7</td><td>din-vpd-f(din-vpd)</td></tr>
	<tr><td>1000</td><td>0x8</td><td>f(din)-fpd</td></tr>
	<tr><td>1001</td><td>0x9</td><td>f(din-vpd)-fpd</td></tr>
	<tr><td>1010</td><td>0xA</td><td>f(t)-fpd</td></tr>
	<tr><td>1011</td><td>0xB</td><td>f(t)-f(t)</td></tr>
	<tr><td>1100</td><td>0xC</td><td>f(din)-f(din)</td></tr>
	<tr><td>1101</td><td>0xD</td><td>f(din-vpd)-f(din-vpd)</td></tr>
	<tr><td>1110</td><td>0xE</td><td>din-fpd</td></tr>
	<tr><td>1111</td><td>0xF</td><td>din-fpd</td></tr>
        </table>
        The abbrivations have the following meaning:
	- din stands for the data stream coming from the ADC.
	- fpd stands for the fixed pedestal, a constant value
	stored in register VFPED that is to be subtracted from the
	ADC data stream  (see AltroVFPED). 
	- vpd stands for the self-calibrated pedestal value, that is,
	the average DC level that the ADC sees outside the
	acquisition window (i.e. when there is no signal from the
	gas chamber) (see AltroVFPED)
	- @f$ f(x)@f$ Look-up in pedestal memory with index @f$ x@f$. 
	That is, for example
	- f(t) stands for the data of the Pedestal Memory, played
	back as a function of time for the duration of the
	acquisition after a L1 trigger is received. (Pattern
	Generator Mode). 
	- f(din) stands for the data of the Pedestal Memory, played
	back a function of the ADC data at any time. (Look-up
	Table Mode).
    */
    void SetFirstBMode(unsigned int val) { f1stBMode = val; }
    /** 1's compliment */
    bool IsFirstBPol() const { return f1stBPol; }
    /** @param on Set 1's compliment to on or off */
    void SetFirstBPol(bool on=true) { f1stBPol = on; }

    /** 2nd baseline pre excl. */
    unsigned int SecondBPre() const { return f2ndBPre; }
    /** 2nd baseline pre excl. */
    void SetSecondBPre(unsigned int val) { f2ndBPre = val; }
    /** 2nd baseline post exclude */
    unsigned int SecondBPost() const { return f2ndBPost; }
    /** 2nd baseline post exclude */
    void SetSecondBPost(unsigned int val) { f2ndBPost = val; }
    /** Is 2nd baseline correction enabled */
    bool IsSecondBEnable() const { return f2ndBEnable; }
    /** @param on Set 2nd baseline correction to on or off */
    void SetSecondBEnable(bool on=true) { f2ndBEnable = on; }

    /** Zero suppression pre samples */
    unsigned int ZSPre() const { return fZSPre; }
    /** Zero suppression pre samples */
    void SetZSPre(unsigned int val) { fZSPre = val; }
    /** Zero suppression post samples */
    unsigned int ZSPost() const { return fZSPost; }
    /** Zero suppression post samples */
    void SetZSPost(unsigned int val) { fZSPost = val; }
    /** Zero suppression glitch mode */
    unsigned int ZSGlitch() const { return fZSGlitch; }
    /** Zero suppression glitch filter.  This sets the number of
        time-bins that must be above threshold before a bunch is
        made.  This can be in the range from 0 o 3
    */
    void SetZSGlitch(unsigned int val) { fZSGlitch = val; }
    /** Is zero suppression enabled  */
    bool IsZSEnable() const { return fZSEnable; }
    /** @param on Set zero suppression to on or off */
    void SetZSEnable(bool on=true) { fZSEnable = on; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha 
		<< "\tPolarity:\t\t\t" 
		<< (f1stBPol ? "inverse" : "normal") << "\n"
		<< "\t1st baseline mode:\t\t" << std::hex << f1stBMode 
		<< " (" << std::dec << std::flush;
      switch (f1stBMode) {
      case 0x0:  std::cout << "din-fpd";			break;
      case 0x1:  std::cout << "din-f(t)";			break;
      case 0x2:  std::cout << "din-f(din)";			break;
      case 0x3:  std::cout << "din-f(din-vpd)";		break;
      case 0x4:  std::cout << "din-vpd-fpd";		break;
      case 0x5:  std::cout << "din-vpd-f(t)";		break;
      case 0x6:  std::cout << "din-vpd-f(din)";		break;
      case 0x7:  std::cout << "din-vpd-f(din-vpd)";		break;
      case 0x8:  std::cout << "f(din)-fpd";			break;
      case 0x9:  std::cout << "f(din-vpd)-fpd";		break;
      case 0xA:  std::cout << "f(t)-fpd";			break;
      case 0xB:  std::cout << "f(t)-f(t)";			break;
      case 0xC:  std::cout << "f(din)-f(din)";		break;
      case 0xD:  std::cout << "f(din-vpd)-f(din-vpd)";	break;
      case 0xE:  std::cout << "din-fpd";			break;
      case 0xF:  std::cout << "din-fpd";			break;
      }
      std::cout << ")\n" 
		<< "\t2nd baseline enabled:\t\t" << f2ndBEnable << "\n"
		<< "\t\tPre-excluded:\t\t" << f2ndBPre << "\n"
		<< "\t\tPost-excluded:\t\t" << f2ndBPost << "\n"
		<< "\tZero suppression enabled:\t" << fZSEnable << "\n" 
		<< "\t\tPre-excluded:\t\t" << fZSPre << "\n"
		<< "\t\tPost-excluded:\t\t" << fZSPost << "\n"
		<< "\t\tGlitch filter:\t\t" << fZSGlitch << std::endl;
    }

  protected:
    /** Mode */
    unsigned int f1stBMode;
    /** 1's compliment */
    bool         f1stBPol;

    /** Pre excl. */
    unsigned int f2ndBPre;
    /** Post excl. */
    unsigned int f2ndBPost;
    /** Enable 2nd baseline correction  */
    bool         f2ndBEnable;

    /** pre samples */
    unsigned int fZSPre;
    /** post samples */
    unsigned int fZSPost;
    /** Glitch moe */
    unsigned int fZSGlitch;
    /** enable  */
    bool         fZSEnable;
  };


}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
