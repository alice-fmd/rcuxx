// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROERSTR_H
#define RCUXX_ALTROERSTR_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroERSTR rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register ERSTR.  It contains 8 bit for
   *  coding errors in the circuit: Readout error, single and double
   *  event  upsets (SEU) in the MMU and Interface modules, trigger
   *  overlap and instruction error.  This last error embraces the
   *  cases of writing or reading in the wrong or non-existent
   *  address. The lower 12 bits give information on the state of the 
   *  multi-event buffer: empty, full, remaining buffers and the
   *  position of the Read and Write pointers. The table below
   *  summarizes the error and status register. 
   * 
   *  This table summarizes the meaning og register's bits 
   * 
   *  <table>
   *    <tr><th>Parameter</th><th>Description</th></tr>
   *    <tr><td>Read Pointer</td><td>Pointer to the buffer that is to
   *      be read out</td></tr>
   *    <tr><td>Write Pointer</td><td>Pointer to the buffer that is to
   *      be written on next trigger</td></tr>
   *    <tr><td>Remaining Buffers</td><td>Number of empty buffers
   *      remaining in the Data Memory</td></tr>
   *    <tr><td>FULL</td><td>Flag signalling that all the buffers of
   *      the memory are filled with valid event</td></tr>
   *    <tr><td>EMPTY</td><td>Flag signalling that all the buffers of
   *      the memory are available for writing</td></tr>
   *    <tr><td>Parity Error</td><td>A parity error has been detected
   *      while decoding an instruction (sticky bit)</td></tr>
   *    <tr><td>Instruction Error</td><td>An illegal instruction has
   *      been received (sticky bit)</td></tr>
   *    <tr><td>Trigger Overlap</td><td>A trigger pulse has been
   *      received during the processing window of a previous trigger  
   *      (sticky bit)</td></tr>  
   *    <tr><td>MMU 1 SEU</td><td>One Single Event Upset has been
   *      detected in the state machine that controls the buffers of
   *      the Data Memory (sticky bit)</td></tr>
   *    <tr><td>MMU 2 SEU</td><td>Two Single Event Upsets have been
   *      detected in the state machine that controls the buffers of
   *      the Data Memory</td></tr>
   *    <tr><td>INT 1 SEU</td><td>One Single Event Upset has been
   *      detected in the state machine that controls the interface to
   *      the external bus (sticky bit)</td></tr> 
   *    <tr><td>INT 2 SEU</td><td>Two Single Event Upsets have been
   *      detected in the state machine that controls the interface to
   *      the external bus (sticky bit)</td></tr> 
   *    <tr><td>RDO Error</td><td>A readout command has been received
   *      when there was nothing to read out. (sticky bit)</td></tr> 
   *  </table>
   *  @note 
   *    - Single Event Upsets (SEU) will only occur in the presence of
   *      radiation. If a SEU happens, the affected state machine will
   *      recover automatically. If a double SEU is detected, the
   *      corresponding state machine has interrupted its logical
   *      sequence and gone to idle state.  The chip must therefore be
   *      reset when possible. 
   *    - All of the error bits are sticky, that is, they remain in
   *      the @b 1 state after they are set. The error bits are reset
   *      when the chip is reset or powered off or the ERCLR command
   *      is issued. 
   *    - When running in 4-buffer mode, the Write Pointer and Read
   *      Pointer can only take the values 0, 2, 4 or 6. In the
   *      8-buffer mode, they take all values between 0 and 7. 
   *    - The number of remaining buffers ranges from 0 to 4 in the
   *      4-buffer mode and from 0 to 8 in the 8-buffer mode.
   *    - Valid instructions can produce an instruction error if they
   *      are issued in the wrong mode (e.g. broadcasting a register
   *      read, or writing a read-only register) 
   *  @note When the FULL flag is set, any further L1 or L2 triggers 
   *  will be ignored. The Readout Controller Unit must take care of
   *  filtering the triggers and avoiding this
   *  situation. Nevertheless, if a lost L1 trigger was to be
   *  identified, the user can check the value of the Trigger Counter
   *  Register (TRCNT). 
   *  @ingroup rcuxx_altro
   */
  struct AltroERSTR : public AltroRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroERSTR(Rcu& rcu)
      : AltroRegister("ERSTR", "Error and status", 0x10, rcu, true, false, true),
	fRdo(false),
	fInt2Seu(false),
	fInt1Seu(false),
	fMMU2Seu(false),
	fMMU1Seu(false),
	fTrigger(false),
	fInstruction(false),
	fParity(false),
	fEmpty(false),
	fFull(false),
	fBuffers(0),
	fWp(0),
	fRp(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data);
    /** Clear the register */
    unsigned int Clear() { return Write(0); }
    /** Is there Nothing to readout? */
    bool IsRdo() const { return fRdo; }		
    /** Is there 2 single event upsets in bus FSM? */
    bool IsInt2Seu() const { return fInt2Seu; }
    /** Is there 1 single event upset in bus FSM? */
    bool IsInt1Seu() const { return fInt1Seu; }
    /** Is there 2 single event upsets in buffer FSM? */
    bool IsMMU2Seu() const { return fMMU2Seu; }
    /** Is there 1 single event upset in buffer FSM? */
    bool IsMMU1Seu() const { return fMMU1Seu; }
    /** Is there Overlapping triggers? */
    bool IsTrigger() const { return fTrigger; }
    /** Is there Illegal instruction? */
    bool IsInstruction() const { return fInstruction; }
    /** Is there Error in parity of instruction? */
    bool IsParity() const { return fParity; }
    /** Is there The buffers are empty? */
    bool IsEmpty() const { return fEmpty; }
    /** Is there All buffers full? */
    bool IsFull() const { return fFull; }
    /** Number of buffers */
    unsigned int Buffers() const { return fBuffers; }
    /** Write pointer */
    unsigned int Wp() const { return fWp; }
    /** Read pointer */
    unsigned int Rp() const { return fRp; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Readout - Nothing to readout */
    bool fRdo;		
    /** 2 bus SEU - 2 single event upsets in bus FSM */
    bool fInt2Seu;
    /** 1 bus SEU - 1 single event upset in bus FSM */
    bool fInt1Seu;
    /** 2 buffer SEU - 2 single event upsets in buffer FSM */
    bool fMMU2Seu;
    /** 1 buffer SEU - 1 single event upset in buffer FSM */
    bool fMMU1Seu;
    /** Trigger - Overlapping triggers */
    bool fTrigger;
    /** Instr. - Illegal instruction */
    bool fInstruction;
    /** Parity - Error in parity of instruction */
    bool fParity;
    /** Empty - The buffers are empty */
    bool fEmpty;
    /** Full - All buffers full */
    bool fFull;
    /** Number of buffers */
    unsigned int fBuffers;
    /** Write pointer */
    unsigned int fWp;
    /** Read pointer */
    unsigned int fRp;
  };
  //====================================================================
  inline void
  AltroERSTR::Decode(unsigned int data) 
  {
    fRp	   	= data & 0x7;
    fWp	   	= (data >> 3) & 0x7;
    fBuffers	= (data >> 6) & 0xf;
    fFull	= ! (data & (1 << 10));
    fEmpty	= ! (data & (1 << 11));
    fParity	= ! (data & (1 << 12));
    fInstruction= ! (data & (1 << 13));
    fTrigger	= ! (data & (1 << 14));
    fMMU1Seu	= ! (data & (1 << 15));
    fMMU2Seu	= ! (data & (1 << 16));
    fInt1Seu	= ! (data & (1 << 17));
    fInt2Seu	= ! (data & (1 << 18));
    fRdo	= ! (data & (1 << 19));
  }
  //____________________________________________________________________
  inline void
  AltroERSTR::Print() const
  {
    AltroRegister::Print();
    std::cout << std::boolalpha 
	      << "\tEmpty:\t\t\t\t" << fEmpty << "\n" 
	      << "\tFull:\t\t\t\t" << fFull << "\n" 
	      << "\t# buffers:\t\t\t" << fBuffers << "\n" 
	      << "\tWrite pointer:\t\t\t0x" << std::hex << fWp << "\n" 
	      << "\tRead pointer:\t\t\t0x" << std::hex << fRp << "\n"
	      << "\tErrors set:\n";
    if (!fRdo)         std::cout << "\t\tNothing to readout\n";
    if (!fInt2Seu)     std::cout << "\t\t2 single event upsets in bus FSM\n";
    if (!fInt1Seu)     std::cout << "\t\t1 single event upset in bus FSM\n";
    if (!fMMU2Seu)     std::cout << "\t\t2 single event upsets in buffer FSM\n";
    if (!fMMU1Seu)     std::cout << "\t\t1 single event upset in buffer FSM\n";
    if (!fTrigger)     std::cout << "\t\tOverlapping triggers\n";
    if (!fInstruction) std::cout << "\t\tIllegal instruction\n";
    if (!fParity)      std::cout << "\t\tError in parity of instruction\n";
    std::cout << std::dec << std::flush;
  }

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
