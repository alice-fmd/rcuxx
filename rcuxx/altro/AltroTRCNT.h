// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROTRCNT_H
#define RCUXX_ALTROTRCNT_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroTRCNT rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register TRCNT.  The 16 lower bits
   *  code the number of level 1 triggers received by the ALTRO chip. 
   *  @note This counter is set to 0 when the chip is reset or when
   *  the command TRCLR is issued. The count includes also the
   *  triggers that are ignored when the memory is full. 
   *  @note Read only
   *  @ingroup rcuxx_altro
   */
  struct AltroTRCNT : public AltroRegister 
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroTRCNT(Rcu& rcu)
      : AltroRegister("TRCNT", "Trigger counter", 0x12, rcu, true, false, true),
	fTRCNT(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data) { fTRCNT = data; }
    /** Clear the register */
    unsigned int Clear()
    {
      unsigned int save_addr = fAddress;
      fAddress = 0x1C;
      unsigned int ret = Write(0);
      fAddress = save_addr;
      return ret;
    }
    /** @return Trigger count  */
    unsigned int  TRCNT() const { return fTRCNT; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\t# Triggers:\t\t\t" << fTRCNT << std::endl;
    }
  protected:
    /** Trigger counter */
    unsigned int  fTRCNT;
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
