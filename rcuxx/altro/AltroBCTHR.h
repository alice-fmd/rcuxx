// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROBCTHR_H
#define RCUXX_ALTROBCTHR_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroBCTHR rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register BCTHR - Base line correction
   *  threshold.  The higher 10bit correspond to the upper threshold
   *  and the lower 10bit to the bottom threshold. The range in
   *  between these two levels is indeed an estimation of where the
   *  baseline really is. The average baseline is calculated whenever
   * the input signal lies in that range. 
   *  @ingroup rcuxx_altro
   */
  struct AltroBCTHR : public AltroRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroBCTHR(Rcu& rcu)
      : AltroRegister("BCTHR", "2nd baseline threshold", 0x9, rcu,
		      false, true, true), fTHR_HI(0), fTHR_LO(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data)
    {
      fTHR_HI = (data >> 10) & 0x3ff;
      fTHR_LO = data & 0x3ff;
    }
    /** Set the internal cache before commit */
    unsigned int Encode() const 
    { 
      return ((fTHR_HI & 0x3ff) << 10)+(fTHR_LO & 0x3ff); 
    }
    /** Baseline correction high threshold */
    unsigned int THR_HI() const { return fTHR_HI; }
    /** Baseline correction high threshold */
    void SetTHR_HI(unsigned int val) { fTHR_HI = val; }
    /** Baseline correction low threshold */
    unsigned int THR_LO() const { return fTHR_LO; }
    /** Baseline correction low threshold */
    void SetTHR_LO(unsigned int val) { fTHR_LO = val; }
    /** Print contents of this register */
    virtual void Print() const
{
  AltroRegister::Print();
  std::cout << "\tHigh:\t\t\t\t" << fTHR_HI << "\n" 
	    << "\tLow:\t\t\t\t"<< fTHR_LO << std::endl;
}
  protected:
    /** High threshold */
    unsigned int fTHR_HI;
    /** Low threshold */
    unsigned int fTHR_LO;
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
