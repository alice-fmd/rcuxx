// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROREGISTER_H
#define RCUXX_ALTROREGISTER_H
#include <rcuxx/Register.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuRMEM.h>
#include <rcuxx/DebugGuard.h>
#include <rcuxx/compat.h>
#include <stdexcept>

namespace Rcuxx 
{
  //====================================================================
  /** @struct AltroRegister rcuxx/Altro.h <rcuxx/Altro.h> 
   *  @brief Interface to an ALTRO (bus) register.
   *  Note, that the access is implemented via the RcuIMEM and RcuRMEM
   *  interfaces, so this interface, and it's derived classes are
   *  completly general and does not need to be specialised for a
   *  particular back-end. 
   *  @ingroup rcuxx_altro
   */
  struct AltroRegister : public Register
  {
    /** 
     * A guard for the ALTRO registers 
     */
    struct Guard : public Register::Guard
    {
      /**
       * Constructor 
       * 
       * @param reg     Register 
       * @param verify  If we should verify/update 
       * @param debug   Debug messages 
       */
      Guard(AltroRegister& reg, bool verify, bool debug)
	: Register::Guard(verify, debug),
	  fRegister(reg),
	  fBroadcast(reg.IsBroadcast()),
	  fBoard(reg.Board()),
	  fChip(reg.Chip()),
	  fChannel(reg.Channel())
      {
	if (fVerify) { 
	  unsigned int ret = fRegister.Update();
	  if (ret != 0) { 
	    // If we failed to update the register, ignore it
	    throw std::runtime_error("Failed to update register");
	  }
	}
	// Get the encoded value of the register 
	fCached = fRegister.Encode();
	if (fDebug) {
	  std::cout << "On guard: ";
	  fRegister.Print();
	}
      }
      /** 
       * Destructor 
       */
      ~Guard()
      {
	if (fDebug) { 
	  fRegister.Update();
	  std::cout << "On exit: ";
	  fRegister.Print();
	}
	// Set value on the register 
	fRegister.Decode(fCached);
	fRegister.SetAddress(fBoard, fChip, fChannel);
	if (fBroadcast) fRegister.SetBroadcast();
	
	// And commit to hardware 
	unsigned int ret = fRegister.Commit();
	if (ret != 0) 
	  throw std::runtime_error("Failed to restore register");
	
	if (fVerify && !fRegister.Verify()) 
	  throw std::runtime_error("Failed to validate register");
	if (fDebug) { 
	  fRegister.Update();
	  std::cout << "On restore: ";
	  fRegister.Print();
	}
      }
      /** Reference to register */
      AltroRegister& fRegister;
      /** The cached value */
      unsigned int fCached;
      /** Broadcast flag */
      bool fBroadcast;
      /** Board */
      unsigned short fBoard;
      /** Chip */
      unsigned short fChip;
      /** Channel */
      unsigned short fChannel;
    };
    /** 
     * Create a guard 
     * 
     * @return A newly allocated guard 
     */
    virtual Register::Guard* CreateGuard(bool verify, bool debug)
    {
      return new Guard(*this, verify, debug);
    }
    /** Constructor 
	@param name Name 
	@param title Title
	@param instr Instruction code 
	@param rcu Reference to RCU interface 
	@param clear Whether to enable Clear command 
	@param submit Whether to enable submit command 
	@param global Whether this is global 
	@param bcast Whether to allow broadcast */
    AltroRegister(const char* name, 
		  const char* title, 
		  unsigned int instr, 
		  Rcu& rcu, bool clear, bool submit, bool global=false,
		  bool bcast=true);
    /** Set to broadcast, if allowed. */
    virtual void   SetBroadcast();
    /** Set target address
	@param board   Board number   (0x1f)
	@param chip    Chip number    (0x7)
	@param channel Channel number (0xf) */
    virtual void   SetAddress(unsigned int board, unsigned int chip, 
			      unsigned int channel=0);
    /** Write @a data to register 
	@param data Data to put in register
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(unsigned int data);
    /** Read value of register into @a data
	@param data On return, value of register 
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(unsigned int& data);
    /** Make write instructions and store them in @a imem
	@return false if the operation isn't allowed in current mode
	(broadcast) or this register is read-only.  */
    virtual unsigned int WriteInstructions();
    /** Make write instructions and store them in @a imem
	@return false if the operation isn't allowed in current mode
	(broadcast) or this register is read-only.  */
    virtual unsigned int WriteInstructions(Rcuxx::RcuIMEM::Cache_t& cache, 
					   size_t& marker);
    /** Make read instructions and store them in @a imem
 	@return false if the operation isn't allowed in current mode
	(broadcast) */
    virtual unsigned int ReadInstructions();
    /** Make read instructions and store them in @a imem
 	@return false if the operation isn't allowed in current mode
	(broadcast) */
    virtual unsigned int ReadInstructions(Rcuxx::RcuIMEM::Cache_t& cache, 
					  size_t& marker);
    /** Set internal structures */
    virtual void Set() { fData = Encode(); }
#if 0
    /** Get internal structures */
    virtual void Get() { }
#endif
    /** Commit stored values to hardware 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
    /** Read from hardware to stored values
	@return 0 on success, error code otherwise */
    virtual unsigned int Update();
    /** Verify the content of this object against hardware */
    virtual bool Verify();
    /** Comare passed value against this objects values */
    virtual bool Compare(unsigned int v) const;
    /** Print contents of this register */
    virtual void Print() const;
    /** Whether we got Clear */
    virtual bool IsClearable() const { return fClear; }
    /** Whether this can be broadcast */ 
    virtual bool IsBroadcastable() const { return fAllowBroadcast; }
    /** Check if we're broadcasting */ 
    bool IsBroadcast() const { return (fAddress >> 18) & 0x1; }
    unsigned int Board() const { return ((fAddress >> 7) >> 5) & 0x1F; }
    unsigned int Chip() const { return ((fAddress >> 4) >> 5) & 0x7; }
    unsigned int Channel() const { return ((fAddress >> 0) >> 5) & 0xF; }
    /** Get the address */ 
    unsigned int Address() const { return fAddress; } 
    /** Get the instruction */ 
    unsigned int Instr() const { return fInstr; }
    /** Get the full address+instruction code */ 
    unsigned int FullAddress() const { return Address() | Instr(); }
    /** Get RCU */
    Rcu& Mother() { return fRcu; }
    /** Get RCU */
    const Rcu& Mother() const { return fRcu; }
  protected:
    /** Can clear */
    bool fClear;
    /** Instruction number */
    unsigned int fInstr;
    /** Reference to RCU */
    Rcu& fRcu;
#if 0
    /** Instruction memory */
    RcuIMEM&        fIMEM;
    /** Result memory */
    RcuRMEM&        fRMEM;
#endif
    /** Address */
    unsigned int fAddress;
    /** Whether it's global */
    bool         fGlobal;
    /** Whether it allows broad cast */
    bool         fAllowBroadcast;
    /** Data cache */
    unsigned int fData;
  };
  //====================================================================
  inline
  AltroRegister::AltroRegister(const char*  name, 
			       const char*  title, 
			       unsigned int instr, 
			       Rcu&         rcu,
			       bool         clear, 
			       bool         submit, 
			       bool         global,
			       bool         bcast) 
    : Register(name, title, submit), 
      fClear(clear), 
      fInstr(instr & 0x1f),
      fRcu(rcu),
    // fIMEM(imem),
      // fRMEM(rmem),
      fAddress(0), 
      fGlobal(global),
      fAllowBroadcast(bcast) /* , 
				fData(0) */
  {}

  //____________________________________________________________________
  inline unsigned int
  AltroRegister::WriteInstructions()
  {
    return WriteInstructions(fRcu.IMEM()->Data(),
			     fRcu.IMEM()->Marker());
  }
  //____________________________________________________________________
  inline unsigned int
  AltroRegister::WriteInstructions(Rcuxx::RcuIMEM::Cache_t& cache, 
				   size_t& marker)
  {
    if (!IsSubmitable()) return Rcu::kNotWriteable;
    if (!fAllowBroadcast && (fAddress & (1 << 18))) 
      return Rcu::kCannotBroadcast;
    return fRcu.IMEM()->FecWrite(fAddress | fInstr, (fData & 0xFFFFF), 
				 cache, marker);
  }
  //____________________________________________________________________
  inline unsigned int
  Rcuxx::AltroRegister::ReadInstructions()
  {
    return ReadInstructions(fRcu.IMEM()->Data(),
			    fRcu.IMEM()->Marker());
  }
  //____________________________________________________________________
  inline unsigned int
  Rcuxx::AltroRegister::ReadInstructions(Rcuxx::RcuIMEM::Cache_t& cache, 
				   size_t& marker)
  {
    if (fAddress & (1 << 18)) return Rcu::kCannotBroadcast;
    return fRcu.IMEM()->FecRead(fAddress | fInstr, cache, marker);
  }
  
  //____________________________________________________________________
  inline void
  AltroRegister::SetBroadcast()
  { 
    if (!fAllowBroadcast) return;
    fAddress = (1 << 18); 
  }

  //____________________________________________________________________
  inline void
  AltroRegister::SetAddress(unsigned int board, unsigned int chip, 
			    unsigned int channel) 
  {
    unsigned int addr = (((board & 0x1f) << 7) + ((chip & 0x7) << 4) + 
			 (fGlobal ? 0 : (channel & 0xf)));
    // DebugGuard::Message(fDebug, "AltroRegister::SetAddress: "
    // "Setting address to 0x%02x,0x%02x.0x%02x -> 0x%04x", 
    // board, chip, channel, addr);
    fAddress = addr << 5;
  }
  
  //____________________________________________________________________
  inline unsigned int
  AltroRegister::Commit()
  {
    fData = Encode(); // Set();
    return Write(fData);
  }
  
  //____________________________________________________________________
  inline unsigned int
  AltroRegister::Update() 
  {
    unsigned int ret = Read(fData);
    Decode(fData);
    return ret;
  }
  //____________________________________________________________________
  inline bool 
  AltroRegister::Verify() 
  {
    unsigned int data;
    unsigned int ret = Read(data);
    if (ret != 0) return false;
    return Compare(data);
  }
  //____________________________________________________________________
  inline bool 
  AltroRegister::Compare(unsigned int v) const
  {
    return (fSubmit ? v == Encode() : true);
  }
  //____________________________________________________________________
  inline void
  AltroRegister::Print() const 
  {
    std::cout << "Contents of ALTRO register " << fName << " @ 0x" 
	      << std::hex << fInstr << ": (0x" << fData << ")" 
	      << std::dec << std::endl;
  }

  //____________________________________________________________________
  inline unsigned int
  AltroRegister::Read(unsigned int& data)
  {
    unsigned int ret = 0;
    if ((ret = ReadInstructions())) return ret;
    if ((ret = fRcu.IMEM()->Execute())) return ret;
    USLEEP(1000);
    if ((ret = fRcu.RMEM()->Update(0,2))) return ret;
    ret = fRcu.RMEM()->FecRead(fAddress | fInstr, data, 0);
    return ret;
  }

  //____________________________________________________________________
  inline unsigned int
  AltroRegister::Write(unsigned int /* data */)
  {
    unsigned int ret = 0;
    if ((ret = WriteInstructions())) return ret;
    if ((ret = fRcu.IMEM()->Execute())) return ret;
    USLEEP(1000);
    if ((ret = fRcu.RMEM()->Update(0,2))) return ret;
    if ((ret = fRcu.RMEM()->FecWrite(fAddress | fInstr, fData, 0))) return ret;
    return ret;
  }
}
#endif
//
// Local Variables:
//  mode: c++
// End:
//
