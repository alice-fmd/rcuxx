// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTROPMADD_H
#define RCUXX_ALTROPMADD_H
#include <rcuxx/altro/AltroRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct AltroPMADD rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register PMADD.  It contains the value
   *  of the pedestal memory address.
   *  @note The value set in PMA is common for all the
   *  channels. Therefore, the recommended strategy to fill up the
   *  Pedestal Memories is to write the PMA first, and then the
   *  corresponding data across all the 16 channels. This sequence is
   *  repeated until all the memories all filled up.  
   *
   *  Before writing
   *  or reading the Pedestal Memory, make sure that the First
   *  Baseline Correction is in a mode that does not access the
   *  memory, otherwise data will be corrupted. The recommended
   *  operation mode is @b din-fpd. 
   *  @note This is per channel
   *  @ingroup rcuxx_altro
   */
  struct AltroPMADD : public AltroRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    AltroPMADD(Rcu& rcu)
      : AltroRegister("PMADD", "Pedestal memory address", 0xD, rcu, true, true),
	fPMADD(0)
    {}
    /** Get the values from internal cache after update */
    void Decode(unsigned int data) { fPMADD = data & 0x3ff; }
    /** Set the internal cache before commit */
    unsigned int Encode() const { return fPMADD & 0x3ff; }
    /** @return  Pedestal memory address */
    unsigned int PMADD() const { return fPMADD; }
    /** Set the pedestal memory address */
    void SetPMADD(unsigned int val) { fPMADD = val; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\tPedestal address:\t\t" << fPMADD << std::endl;
    }
  protected:
    /** Pedestal memory address */
    unsigned int fPMADD;
  };

}
#endif
//
// Local Variables:
//  mode: c++ 
// End:
//
