// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Register.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_REGISTER
#define RCUXX_REGISTER
#include <string>
#include <iostream>

namespace Rcuxx
{
  class Rcu;

  //__________________________________________________________________
  /** @struct Register rcuxx/Register.h <rcuxx/Register.h>
      @brief Base class for registers 
      @ingroup rcuxx_rcu 
  */
  struct Register
  {
    struct Guard
    {
      Guard(bool verify, bool debug)
	: fVerify(verify), fDebug(debug)
      {}
      virtual ~Guard() {}
      bool fVerify;
      bool fDebug;
    };
    /** Error codes */
    enum {
    };
    /** Destructor */
    virtual ~Register() {}
    /** Constructor 
	@param name Name of register
	@param title Title of register
	@param submit Whether it's writeable  */
    Register(const char* name, 
	     const char* title, 
	     bool submit)
      : fName(name), fTitle(title), fSubmit(submit)
    {}
    /** 
     * Create a guard 
     * 
     * @return A newly allocated guard 
     */
    virtual Guard* CreateGuard(bool verify, bool debug) { return 0; }
    /** Get the name */ 
    const std::string& Name() const { return fName; }
    /** Get the title */ 
    const std::string& Title() const { return fTitle; }
    /** Whether we got Clear */
    virtual bool IsClearable() const { return false; }
    /** Whether we got Submit */
    bool IsSubmitable() const { return fSubmit; }
    /** Whether this can be broadcast */ 
    virtual bool IsBroadcastable() const { return false; }
    /** Update the cache of the register
	@return 0 on success, error code otherwise */
    virtual unsigned int Update() { return 0; }
    /** Clear the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Clear() { return 0; }
    /** Commit to the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { return 0; }
    /** Encode set value to register value */ 
    virtual unsigned int Encode() const { return 0; }
    /** Decode register value to set value */
    virtual void Decode(unsigned int dat) {}
    /** 
     * Verify that the hardware value is the same as the values set in
     * this object. 
     * 
     * @return true if the values correspond. 
     */
    virtual bool Verify() { return true; }
    /** Print contents of register to standard output */
    virtual void Print() const
    {
      std::cout << "Contents of register " << fName << std::endl;
    }
    /** Get reference to mother */
    virtual Rcu& Mother() = 0;
    /** Get reference to mother */
    virtual const Rcu& Mother() const = 0;
  protected:
    /** Name */
    std::string fName;
    /** Name */
    std::string fTitle;
    /** Clear */
    bool fClear;
    /** Submit */
    bool fSubmit;
  };
}
#endif
//
// EOF
//
