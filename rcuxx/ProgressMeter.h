// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    ProgressMeter.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:26 2006
    @brief   Base class for data acquisition - declaration
*/
#ifndef RCUXX_PROGRESSMETER_H
#define RCUXX_PROGRESSMETER_H
#ifndef __CTIME__
# include <ctime>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace Rcuxx 
{
  /** @class ProgressMeter rcuxx/ProgressMeter.h <rcuxx/ProgressMeter.h>
      @ingroup rcuxx_util
   */
  class ProgressMeter
  {
  public:
    /** Constructor 
	@param n Number of iterations
	@param freq Update frequency */
    ProgressMeter(int n=0, int freq=10);
    /** Step on iteration */
    void Step();
    /** Reset progress bar to new number of iterations 
	@param n New number of iterations 
	@param pre Prefix */
    void Reset(int n, const char* pre="Event #") 
    { fN = n; fCurrent = 0; fThrobber = '-';  fPre = pre; }
  private:
    /** When we started */
    time_t fStart;
    /** Now */
    time_t fNow;
    /** Number of iterations */
    int    fN;
    /** Current iteration */
    int    fCurrent;
    /** Write out frequency */
    int    fFreq;
    /** Current throbber character */
    char   fThrobber;
    /** Pre-string */
    std::string fPre;
  };  
}

#endif
//
// EOF
//
