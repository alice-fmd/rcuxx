// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
//
// ROOT based acquisition 
//
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <vector>

//____________________________________________________________________
/** Function template to turn a string into a value 
    @param str String to read from
    @param v   Return value
*/
template <typename T>
void str2val(const std::string& str, T& v) 
{
  std::stringstream s(str);
  if (str[0] == '0') {
    if (str[1] == 'x' || str[1] == 'X') s >> std::hex >> v;
    else                                s >> std::oct >> v;
  }
  else 
    s >> v;
}

//____________________________________________________________________
/** Function template to turn a string into a value 
    @param str String to read from
    @param v   Return value
*/
template <typename T>
void str2val(const char* str, T& v)
{
  str2val<T>(std::string(str), v);
}


//____________________________________________________________________
/** @class OptionBase 
    @brief Base class for options 
 */
struct OptionBase
{
  /** Constructor 
      @param s   Short format character
      @param l   Long format string
      @param h   Help string
      @param arg Whether it needs an argument  */
  OptionBase(char s, const char* l, const char* h, bool arg)
    : fShort(s), 
      fLong(l), 
      fHelp(h), 
      fNeedArg(arg), 
      fSet(false)
  {}
  /** @return @c true if the option was given */
  bool IsSet() const { return fSet; }
  /** @return @c true if the option needs an argument */
  bool NeedArg() const { return fNeedArg; }
  /** @return Short format character */
  virtual char Short() const { return fShort; }
  /** @return Long format string */
  virtual const std::string& Long() const { return fLong; }
  /** Try to handle the current short option @a s.  If this option can
      handle the option, return @c true.  
      @param s    Current option
      @param arg  Possible argument for this option. 
      @return @c true if this option handled the option */
  virtual bool Handle(char s, const char* arg) = 0;
  /** Try to handle the current long option @a l.  If this option can
      handle the option, return @c true.  
      @param l    Current option
      @param arg  Possible argument for this option. 
      @return @c true if this option handled the option */
  virtual bool Handle(const std::string& l, const std::string& arg) = 0;
  /** Print the help string to standard out, formatted in a way
      suitable for the help output. */
  virtual void Help() const 
  {
    std::cout << "\t-" << fShort << ",--" << fLong 
	      << (fNeedArg ? "=ARG" : "") << "\t" << fHelp;
  }
protected:
  /** Short format character */
  char        fShort;
  /** Long format strin */
  std::string fLong;
  /** Help string */
  std::string fHelp;
  /** Whether we need an argument */
  bool        fNeedArg;
  /** Whether this option was seen */
  bool        fSet;
};
  
namespace std 
{
  /** Specialization of the @c std::less structure for OptionBase
      object.  This is used to compare to OptionBase objects.  This is
      needed by the @c std::map interface used. 
   */
  template <>
  struct less<OptionBase*> 
  {
    /** Compare @a lhs to @a rhs
	@param lhs Left hand side 
	@param rhs Right hand side 
	@return @c true if @a lhs is less than @a rhs, false if
	they are equal, or @a lhs is greater than @a rhs */
    bool operator()(OptionBase* const & lhs, OptionBase* const & rhs) 
    {
      if (lhs->Short() == '\0' && rhs->Short())
	return lhs->Long() < rhs->Long();
      return lhs->Short() < rhs->Short();
    }
  };
}

//____________________________________________________________________
/** @class Option 
    @brief Class template for command line options 
    @param T The type of the option 
 */
template <typename T> 
struct Option : public OptionBase
{
  /** Constructor 
      @param s   Short format character
      @param l   Long format string
      @param h   Help string
      @param v   Default value 
      @param arg Whether it needs an argument  */
  Option(char s, const char*  l, const char* h, const T& v=T(), bool arg=true)
    : OptionBase(s, l, h, arg),
      fValue(v)
  {}
  /** Try to handle the current short option @a s.  If this option can
      handle the option, return @c true.  
      @param s    Current option
      @param arg  Possible argument for this option. 
      @return @c true if this option handled the option */
  bool Handle(char s, const char* arg) 
  {
    if (s != fShort) return false;
    if (!fNeedArg)   {
      std::stringstream s;
      s << true;
      s >> fValue;
      return fSet = true;
    }
    if (!arg)        return true;
    str2val(arg, fValue);
    return true;
  }
  /** Try to handle the current long option @a l.  If this option can
      handle the option, return @c true.  
      @param l    Current option
      @param arg  Possible argument for this option. 
      @return @c true if this option handled the option */
  bool Handle(const std::string& l, const std::string& arg) 
  {
    if (l != fLong) return false;
    if (!fNeedArg)  return fSet = true;
    str2val(arg, fValue);
  }
  /** Get the value of the option 
      @return  Value of the option as given by the user or the default
      value if not specified. */
  const T& Value() const { return fValue; }
  /** Print the help string to standard out, formatted in a way
      suitable for the help output. */
  void Help() const 
  {
    OptionBase::Help();
    if (NeedArg()) {
      std::cout << "\t[";
      if (!IsSet()) std::cout << "default: ";
      std::cout  << fValue << "]";
    }
    std::cout << std::endl;
  }
  /** Access operator.  Access the value (via a pointer) of the value 
      @return  Constant pointer to the value of the option. */
  const T* operator->() const { return &fValue; }
  /** Type cast operator.  Allows this class to be interpreted as an
      object of type @a T. 
      @return This options value as a constant reference type @a T */
  operator const T&() const { return fValue; }
  /** Assignment operator.  Assigns a value to this option from an
      object of its contained type @a T. 
      @param v Value to assign from. 
      @return Reference to this object.  */
  Option<T>& operator=(const T& v) { fValue = v; return *this; }
private: 
  /** The value of this option */
  T fValue;
};

//____________________________________________________________________
/** @class CommandLine 
    @brief Class to handle command line arguments. 
 */
struct CommandLine
{
  /** Constructor. 
      @param args Help string argument specification.  */
  CommandLine(const char* args="") : fArg(args) {}
  /** Process the command line 
      @param argc Number of arguments (including program name) user
      passed to the program.  
      @param argv Array of the arguments (including program name) user
      passed to the program as C strings. 
      @return @c true if everything was passed correctly. */
  bool Process(int argc, char** argv) 
  {
    fProg = argv[0];
    int ret = 0;
    for (size_t i = 1; i < argc; i++) {
      if (argv[i][0] == '-') {
	if (argv[i][1] == '-') {
	  std::string opt, val;
	  GetLong(argv[i], opt, val);
	  ret = CheckOptions(opt, val);
	}
	else 
	  ret = CheckOptions(argv[i][1], argv[i+1]);
	if (ret < 0) {
	  std::cerr << fProg << ": Unknown option '" << argv[i] 
		    << "', try '" << fProg << " --help'" 
		    << std::endl;
	  return false;
	}
	if (ret > 0) {
	  ret = 0; 
	  i++;
	}
      }
      else fRemain.push_back(argv[i]);
    }
    return true;
  }
  /** Get the remaining arguments not handled. 
      @return Arguments not handled */
  std::vector<std::string>& Remain() { return fRemain; }
  /** Add an option to the manager. 
      @param option Option to add. 
  */
  void Add(OptionBase& option) 
  {
    fList.insert(&option);
  }
  /** Show the help screen on standard output.  Each option is printed
      as specified.  The options are sorted after lexical order on the
      short option first. 
   */
  void Help() const
  {
    std::cout << "Usage: " << fProg << " [OPTIONS] " << fArg << "\n\n" 
	      << "Options:"  << std::endl;
    for (OptionList::const_iterator i = fList.begin(); i != fList.end(); i++) 
      (*i)->Help();
    std::cout << std::endl;
  }
protected:
  /** Decode the string @a str into a long format option and a possibl
      argument. 
      @param str Command line parameter.  
      @param opt Decoded long format option
      @param val Possible decoded argument.  
  */
  void GetLong(const char* str, std::string& opt, std::string& val) 
  {
    std::string arg(str);
    size_t eq = arg.find("=");
    if (eq != std::string::npos) {
      opt = arg.substr(0, eq);
      val = arg.substr(eq+1, arg.size() - eq);
    }
    else 
      opt = arg;
    opt.erase(0,2);
  }
  /** Check all options if one will handle the current long option @a
      opt with argument @a arg. 
      @param opt Current long option
      @param arg Current argument. 
      @return  negative if no option handled this option, otherwise
      0. */
  int CheckOptions(const std::string& opt, const std::string& arg) 
  {
    for (OptionList::iterator i = fList.begin(); i != fList.end(); i++) 
      if ((*i)->Handle(opt, arg)) return 0;
    return -1;
  }
  /** Check all options if one will handle the current short option @a
      s with argument @a arg. 
      @param s   Current long option
      @param arg Current argument. 
      @return  negative if no option handled this option, 1 if an
      option handled the option and ate an argument, otherwise 0. */ 
  int CheckOptions(char s, const char* arg) 
  {
    for (OptionList::iterator i = fList.begin(); i != fList.end(); i++) {
      if ((*i)->Handle(s, arg)) { 
	if ((*i)->NeedArg()) return 1;
	return 0;
      }
    }
    return -1;
  }
  /** Help string argument */
  std::string fArg;
  /** Program name */
  std::string fProg;
  /** Type of list of options */
  typedef std::set<OptionBase*> OptionList;
  /** List of options. */
  OptionList fList;
  /** The arguments left after processing all options */
  std::vector<std::string> fRemain;
};
  
//____________________________________________________________________
//
// EOF
//
