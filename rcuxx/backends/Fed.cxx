//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/Fed.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of FedClient back-end 
*/
#include "rcuxx/compat.h"
#include "rcuxx/backends/Fed.h"
#include "rcuxx/Url.h"
#include "rcuxx/DebugGuard.h"
#include "rcuxx/Locking.h"
#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <numeric>
#include <stdexcept>
#include <dim/dic.hxx>
#ifdef HAVE_FEECLIENT_RCU_ISSUE_H
# include <feeclient/rcu_issue.h>
#else 
# include "rcuxx/backends/rcu_issue.h"
#endif 

//====================================================================
namespace 
{
  bool fDebug = false;
}

/** This is for debugging */
#define PRINT_DATA(D,B,E,N,S) \
  do { \
    std::cout << std::setfill('0') << std::hex; \
    for (size_t i = B; i < E; i++) { \
      if (i % N == 0) { \
        if (i != 0) std::cout << '\n'; \
        std::cout << ' '; \
      } \
      std::cout << " 0x" << std::setw(S) << unsigned(D[i]); \
	} \
    std::cout << std::setfill(' ') << std::dec; } while (false)
 
 
//==================================================================== 
namespace Rcuxx
{
  /** Log everything */
# define logLevelAll       0
  /** Log bench mark stuff */
# define logLevelBenchmark 1
  /** Log debug messages */
# define logLevelDebug     2
  /** Log informative messages */
# define logLevelInfo      3
  /** Log warning messages */
# define logLevelWarning   4
  /** Log error messages */
# define logLevelError     5
  /** Log fatal messages */
# define logLevelFatal     6
  /** Log nothing */
# define logLevelNone      7
  /** Default log level */
# define logLevel          logLevelAll

  // Client flags 
  /** Instruction codes */
# define I_WRITE  0
  /** Instruction codes */
# define I_READ   1
  /** Instruction codes */
# define I_GRESET 2

  /** Error codes */
# define FEE_ACK_LINK_OK 	0 
  /** Error codes */
# define FEE_ACK_LOST_LINK  	-71
  /** Error codes */
# define FEE_ACK_STRANGE_DATA 	-72
  /** Error codes */
# define FEE_ACK_UNEXPECTED     -73;

  //====================================================================
  /** Special kind of exception  */
  struct FedException : public std::exception
  {
    /** Error string */
    std::string fWhat;
    /** Destructor  */
    virtual ~FedException() throw () {}
    /** Make an exception 
	@param func Function name
	@param format Format  */
    FedException(const char* func, const char* format, ...)
      : fWhat("")
    {
      static char buf[1024];
      va_list ap;
      va_start(ap, format);
      size_t n = vsprintf(buf,format, ap);
      buf[n] = '\0';
      va_end(ap);
      std::stringstream s;
      s << std::setw(8) << "Error" << " from " << func << ": " << buf;
      fWhat = s.str();
      // fgErrorStr = fWhat;
    }
    /** Get the message */
    const char* what() const throw () { return fWhat.c_str(); }
  };

  //====================================================================
  /** Global lock used in this implementation */
  Lock fgFedLock;

  //____________________________________________________________________
  /** Structure of the acknowledge service */
  struct AckStruct 
  {
    /** Error code */
    int   errorCode;
    /** Data */
    char  data[11000];
    /** Make the dummy structure for when we have no link 
	@return The no-link value */
    static AckStruct* makeNoLink() 
    {
      AckStruct* ret = new AckStruct;
      ret->errorCode = -1000;
      ret->data[0]   = '\0';
      return ret;
    }
    /** Comparison operator 
	@param o The object to compare to. 
	@return @c true if both have the same error code */
    bool operator==(const AckStruct& o) 
    {
      return errorCode == o.errorCode;
    }
  };
  /** The no-link value */
  static AckStruct* fgNoAckLink = AckStruct::makeNoLink();
  
  //____________________________________________________________________
  /** Structure of the message service */
  struct MsgStruct 
  {
    /** Type of message */
    int  type;
    /** Detector sending the message */
    char detector[4];
    /** Source of the message */ 
    char source[256];
    /** Description of the message */
    char description[256];
    /** Date and time of the message */
    char date[20];
    /** Check if this is an informative message */
    bool isInfo()         { return type & 0x01; }
    /** Check if this is an waring message */
    bool isWarning()      { return type & 0x02; }
    /** Check if this is an error message */
    bool isError()        { return type & 0x04; }
    /** Check if this is an failed audit message */
    bool isFailureAudit() { return type & 0x08; }
    /** Check if this is an Succesful audit message */
    bool isSuccessAudit() { return type & 0x10; }
    /** Check if this is an debug message */
    bool isDebug()        { return type & 0x20; }
    /** Check if this is an alarm message */
    bool isAlarm()        { return type & 0x40; }
    /** Make the dummy structure for when we have no link 
	@return The no-link value */
    static MsgStruct* makeNoLink() 
    {
      MsgStruct* ret = new MsgStruct;
      ret->type           = -1000;
      ret->detector[0]    = '\0';
      ret->source[0]      = '\0';
      ret->description[0] = '\0';
      ret->date[0]        = '\0';
      return ret;
    }
    /** Comparison operator.
	@param o  Object to compare to. 
	@return @c true if all fields are the same */
    bool operator==(const MsgStruct& o) 
    {
      return (type                      == o.type && 
	      std::string(detector)     == o.detector && 
	      std::string(source)       == o.source && 
	      std::string(description)  == o.description && 
	      std::string(date)         == o.date);
    }
  };
  /** The no-link value */
  static MsgStruct* fgNoMsgLink = MsgStruct::makeNoLink();

  //====================================================================
  /** @struct FedClient 
      @brief Dummy client interface 
      @ingroup feexx
  */
  struct FedClient : public DimClient
  {
    /** Constructor */
    FedClient(const std::string& icl, const std::string& server) 
      : fIclPrefix(icl),
	fServerName(server), 
	fAckName(fIclPrefix + "_" + fServerName +  "_ACK"),
	fMsgName(fIclPrefix + "_Msg"),
	fAcknowledge(fAckName.c_str(), fgNoAckLink, sizeof(AckStruct), this),
	fMessage(fMsgName.c_str(), fgNoMsgLink, sizeof(MsgStruct), this)
    {
      DebugGuard g(fDebug, "FedClient CTOR: server=\"%s\" ICL=\"%s\"",
		   fServerName.c_str(), fIclPrefix.c_str());
    }
    /** Destructor */
    virtual ~FedClient() {}

    /** Get FW version number 
	@return firmware version number */ 
    int getFWVersion() const { return 0; }
    
    /** Type of flags */
    // typedef unsigned short FlagBits;
    /** Type of data array */
    typedef std::vector<unsigned int> DataArray;
    typedef std::vector<char>         ByteArray;
    
    /** Send a command
	@param size Input data size (in elements).  On return the size of
	the returned data (in elements). 
	@param data Message data.  On return the possible return data. 
	@param flags Message flags 
	@param errorCode On return,  error code if any
	@param status On return, the status
	@return @c true (1) on success, @c false (0) otherwise */
    bool writeReadData(unsigned int&       size, 
		       DataArray&          data, 
		       //dcs::fee::FlagBits& flags, 
		       short&              errorCode, 
		       short&              status)
    {
      Rcuxx::DebugGuard g(fDebug, 
			  "FedClient::writeReadData(%d,%p,%d,%d,%d)", 
			  size,&(data[0]),/*flags,*/errorCode,status);
      int id  = 0;
      int ret = 0;
      fCurId  = 0;  // reset id to filter out stucking acknowledge data
      try {
	Rcuxx::DebugGuard::Message(fDebug, 
				   "FedClient::writeReadData: size is %d", 
				   size);
	if(size == 0)    
	  FedLogging(logLevelWarning, -1, -1, "writeReadData", 
		     "Size is zero");
	// This doesn't work, because reads may not have allocated
	// enough memory when we enter here.  However, that memory
	// will be allocated later, so there's no need to worry
	// really.
	if(data.size()<size)     
	  throw FedException("writeReadData", 
			     "Too little data %d < %d", data.size(), size);

	size_t osize = size * sizeof(DataArray::value_type);
	if (fBuf.size() < osize+20) fBuf.resize(osize+20);
	std::copy(fServerName.begin(), fServerName.end(), fBuf.begin());
	fBuf[fServerName.size()] = '\0';
	memcpy(&(fBuf[20]), &(data[0]), osize);
	

	Rcuxx::DebugGuard::Message(fDebug, "sending command to %s", 
				   fServerName.c_str());
	void* buf = reinterpret_cast<void*>(&(fBuf[0]));
	sendCommandNB("ControlFero", buf, osize+20);
	id = 1;

	Rcuxx::DebugGuard::Message(fDebug, " id returned is %d", id);
	size        = 0;
	errorCode   = 0;
	status      = 0;
	ret = getAcknowledge(size, data, id, errorCode, status);
	
	Rcuxx::DebugGuard::Message(fDebug, "size returned is %d", size);
	if(!ret) 
	  throw FedException("writeReadData", 
			     "ret=%i, getAcknowledge failed", ret);
      }
      catch (std::exception& e) {
	std::cerr << "FedClient::writeReadData(" << size << "," 
		  << &(data[0]) << "," <<  errorCode << "," << status 
		  << "):\n\t" << e.what() << std::endl;
	return false;
      }
      return true;
    }
    /** Get an aknowledgement 
	@param serverName Name of server responding
	@param size Size of data returned
	@param data data returned from server. 
	@param id Identifier obtained by sendCommand
	@param flags Flags used 
	@param errorCode Return error code
	@param status Return status code 
	@return time left before timeout, or 0 on timeout */
    int getAcknowledge(unsigned int&       size, 
		       DataArray&          data, 
		       unsigned int        id, 
		       short&              errorCode, 
		       short&              status)
    {
      Rcuxx::DebugGuard g(fDebug, "FedClient::getAcknowledge(%d,%p,%d,%d,%d)", 
			  size, &(data[0]), id, errorCode, status);
      int maxtime      = 50;
      int timeout      = maxtime;
      int usleepPeriod = 50000;
      int done         = false;
      try {
	while(!done && timeout > 0) {
	  Rcuxx::DebugGuard::Message(fDebug, "Timeout is now %i", timeout);
	  USLEEP(usleepPeriod);
	  timeout--;

	  LockGuard l(fgFedLock);
	  if(id == fCurId) {
	    Rcuxx::DebugGuard::Message(fDebug,"Size returned from id %d is %i",
				       fCurId, fCurSize);
	    
	    size       = fCurSize;
	    errorCode  = fCurErrorCode;
	    status     = fCurStatus;
	    done       = true;
	    if (fCurSize > 0) {
	      if (data.size() < fCurSize) data.resize(fCurSize);
	      std::copy(&(fCurData[0]), &(fCurData[fCurSize]), data.begin());
	    }
	  }
	}
	if(timeout <= 0) 
	  throw FedException("getAcknowledge", 
			     "timeout=%i, no response from FedServer "
			     "within timeout", timeout);
      }
      catch (std::exception& e) {
	std::cerr << e.what() << std::endl;
	return 0;
      }
      return timeout;
    }
    void infoHandler() 
    {
      DimInfo* sender = getInfo();
      DebugGuard g(fDebug, "In FedClient infoHandler %p", sender);
      if (sender == &fAcknowledge) {
	Rcuxx::DebugGuard::Message(fDebug, "Got an ACK on %s",
				   sender->getName());
	int   status    = FEE_ACK_LINK_OK;
	int   errorCode = 0;
	int   size      = sender->getSize();
	char* data      = 0;
	AckStruct* ack = reinterpret_cast<AckStruct*>(sender->getData());
	if (!ack || sender->getSize() <= 0) {
	  std::cerr << "No data from ACK!" << std::endl;
	  status = FEE_ACK_STRANGE_DATA;
	}
	else if (*ack == *fgNoAckLink) {
	  std::cerr << "No link to ACK!" << std::endl;
	  status = FEE_ACK_LOST_LINK;
	}
	else {
	  errorCode  =  ack->errorCode;
	  size       -= sizeof(int);
	  data       =  ack->data;
	  if (size > 0) {
	    std::string server;
	    std::string timestamp;
	    std::string ssize;
            std::stringstream stream(data);
	    std::stringstream::pos_type start = stream.tellg();
	    std::getline(stream, server, ';');
	    std::getline(stream, timestamp, ';');
	    std::getline(stream, ssize, ';');
	    std::stringstream::pos_type end = stream.tellg();
	    size -= int(end - start) + 1;
	    data += int(end - start) + 1;
	    // PRINT_DATA(data, 0, size, 10, 2);
	    std::stringstream sstream(ssize);
	    sstream >> size;


	    DebugGuard::Message(fDebug, "Got reply from %s send at %s with " 
				"error code %d of size %d", 
				server.c_str(), timestamp.c_str(), 
				errorCode, size);

	    // Check the reply source - should this be fAckName?
	    if (server != (fServerName +  "_Acknowledge")) {
	      std::cerr << "Unexpected reply from server " << server 
			<< std::endl;
	      status = FEE_ACK_UNEXPECTED;
	    }
	  }
	}
	acknowledgeHandler(size, data, 1, errorCode, status);
      }
      else if (sender == &fMessage) {
	MsgStruct* ret = reinterpret_cast<MsgStruct*>(sender->getData());
	messageHandler(ret);
      }
      else if (sender)
	FedLogging(logLevelWarning, -1, -1, "writeReadData", 
		   "Unknown DimInfo sender ", sender->getName());
      else 
	FedLogging(logLevelWarning, -1, -1, "writeReadData", 
		   "No sender!!!");
    }
  protected:
    /** Log messages */
    void FedLogging(int         type, 
		    int         ref, 
		    int         id, 
		    const char* func, 
		    const char* format, 
		    ... )
    {
      if (type > logLevel) return;
      
      static char buf[1024];
      va_list ap;
      va_start(ap, format);
      size_t n = vsprintf(buf,format, ap);
      buf[n] = '\0';
      va_end(ap);
      std::cerr << "id=" << id << ": "; 
      switch(type){
      case logLevelDebug:   std::cerr << std::setw(8)  << "Debug"; break;
      case logLevelWarning: std::cerr << std::setw(8)  << "Warning"; break;
      case logLevelError:   std::cerr << std::setw(8)  << "Error"; break;
      }
      std::cerr << "(" << ref << ") from " << func << ": " << buf << std::endl;
    }
    /** Handle acknowledgments
	@param serverName Server 
	@param size data size 
	@param data data
	@param id Id 
	@param flags Flags
	@param errorCode Error code
	@param status status */
    void acknowledgeHandler(unsigned int       size, 
			    char*              data, 
			    unsigned int       id, 
			    short              errorCode, 
			    short              status)
    {
      try {
	Rcuxx::DebugGuard 
	  g(fDebug, "FedClient, handling acknowledge of size %d", size);
	if(!data)         
	  throw FedException("acknowledgeHandler", "no data");
	if(id == 0 && status < 0) {
	  std::cerr << "Received status " << status << " from FedServer\n"
		    << "size      =" << size << "\n"
		    << "id        =" << id << "\n" 
		    << "errorCode =" << errorCode << std::endl;
	}
	else{
	  LockGuard l(fgFedLock);
	  Rcuxx::DebugGuard::Message(fDebug, "Storing data of size %d", size);
	  
	  fCurId        = 1; // id;
	  fCurErrorCode = errorCode;
	  fCurStatus    = status;
	  fCurSize      = size / sizeof(DataArray::value_type);
	  if (fCurSize > 0) {
	    if (fCurData.size() < fCurSize) fCurData.resize(fCurSize);
	    memcpy(&(fCurData[0]), data, size);
	    // PRINT_DATA(fCurData, 0, fCurSize, 6, 8);
	  }
	}
      }
      catch (std::exception& e) {
	std::cerr << e.what() << std::endl;
	// throw;
	return;
      }
    }
    /** Handle messages
	@param msg Message */
    void messageHandler(MsgStruct* msg)
    {
      if (!msg) return;
      if (*msg == *fgNoMsgLink) {
	std::cerr << "No link to MSG! " << std::endl;
	return;
      }
      // cerr << "Received message from " 
      //      << msg.detector << "-detector: " << endl;
      if (msg->isError() || 
	  msg->isFailureAudit() ||
	  msg->isSuccessAudit() ||
	  msg->isDebug()        || 
	  msg->isAlarm()) {
	std::cerr << "Message Type: " << msg->type << ", " 
		  << "Source: "       << msg->detector << ", "
		  << "Date: "         << msg->date << "\n"
		  << "- "             << msg->description << " -\n" 
		  << std::endl;
	throw std::runtime_error("Argh!");
      }
    }
  private:
    /** Current server name */
    std::string  fIclPrefix;
    /** Current server name */
    std::string  fServerName;
    /** Acknowledge name */
    std::string fAckName;
    /** Message name */
    std::string fMsgName;
    /** The acknowledge service */
    DimInfo      fAcknowledge;
    /** The message service */
    DimInfo      fMessage;
    /** Current data size (in bytes) */
    size_t       fCurSize;
    /** Current data */
    DataArray    fCurData;
    /** Current ID */
    unsigned int fCurId;
    /** Current flags */
    //  dcs::fee::FlagBits fCurFlags;
    /** Current error code */
    short        fCurErrorCode;
    /** Current status */
    short        fCurStatus;
    /** Temporary storage for commands, etc. */
    ByteArray    fBuf;
  };
};//BLA

//====================================================================
Rcuxx::Fed::Fed()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Fed constructor");
}

//____________________________________________________________________
Rcuxx::Fed::Fed(const Fed& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Fed copy constructor");
}

//____________________________________________________________________
void
Rcuxx::Fed::ParseUrl(const Url& url) 
{
  DebugGuard g(fDebug, "Parsing node %s", url.Raw().c_str());

  if (!url.Host().empty()) {
    // if (SETENV("DIM_DNS_NODE", url.Host().c_str())) 
    //  throw std::runtime_error("Couldn't set DIM_DNS_NODE");
	DimClient::setDnsNode(url.Host().c_str());
  }
  else if (!getenv("DIM_DNS_NODE")) 
    throw std::runtime_error("DIM_DNS_NODE not set");
  
  DebugGuard::Message(fDebug,"DIM DNS node to use: %s",getenv("DIM_DNS_NODE"));


  if (!url.Path().empty()) {
    std::string path = url.Path();
    if (path[0] == '/') path.erase(0,1);
    if (!path.empty()) fIclPrefix = path;
  }
  else 
    fIclPrefix = getenv("FED_SERVER_NAME");
  DebugGuard::Message(fDebug, "Fed Server name set to \"%s\"", 
		      fIclPrefix.c_str());

  if (!url.Anchor().empty()) fServerName = url.Anchor();
  else                       fServerName = getenv("FEE_SERVER_NAME");
  DebugGuard::Message(fDebug, "Fee Server name set to \"%s\"", 
		      fServerName.c_str());
}

//____________________________________________________________________
Rcuxx::Fed::Fed(const Url& url, bool debug) 
  : fServerName(""), fClient(0)
{
  fDebug = debug;
  DebugGuard g(fDebug, "Opening FED server %s", url.Raw().c_str());

  ParseUrl(url);

  if (fServerName.empty()) throw std::runtime_error("No server name");
  if (fIclPrefix.empty())  throw std::runtime_error("No ICL prefix");
  fClient = new FedClient(fIclPrefix, fServerName);
}

//____________________________________________________________________
int
Rcuxx::Fed::GetFWVersion()
{
  fFirmwareVersion = fClient->getFWVersion();
  return 0;
}
//____________________________________________________________________
Rcuxx::Fed::~Fed()
{
  std::cout << "Closing connection to FED via DTOR" << std::endl;
  delete fClient;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Fed::ExecCommand(unsigned int cmd, 
			unsigned int data)
{
  unsigned int size = 1;
  unsigned int off  = 0;
  return WriteMemory(cmd, off, size, &data);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Fed::ReadMemory(unsigned int base, unsigned int offset,  
		       unsigned int& isize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Fed::ReadMemory(%p,%d, %d, %p) [\"%s\"]", 
		      base,offset,isize,data,fServerName.c_str());
  unsigned int   retval    = 0;
  short          errorCode = 0;
  short          status    = 0;
  unsigned int   size      = 3;
  std::string    server    = fServerName;
  
  try {
    size_t newSize = std::max(3U, isize);
    if (fBuffer.size() < newSize) fBuffer.resize(newSize);
    fBuffer[0] = RCU_READ_MEMBLOCK | isize;
    fBuffer[1] = base + offset;
    fBuffer[2] = CE_CMD_TAILER;
    if (!fClient->writeReadData(size,fBuffer,errorCode,status)) {
      std::cerr << "FedClient::writeReadData returned bad value" 
		<< std::endl;
      return kMessageError;
    }
    
    // Check consistency of size 
    size_t osize = size;
    if (osize != isize) { 
      DebugGuard::Message(fDebug, "FedClient didn't return right amount of "
			  "data (%d != %d)", osize, isize);
      return kNotEnough;
    }
    // Copy to user 
    if (size > isize) {
      DebugGuard::Message(fDebug, "FedClient returned  too much data "
			  "(%d > %d)", size, isize);
      return kNotEnough;
    }

    isize = size;
    std::copy(&(fBuffer[0]), &(fBuffer[size]), data);
  }
  catch (std::exception& e) {
    fClientError = e.what();
    return kClient;
  }
  return retval;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Fed::WriteMemory(unsigned int base, unsigned int offset,  
			unsigned int& osize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Fed::WriteMemory(%p, %d, %d, %p) [\"%s\"]", 
		      base, offset, osize, data,fServerName.c_str());
  unsigned int      retval    = 0;
  short             errorCode = 0;
  short             status    = 0;
  unsigned int      size      = 3 + osize;
  std::string       server    = fServerName;
  try {
    if (fBuffer.size() < size) fBuffer.resize(size);
    fBuffer[0]          = RCU_WRITE_MEMBLOCK | osize;
    fBuffer[1]          = base + offset;
    fBuffer[osize + 2]  = CE_CMD_TAILER;
    std::copy(&(data[0]), &(data[osize]), &(fBuffer[2]));
    if (!fClient->writeReadData(size, fBuffer,errorCode, status))
      return kMessageError;
  }
  catch (std::exception& e) {
    fClientError = e.what();
    return kClient;
  }
  return retval;
}

//____________________________________________________________________
void
Rcuxx::Fed::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}

//____________________________________________________________________
void
Rcuxx::Fed::PrintUrlHelp(std::ostream& o)
{
  o << "\n  FED client interface (InterCom Layer in DCS):\n\n"
    << "\tfed://HOST/FED_SERVER:FEE_SERVER\n\n" 
    << "    where HOST is the address of the DIM DNS, FED_SERVER is"
    << " the Fed Server\n"
    << "    name, and FEE_SERVER is the Fee Server name" << std::endl;
}
//____________________________________________________________________
std::string
Rcuxx::Fed::ErrorString(unsigned int code)
{
  switch (code) {
  case kNoName:
    return std::string("No server name specified");
  case kNoStart:
    return std::string("Couldn't start client");
  case kMessageError:
    return std::string("Message error");
  case kClient:
    return std::string(fClientError);
  }
  return Rcu::ErrorString(code);
}

//====================================================================
//
// EOF
//
