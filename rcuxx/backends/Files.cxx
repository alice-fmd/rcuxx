//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/Files.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of FeeClient back-end 
*/
#include "compat.h"
#include "Files.h"
#include "DebugGuard.h"
#include "Url.h"
#include <stdexcept>
#include <cstdarg>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <numeric>

//====================================================================
Rcuxx::Files::Files()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Files constructor");
}

//____________________________________________________________________
Rcuxx::Files::Files(const Files& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Files copy constructor");
}

//____________________________________________________________________
void
Rcuxx::Files::ParseUrl(const std::string& path) 
{
  DebugGuard g(fDebug, "Parsing node %s", path.c_str());
  std::string::size_type colon = path.find(':');
  std::string in;
  std::string out;
  if (colon != std::string::npos) {
    in  = path.substr(0, colon);
    out = path.substr(colon+1, path.length());
    std::cerr << "Input file is " << in << " output is " << out << std::endl;
  }
  if (!in.empty())  fInput  = new std::ifstream(in.c_str());
  if (!out.empty()) fOutput = new std::ofstream(out.c_str());
  if (!fInput)      fInput  = &std::cin;
  if (!fOutput)     fOutput = &std::cout;
  *fOutput << std::hex << std::setfill('0');
}

//____________________________________________________________________
Rcuxx::Files::Files(const Url& url, bool debug) 
  : fInput(0), fOutput(0)
{
  fDebug = debug;
  DebugGuard g(fDebug, "Opening FILES server %s", url.Raw().c_str());

  ParseUrl(url.Path());

  if (!fInput) throw std::runtime_error("No input");
  if (!fOutput) throw std::runtime_error("No output");

  fFirmwareVersion = RcuLayout::kAncient;
}

//____________________________________________________________________
Rcuxx::Files::~Files()
{
  std::cout << "Closing connection to FILES via DTOR" << std::endl;
  if (fInput) {
    std::ifstream* f = dynamic_cast<std::ifstream*>(fInput);
    if (f) f->close();
    delete fInput;
    fInput = 0;
  }
  if (fOutput) {
    std::ofstream* f = dynamic_cast<std::ofstream*>(fOutput);
    if (f) f->close();
    delete fOutput;
    fOutput = 0;
  }
}

//____________________________________________________________________
unsigned int 
Rcuxx::Files::ExecCommand(unsigned int cmd, 
			  unsigned int data)
{
  unsigned int size = 1;
  unsigned int off  = 0;
  return WriteMemory(cmd, off, size, &data);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Files::ReadMemory(unsigned int base, unsigned int offset,  
			 unsigned int& isize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Files::ReadMemory(%p,%d, %d, %p)", 
		      base,offset,isize,data);
  unsigned int   retval    = 0;

  // Write that we want to read some data 
  // std::cerr << "Writing read instructions" << std::endl;
  *fOutput << "s\n" << std::setfill('0') << std::hex
	   << "r 0x" << std::setw(5) << base+offset 
	   << " 0x" << std::setw(8) << isize << "\n"
	   << "e"  << std::setfill(' ') << std::dec << std::endl;
  
  // Get the initial 's'
  while (true) {
    char c = fInput->peek();
    // std::cerr << "Waiting for 's', got '" << c << "'" << std::endl;
    if (c == 's') {
      fInput->get();
      std::string line;
      std::getline(*fInput, line);
      break;
    }
    USLEEP(2);
  }
  // Get data 
  unsigned n = 0;
  while (true) {
    USLEEP(2);
    
    // Check for end 
    char c = fInput->peek();
    //std::cerr << "Looping until 'e', got '" << c << "'" << std::endl;
    if (c == '\n') {
      fInput->get();
      c = fInput->peek();
    }
    if (c == 'e') {
      // std::cerr << "We got an e!" << std::endl;
      fInput->get();
      std::string line;
      std::getline(*fInput, line);
      break;
    }
    *fInput >> std::hex >> data[n];
    if (fInput->bad()) {
      std::cerr << "Read of line " << n << " failed!" << std::endl;
      retval = 1;
      break;
    }
    n++;
  }
  isize = n;
  return retval;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Files::WriteMemory(unsigned int base, unsigned int offset,  
			  unsigned int& osize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Files::WriteMemory(%p, %d, %d, %p)",
		      base, offset, osize, data);
  unsigned int      retval    = 0;
  *fOutput << 's' << std::endl;
  for (unsigned i = 0; i < osize; i++) {
    *fOutput << "w 0x" << std::setfill('0') << std::hex << std::setw(5) 
	     << base + offset + i << " 0x" << std::setw(8) << data[i] 
	     << std::setfill('0') << std::dec << std::endl;
  }
  *fOutput << 'e' << std::endl;
  return retval;
}

//____________________________________________________________________
void
Rcuxx::Files::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}
//____________________________________________________________________
void
Rcuxx::Files::PrintUrlHelp(std::ostream& o)
{
  o << "\n  Pipes (Special VHDL simulation back-end):\n\n" 
    << "\tpipe:INPUT:OUTPUT\n\n"
    << "    where INPUT is the input pipe, and OUTPUT is the output pipe"
    << std::endl;
}

//____________________________________________________________________
std::string
Rcuxx::Files::ErrorString(unsigned int code)
{
  switch (code) {
  case 1: 
    return std::string("Bad read from input");
  }
  return Rcu::ErrorString(code);
}

//====================================================================
//
// EOF
//
