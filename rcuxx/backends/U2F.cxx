//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/U2F.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:37 2006
    @brief   Implementation of U2F concrete RCU interface 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "U2F.h"
#include "Url.h"
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <cstring>

#ifndef __U2F_H
# include <altro/u2f.h>
#endif
#include "DebugGuard.h"

/** Package id for RCC error handling */
extern int packageId;
/** Trace level for RCC error handling */
extern int traceLevel;

namespace 
{
  bool fDebug = false;
}

//====================================================================
Rcuxx::U2F::U2F()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::U2F constructor");
}

//____________________________________________________________________
Rcuxx::U2F::U2F(const U2F& other)
{
  throw std::runtime_error("Cannot use Rcuxx::U2F copy constructor");
}

//____________________________________________________________________
Rcuxx::U2F::U2F(const Url& url, bool debug) 
{
  fDebug = debug;
  Rcuxx::DebugGuard g(fDebug, "Opening %s", url.Path().c_str());
  const char* node = (url.Path().empty() ? DEFAULT_NODE : url.Path().c_str());
  int ret = U2F_Open(const_cast<char*>(node), &fHandle, HW); 
  // emulate ? SW : HW);
  if (ret) throw std::runtime_error("Failed to open U2F interface");
  fFirmwareVersion = RcuLayout::kU2F;
}

//____________________________________________________________________
Rcuxx::U2F::~U2F()
{
  U2F_Close(fHandle);
}

//____________________________________________________________________
void
Rcuxx::U2F::SetupRegisters()
{
  fMONS = fREGS;
  Rcu::SetupRegisters();
  fMONS = 0;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::ExecCommand(unsigned  int cmd, 
			unsigned  int data) 
{
  Rcuxx::DebugGuard g(fDebug, "ExecCommand(%p)", cmd);
  unsigned int ret = 0;
  ret = U2F_Exec_Command(fHandle, cmd, data);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::ReadMemory(unsigned int base, unsigned int offset, 
		       unsigned int& size, unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "ReadMemory(%p, %d, %d, %p)", 
		      base, offset, size, data);
  unsigned int ret = 0, s = size;
  if      (base == fLayout.kA_IMEM)
    ret = U2F_IMEM_Read(fHandle, size, &s, offset, data);
  else if (base == fLayout.kA_RMEM) 
    ret = U2F_RMEM_Read(fHandle, size, &s, offset, data);
  else if (base == fLayout.kA_PMEM)
    ret = U2F_PMEM_Read(fHandle, size, &s, offset, data); 
  else if (base == fLayout.kA_ACL)
    ret = U2F_ACL_Read(fHandle,  size, &s, offset, data);
  else if (base == fLayout.kA_REGISTERS) {
    // case fLayout.kA_MONITOR:
    Rcuxx::DebugGuard::Message(fDebug, "Reading from register %p", offset);
    ret = U2F_Reg_Read(fHandle, offset, &(data[0]));
  }
  else 
    ret = kUnknownMemory;
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::WriteMemory(unsigned int base, unsigned int offset, 
			unsigned int& size, unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "WriteMemory(%p, %d, %d, %p)", 
		      base, offset, size, data);
  unsigned int ret = 0;
  if (base == fLayout.kA_IMEM)
    ret = U2F_IMEM_Write(fHandle, size, offset, data); 
  else if (base == fLayout.kA_RMEM)
    ret = U2F_RMEM_Write(fHandle, size, offset, data);
  else if (base == fLayout.kA_PMEM)
    ret = U2F_PMEM_Write(fHandle, size, offset, data); 
  else if (base == fLayout.kA_ACL)
    ret = U2F_ACL_Write(fHandle,  size, offset, data); 
  else if (base == fLayout.kA_REGISTERS) {
    Rcuxx::DebugGuard::Message(fDebug, "Writing to register %p", offset);
    ret = U2F_Reg_Write(fHandle, offset, data[0]);
  }
  else 
    ret = kUnknownMemory;
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::ReadDataMemory(unsigned int lbase, unsigned int hbase, 
			   unsigned int offset, unsigned int& size, 
			   unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "ReadDataMemory(%p, %p, %d, %d, %p)", 
		      lbase, hbase, offset, size, data);
  unsigned int ret = 0, s = size;
  if (s < 1) return 0;
  if      (lbase == fLayout.kA_DML1 && hbase == fLayout.kA_DMH1) 
    ret = U2F_DM1_Read(fHandle, size, &s, offset, data);
  else if (lbase == fLayout.kA_DML2 && hbase == fLayout.kA_DMH2) 
    ret = U2F_DM2_Read(fHandle, size, &s, offset, data);
  else 
    ret = kUnknownMemory;
  size = s;
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::WriteDataMemory(unsigned int lbase, unsigned int hbase, 
			    unsigned int offset, unsigned int& size, 
			    unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "WriteDataMemory(%p, %p, %d, %d, %p)", 
		      lbase, hbase, offset, size, data);
  unsigned int ret = 0;
  if      (lbase == fLayout.kA_DML1 && hbase == fLayout.kA_DMH1) 
    ret = U2F_DM1_Write(fHandle, offset, size, data);
  else if (lbase == fLayout.kA_DML2 && hbase == fLayout.kA_DMH2) 
    ret = U2F_DM2_Write(fHandle, offset, size, data);
  else 
    ret = kUnknownMemory;
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}


/** IF the connection isn't open yet */
#define U2F_NOT_OPEN "No connection to U2F"
//____________________________________________________________________
std::string
Rcuxx::U2F::ErrorString(unsigned int code)
{
  if (code >= kNotEnough) return Rcu::ErrorString(code);
  err_pack err = code;
  err_str  pid;
  err_str  str;
  if (code == U2F_NOTOPEN) 
    strcpy(str, U2F_NOT_OPEN);
  else 
    U2F_err_get(err, pid, str);
  return std::string(str);
}
//____________________________________________________________________
void
Rcuxx::U2F::PrintUrlHelp(std::ostream& o)
{
  o << "\n U2F interface (USB test card):\n\n"
    << "\tDEVICE\n" 
    << "\tusb:DEVICE\n" 
    << "\tu2f:DEVICE\n\n" 
    << "    where DEVICE is something like '/dev/altro0'" << std::endl;
}

//____________________________________________________________________
void
Rcuxx::U2F::SetDebug(int id, int lvl) 
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case 1:        packageId = P_ID_U2F;   traceLevel = lvl; break;
  case 2:        packageId = P_ID_ALTRO; traceLevel = lvl; break;
  case kBackend: fDebug = true;  break;
  default:       break;
  }
}

//====================================================================
//
// EOF
//
