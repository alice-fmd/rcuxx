//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/Echo.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of EchoClient back-end 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "rcuxx/backends/Echo.h"
#include "rcuxx/Url.h"
#include "rcuxx/DebugGuard.h"
#include "rcuxx/rcu/RcuRMEM.h"
#include "rcuxx/rcu/RcuIMEM.h"
#include <stdexcept>
#include <cstdarg>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

//====================================================================
namespace 
{
  bool fDebug = false;
}

//====================================================================
Rcuxx::Echo::Echo()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Echo constructor");
}

//____________________________________________________________________
Rcuxx::Echo::Echo(const Echo& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Echo copy constructor");}


//____________________________________________________________________
Rcuxx::Echo::Echo(const Url& url, bool debug) 
  : fData(0x8200)
{
  fDebug = debug;
  DebugGuard g(fDebug, "Opening ECHO server %s", url.Raw().c_str());
  fFirmwareVersion = 0x250510; // k260210;
}

//____________________________________________________________________
int
Rcuxx::Echo::GetFWVersion()
{
  return 0; 
}
//____________________________________________________________________
Rcuxx::Echo::~Echo()
{
  DebugGuard g(fDebug, "Echo::~Echo");
}

//____________________________________________________________________
bool
Rcuxx::Echo::HandleOption(const std::string& opt, const std::string& arg)
{
  if      (opt == "quiet")   fVerbose = false;
  else if (opt == "verbose") fVerbose = true;
  else  
    return Rcu::HandleOption(opt, arg);
  return true;
}


//____________________________________________________________________
unsigned int 
Rcuxx::Echo::ExecCommand(unsigned int cmd, 
			 unsigned int data)
{
  DebugGuard g(fDebug, "Echo::ExecCommand(0x%x,0x%x)", cmd, data);
  if (fVerbose) 
    std::cout << "Executing command 0x" << std::hex << cmd << std::endl;	

  if (cmd == fLayout.kC_EXEC && fLayout.fVersion >= RcuLayout::k000408) {
    if (fVerbose) 
      std::cout << "Populating RMEM with results" 
		<< std::hex << std::setfill('0') << std::endl;
    RcuMemory::Cache_t& imem = fIMEM->Data();
    
    // Loop over IMEM and make results
    bool atend = false;
    for (size_t i = 0; i < imem.size() && !atend; i++) {
      unsigned int what1 = (imem[i] & 0xf00000);
      unsigned int what2 = (imem[i] & 0xff0000);
      unsigned int idx   = fLayout.kA_RMEM + i;
      if      (what1 == fLayout.kIMEM_FEC_WR1)
	fData[idx] = fLayout.kRMEM_FEC_WR | (imem[i] & 0xfffff);
      else if (what1 == fLayout.kIMEM_FEC_RD1) 
	fData[idx] = fLayout.kRMEM_FEC_RD | (imem[i] & 0xfffff);
      else if (what1 == fLayout.kIMEM_FEC_CMD1) 
	fData[idx] = fLayout.kRMEM_FEC_CMD | (imem[i] & 0xfffff);
      else if (what2 == fLayout.kIMEM_STOP) {
	fData[idx] = fLayout.kRMEM_RCU_STOP;
	atend      = true;
      }
      else if (what2 == fLayout.kIMEM_END) {
	fData[idx] = fLayout.kRMEM_RCU_END;
	atend      = true;
      }
      else {
	std::cerr << "Unknown IMEM instruction: 0x" << std::hex << what1 
		  << std::dec << std::endl;
	fData[idx] = (0xf << 21) | (1 << 20);
      }
      if (fVerbose) { 
	if (i % 4 == 0) 
	  std::cout << "  0x" << std::setw(4) << idx << ":";
	std::cout << " 0x" << std::setw(8) << fData[idx] << std::flush;
	if (i % 4 == 3) std::cout << std::endl;
      }
    }
    if (fVerbose) 
      std::cout << std::setfill(' ') << std::dec << std::endl;
  }
  
  return 0;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Echo::ReadMemory(unsigned int base, unsigned int offset,  
		       unsigned int& isize, unsigned int* data)
{
  DebugGuard g(fDebug, "Echo::ReadMemory(0x%x,%d,%d,%p)", 
	       base, offset, isize, data);
  size_t start = base+offset;
  size_t end   = base+offset+isize;
  if (fVerbose) {
    std::cout << "Reading " << isize << " words from memory at 0x" 
	      << std::hex << base << " starting at " << offset 
	      << std::setfill('0') << std::endl;
    size_t j = 0;
    for (size_t i = start; i < end; i++, j++) { 
      if (j % 4 == 0) 
	std::cout << "  0x" << std::setw(4) << i << ":";
      std::cout << " 0x" << std::setw(8) << fData[i];
      if (j % 4 == 3) std::cout << std::endl;
    }
    std::cout << std::setfill(' ') << std::dec << std::endl;
  }
  std::copy(&(fData[start]), &(fData[end]), data);
  return 0;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Echo::WriteMemory(unsigned int base, unsigned int offset,  
			unsigned int& osize, unsigned int* data)
{
  DebugGuard g(fDebug, "Echo::WriteMemory(%p,%d,%d,%p)",
	       base,offset,osize,data);
  size_t start = base+offset;
  size_t end   = base+offset+osize;
  std::copy(data, &(data[osize]), &(fData[start]));
  if (fVerbose) {
    std::cout << "Writing " << osize << " words to memory at 0x" 
	      << std::hex << base << " starting at " << offset 
	      << std::setfill('0') << std::endl;
    size_t j = 0;
    for (size_t i = start; i < end; i++, j++) { 
      if (j % 4 == 0) 
	std::cout << "  0x" << std::setw(4) << i << ":";
    std::cout << " 0x" << std::setw(8) << fData[i];
    if (j % 4 == 3) std::cout << std::endl;
    }
    std::cout << std::setfill(' ') << std::dec << std::endl;
  }
  return 0;
}

//____________________________________________________________________
void
Rcuxx::Echo::PrintUrlHelp(std::ostream& o)
{
  o << "\n  Echo (Debug backend that echos what's going on):\n\n"
    << "\techo:[?OPTIONS]\n\n"
    << "    where OPTIONS is one or more of 'fwvers=VERSION', separated by"
    << " an '&'.\n"
    << "    VERSION is the RCU firmware version" << std::endl;
}
//____________________________________________________________________
void
Rcuxx::Echo::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}


//====================================================================
//
// EOF
//
