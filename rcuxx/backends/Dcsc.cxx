//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/Dcsc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of DcscClient back-end 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "rcuxx/backends/Dcsc.h"
#include "rcuxx/Url.h"
#include "rcuxx/DebugGuard.h"
#include "rcuxx/rcu/RcuRMEM.h"
#include "rcuxx/rcu/RcuIMEM.h"
#include <stdexcept>
#include <cstdarg>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <dcsc/dcscMsgBufferInterface.h>
#include <cstring>
#include <cerrno>

//====================================================================
namespace 
{
  bool fDebug = false;
}

//====================================================================
Rcuxx::Dcsc::Dcsc()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Dcsc constructor");
}

//____________________________________________________________________
Rcuxx::Dcsc::Dcsc(const Dcsc& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Dcsc copy constructor");}


//____________________________________________________________________
Rcuxx::Dcsc::Dcsc(const Url& url, bool debug) 
  : fIsOpen(false),
    fUseSingle(false)
{
  fDebug = debug;
  DebugGuard g(fDebug, "Opening DCSC server %s", url.Raw().c_str());

  int ret = initRcuAccess(NULL);
  DebugGuard::Message(fDebug, "Init of RCU acccess returned %d", ret);
  if (ret < 0) throw std::runtime_error("Failed to open RCU dcsc access");
  fIsOpen = true;

  unsigned int dcsc_options = (PRINT_RESULT_HUMAN_READABLE          |
			       (debug ? PRINT_COMMAND_BUFFER : 0) |
			       (debug ? PRINT_RESULT_BUFFER  : 0) |
			       (debug ? PRINT_COMMAND_RESULT : 0) |
			       (debug ? PRINT_SPLIT_DEBUG    : 0));
  setDebugOptions(dcsc_options);

  fFirmwareVersion = 0; // RcuLayout::k000408;
}

//____________________________________________________________________
int
Rcuxx::Dcsc::GetFWVersion()
{
  if (fFirmwareVersion != 0) return fFirmwareVersion;
  DebugGuard g(fDebug, "Get firmware version");

  unsigned int  ret    = 0;
  unsigned int  addr[] = { 0x5106, 0x8008, 0x8006, 0 };
  unsigned int* p      = addr;
  while(*p) { 
    g.Message(fDebug, "Try to get FW version from 0x%04x", *p);
    unsigned int isize = 1;
    ret = ReadMemory(*p, 0u, isize, &fFirmwareVersion);
    g.Message(fDebug, "Got back 0x%06x (return code %d)", 
	      fFirmwareVersion, ret);
    if (ret == 0 && fFirmwareVersion != 0) break;
    p++;
  }
  return ret;
}
//____________________________________________________________________
Rcuxx::Dcsc::~Dcsc()
{
  DebugGuard g(fDebug, "Dcsc::~Dcsc");
  if (!fIsOpen) return;
  releaseRcuAccess();
  fIsOpen = false;
}

//____________________________________________________________________
bool
Rcuxx::Dcsc::HandleOption(const std::string& opt, const std::string& arg)
{
  if      (opt == "quiet")   fVerbose = false;
  else if (opt == "verbose") fVerbose = true;
  else if (opt == "single")  fUseSingle = true;
  else  
    return Rcu::HandleOption(opt, arg);
  return true;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Dcsc::ExecCommand(unsigned int cmd, 
			 unsigned int data)
{
  DebugGuard g(fDebug, "Dcsc::ExecCommand(0x%x)", cmd);
  if (fVerbose) 
    std::cout << "Executing command 0x" << std::hex << cmd << std::endl;	

  int ret = rcuSingleWrite(cmd, data);
  
  return DecodeError(ret);
}
//____________________________________________________________________
unsigned int 
Rcuxx::Dcsc::DecodeError(int ret)
{
  // If we get a positive number, return true 
  if (ret >= 0) return 0;
  switch (-ret) { 
  case ETIMEDOUT: return kNoAcknowledge;
  case EBADFD:    return kNotActive;
  case EIO:       return kNoAcknowledge;
  case EMISS_MARKER: return kInconsistentSizes;
  case ENOTARGET:    return kNotActive;
  case ENOBUSGRANT:  return kNotActive;
  case EMISS_ENDMARKER: return kNotEnough;
  case EFAULT:          return kUnknownMemory;
  case EINVAL:          return kUnknownMemory;
  }
  return -ret;
}
//____________________________________________________________________
unsigned int 
Rcuxx::Dcsc::ReadMemory(unsigned int base, unsigned int offset,  
			unsigned int& isize, unsigned int* data)
{
  DebugGuard g(fDebug, "Dcsc::ReadMemory(0x%x,%d,%d,%p)", 
	       base, offset, isize, data);
  size_t start = base+offset;
  size_t end   = base+offset+isize;

  int ret = 0;
  if (!fUseSingle) 
    ret = rcuMultipleRead(start, isize, data);
  else 
    for (unsigned int addr = start, i = 0; addr < end; addr++, i++) 
      ret = rcuSingleRead(addr, &(data[i]));
  return DecodeError(ret);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Dcsc::WriteMemory(unsigned int base, unsigned int offset,  
			unsigned int& osize, unsigned int* data)
{
  DebugGuard g(fDebug, "Dcsc::WriteMemory(%p,%d,%d,%p)",
	       base,offset,osize,data);
  size_t start = base+offset;
  size_t end   = base+offset+osize;

  int ret = 0;
  if (!fUseSingle) 
    ret = rcuMultipleWrite(start, data, osize, 4);
  else 
    for (unsigned int addr = start, i = 0; addr < end; addr++, i++) 
      ret = rcuSingleWrite(start+i, data[i]);
  return DecodeError(ret);
}

//____________________________________________________________________
void
Rcuxx::Dcsc::PrintUrlHelp(std::ostream& o)
{
  o << "\n  Dcsc (Debug backend that dcscs what's going on):\n\n"
    << "\tdcsc:[?OPTIONS]\n\n"
    << "    where OPTIONS is one or more of 'fwvers=VERSION', 'single', "
    << "    separated by an '&'.\n"
    << "    VERSION is the RCU firmware version" << std::endl;
}
//____________________________________________________________________
void
Rcuxx::Dcsc::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}


//====================================================================
//
// EOF
//
