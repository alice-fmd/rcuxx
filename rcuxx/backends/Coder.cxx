//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/Coder.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of FeeClient back-end 
*/
#include "rcuxx/compat.h"
#include "rcuxx/backends/Coder.h"
#include "rcuxx/DebugGuard.h"
#include "rcuxx/Url.h"
#include <stdexcept>
#include <cstdarg>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <numeric>
#ifdef HAVE_FEECLIENT_RCU_ISSUE_H
# include <feeclient/rcu_issue.h>
#else 
# include "rcuxx/backends/rcu_issue.h"
#endif 

// Assign the static instance to our class. 
// CommandCoderBase* CommandCoderBase::instance = new CoderClient;
    

//====================================================================
Rcuxx::Coder::Coder()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Coder constructor");
}

//____________________________________________________________________
Rcuxx::Coder::Coder(const Coder& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Coder copy constructor");
}

//____________________________________________________________________
Rcuxx::Coder::Coder(const Url& url,bool debug)
{
  fDebug      = debug;
  fSize       = 0;
  fUseFee     = true;
  fLastIMEM   = 0;
  fBundleIMEM = false;
  DebugGuard g(fDebug, "Opening CODER server %s", url.Raw().c_str());

  int fwvers = 0;
  const Url::StringVector& opts = url.Options();
  for (Url::StringVector::const_iterator i = opts.begin(); 
       i != opts.end(); ++i) {
    std::string::size_type eq = i->find('=');
    std::string opt = i->substr(0, eq);
    std::string arg = i->substr(eq+1, i->size()-eq-1);
    DebugGuard::Message(fDebug, "Option %s (%s %s)", (*i).c_str(), 
			opt.c_str(), arg.c_str());
    if (opt == "target") {
      if      (arg == "fee") fUseFee= true;
      else if (arg == "ddl") fUseFee= false;
      else if (arg == "fed") fUseFee= true;
    }
    else if (opt == "imem") {
      fBundleIMEM = (arg == "bundle");
    }
  }
}

//____________________________________________________________________
Rcuxx::Coder::~Coder()
{
  std::cout << "Closing connection to CODER via DTOR" << std::endl;
}

//____________________________________________________________________
void
Rcuxx::Coder::StartBlock()
{
  // We don't really need to clear the block.  After all, our
  // fSize member keeps check of how much we need to write, and
  // the next block will overwrite the old data anyways. 
  // fData.clear();
  fBlock.resize(fLayout.kS_IMEM);
  DebugGuard g(fDebug, "StartBlock: Making block (usefee=%s)",
	       fUseFee ? "yes" : "no");
  fSize     = 0;
  if (fUseFee) {
    Push(FEE_CONFIGURE);
    Push(1 << 12);  // Always for the RCU
    Push(0);
  }
}

//____________________________________________________________________
int
Rcuxx::Coder::EndBlock()
{
  // Here, we should end the code block with whatever we need. 
  DebugGuard g(fDebug, "EndBlock: Making block (usefee=%s)",
	       fUseFee ? "yes" : "no");
  if (fUseFee) {
    FinishIMEM();
    Push(FEE_VERIFICATION);
    Push(CE_CMD_TAILER);
    Push(FEE_CONFIGURE_END);
    Push(1 << 12); // Always for the RCU
    // Set the payload size
    fBlock[0] = (FEE_CONFIGURE | ((fSize - 1) & 0xFFFF));
    DebugGuard::Message(fDebug, "payload is %d", fSize-1);
  }
  return fSize;
}

//____________________________________________________________________
void
Rcuxx::Coder::FinishIMEM()
{
  if (!fBundleIMEM || fLastIMEM <= 0) return;
  // Push a stop 
  Push(0x390000);
  // Adjust the size of the payload. We have writen a total of
  // fLastIMEM words, so we need to modify the word 2 times before
  // that word.  That is, the last word is fBlock[fSize-1], and
  // subtracting (fLastIMEM+1) from that gives us
  // fBlock[fSize-1-fLastIMEM-2]
  // std::cout << "adjusting size @ " << fSize-1-fLastIMEM-2 << " ... "
  //           << std::flush;
  fBlock[fSize-1-fLastIMEM-2] = RCU_WRITE_MEMBLOCK + fLastIMEM + 1;
  // Push a tRailer 
  Push(CE_CMD_TAILER);
  // Reset counter 
  fLastIMEM = 0;
  // Push an exec 
  ExecCommand(fLayout.kC_EXEC);
}

  
//____________________________________________________________________
void
Rcuxx::Coder::Push(unsigned int w)
{
  // Resize if needed (double the size). 
  if (fSize >= int(fBlock.size())) fBlock.resize(2 * fSize);
  fBlock[fSize] = w;
  fSize++;
}




//____________________________________________________________________
unsigned int 
Rcuxx::Coder::ExecCommand(unsigned int cmd, 
			  unsigned int data)
{
  unsigned int size = 1;
  unsigned int off  = 0;
  if (fBundleIMEM && cmd < fLayout.kS_IMEM && fLastIMEM > 0) return 0;
  return WriteMemory(cmd, off, size, &data);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Coder::ReadMemory(unsigned int base, unsigned int offset,  
			 unsigned int& isize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Coder::ReadMemory(%p,%d, %d, %p)", 
		      base,offset,isize,data);
  std::cerr << "Cannot read from coder - not connected!" << std::endl;
  return 0;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Coder::WriteMemory(unsigned int base, unsigned int offset,  
			  unsigned int& osize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Coder::WriteMemory(0x%04x, %d, %d, %p)",
		      base, offset, osize, data);
  if (fBundleIMEM) {
    if (base == fLayout.kA_IMEM) {
      // Check if we would overflow the IMEM, and if so, flush out our
      // current commands and execute them. 
      if (fLastIMEM + osize >= fLayout.kS_IMEM) FinishIMEM();

      // In cast this is the first of the IMEM block, we writeo out the
      // header 
      if (fLastIMEM == 0) {
	// std::cout << "Header @ " << fSize << " ... " << std::flush;
	Push(RCU_WRITE_MEMBLOCK | osize);
	Push(base + offset);
      }

      // If the last word is a stop command, ignore it. 
      if (data[osize-1] == 0x390000)  osize--;
      // OK, we can write more stuff in IMEM, so we do that, and
      // increase our counter 
      for (unsigned int i = 0; i < osize; i++) Push(data[i]);
      fLastIMEM += osize;
      return 0;
    }
    else if (fLastIMEM > 0) FinishIMEM();
    // std::cout << "Done" << std::endl;
  }
  Push(RCU_WRITE_MEMBLOCK | osize);
  Push(base + offset);
  for (unsigned int i = 0; i < osize; i++) Push(data[i]);
  Push(CE_CMD_TAILER);
  return 0;
}

//____________________________________________________________________
void
Rcuxx::Coder::PrintUrlHelp(std::ostream& o)
{
  o << "\n  Coder (Command coder used by InterCom Layer):\n\n" 
    << "\tcoder:[?OPTIONS]\n\n"
    << "    where OPTIONS is one or more of 'target=TARGET' and"
    << " 'fwvers=VERISON'\n"
    << "    separated by an '&'. TARGET is the target type (fee,ddl,fed), and\n"
    << "    VERSION is the RCU firmware version"
    << std::endl;
}
//____________________________________________________________________
void
Rcuxx::Coder::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}

//====================================================================
//
// EOF
//
