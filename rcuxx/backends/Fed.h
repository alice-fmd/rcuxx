// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:04 2006
    @brief   Declaration of Fed interface to RCU's
*/
#ifndef RCUXX_FED_H
#define RCUXX_FED_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

namespace Rcuxx 
{
  class FedClient;
  //====================================================================
  /** @class Fed 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the FEE (USB to Front end)
      driver and library. 
      @ingroup rcuxx_backend
  */
  class Fed : public Rcu
  {
  public:
    enum {
      kNoName            = 1,
      kNoStart, 
      kMessageError,
      kClient
    };
    /** Destructor */
    ~Fed();
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    std::string ErrorString(unsigned int code);
    /** Execute a command 
	@param cmd Command to execute 
	@param data Argument to command - if any
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd,
				     unsigned int data=0);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
  protected:
    friend class Rcu;
    /** Create the Fed interface
	@param url Connection url
	@param debug debug flag */
    Fed(const Url& url, bool debug=false);
    /** Default constructor */
    Fed();
    /** Copy constructor */
    Fed(const Fed& other);
    /** Initialize backend 
	@return 0 on success, error code otherwise */
    int GetFWVersion();
    /** Server name */
    std::string   fServerName;
    /** Server name */
    std::string   fIclPrefix;
    /** Client */
	FedClient* fClient;
    /** Last client error */
    std::string   fClientError;
    /** Buffer */
    std::vector<unsigned int> fBuffer;
    /** Function to parse the passed URL 
	@param url the URL */
    void ParseUrl(const Url& url);
    /** 
     * Print help on how to connect to this backend
     * 
     * @param o Output stream
     */    
    static void PrintUrlHelp(std::ostream& o);
  };
}

#endif

//====================================================================
//
// EOF
//
