//// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/backends/Rorc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of RorcClient back-end 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "Rorc.h"
#include "Url.h"
#include "DebugGuard.h"
#include "rcu/RcuCommand.h"
#include "Register.h"
#include "rcu/RcuRegister.h"
// #include <iostream>
// This is needed, because `rorc_lib.h' does not have 
// the necessary protection. 
extern "C" {
#ifdef DATE_USE_FEC2RORC
# define OUR_VERSION VERSION
# undef VERSION
# include <fec2rorc_lib.h>
#else
# ifndef _RORC_LIB_H_
#  include <rorc_lib.h>
# endif
# ifndef _RORC_DDL_H_
#  include <rorc_ddl.h>
# endif
# define MAX_PHYSMEM     (1024*1024*8)
# define SEGMENT_SIZE    (1024*1024*8)
# define DDL_PARAM_MASK  0x7FFFF
#endif
};
#include <ddl_def.h>
#define RORC_DELAY 200
#define DATE_ENC_VERSION(X,Y,Z) (((X*1000)+Y)*1000+Z)
// #error DATE_VERSION
// #error DATE_ENC_VERSION(7,0,0)
#if DATE_VERSION < DATE_ENC_VERSION(7,0,0)
# if SIZEOF_UNSIGNED_LONG == SIZEOF_UNSIGNED_INT
#  define DATA_CAST (unsigned long*)
# else 
#  define DATA_CAST (unsigned int*)
# endif
#else
# define DATA_CAST
#endif

/** Max size of physical memory */
#include <stdexcept>
#include <cstdarg>
#include <string>
#include <cassert>
#include <iostream>
#include <algorithm>

// #include <sstream>
//====================================================================
/** Parameter mask */
#define RCU_WRD_RD     		0x20000 // 0x5000
#define SIBSOBERR            	0x10000
#define SIBSOBSTATUS         	0x0000
#define SIFSTATUS		0x0000
#define SIFERRORS		0x10000
#define M_SIFSTATUS 		0x73f0
#define M_SIFERRORS 		0x3f
#define SIFRESET      		0x00001
#define RESETALL      		0x00002
#define RCURESET      		0x00004
#define FECRESET      		0x00008
#define CLEARSIB      		0x00010
#define CLEARSOB      		0x00020
#define CLEARERR      		0x10000
#define SETBLREG      		0x20000
#define SIBTIMEOUT		0xFFFF
#define MAX_POLL_STATUS_REG   	100
namespace 
{
  bool fDebug = true;

  struct to_lower 
  {
    char operator()(char c) const { return std::tolower(c); }
  };

  std::string& downcase(std::string& s)
  {
    std::transform(s.begin(), s.end(), s.begin(),  to_lower());
  }
}

//==================================================================== 
namespace Rcuxx
{    
  //====================================================================
  /** 
   * @class RorcClient
   * @brief Interface to a memory via RORC layer
   * @ingroup rorcxx
  */
  class RorcClient 
  {
  public:
    enum {
      Rorc   = 0x1, 
      Siu    = 0x2, 
      Diu    = 0x4
    };
    
    /** 
     * Constructor  
     *
     * Make client 
     * 
     * @param minor Device minor number 
     * @param channel DDL channel on device 
     */    
    RorcClient(int minor, int channel) 
      : fMinor(minor), 
	fChannel(channel), 
	fSerial(-1),
	fRevision(-1),
#ifdef DATE_USE_FEC2RORC
	fKey(0),
#else 
	fUsTimeout(100*DDL_RESPONSE_TIME), 
	fIsOpen(false),
# ifdef DATE_PHYSMEM_FD
	fPhysMem(-1),
# endif 
	fUserAddress(0), 
	fPhysMemSize(0),
	fPhysMemOffset(0),
	fNReply(0),
#endif
	fErrStr("")
    {
    }
    /** 
     * Constructor 
     *
     * Make the client 
     * 
     * @param serial    Serial number of RORC
     * @param revision  Revision number of RORC
     * @param channel   Channel number of RORC
     */
    RorcClient(int revision, int serial, int channel)
      : fMinor(-1), 
	fChannel(channel), 
	fSerial(serial),
	fRevision(revision),
#ifdef DATE_USE_FEC2RORC
	fKey(0),
#else
	fUsTimeout(100*DDL_RESPONSE_TIME), 
	fIsOpen(false),
# ifdef DATE_PHYSMEM_FD
	fPhysMem(-1),
# endif 
	fUserAddress(0), 
	fPhysMemSize(0),
	fPhysMemOffset(0),
	fNReply(0),
#endif 
	fErrStr("")
    {
    }
    /** Destructor  */
    virtual ~RorcClient() 
    {
      Close();
    }
    virtual rorc_pci_dev_t* GetDevice()
    {
#ifdef DATE_USE_FEC2RORC
      return &(fKey->rorc);
#else
      return &fRorc;
#endif
    }
    /**
     * Reset RORC, SIU, and/or DIU 
     *
     * @param opts Which parts to reset. Bit mask of 
     *  - 0x1   Reset RORC 
     *  - 0x2   Reset SIU
     *  - 0x4   Reset DIU
     *  - 0xF   Do everything in one go
     * @param tries  Number of tries 
     *
     * @return true on success 
     */
    virtual bool Reset(unsigned short opts=Rorc|Siu|Diu,
		       unsigned short tries=3)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::Reset(0x%x,%d)", opts, tries);
      for (unsigned short i = 0; i < tries; i++) {
	if (DoReset(opts)) return true;
      }
      return true;
    }
    /**
     * @param opts Which parts to reset. Bit mask of 
     *  - 0x1   Reset RORC 
     *  - 0x2   Reset SIU
     *  - 0x4   Reset DIU
     *  - 0xF   Do everything in one go
     *
     * @return true on success 
     */
    virtual bool DoReset(unsigned short opts=Rorc|Siu|Diu)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::DoReset(0x%x)", opts);
      rorc_pci_dev_t dev; // GetDevice();
      if (fRevision > 0) {
	if (rorcFind(fRevision, fSerial, &fMinor) != RORC_STATUS_OK) {
	  std::cerr << "DoReset: Cannot find minor number for revision="
		    << fRevision << " serial=" << fSerial << std::endl;
	  return false;
	}
      }
      int ret = rorcMapChannel(&dev, fMinor, fChannel);
      if (ret && (ret != RORC_PCI_ERROR)) {
	std::cerr << "DoReset: Failed to open interface" << std::endl;
	return false;
      }

      // if (opts == 0xf) { 
      // 	rorcReset(dev, RORC_RESET_ALL);
      //	return;
      // }
      const unsigned long wait = 1000;
      if (opts & Rorc) {
	usleep(wait);
	rorcReset(&dev, RORC_RESET_RORC);
      }
      if (opts & Diu) {
	usleep(wait);
	rorcReset(&dev, RORC_RESET_DIU);
      }
      if (opts & Siu) {
	usleep(wait);
	rorcReset(&dev, RORC_RESET_SIU);
      }
      if (opts & Siu) {
	usleep(wait);
	rorcReset(&dev, RORC_RESET_SIU);
      }
      if (opts & Diu) {
	usleep(wait);
	rorcReset(&dev, RORC_RESET_DIU);
      }
      if (opts & (Siu|Diu)){
	usleep(wait);
	rorcReset(&dev, RORC_RESET_RORC);
      }

      // Another wait to let things settle 
      usleep(wait);
      
      ret = Status(&dev, opts);

      rorcClose(&dev);

      return (ret == 0);
    }
    /**
     * Read status of DIU and/or SIU 
     *
     * @param opts Which status to read 
     *  - 0x1   RORC status
     *  - 0x2   SIU status
     *  - 0x4   DIU status
     *
     * @return A bit mask of components with bad status 
     */
    virtual unsigned short Status(rorc_pci_dev_t* dev,
				  unsigned short opts=0xf)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::Status(%p,0x%x)", dev, opts);
      if (opts == 0x0) return 0;

      typedef long long int to_t;
      
      to_t            to  = DDL_RESPONSE_TIME * to_t(dev->pci_loop_per_usec);
      unsigned int    ret = 0;
      bool            rrc = true;
      bool            siu = true;
      bool            diu = true;
      if (opts & Rorc) {
	usleep(500000);
	rorcStatus_t status; 
	rorcReadRorcStatus(dev, &status);
	if (status.ccsr & DRORC_STAT_LINK_DOWN      || 
	    status.ccsr & DRORC_STAT_LINK_FULL      ||
	    status.ccsr & DRORC_STAT_CMD_NOT_EMPTY  || 
	    status.ccsr & DRORC_STAT_ERR_NOT_EMPTY  || 
	    status.cerr != 0) {
	  rrc = false;
	}
      }
      if (opts & Siu) { 
	usleep(500000);
        unsigned long status = ddlReadSiu(dev, 0, to);
	if (status == (unsigned long)-1) siu = false;
	else {
	  status &= STMASK;
	  if      (status & ERROR_BIT) siu = false;
	  else if (status & S_OPTRAN)  siu = false;
	}
      }
      if (opts & Diu) { 
	usleep(500000); 
	
	unsigned long status = ddlReadDiu(dev, 0, to); 
	if (status == (unsigned long)-1) {
	  std::cerr << "Failed to read from DIU" << std::endl;
	  diu = false;
	}
	else {
	  status &= STMASK;
	  if  (status & ERROR_BIT) diu = false;
	}
      }


      if (!rrc) {
	std::cerr << "RORC in bad state:\n" 
		  << rorcPrintStatus(dev, 0) << std::flush;
	ret |= Rorc;
      }
      if (!siu) {
	std::cerr << "SIU in bad state:\n" 
		  << ddlPrintStatus(dev, SIU, 0) << std::flush;
	ret |= Siu;
      }
      if (!diu) {
	std::cerr << "DIU in bad state:\n" 
		  << ddlPrintStatus(dev, DIU, 0) << std::flush;
	ret |= Diu;
      }
      // fprintf(stderr, "Status: 0x%02x\n", ret);
      return ret;
    }
    /** 
     * open connection to RORC.  
     * 
     * @return  0 on success, error code otherwise 
     */
    virtual int Open(bool reset, bool hard=true) 
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::Open(reset=%s,hard=%s)",
			  reset ? "true" : "false", 
			  hard  ? "true" : "false");
      if (reset) {
	Rcuxx::DebugGuard::Message(fDebug, "Reseting DDL interface");
	bool ret = Reset(Rorc|Diu|Siu);
	if (!ret && hard) return -1;
	usleep(100000);
      }
      
#ifdef DATE_USE_FEC2RORC
# define PHYSMEM_MINOR 0
      if (fKey && fKey->status == R_OPEN) return 0;
      
      Rcuxx::DebugGuard::Message(fDebug, "Using DATE Fec2RORC library ");
      fKey = get_channel(fRevision > 0 ? fRevision : -1,
			 fRevision > 0 ? fSerial   : fMinor, 
			 fChannel
#if DATE_VERSION > DATE_ENC_VERSION(7,45,9)
			 , PHYSMEM_MINOR
#endif
			 );
      if (fKey->status != R_OPEN) { 
	fKey = 0;
	return -1;
      }
      
      if (reset) { 
	Rcuxx::DebugGuard::Message(fDebug, "Clearing SIB/SOB");
	int ret = ClearSibSob();
	if (!DecodeError(ret, "Open")) { 
	  ret = CheckSibSobErr();
	  if (!DecodeError(ret, "Open") && hard) return -1;
	}
      }
      return 0;
	
#else
      if (fIsOpen) return 0;
      int ret = 0;

      // If minor number not specified, use rorcFind to find the minor
      // number given the revision and serial numbers.
      if (fMinor < 0) {
	if ((ret = rorcFind(fRevision, fSerial, &fMinor) != 0)) {
	  Rcuxx::DebugGuard::Message(fDebug, "Didn't find minor node for "
				     "revision %d, serial %d", 
				     fRevision, fSerial);
	  return ret;
	}
      }
      Rcuxx::DebugGuard::Message(fDebug, "Minor number is %d", fMinor);

      // Open the channel 
      // printf("Opening rorc://%d:%d\n", fMinor, fChannel);
      if ((ret = rorcOpenChannel(&fRorc, fMinor, fChannel))) return ret;
      fRevision = fRorc.rorc_revision;
      // fSerial   = fRorc.rorc_serial;
      fSerial   = rorcSerial(&fRorc).serial;
      fMinor    = fRorc.minor;
      fTimeout  = fUsTimeout * fRorc.loop_per_usec;
      Rcuxx::DebugGuard::Message(fDebug, "Time out set to %dus (=%d*%d)", 
				 fUsTimeout, fRorc.loop_per_usec, fTimeout);

      // Reset the RORC 
      rorcReset(&fRorc, RORC_RESET_RORC);
      

      volatile unsigned long *addr_user;
# ifndef DATE_PHYSMEN_FD
#  define PHYSMEM_MINOR 0
#  define FULL_SIZE 1
      ret = physmemOpen(&fPhysMem, PHYSMEM_MINOR, FULL_SIZE);
      if (ret) return ret;
      
      fUserAddress    = const_cast<volatile unsigned long*>(fPhysMem.addr_user_physmem);
      fPhysMemSize    = fPhysMem.full_size_physmem;
      fPhysMemAddress = fPhysMem.phys_addr_physmem;
      fPhysMemOffset  = (fMinor * MAX_CHANNEL + fChannel) *  MAX_PHYSMEM;
      
# else
      // Open the physmem. 
      ret = physmemOpen(&fPhysMem, &addr_user,
			&fPhysMemAddress, &fPhysMemSize);
      if (ret) return ret;
      fUserAddress = const_cast<long unsigned int*>(addr_user);
      // fPhysMemOffset = (fMinor * MAX_CHANNEL + fChannel) * MAX_PHYSMEM;
      fPhysMemOffset = (fMinor * (2*SEGMENT_SIZE) + (fChannel*SEGMENT_SIZE));
# endif
      Rcuxx::DebugGuard::Message(fDebug, 
				 "Allocated physical memory @ 0x%08lx of size" 
				 " %ld (%ld MB) bytes, user @ 0x%08lx",  
				 fPhysMemAddress, fPhysMemSize,  
				 fPhysMemSize / 1024 / 1024, fUserAddress); 
      if ((fPhysMemOffset + MAX_PHYSMEM) > fPhysMemSize) {
	fprintf(stderr, 
		"Usable memory offset (%ld MB) + size (%ld MB) must be less " 
		"than the physmem area size (%ld MB)\n", 
		long(fPhysMemOffset / 1024 / 1024), 
		long(MAX_PHYSMEM / 1024 / 1024), 
		long(fPhysMemSize / 1024 / 1024));
	return 1;
      }

      fPhysMemAddress += fPhysMemOffset;
      fUserAddress += fPhysMemOffset / 4;
      Rcuxx::DebugGuard::Message(fDebug, 
				 "Physical memory @ 0x%08lx, user @ 0x%08lx",  
				 fPhysMemAddress, fUserAddress); 

      /* reset SIU, RORC, DIU */ 
      rorcReset (&fRorc, RORC_RESET_SIU);  // SIU 
      usleep(RORC_DELAY);
      rorcReset (&fRorc, RORC_RESET_DIU);  // DIU
      usleep(RORC_DELAY);
      rorcReset (&fRorc, RORC_RESET_RORC); // RORC
      usleep(RORC_DELAY);

      fIsOpen = true;
      // Return exit code 
      return ret;
#endif
    }
    /** 
     * close connection to RORC.  
     * 
     * @return  0 on success, error code otherwise 
     */
    virtual int Close() 
    {
#ifdef DATE_USE_FEC2RORC
      DebugGuard g(fDebug, "RorcClient::Close() <%s>", 
		   fKey ? "open" : "closed");
      if (!fKey) return 0;
      if (fKey->status == R_CLOSED || 
	  fKey->status != R_OPEN) { 
	fKey = 0; 
	return 0; 
      }
      int ret = release_channel(fKey);
      fKey = 0;
      if (!DecodeError(ret, "Close")) return ret;
      return 0;
#else 
      DebugGuard g(fDebug, "RorcClient::Close() <%s>", 
		   fIsOpen ? "open" : "closed");
      if (!fIsOpen) return 0;
      // Close the physmem 
# ifndef DATE_OLLD
      physmemClose(fPhysMem);
# else
      physmemClose(fPhysMem, fUserAddress, fPhysMemSize);
# endif
      // Close rorc 
      rorcClose(&fRorc);
      // Flag as closed
      fIsOpen = false;
      // Return 0 for success. 
      return 0;
#endif
    }
    /** 
     * Write @a size words of data to the FEE at the address @a addr.
     *
     * @param addr Address to write to
     * @param size Size of data to write 
     * @param data The data to write
     *
     * @return  0 on success, error code otherwise  
     */
    virtual int WriteBlock(unsigned long addr, unsigned long offset, 
			   int size, unsigned int* data)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::WriteBlock(0x%0x,%d,%p)", 
			  addr, size, data);
#ifdef DATE_USE_FEC2RORC
      int ret = 0;

      // Check SIB/SOB error register 
      ret = CheckSibSobErr();
      if (!DecodeError(ret, "WriteBlock")) return ret;
      
      // Check SIB/SOB status register 
      ret = CheckSibSobStatus(); 
      if (!DecodeError(ret, "WriteBlock")) return ret;
      
      // Read from the fec address - Note, that data _must_ be 32 bit 
      // integers  
      ret = ddl_writeBlock(fKey, DATA_CAST data, size, addr+offset);
      if (ret <= 0 && !DecodeError(ret, "WriteBlock")) return ret;

      return 0;      
#else
      unsigned long  returnPhys = fPhysMemAddress;
      unsigned long* returnAddr = const_cast<unsigned long*>(fUserAddress);
      unsigned long  bufferPhys = returnPhys + 0x40 + DDL_MAX_BYTE + 4;
      unsigned long* bufferAddr = returnAddr + 0x10 + DDL_MAX_WORD + 1;
      int            step       = 0;
      unsigned long  raddr      = addr+offset;      
      unsigned long  tmp        = raddr & DDL_PARAM_MASK;
      const int bufSize=1024;
      static char buffer[bufSize];
      if (tmp != raddr) {
	snprintf(buffer, bufSize, 
		 "In write, address 0x%08lx too large (> 0x%08lx)\n",
		long(raddr), long(DDL_PARAM_MASK));
	fErrStr = buffer;
	return 1;
      }
      
      int ret = 0;
      ret = WaitForSibEmpty();
      if (!DecodeError(ret, "WriteBlock")) return ret;

      // Copy the data from user into physmem 
      // size = std::min(size, DDL_MAX_WORD);
      Rcuxx::DebugGuard::Message(fDebug, "Write %d words", size);
      for (int i = 0; i < size; i++) { 
	bufferAddr[i] = data[i];
	Rcuxx::DebugGuard::Message(fDebug, "\t%4d\t0x%08lx -> 0x%08lx", 
				   i, data[i], bufferAddr[i]);
      }
      ret = ddlWriteDataBlock(&fRorc, bufferPhys, size, returnPhys, 
			      (volatile unsigned int*)returnAddr, 
			      raddr, fTimeout, fStw, 
			      &fNReply, &step);
      if (!DecodeError(ret, "WriteBlock")) return ret;

      ret = WaitForSibEmpty();
      if (!DecodeError(ret, "WriteBlock")) return ret;

      return ret;
#endif
    }
    /** 
     * Send a command @a addr to the FEE
     * 
     * @param addr Command to send 
     *
     * @return 0 on success, error code otherwise 
     */
    virtual int SendCommand(unsigned long addr) 
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::SendCommand(0x%x)", addr);

#ifdef DATE_USE_FEC2RORC
      int ret = ddl_sendCommand(fKey, addr);
      if (!DecodeError(ret, "SendCommand")) return ret;
      usleep(RORC_DELAY);
      return 0;
#else
      int ret    = ddlSendCommandAndWaitReply(&fRorc, FECTRL, addr, 
					      fTimeout, fStw, 1, &fNReply);
      Rcuxx::DebugGuard::Message(fDebug, "SendCommand(0x%x) returned %d", 
				 addr, ret);
      if (!DecodeError(ret, "SendCommand")) return ret;
      return ret;
#endif
    }
    
    /** 
     * Read @a size words of data from Fee at address @a addr. 
     * 
     * @param addr Address to read from 
     * @param size Number of words to read.  On return, this contains
     *        the actual number of words read.  
     * @param data Data read. 
     *
     * @return 0 on success, error code otherwise  
     */
    virtual int ReadBlock(unsigned long addr, unsigned long offset, 
			  int& size, unsigned int* data)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::ReadBlock(0x%0x,%d,%d,%p)", 
			  addr, offset, size, data);
      int ret = 0;
#ifdef DATE_USE_FEC2RORC
      // Check SIB/SOB error register 
      // ret = CheckSibSobErr();
      // if (!DecodeError(ret, "ReadBlock")) return ret;
      
      // Check SIB/SOB status register 
      // ret = CheckSibSobStatus(); 
      // if (!DecodeError(ret, "ReadBlock")) return ret;
      
      // Instruct RCU that we want to read the words 
      ret = SendCommand(RCU_WRD_RD + size);
      if (!DecodeError(ret, "ReadBlock")) return ret;

      // Read from the fec address  - Note, that data _must_ be 32 bit 
      // integers  
      ret = ddl_readBlock(fKey, addr+offset, DATA_CAST data, size);
      if (ret <= 0 && !DecodeError(ret, "ReadBlock")) return ret;
      size = ret;

      return 0;
#else
      // ret = CheckSibSobErr();
      // if (!DecodeError(ret, "ReadBlock")) return ret;
      
      // ret = CheckSibSobStatus();
      // if (!DecodeError(ret, "ReadBlock")) return ret;

      unsigned long    returnPhys = fPhysMemAddress;
      unsigned long*   returnAddr = const_cast<unsigned long*>(fUserAddress);
      rorcReadyFifo_t* rf         = (rorcReadyFifo_t*)(returnAddr);
      unsigned long    dataPhys   = returnPhys + 8; // 0x40/* 8*/;
      unsigned long*   dataAddr   = returnAddr + 2; // 0x10/* 2*/;
      int              step       = 0;
      unsigned long    raddr      = addr+offset;
      unsigned long    tmp        = raddr & DDL_PARAM_MASK;
      static const int bufSize    = 1024;
      static char buffer[128];
      if (tmp != raddr) {
	snprintf(buffer, bufSize, 
		 "In read, address 0x%08lx too large (> 0x%08lx)\n",
		long(addr), long(DDL_PARAM_MASK));
	fErrStr = buffer;
	return 1;
      }

      ret = SendCommand(RCU_WRD_RD + size);
      if (!DecodeError(ret, "ReadBlock")) return ret;

      Rcuxx::DebugGuard::Message(fDebug, 
				 "\tRorc:          %p\n"
				 "\tPhys buffer:   %p\n"
				 "\tReturn buffer: %p\n"
				 "\tRF:            %p\n"
				 "\tAddress:       %p\n"
				 "\tTimeout:       %d\n"
				 "\tSTW:           %p\n"
				 "\tN reply:       %d\n"
				 "\tstep:          %d\n",
				 &(fRorc), 
				 dataPhys, 
				 returnPhys,
				 rf,
				 raddr,
				 fTimeout,
				 fStw,
				 fNReply,
				 step);
        ret = ddlReadDataBlock(&fRorc, dataPhys, returnPhys, rf, 
			     raddr, fTimeout, fStw, &fNReply, &step);
      if (!DecodeError(ret, "ReadBlock")) return ret;

      // Decode error 
      // if (ret == RORC_STATUS_OK) return ret;
      
      // std::ostringstream s(fErrStr);
      if (rf->status && ((rf->status & 0xFF) != DTSW)) {
	sprintf(buffer, "Protocol error: status!=DTSW (0x%x!=0x%x)", 
		int(rf->status & 0xF), int(DTSW));
	fErrStr = buffer;
	return 1;
      }
      if ((rf->length < 0) || (rf->length > DDL_MAX_TX_WORD)) {
	sprintf(buffer, "Protocol error: data length=%ld", rf->length);
	fErrStr = buffer;
	return 1;
      }

      // Copy data from physmem to user
      //  size = std::min(size, rf->length);      
      size = rf->length;
      Rcuxx::DebugGuard::Message(fDebug, "Got %d word%sback", size, 
				 (size == 1 ? " " : "s "));
      for (int i = 0; i < size; i++) {
	data[i] = dataAddr[i];
	Rcuxx::DebugGuard::Message(fDebug, "\t%3d: 0x%08lx (0x%08lx)", 
				   i, dataAddr[i], data[i]);
      }
      return ret;
#endif
    }
    /** 
     * Append error string to error string cache 
     *
     * @param prefix (optional) prefix 
     * @param error  Error string 
     */
    void AppendError(const char* prefix, 
		     const char* error) 
    {
      if (fErrStr.length() > 0) fErrStr.append(", ");
      if (prefix) { 
	fErrStr.append(prefix);
	if (error) fErrStr.append(": ");
      }
      if (error)  fErrStr.append(error);
    }
    /** 
     * Decode an error number, and store result in internal buffer. 
     *
     * @param ret Error code.
     * @param prefix What to put in front.
     *
     * @return @c true if there's an error, @c false otherwise 
     */
    virtual bool DecodeError(int& ret, const char* prefix) 
    {
      if (fErrStr.length() > 0) fErrStr.erase();
      
      if (ret == RORC_STATUS_OK) return true;

      const int bufSize=1024;
      static char buffer[bufSize];
      // std::ostringstream s(fErrStr);

      unsigned int uret = abs(ret);
      if (uret & RORC_STATUS_ERROR)   AppendError(prefix, "Error bit set");
      if (uret & RORC_INVALID_PARAM)  AppendError(prefix, "Invalid parameter");
      if (uret & RORC_LINK_NOT_ON)    AppendError(prefix, "No link");
      if (uret & RORC_CMD_NOT_ALLOWED)AppendError(prefix, "Invalid command");
      if (uret & RORC_NOT_ACCEPTED)   AppendError(prefix, "Not accepted");
      if (uret & RORC_NOT_ABLE)       AppendError(prefix, "Not able to do");
      if (uret & RORC_TIMEOUT)        AppendError(prefix, "Timeout");
      if (uret & RORC_FF_FULL)        AppendError(prefix, "Fifo full");
      if (uret & RORC_FF_EMPTY)       AppendError(prefix, "Fifo empty");
      if (uret & RORC_NOT_ENOUGH_REPLY)AppendError(prefix,"Not enough replies");
      if (uret & RORC_TOO_MANY_REPLY) AppendError(prefix, "Too many replies");
      if (uret & RORC_PCI_ERROR)      AppendError(prefix, "PCI error");
      if (uret == SIBTIMEOUT) {
	AppendError(prefix, "Timeout while waiting for SIB to be empty");
      }
#ifdef DATE_USE_FEC2RORC
      if (fKey && fKey->status == R_OPEN) { 
	for (int i = 0; i < fKey->n_reply; i++) {
	  snprintf(buffer, bufSize, "\n  0x%lx", fKey->stw[i].stw);
	  fErrStr.append(buffer);
	}
      }
#else
      for (int i = 0; i < fNReply; i++) {
	snprintf(buffer, bufSize, "\n  0x%lx", fStw[i].stw);
	fErrStr.append(buffer);
	// s << "\n  0x" << std::hex << fStw[i].stw;
      }
#endif
      Rcuxx::DebugGuard::Message(fDebug, "Error(%3d): - %s -", 
				 ret, fErrStr.c_str());

      // Make the return value positive 
      ret = abs(ret);
      return false;
    }

    /** @return String of last error.  It is empty if there's no error. */
    std::string ErrorString() const { return fErrStr; }
    /** 
     * Send a DDL status (SIF) command
     * 
     * @param cmd Command 
     * 
     * @return negative error code on failure, result otherwise
     */    
    int DdlStatus(int cmd) 
    { 
      Rcuxx::DebugGuard g(fDebug, "RorcClient::DdlStatus(0x%0x)", cmd);
      
#ifdef DATE_USE_FEC2RORC
      int ret = ddl_statusReadout(fKey, cmd);
      DecodeError(ret, "DdlStatus");
      return ret;
#else
      int ret = ddlSendCommandAndWaitReply(&fRorc, 
					   FESTRD,
					   cmd,
					   fTimeout, 
					   fStw, 
					   2,
					   &fNReply);
      Rcuxx::DebugGuard::Message(fDebug,"low-level function returned %d", ret);
      if (!DecodeError(ret, "DdlStatus")) return ret;
      return fStw[0].part.param;
#endif
    }      
    /** 
     * Clear DDL input buffer
     * 
     * 
     * @return  negative error code on failure, result otherwise
     */    
    int ClearSib() 
    { 
      Rcuxx::DebugGuard g(fDebug, "RorcClient::ClearSib()");
      return SendCommand(CLEARSIB); 
    }
    /** 
     * Clear DDL output buffer
     * 
     * 
     * @return  negative error code on failure, result otherwise
     */    
    int ClearSob() 
    { 
      Rcuxx::DebugGuard g(fDebug, "RorcClient::ClearSob()");
      return SendCommand(CLEARSOB); 
    }
    /**
     * Clear SIB/SOB errors 
     * 
     * @return negative error code on failure, result otherwise
     */
    int ClearSibSob()
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::ClearSibSob()");
      return SendCommand(CLEARERR);
    }
    
    /** 
     * Check the DDL input/output buffer errors
     * 
     * 
     * @return  negative error code on failure, result otherwise
     */
    int CheckSibSobErr()
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::CheckSibSobErr()");
      int ret = DdlStatus(SIBSOBERR);
      DecodeSifError(ret);
      return ret;
    }
    /** 
     * Check the DDL input/output buffer status
     * 
     * 
     * @return  negative error code on failure, result otherwise
     */
    int CheckSibSobStatus()
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::CheckSibSobStatus()");
      int ret = 0;
      for (int i = 0; i < MAX_POLL_STATUS_REG; i++) { 
	ret = DdlStatus(SIBSOBSTATUS);
	
	DecodeSifStatus(ret);
	if ((ret  & 0x3ff) == 0x0) return 0;
	usleep(2000);
      }
      return ret;
    }
    /** 
     * Wait for the DDL input buffer to be empty
     * 
     * 
     * @return negative error code on failure, result otherwise 
     */
    int WaitForSibEmpty()
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::WaitForSibEmpty()");
      bool sibNotEmpty = true;
      
      int status = 0;
      int errors = 0;
      int count  = 0;
      while (sibNotEmpty) { 
	if ((status = DdlStatus(SIFSTATUS)) < 0) { 
	  Rcuxx::DebugGuard::Message(fDebug, "SIF status returned %d", status);
	  return status;
	}
	DecodeSifStatus(status);

	sibNotEmpty = (status >> 4) & 0x1;
	
	if (sibNotEmpty) { 
	  errors = DdlStatus(SIFERRORS);
	  Rcuxx::DebugGuard::Message(fDebug, "Waiting for SIB to be empty, "
				     "status=0x%04x, errors=0x%02x", 
				     status & M_SIFSTATUS, 
				     errors & M_SIFERRORS);
	  DecodeSifError(errors);
	}
	
	if (count >= 100) { 
	  Rcuxx::DebugGuard::Message(fDebug,"Timeout in waiting for SIB empty, "
				     "status=0x%04x, errors=0x%02x", 
				     status & M_SIFSTATUS, 
				     errors & M_SIFERRORS);
	  DecodeSifStatus(status);
	  return -SIBTIMEOUT;
	}
	
	// Sleep for a while 
	usleep(2000);
	count++;
      }
      Rcuxx::DebugGuard::Message(fDebug,"SIB status, count=%3d"
				 "status=0x%04x, errors=0x%02x", 
				 count, 
				 status & M_SIFSTATUS, 
				 errors & M_SIFERRORS);
      DecodeSifStatus(status);

      return 0;
    }
    std::string& DecodeSifStatus(unsigned int data) 
    {
      static std::string txt = "";
      if (txt.length() > 0) txt.erase();

      Rcuxx::DebugGuard g(fDebug, "RorcClient::DecodeSifStatus()");
      unsigned int s = (data & 0x39F0);
      for (size_t i = 4; i <= 14; i++) { 
	bool on = (s >> i) & 0x1;
	if (!on) continue;
	switch (i) { 
	case  4: txt = "SIB not empty";          break;
	case  5: txt = "SOB not empty";          break;
	case  6: txt = "SIB full";               break;
	case  7: txt = "SOB full";               break;
	case  8: txt = "SIF bus request";        break;
	case  9: txt = "SIF bus grant";          break;
	case 12: txt = "SIF block write active"; break;
	case 13: txt = "SIB block read active";  break;
	case 14: txt = "SIB read-out active";    break;
	}
      }
      if (!txt.empty()) 
	Rcuxx::DebugGuard::Message(fDebug, txt.c_str());
      return txt;
    }
    std::string& DecodeSifError(unsigned int data) 
    {
      static std::string txt = "";
      if (txt.length() > 0) txt.clear();

      Rcuxx::DebugGuard g(fDebug, "RorcClient::DecodeSifStatus()");
      for (size_t i = 0; i <= 6; i++) { 
	bool on = (data >> i) & 0x1;
	if (!on) continue;
	switch (i) { 
	case 0: txt.append("SIF write error, ");		break;
	case 1: txt.append("SIF read error, ");			break;
	case 2: txt.append("SIF readout error, ");		break;
	case 3: txt.append("DDL error, ");			break;
	case 4: txt.append("SIF write block length error, ");	break;
	case 5: txt.append("SIF write timeout, "); 		break;
	case 6: txt.append("SIF read timeout, ");		break;
	}
      }
      if (!txt.empty()) 
	Rcuxx::DebugGuard::Message(fDebug, txt.c_str());
      return txt;
    }
  protected:
    
    /** Minor device number */
    int fMinor;
    /** Channel number */
    int fChannel;
    /** Serial number */
    int fSerial;
    /** Revision number */
    int fRevision;
#ifdef DATE_USE_FEC2RORC
    /** Fec2Rorc channel key */
    chan_key* fKey;
#else
    /** Timeout in @f$ \mu s@f$ */
    int fUsTimeout;
    /** Whether we're opened yet */
    bool fIsOpen;
    /** Timeout in RORC loops */
    int fTimeout;
    /** Device structure */
    rorc_pci_dev_t fRorc;    
# ifndef DATE_PHYSMEN_FD
    /** Descriptor of physmem */
    physmemHandler_t fPhysMem;
# else
    /** File descriptor for physmem */
    int fPhysMem;
# endif
    /** User address for PhysMem */
    volatile unsigned long* fUserAddress;
    /** PhysMem address */ 
    unsigned long fPhysMemAddress;
    /** Size of PhysMem */
    unsigned long fPhysMemSize;
    /** Offset in PhysMem */
    unsigned long fPhysMemOffset;
    /** Status array */ 
    stword_t fStw[DDL_MAX_REPLY];
    /** Available status words */ 
    int fNReply;
#endif
    /** Last error string */
    std::string fErrStr;
  };

  //==================================================================
  struct SibSobStatus : public RcuRegister
  {
    SibSobStatus(RorcClient* client, RcuMemory& m)
      : RcuRegister(m, 0, "SIBSOB_STATUS", "Status of SIB/SOB", 0, false), 
	fClient(client), 
	fStatus(0)
    {}
    unsigned int Update()
    {
      fStatus = fClient->DdlStatus(SIBSOBSTATUS);
      return 0;
    }
    unsigned int Commit() { return 0; }
    void Print() const
    {
      const std::string& txt = fClient->DecodeSifStatus(fStatus);
      Register::Print();
      std::cout << "  " << txt << std::endl;
    }
    RorcClient* fClient;
    int         fStatus;
  };
  //==================================================================
  struct SibSobError : public RcuRegister
  {
    SibSobError(RorcClient* client, RcuMemory& m)
      : RcuRegister(m, 0, "SIBSOB_ERROR", "Error of SIB/SOB", 0, false), 
	fClient(client), 
	fError(0)
    {}
    unsigned int Update()
    {
      fError = fClient->DdlStatus(SIBSOBERR);
      return 0;
    }
    unsigned int Commit() { return 0; }
    void Print() const
    {
      const std::string& txt = fClient->DecodeSifError(fError);
      Register::Print();
      std::cout << "  " << txt << std::endl;
    }
    RorcClient* fClient;
    int fError;
  };

}


//====================================================================
Rcuxx::Rorc::Rorc()
  : fMinor(-1), 
    fChannel(-1), 
    fRevision(-1), 
    fSerial(-1), 
    fBuffer(0), 
    fNBuffer(0), 
    fClient(0)
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Rorc constructor");
}

//____________________________________________________________________
Rcuxx::Rorc::Rorc(const Rorc& other)
  : fMinor(other.fMinor), 
    fRevision(other.fRevision), 
    fSerial(other.fSerial), 
    fChannel(other.fChannel), 
    fBuffer(0), 
    fNBuffer(0), 
    fClient(other.fClient)
{
  throw std::runtime_error("Cannot use Rcuxx::Rorc copy constructor");
}

//____________________________________________________________________
void
Rcuxx::Rorc::ParseUrl(const std::string& path) 
{
  DebugGuard g(fDebug, "Parsing node %s", path.c_str());
  // std::stringstream s(path);
  size_t i = 0;
  std::vector<int> numbers;
  while (i < path.size() && i != std::string::npos) {
    size_t last = (i == 0 ? i : i + 1);
    i           = path.find(':', last);
    std::string part;
    if (i == std::string::npos) 
      part = path.substr(last);
    else 
      part = path.substr(last, i - last);
    DebugGuard::Message(fDebug, "Part: %s (i=%d, j=%d)", part.c_str(), i, last);
    if (part.empty()) continue;

    int number = 0;
    sscanf(part.c_str(), "%d", &number);
    numbers.push_back(number);
  }
  if (numbers.size() >= 3) { 
    fRevision = numbers[0];
    fSerial   = numbers[1];
    fChannel  = numbers[2];
  }
  else if (numbers.size() == 2) { 
    fMinor   = numbers[0];
    fChannel = numbers[1];
  }
#if 0
  std::string::size_type colon = path.find(':');
  if (colon != std::string::npos) {
    std::string sminor(path.substr(0, colon));
    if (!sminor.empty()) {
      // std::stringstream sm(sminor);
      // sm >> fMinor;
      sscanf(sminor.c_str(), "%d", &fMinor);
    }
    std::string schannel(path.substr(colon+1, path.size()-colon-1));
    if (!schannel.empty()) {
      // std::stringstream sc(schannel);
      // sc >> fChannel;
      sscanf(schannel.c_str(), "%d", &fChannel);
    }
  }
#endif
  DebugGuard::Message(fDebug, "Minor=%d and Channel=%d Revision=%d Serial=%d", 
		      fMinor, fChannel, fRevision, fSerial);
}

//____________________________________________________________________
Rcuxx::Rorc::Rorc(const Url& url, bool debug) 
  : fMinor(-1), fRevision(-1), fSerial(-1), fChannel(-1), 
    fBuffer(0), fNBuffer(0), fClient(0)
{
  fDebug = debug;
  DebugGuard g(fDebug, "Opening RORC server %s", url.Raw().c_str());

  ParseUrl(url.Path());
  std::string opt = url.Option("reset");
  bool reset = !opt.empty();
  bool hard  = opt.find("hard");

  fClient = 0;
  if (fMinor >= 0) fClient = new RorcClient(fMinor, fChannel);
  else             fClient = new RorcClient(fRevision, fSerial, fChannel);
  int isOpened = fClient->Open(reset, hard);
  DebugGuard::Message(fDebug, "Open returned %d\n",isOpened);
  if (isOpened) 
    throw std::runtime_error("Failed to open channel");
}

//____________________________________________________________________
Rcuxx::Rorc::~Rorc()
{
  DebugGuard g(fDebug, "Rorc::~Rorc");
  if (fClient) fClient->Close();
}

//____________________________________________________________________
bool
Rcuxx::Rorc::HandleOption(const std::string& opt, const std::string& arg)
{
  if (opt == "sifreset") { 
    assert(fClient);
    fClient->ClearSib();
    fClient->ClearSob();
    return true;
  }
  if (opt == "reset") return true;
  return Rcuxx::Rcu::HandleOption(opt, arg);
}


//____________________________________________________________________
int 
Rcuxx::Rorc::GetFWVersion() 
{
  DebugGuard g(fDebug, "Rorc::GetFWVersion");
  fFirmwareVersion = 0;
  int ret = 0;
  // if ((ret = ExecCommand(0xF000))) return ret;
  
  unsigned int  addr[] = { 0x5106, 0x8008, 0x8006, 0 };
  unsigned int* p      = addr;
  while(*p) { 
    g.Message(fDebug, "Try to get FW version from 0x%04x", *p);
    unsigned int isize = 1;
    ret = ReadMemory(*p, 0u, isize, &fFirmwareVersion);
    g.Message(fDebug, "Got back 0x%06x (return code %d)", 
	      fFirmwareVersion, ret);
    if (ret == 0 && fFirmwareVersion != 0) break;
    p++;
  }
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Rorc::SetupCommands()
{
  Rcu::SetupCommands();
  
  fCommands.push_back(new RcuCommand(*this, "ClrSibSob", 
				     "Clear Sib/Sob registers", 
				     CLEARERR));
  fCommands.push_back(new RcuCommand(*this, "ClrSib", 
				     "Clear Sib registers", 
				     CLEARSIB));
  fCommands.push_back(new RcuCommand(*this, "ClrSob", 
				     "Clear Sib registers", 
				     CLEARSOB));
}
//____________________________________________________________________
void
Rcuxx::Rorc::SetupRegisters()
{
  Rcu::SetupRegisters();

  // fRegisters.push_back(new SibSobStatus(fClient));
  // fRegisters.push_back(new SibSobError(fClient));
}


//____________________________________________________________________
unsigned int 
Rcuxx::Rorc::ExecCommand(unsigned int cmd, unsigned int data)
{
  DebugGuard g(fDebug, "Rorc::ExecCommand(0x%x)", cmd);
  assert(fClient);
  // For the DDL link, some of the commands are different
  if      (cmd == fLayout.kC_RCU_RESET)   cmd = RCURESET;
  else if (cmd == fLayout.kC_GLB_RESET)   cmd = RESETALL;
  else if (cmd == fLayout.kC_FECRST)      cmd = FECRESET;

  // Make sure we call the right function for these 
  switch (cmd) { 
  case SIFRESET: 
  case RCURESET:
  case RESETALL:
  case FECRESET:
  case CLEARSIB:
  case CLEARSOB:
  case CLEARERR:
  case SETBLREG:
    return fClient->SendCommand(cmd);
  }
  
  unsigned int size = 1;
  return WriteMemory(cmd, 0, size, &data);
}

//____________________________________________________________________
void
Rcuxx::Rorc::ReAllocBuffer(unsigned int size)
{
  if (size <= fNBuffer) { 
    memset(fBuffer, 0, sizeof(unsigned long) * fNBuffer);
    return;
  }
  if (fBuffer) { 
    delete[] fBuffer;
    fNBuffer = 0;
  }
  fNBuffer = size;
  fBuffer  = new unsigned long[fNBuffer];
  memset(fBuffer, 0, sizeof(unsigned long) * fNBuffer);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Rorc::ReadMemory(unsigned int base, unsigned int offset,  
		       unsigned int& isize, unsigned int* data)
{
  assert(fClient);
  DebugGuard g(fDebug, "Rorc::ReadMemory(0x%x,%d,%d,%p)", 
	       base, offset, isize, data);
  // ReAllocBuffer(isize);
  int osize = isize;
  unsigned int ret = fClient->ReadBlock(base, offset, osize, data);
  isize = osize;
  // ReAllocBuffer(isize);
  // for (unsigned int i = 0; i < isize; i++) fBuffer[i] = data[i];
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Rorc::WriteMemory(unsigned int base, unsigned int offset,  
			unsigned int& osize, unsigned int* data)
{
  DebugGuard g(fDebug, "Rorc::WriteMemory(0x%0x,%d,%d,%p)",
	       base,offset,osize,data);
  assert(fClient);
  // ReAllocBuffer(osize);
  // for (unsigned int i = 0; i < osize; i++) fBuffer[i] = data[i];
  int ret = fClient->WriteBlock(base, offset, osize, data);
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Rorc::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}

//____________________________________________________________________
void
Rcuxx::Rorc::PrintUrlHelp(std::ostream& o)
{
  printf("\n  RORC interface (SIU interface on RCU):\n\n" 
	 "\trorc://MINOR:CHANNEL\n" 
	 "\trorc://REVISION:SERIAL:CHANNEL\n\n" 
	 "    where MINOR is the minor device number, CHANNEL is the DDL\n"
	 "    channel number, REVISION is the revision of the RORC card,\n"
	 "    and SERIAL is serial number of the RORC card\n");
}
//____________________________________________________________________
std::string
Rcuxx::Rorc::ErrorString(unsigned int code)
{
  // Rcuxx::Rcu::ErrorString(code);
  if (code < kNotEnough) {
    assert(fClient);
    return fClient->ErrorString();
  }
  return Rcu::ErrorString(code);
}

//====================================================================
//
// EOF
//
