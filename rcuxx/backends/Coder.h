// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:04 2006
    @brief   Declaration of Coder interface to RCU's
*/
#ifndef RCUXX_CODER_H
#define RCUXX_CODER_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __STRING__
# include <string>
#endif

/**
 * Base class for command coders 
 */
class CommandCoderBase
{
public:
  /** 
   * Create a data block 
   * @param target Where to send it 
   * @param tag The tag 
   * @return  Error code 
   */
  virtual int createDataBlock(char *target, int tag) = 0;
  /** 
   * Get the command block 
   * @return  data block 
   */
  virtual long int * getDataBlock() = 0;
  /** 
   * Get list of error strings 
   * @return  List of error  strings 
   */
  virtual std::vector<std::string> getError() = 0;
  /** 
   * Get singleton object 
   * @return The singleton object 
   */
  static CommandCoderBase* createInstance() {return instance;};

private:
  /** The singleton object */
  static CommandCoderBase* instance;
};

namespace Rcuxx 
{
  //====================================================================
  /** @class Coder 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the FEE (USB to Front end)
      driver and library. 
      @ingroup rcuxx_backend
  */
  class Coder : public Rcu
  {
  public:
    /** Destructor */
    ~Coder();
    /** Start a block */ 
    void StartBlock();
    /** End block */ 
    int EndBlock();

    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Execute a command 
	@param cmd Command to execute 
	@param data Argument to command - if any
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd, 
				     unsigned int data=0);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
  protected:
    friend class Rcu;
    /** Create the Coder interface */
    Coder(const Url& url, bool debug=false);
    /** Default constructor */
    Coder();
    /** Copy constructor */
    Coder(const Coder& other);
    /** Initialize backend 
	@return 0 on success, error code otherwise */
    int GetFWVersion(unsigned int& version) { version = 0; return 0; }
    /** Debug flag */
    bool fDebug;
    /** The client */ 
    // CoderClient* fClient;
    int fSize;
    /** Use FEE configure command blocks */
    bool fUseFee;
    /** The last base address we wrote to */ 
    unsigned int fLastIMEM;
    /** Whether we should bundle IMEM writes */
    bool fBundleIMEM;
    /** Flush an IMEM block to the output */
    void FinishIMEM();
    /** Push data onto the block */ 
    void Push(unsigned int d);
    /** 
     * Print help on how to connect to this backend
     * 
     * @param o Output stream
     */    
    static void PrintUrlHelp(std::ostream& o);
  };
}

#endif

//====================================================================
//
// EOF
//
