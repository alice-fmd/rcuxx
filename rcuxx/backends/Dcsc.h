// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USAq
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:34:01 2006
    @brief   Declaration of DCSC concrete interface  
*/
#ifndef RCUXX_DCSC_H
#define RCUXX_DCSC_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#include <vector>
#include <iosfwd>

namespace Rcuxx 
{
  //====================================================================
  /** @class Dcsc 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the Dcsc driver and library.  
      @ingroup rcuxx_backend
  */
  class Dcsc : public Rcu
  {
  public:
    /** Destructor */
    ~Dcsc();
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Execute a command 
	@param cmd Command to execute 
	@param data Argument to command - if any
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd,
				     unsigned int data=0);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
  protected:
    friend class Rcu;
    /** Default ctor */
    Dcsc();
    /** Copy ctor */
    Dcsc(const Dcsc& other);
    /** Create DCSC interface 
	@param debug Debugging on/off 
	@param url Connection url */
    Dcsc(const Url& url, bool debug=false);
    /** 
     * Handle an option passed in the URI
     * 
     * @param opt Option
     * @param arg Option argument
     * 
     * @return @c true on success, @c false otherwise 
     */    
    virtual bool HandleOption(const std::string& opt, const std::string& arg);
    /** Utility function to find the firmware version 
	@todo Implement this function - look in FeeServer 
	@return The firmware version number */ 
    int GetFWVersion();
    /** Decode error messages 
	@param ret The turn value to test 
	@return 0 on success, error code otherwise */
    unsigned int DecodeError(int ret);
    /** 
     * Print help on how to connect to this backend
     * 
     * @param o Output stream
     */    
    static void PrintUrlHelp(std::ostream& o);
    /** Whether to use single writes */
    bool fUseSingle;
    /** Whether the interface is open or not */
    bool fIsOpen;
    /** Whether to be verbose or not */
    bool fVerbose;
  };
}

#endif

//====================================================================
//
// EOF
//
