// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:04 2006
    @brief   Declaration of Files interface to RCU's
*/
#ifndef RCUXX_FILES_H
#define RCUXX_FILES_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace Rcuxx 
{
  class FilesClient;
  //====================================================================
  /** @class Files 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the FEE (USB to Front end)
      driver and library. 
      @ingroup rcuxx_backend
  */
  class Files : public Rcu
  {
  public:
    /** Destructor */
    ~Files();
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    std::string ErrorString(unsigned int code);
    /** Execute a command 
	@param cmd Command to execute 
	@param data Argument to command - if any
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd, 
				     unsigned int data=0);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
  protected:
    friend class Rcu;
    /** Create the Files interface
	@param url Connection url 
	@param debug Debugging on/off */
    Files(const Url& url, bool debug=false);
    /** Default constructor */
    Files();
    /** Copy constructor */
    Files(const Files& other);
    /** Input stream */
    std::istream* fInput;
    /** Output stream */
    std::ostream* fOutput;
    /** Debug flag */
    bool fDebug;
    /** Function to parse the passed URL 
	@param url the URL */
    void ParseUrl(const std::string& url);
    /** 
     * Print help on how to connect to this backend
     * 
     * @param o Output stream
     */    
    static void PrintUrlHelp(std::ostream& o);
  };
}

#endif

//====================================================================
//
// EOF
//
