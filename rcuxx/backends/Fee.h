// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:04 2006
    @brief   Declaration of Fee interface to RCU's
*/
#ifndef RCUXX_FEE_H
#define RCUXX_FEE_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

namespace Rcuxx 
{
  class FeeClient;
  //====================================================================
  /** @class Fee 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the FEE (USB to Front end)
      driver and library. 
      @ingroup rcuxx_backend
  */
  class Fee : public Rcu
  {
  public:
    /** Errors */
    enum {
      /** No name given */
      kNoName            = 1,
      /** Start not given */
      kNoStart, 
      /** Errors from messages */
      kMessageError,
      /** Client error */
      kClient
    };
    /** Destructor */
    ~Fee();
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    std::string ErrorString(unsigned int code);
    /** Execute a command 
	@param cmd Command to execute 
	@param data Argument to command - if any
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd, 
				     unsigned int data);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
    /** Begin a data block.  This can be used with a command coder
	backend like  
	@code 
        class CommandCoder : public CommandCoderBase
	{
	  CommandCoder() 
          {
	    fRcu   = Rcuxx::Rcu::Open("coder:");
  	    fAltro = new Rcuxx::Altro(*rcu);
	    fBc    = new Rcuxx::Bc(*rcu);
          }
	  int createDataBlock(const char* t, int w)
	  {
	    // Start the block 
	    fRcu->StartBlock();
	    // Set the various registers in the RCU, ALTRO, and BC 
	    setRegisters(t, w);
	    // Finish the block 
	    return rcu->EndBlock();
          }
	  long int* getDataBlock() 
	  {
	    // Get the block of code to send 
	    const Rcuxx::Rcu::Block_t& block = rcu->GetBlock();
	    return block;
          }
          void setRegisters(const char* t int w)
	  {
	    // Should get config data from DB, set register 
	    // values, etc.  and then call the appropriate Commit 
	    // member functions 
	  }
	  std::vector<std::string> getError() 
	  {
	    // Get error strings - should really be by reference 
	  }
        };
        @endcode */
    virtual void StartBlock();
    /** End a data block.  
	@return the size of the created block 
	@see StartBlock */
    virtual int EndBlock();
  protected:
    friend class Rcu;
    /** Create the Fee interface 
	@param debug Debugging on/off 
	@param url Url */
    Fee(const Url& url, bool debug=false);
    /** Default constructor */
    Fee();
    /** Copy constructor */
    Fee(const Fee& other);
    /** Get the firmware version */
    int GetFWVersion();
    /** Server name */
    std::string   fServerName;
    /** Client */
    FeeClient* fClient;
    /** Last client error */
    std::string   fClientError;
    /** Buffer */
    std::vector<unsigned int> fBuffer;
    /** Function to parse the passed URL 
	@param url the URL */
    void ParseUrl(const Url& url);
    /** 
     * Print help on how to connect to this backend
     * 
     * @param o Output stream
     */    
    static void PrintUrlHelp(std::ostream& o);
  };
}

#endif

//====================================================================
//
// EOF
//
