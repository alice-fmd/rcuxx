// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USAq
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:34:01 2006
    @brief   Declaration of RORC concrete interface  
*/
#ifndef RCUXX_RORC_H
#define RCUXX_RORC_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#include <iosfwd>

namespace Rcuxx 
{
  class RorcClient;

  //====================================================================
  /** @class Rorc 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the Rorc driver and library.  
      @ingroup rcuxx_backend
  */
  class Rorc : public Rcu
  {
  public:
    /** Destructor */
    ~Rorc();
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    std::string ErrorString(unsigned int code);
    /** Execute a command 
	@param cmd Command to execute 
	@param data Argument to command - if any
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd, 
				     unsigned int data=0);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
    /** 
     * Handle an option
     * 
     * @param opt 
     * @param arg 
     * 
     * @return 
     */
    virtual bool HandleOption(const std::string& opt, const std::string& arg);
    /** Set up registers, command, and memories based on layout */
    virtual void SetupRegisters();
    /** Set up registers, command, and memories based on layout */
    virtual void SetupCommands();
  protected:
    friend class Rcu;
    /** Default ctor */
    Rorc();
    /** Copy ctor */
    Rorc(const Rorc& other);
    /** Create RORC interface 
	@param url Connection url 
	@param debug Debugging on/off */
    Rorc(const Url& url, bool debug=false);
    /** Parse the passed URL and extract parameters 
	@param path URL to parse */
    void ParseUrl(const std::string& path);
    /** Utility function to find the firmware version 
	@todo Implement this function - look in FeeServer 
	@return 0 on success, error code otherwise */ 
    int GetFWVersion();
    /** 
     * Print help on how to connect to this backend
     * 
     * @param o Output stream
     */    
    static void PrintUrlHelp(std::ostream& o);
    /** 
     * (Re)allocate memory for internal buffer. 
     * 
     * If @a size is less than or equal to the current size, no new
     * memory is allocated.
     *
     * If more memory is needed than currently in the buffer, of if
     * the buffer is null, then the buffer is deallocated, and an new
     * buffer allocated on the heap.
     *
     * The buffer is reset to 0 in all cases. 
     *
     * @param size Needed size of buffer 
     */
    void ReAllocBuffer(unsigned int size);
    /** 
     * Check if this can access SC partition 
     *
     * @return false 
     */
    bool SCAccess() const { return false; }
    /** Minor device number of @c /dev/prorc @e x */
    int fMinor;
    /** Channel on RORC card */
    int fChannel;
    /** Revision number */
    int fRevision;
    /** Serial number */
    int fSerial;
    /** Data buffer */
    unsigned long* fBuffer;
    /** Number of entries in buffer */
    unsigned int   fNBuffer;
    /** Client inferface */ 
    RorcClient* fClient;
  };
}

#endif

//====================================================================
//
// EOF
//
