//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_REGISTERGUARD_H
# include "RegisterGuard.h"
#endif
#include <iostream>

//____________________________________________________________________
Rcuxx::RegisterGuard::RegisterGuard(Register* r, 
				    bool update, 
				    bool debug)
  : fGuard(0)
{
  if (!r || !r->IsSubmitable())
    // If there's no register, or we cannot write to this register,
    // then simply ignore it
    return;

  // Create the guard
  fGuard = r->CreateGuard(update,debug);
}
Rcuxx::RegisterGuard::~RegisterGuard()
{
  if (!fGuard) 
    // If this register does not exist, isn't writeable, or was 
    // ignored for some other reason, get out here 
    return;

  // Now remove the actual guard
  delete fGuard;
  fGuard = 0;
}
//
// EOF
//
