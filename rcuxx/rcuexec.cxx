//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuexec.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:48 2006
    @brief   Program to interactively execute instructions in the RCU
    IMEM 
*/
# include <rcuxx/Rcu.h>
# include <rcuxx/rcu/RcuIMEM.h>
# include <rcuxx/rcu/RcuRMEM.h>
# include "config.h"
# ifndef VERSION
/** Version number */
#  define VERSION "?.?"
# endif
#include "Options.h"
#include <stdexcept>
#include <fstream>
#include "compat.h"
#ifdef HAVE_ALTROCC_COMPILER_H
# include <altrocc/compiler.h>
/** Compiled program structure */
static const struct RCUC_set* program = 0;
#endif

/** Program to execute IMEM 
    @param argc Number of command line arguments. 
    @param argv Vector of command line arguments
    @return  0 on success. */

int
main(int argc, char** argv)
{
  Option<bool>        hOpt('h', "help",          "\tThis help", false, false); 
  Option<bool>        vOpt('v', "version",       "\tShow version",false,false);
  Option<std::string> dOpt('d', "debug",         "\tTurn on debug messages");
  Option<unsigned>    rOpt('r', "result-size",   "# of words to read");
  Option<std::string> lOpt('l', "label",         "\tLabel\t\t","");
  Option<std::string> iOpt('i', "input",         "\tInput file\t","");
  Option<unsigned>    OOpt('O', "offset",        "\tExecution offset");
  Option<bool>        eOpt('e', "emulation",     "\tEmulation",false,false);
  Option<bool>        cOpt('c', "compile",       "\tCompile",false,false);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(rOpt);
  cl.Add(lOpt);
  cl.Add(iOpt);
  cl.Add(OOpt);
  cl.Add(eOpt);
  cl.Add(cOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "rcuexec version " << VERSION << std::endl;
    return 0;
  }
  std::string device = (cl.Remain().size() > 0 ? 
			cl.Remain()[0] : "/dev/altro0");
  
#ifndef HAVE_ALTROCC_COMPILER_H
  if (cOpt.IsSet() || !lOpt->empty()) {
    std::cerr << "Compilation or labels not supported without "
	      << "compiler interface library" << std::endl;
    return 1;
  }
#endif

  // Rcuxx::DebugGuard::fDebug = rcuxxdebug;
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), eOpt.IsSet());
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    if (dOpt->find("rcuxx")!=std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
    if (dOpt->find("backend")!=std::string::npos) 
      rcu->SetDebug(Rcuxx::Rcu::kBackend,1);

    std::vector<unsigned int> imem;
    // unsigned int imem[rcu->IMEM()->Size()];
    
    size_t i = 0;
#ifdef HAVE_ALTROCC_COMPILER_H
    if (cOpt.IsSet()) {
      if (iOpt->empty()) 
	throw std::runtime_error("Cannot compile command line");
      program = RCUC_compile(iOpt->c_str(), 0, 0);
      if (!program) throw std::runtime_error(RCUC_get_error());
      for (i = 0; i < program->size; i++) {
	char* lab = program->labels[i];
	if (!lOpt->empty() && lOpt.Value() == lab) OOpt = i;
	imem.push_back(program->data[i]);
      }
    }
#endif
    if (!cOpt) {
      bool ret = true;
      if (!iOpt->empty()) 
	ret = Rcuxx::Rcu::ReadFile(iOpt->c_str(), imem);
      else {
	std::cout << "Please input 32bit instructions, one per line " 
		  << "and finish with Ctrl-D:\n" << std::endl;
	ret = Rcuxx::Rcu::ReadFile(std::cin, imem);
      }
      if (!ret) throw std::runtime_error("Failed to read input");
    }

    unsigned int ret = 0;
    rcu->IMEM()->Set(0, i, &(imem[0]));
    if ((ret = rcu->IMEM()->Commit())) throw ret;
    if ((ret = rcu->IMEM()->Exec(OOpt))) throw ret;
    
    // while (true) USLEEP(2);
    if (rOpt > 0) {
      if (size_t(rOpt) > rcu->RMEM()->Size()) rOpt = rcu->RMEM()->Size();
      unsigned int rmem[rcu->RMEM()->Size()];
      rcu->RMEM()->Update();
      rcu->RMEM()->Get(0, rOpt, rmem);
      std::cout << "The first " << rOpt << " words of RMEM are: " 
		<< std::endl;
      for (size_t j = 0; j < size_t(rOpt); j++) 
	std::cout << std::setw(4) << j << ": 0x" << std::setw(8) 
		  << std::setfill('0') << std::hex << rmem[j] 
		  << std::dec << std::setfill(' ') << std::endl;
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  
  return 0;
}

//
// EOF
//
