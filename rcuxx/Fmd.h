// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Fmd.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMD_H
#define RCUXX_FMD_H
#include <rcuxx/Bc.h>

namespace Rcuxx 
{  
  /** @defgroup fmdxx FMD Interace classes 
   */
  struct FmdAL_ANA_I;		// AL_ANA_I monitor register */
  struct FmdAL_ANA_I_TH;	// AL_ANA_I_TH threshold register
  struct FmdAL_ANA_U;		// AL_ANA_U monitor register */
  struct FmdAL_ANA_U_TH;	// AL_ANA_U_TH threshold register
  struct FmdAL_DIG_I;		// AL_DIG_I monitor register */
  struct FmdAL_DIG_I_TH;	// AL_DIG_I_TH threshold register
  struct FmdAL_DIG_U;		// AL_DIG_U monitor register */
  struct FmdAL_DIG_U_TH;	// AL_DIG_U_TH threshold register
  struct FmdCalIter;		// 
  struct FmdClock;		// Base class FMD clock register 
  struct FmdCommand;		// FMD register Command
  struct FmdConstants;		// 
  struct FmdFLASH_I;		// FLASH_I monitor register */
  struct FmdFLASH_I_TH;		// FLASH_I_TH threshold register
  struct FmdGTL_U;		// GTL_U monitor register */
  struct FmdGTL_U_TH;		// GTL_U_TH threshold register
  struct FmdHoldWait;		// FMD register HoldWait
  struct FmdL0Timeout;		// FMD register L0Timeout
  struct FmdL0Triggers;		// FMD register L0Triggers
  struct FmdL1Timeout;		// FMD register L1Timeout
  struct FmdL1Triggers;		// FMD register L1Triggers
  struct FmdMeb;		// Multi-event buffer 
  struct FmdPulser;		// FMD register Pulser
  struct FmdRange;		// FMD register Range
  struct FmdRegister;		// FMD register Register
  struct FmdSampleClock;	// FMD register SampleClock
  struct FmdShapeBias;		// FMD register Shape Bias
  struct FmdShiftClock;		// FMD register ShiftClock
  struct FmdStatus;		// FMD register Status
  struct FmdT1;			// T1 monitor register */
  struct FmdT1_TH;		// T1_TH threshold register
  struct FmdT1SENS;		// T1SENS monitor register */
  struct FmdT1SENS_TH;		// T1SENS_TH threshold register
  struct FmdT2;			// T2 monitor register */
  struct FmdT2_TH;		// T2_TH threshold register
  struct FmdT2SENS;		// T2SENS monitor register */
  struct FmdT2SENS_TH;		// T2SENS_TH threshold register
  struct FmdT3;			// T3 monitor register */
  struct FmdT3_TH;		// T3_TH threshold register
  struct FmdT4;			// T4 monitor register */
  struct FmdT4_TH;		// T4_TH threshold register
  struct FmdTopBottom;		// Base class FMD register with top/bottom
  struct FmdVA_REC_IM;		// VA_REC_IM monitor register */
  struct FmdVA_REC_IM_TH;	// VA_REC_IM_TH threshold register
  struct FmdVA_REC_IP;		// VA_REC_IP monitor register */
  struct FmdVA_REC_IP_TH;	// VA_REC_IP_TH threshold register
  struct FmdVA_REC_UM;		// VA_REC_UM monitor register */
  struct FmdVA_REC_UM_TH;	// VA_REC_UM_TH threshold register
  struct FmdVA_REC_UP;		// VA_REC_UP monitor register */
  struct FmdVA_REC_UP_TH;	// VA_REC_UP_TH threshold register
  struct FmdVA_SUP_IM;		// VA_SUP_IM monitor register */
  struct FmdVA_SUP_IM_TH;	// VA_SUP_IM_TH threshold register
  struct FmdVA_SUP_IP;		// VA_SUP_IP monitor register */
  struct FmdVA_SUP_IP_TH;	// VA_SUP_IP_TH threshold register
  struct FmdVA_SUP_UM;		// VA_SUP_UM monitor register */
  struct FmdVA_SUP_UM_TH;	// VA_SUP_UM_TH threshold register
  struct FmdVA_SUP_UP;		// VA_SUP_UP monitor register */
  struct FmdVA_SUP_UP_TH;	// VA_SUP_UP_TH threshold register
  struct FmdVFP;		// FMD register VFP
  struct FmdVFS;		// FMD register VFS
  
  //====================================================================
  /** @class Fmd rcuxx/Fmd.h <rcuxx/Fmd.h>
      @brief Interface to FMD 
      @ingroup fmdxx
  */
  class Fmd : public Bc
  {
  public:
    /** constructor 
	@param rcu Reference to RCU interface */
    Fmd(Rcu& rcu);
    /** Destructor */
    virtual ~Fmd() {}
    
    /** @return Pointer to ShapeBias register interface */
    FmdShapeBias*	ShapeBias() const { return fShapeBias; }
    /** @return Pointer to VFS register interface */
    FmdVFS*		VFS() const { return fVFS; }
    /** @return Pointer to VFP register interface */
    FmdVFP*		VFP() const { return fVFP; }
    /** @return Pointer to Pulser register interface */
    FmdPulser*		Pulser() const { return fPulser; }
    /** @return Pointer to Calibration run iterations */ 
    FmdCalIter*         CalIter() const { return fCalIter; }
    /** @return Pointer to ShiftClock register interface */
    FmdShiftClock*	ShiftClock() const { return fShiftClock; }
    /** @return Pointer to SampleClock register interface */
    FmdSampleClock*	SampleClock() const { return fSampleClock; }
    /** @return Pointer to HoldWait register interface */
    FmdHoldWait*	HoldWait() const { return fHoldWait; }
    /** @return Pointer to L0Timeout register interface */
    FmdL0Timeout*	L0Timeout() const { return fL0Timeout; }
    /** @return Pointer to L1Timeout register interface */
    FmdL1Timeout*	L1Timeout() const { return fL1Timeout; }
    /** @return Pointer to Range register interface */
    FmdRange*		Range() const { return fRange; }
    /** @return Pointer to L0Triggers register interface */
    FmdL0Triggers*	L0Triggers() const { return fL0Triggers; }
    /** @return Pointer to L1Triggers register interface */
    FmdL1Triggers*	L1Triggers() const { return fL1Triggers; }
    /** @return Pointer to Status register interface */
    FmdStatus*		Status() const { return fStatus; }
    /** @return Pointer to Multi-event buffer interface */
    FmdMeb*		Meb() const { return fMeb; }
    // 

    /** @return Pointer to T1 interface */
    FmdT1*		T1()		const{ return fT1; }
    /** @return Pointer to T1_TH interface */
    FmdT1_TH*		T1_TH()		const{ return fT1_TH; }
    /** @return Pointer to FLASH_I interface */
    FmdFLASH_I*		FLASH_I()	const{ return fFLASH_I; }
    /** @return Pointer to FLASH_I_TH interface */
    FmdFLASH_I_TH*	FLASH_I_TH()	const{ return fFLASH_I_TH; }
    /** @return Pointer to AL_DIG_I interface */
    FmdAL_DIG_I*	AL_DIG_I()	const{ return fAL_DIG_I; }
    /** @return Pointer to AL_DIG_I_TH interface */
    FmdAL_DIG_I_TH*	AL_DIG_I_TH()	const{ return fAL_DIG_I_TH; }
    /** @return Pointer to AL_ANA_I interface */
    FmdAL_ANA_I*	AL_ANA_I()	const{ return fAL_ANA_I; }
    /** @return Pointer to AL_ANA_I_TH interface */
    FmdAL_ANA_I_TH*	AL_ANA_I_TH()	const{ return fAL_ANA_I_TH; }
    /** @return Pointer to VA_REC_IP interface */
    FmdVA_REC_IP*	VA_REC_IP()	const{ return fVA_REC_IP; }
    /** @return Pointer to VA_REC_IP_TH interface */
    FmdVA_REC_IP_TH*	VA_REC_IP_TH()	const{ return fVA_REC_IP_TH; }
    /** @return Pointer to T2 interface */
    FmdT2*		T2()		const{ return fT2; }
    /** @return Pointer to T2_TH interface */
    FmdT2_TH*		T2_TH()		const{ return fT2_TH; }
    /** @return Pointer to VA_SUP_IP interface */
    FmdVA_SUP_IP*	VA_SUP_IP()	const{ return fVA_SUP_IP; }
    /** @return Pointer to VA_SUP_IP_TH interface */
    FmdVA_SUP_IP_TH*	VA_SUP_IP_TH()	const{ return fVA_SUP_IP_TH; }
    /** @return Pointer to VA_REC_IM interface */
    FmdVA_REC_IM*	VA_REC_IM()	const{ return fVA_REC_IM; }
    /** @return Pointer to VA_REC_IM_TH interface */
    FmdVA_REC_IM_TH*	VA_REC_IM_TH()	const{ return fVA_REC_IM_TH; }
    /** @return Pointer to VA_SUP_IM interface */
    FmdVA_SUP_IM*	VA_SUP_IM()	const{ return fVA_SUP_IM; }
    /** @return Pointer to VA_SUP_IM_TH interface */
    FmdVA_SUP_IM_TH*	VA_SUP_IM_TH()	const{ return fVA_SUP_IM_TH; }
    /** @return Pointer to GTL_U interface */
    FmdGTL_U*		GTL_U()		const{ return fGTL_U; }
    /** @return Pointer to GTL_U_TH interface */
    FmdGTL_U_TH*	GTL_U_TH()	const{ return fGTL_U_TH; }
    /** @return Pointer to T3 interface */
    FmdT3*		T3()		const{ return fT3; }
    /** @return Pointer to T3_TH interface */
    FmdT3_TH*		T3_TH()		const{ return fT3_TH; }
    /** @return Pointer to T1SENS interface */
    FmdT1SENS*		T1SENS()	const{ return fT1SENS; }
    /** @return Pointer to T1SENS_TH interface */
    FmdT1SENS_TH*	T1SENS_TH()	const{ return fT1SENS_TH; }
    /** @return Pointer to T2SENS interface */
    FmdT2SENS*		T2SENS()	const{ return fT2SENS; }
    /** @return Pointer to T2SENS_TH interface */
    FmdT2SENS_TH*	T2SENS_TH()	const{ return fT2SENS_TH; }
    /** @return Pointer to AL_DIG_U interface */
    FmdAL_DIG_U*	AL_DIG_U()	const{ return fAL_DIG_U; }
    /** @return Pointer to AL_DIG_U_TH interface */
    FmdAL_DIG_U_TH*	AL_DIG_U_TH()	const{ return fAL_DIG_U_TH; }
    /** @return Pointer to AL_ANA_U interface */
    FmdAL_ANA_U*	AL_ANA_U()	const{ return fAL_ANA_U; }
    /** @return Pointer to AL_ANA_U_TH interface */
    FmdAL_ANA_U_TH*	AL_ANA_U_TH()	const{ return fAL_ANA_U_TH; }
    /** @return Pointer to T4 interface */
    FmdT4*		T4()		const{ return fT4; }
    /** @return Pointer to T4_TH interface */
    FmdT4_TH*		T4_TH()		const{ return fT4_TH; }
    /** @return Pointer to VA_REC_UP interface */
    FmdVA_REC_UP*	VA_REC_UP()	const{ return fVA_REC_UP; }
    /** @return Pointer to VA_REC_UP_TH interface */
    FmdVA_REC_UP_TH*	VA_REC_UP_TH()	const{ return fVA_REC_UP_TH; }
    /** @return Pointer to VA_SUP_UP interface */
    FmdVA_SUP_UP*	VA_SUP_UP()	const{ return fVA_SUP_UP; }
    /** @return Pointer to VA_SUP_UP_TH interface */
    FmdVA_SUP_UP_TH*	VA_SUP_UP_TH()	const{ return fVA_SUP_UP_TH; }
    /** @return Pointer to VA_SUP_UM interface */
    FmdVA_SUP_UM*	VA_SUP_UM()	const{ return fVA_SUP_UM; }
    /** @return Pointer to VA_SUP_UM_TH interface */
    FmdVA_SUP_UM_TH*	VA_SUP_UM_TH()	const{ return fVA_SUP_UM_TH; }
    /** @return Pointer to VA_REC_UM interface */
    FmdVA_REC_UM*	VA_REC_UM()	const{ return fVA_REC_UM; }
    /** @return Pointer to VA_REC_UM_TH interface */
    FmdVA_REC_UM_TH*	VA_REC_UM_TH()	const{ return fVA_REC_UM_TH; }

    /** This commands initiates the process of changing the control
	voltages for the VA1.  Note, one should set the registers, and
	then execute this command to change any of the voltages.
	Simply changing the register values does not suffice. 
	@return Pointer to Command command interface */
    BcCommand* ChangeDacs() const { return fChangeDacs; } 
    /** Issue a fake trigger sequence - for testing
	@return Pointer to Command command interface */
    BcCommand* FakeTrigger() const { return fFakeTrigger; } 
    /** Turn on test only.  Note, that this does not enable the
	calibration pulser, nor does it introduce an additional delay
	between the L0 and L1 trigger.
	@return Pointer to Command command interface */
    BcCommand* TestOn() const { return fTestOn; } 
    /** Turn test mode off. 
	@return Pointer to Command command interface */
    BcCommand* TestOff() const { return fTestOff; } 
    /** Turn on calibration pulser.  This also turns on test mode (see
	TestOn).  Note, that this introduces a fixed additional delay
	of @f$ 1.2\mu s@f$ - corresponding to the L0 delay from bunch
	crossing.  
	@return Pointer to Command command interface */
    BcCommand* PulserOn() const { return fPulserOn; } 
    /** Turn calibration pulser off. 
	@return Pointer to Command command interface */
    BcCommand* PulserOff() const { return fPulserOff; } 
    /** Reset all FSM's in the FMD BC part. 
	@return Pointer to Command command interface */
    BcCommand* SoftReset() const { return fSoftReset; } 
    /** Enable a calibration run. 
	@return Pointer to Command command interface */
    BcCommand* CalibrationRun() const { return fCalibrationRun; } 
  protected:
    /** Pointer to ShapeBias register interface */
    FmdShapeBias*	fShapeBias;
    /** Pointer to VFS register interface */
    FmdVFS*		fVFS;
    /** Pointer to VFP register interface */
    FmdVFP*		fVFP;
    /** Pointer to Pulser register interface */
    FmdPulser*		fPulser;
    /** Pointer to Calibration run iterations */ 
    FmdCalIter*         fCalIter;
    /** Pointer to ShiftClock register interface */
    FmdShiftClock*	fShiftClock;
    /** Pointer to SampleClock register interface */
    FmdSampleClock*	fSampleClock;
    /** Pointer to HoldWait register interface */
    FmdHoldWait*	fHoldWait;
    /** Pointer to L0Timeout register interface */
    FmdL0Timeout*	fL0Timeout;
    /** Pointer to L1Timeout register interface */
    FmdL1Timeout*	fL1Timeout;
    /** Pointer to Range register interface */
    FmdRange*		fRange;
    /** Pointer to L0Triggers register interface */
    FmdL0Triggers*	fL0Triggers;
    /** Pointer to L1Triggers register interface */
    FmdL1Triggers*	fL1Triggers;
    /** Pointer to Status register interface */
    FmdStatus*		fStatus;
    /** Pointer to Multi-event buffer interface */
    FmdMeb*             fMeb;
    
    // Monitor
    /** Pointer to T1 interface */
    FmdT1*		fT1;
    /** Pointer to T1_TH interface */
    FmdT1_TH*		fT1_TH;
    /** Pointer to FLASH_I interface */
    FmdFLASH_I*		fFLASH_I;
    /** Pointer to FLASH_I_TH interface */
    FmdFLASH_I_TH*	fFLASH_I_TH;
    /** Pointer to AL_DIG_I interface */
    FmdAL_DIG_I*	fAL_DIG_I;
    /** Pointer to AL_DIG_I_TH interface */
    FmdAL_DIG_I_TH*	fAL_DIG_I_TH;
    /** Pointer to AL_ANA_I interface */
    FmdAL_ANA_I*	fAL_ANA_I;
    /** Pointer to AL_ANA_I_TH interface */
    FmdAL_ANA_I_TH*	fAL_ANA_I_TH;
    /** Pointer to VA_REC_IP interface */
    FmdVA_REC_IP*	fVA_REC_IP;
    /** Pointer to VA_REC_IP_TH interface */
    FmdVA_REC_IP_TH*	fVA_REC_IP_TH;
    /** Pointer to T2 interface */
    FmdT2*		fT2;
    /** Pointer to T2_TH interface */
    FmdT2_TH*		fT2_TH;
    /** Pointer to VA_SUP_IP interface */
    FmdVA_SUP_IP*	fVA_SUP_IP;
    /** Pointer to VA_SUP_IP_TH interface */
    FmdVA_SUP_IP_TH*	fVA_SUP_IP_TH;
    /** Pointer to VA_REC_IM interface */
    FmdVA_REC_IM*	fVA_REC_IM;
    /** Pointer to VA_REC_IM_TH interface */
    FmdVA_REC_IM_TH*	fVA_REC_IM_TH;
    /** Pointer to VA_SUP_IM interface */
    FmdVA_SUP_IM*	fVA_SUP_IM;
    /** Pointer to VA_SUP_IM_TH interface */
    FmdVA_SUP_IM_TH*	fVA_SUP_IM_TH;
    /** Pointer to GTL_U interface */
    FmdGTL_U*		fGTL_U;
    /** Pointer to GTL_U_TH interface */
    FmdGTL_U_TH*	fGTL_U_TH;
    /** Pointer to T3 interface */
    FmdT3*		fT3;
    /** Pointer to T3_TH interface */
    FmdT3_TH*		fT3_TH;
    /** Pointer to T1SENS interface */
    FmdT1SENS*		fT1SENS;
    /** Pointer to T1SENS_TH interface */
    FmdT1SENS_TH*	fT1SENS_TH;
    /** Pointer to T2SENS interface */
    FmdT2SENS*		fT2SENS;
    /** Pointer to T2SENS_TH interface */
    FmdT2SENS_TH*	fT2SENS_TH;
    /** Pointer to AL_DIG_U interface */
    FmdAL_DIG_U*	fAL_DIG_U;
    /** Pointer to AL_DIG_U_TH interface */
    FmdAL_DIG_U_TH*	fAL_DIG_U_TH;
    /** Pointer to AL_ANA_U interface */
    FmdAL_ANA_U*	fAL_ANA_U;
    /** Pointer to AL_ANA_U_TH interface */
    FmdAL_ANA_U_TH*	fAL_ANA_U_TH;
    /** Pointer to T4 interface */
    FmdT4*		fT4;
    /** Pointer to T4_TH interface */
    FmdT4_TH*		fT4_TH;
    /** Pointer to VA_REC_UP interface */
    FmdVA_REC_UP*	fVA_REC_UP;
    /** Pointer to VA_REC_UP_TH interface */
    FmdVA_REC_UP_TH*	fVA_REC_UP_TH;
    /** Pointer to VA_SUP_UP interface */
    FmdVA_SUP_UP*	fVA_SUP_UP;
    /** Pointer to VA_SUP_UP_TH interface */
    FmdVA_SUP_UP_TH*	fVA_SUP_UP_TH;
    /** Pointer to VA_SUP_UM interface */
    FmdVA_SUP_UM*	fVA_SUP_UM;
    /** Pointer to VA_SUP_UM_TH interface */
    FmdVA_SUP_UM_TH*	fVA_SUP_UM_TH;
    /** Pointer to VA_REC_UM interface */
    FmdVA_REC_UM*	fVA_REC_UM;
    /** Pointer to VA_REC_UM_TH interface */
    FmdVA_REC_UM_TH*	fVA_REC_UM_TH;

    // commands 
    /** Pointer to @b Change @b DACs command interface */
    BcCommand* fChangeDacs; 
    /** Pointer to @b Fake @b Trigger command interface */
    BcCommand* fFakeTrigger; 
    /** Pointer to @b Pulser @b mode @b on command interface */
    BcCommand* fPulserOn; 
    /** Pointer to @b Pulser @b mode @b off command interface */
    BcCommand* fPulserOff; 
    /** Pointer to @b Test @b mode @b on command interface */
    BcCommand* fTestOn; 
    /** Pointer to @b Test @b mode @b off command interface */
    BcCommand* fTestOff; 
    /** Pointer to @b Soft @b Reset command interface */
    BcCommand* fSoftReset; 
    /** Pointer to @b Calibration @b Manager @b on command interface
     */
    BcCommand* fCalibrationRun;
  };
}

#endif

//____________________________________________________________________
//
// EOF
//

