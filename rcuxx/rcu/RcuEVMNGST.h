//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUEVMNGST
# define RCU_RCUEVMNGST
# include <rcuxx/rcu/RcuRegister.h>
# include <rcuxx/rcu/RcuState_t.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuEVMNGST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the EVent MaNaGer STates

      This register holds the states of the various state machines of
      the event manager.
      @ingroup rcuxx_rcu
  */
  struct RcuEVMNGST : public RcuRegister
  {
    struct TST_TRG_t : public RcuState_t
    {
      TST_TRG_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0:  return "idle";
	case 1:  return "send_l1";
	case 3:  return "s_wait";
	case 2:  return "send_l2";
	}
	return "unknown";
      }
    };
    struct TTC_TRG_t : public RcuState_t
    {
      TTC_TRG_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0:  return "idle";
	case 3:  return "l1_phos";
	case 1:  return "wait_l1";
	case 2:  return "l1_tpc";
	case 6:  return "wait_l2";
	case 4:  return "send_l2";
	case 12: return "rec_l2r";
	case 5:  return "rec_l2t";
	case 7:  return "get_err";
	}
	return "unknown";
      }
    };
    struct EV_t : public RcuState_t
    {
      EV_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0: return "idle";
	case 1: return "send_cmd";
	case 3: return "wait_done";
	case 6: return "dly_1cyc";
	case 2: return "rdrx_0";
	}
	return "unknown";
      }
    };
    struct INSSEQ_t : public RcuState_t
    {
      INSSEQ_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0: return "idle";
	case 1: return "s_sod";
	case 2: return "wait_done";
	case 3: return "s_eod";
	case 7: return "eod_cmd";
	case 6: return "rdrx_0";
	}
	return "unknown";
      }
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuEVMNGST(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "EVMNGST", "EVent MaNaGer STate", 0, false), 
	fTST_TRG("TST_TRG", 10, 0x3), 
	fTTC_TRG("TTC_TRG", 6, 0xf), 
	fEV("EV", 3, 0x7),
	fINSSEQ("INSSEQ", 0, 0x7)
    {}
    /** @return Test trigger state */
    const RcuState_t& TST_TRG() const { return fTST_TRG; } 
    /** @return TTC trigger state */
    const RcuState_t&  TTC_TRG() const { return fTTC_TRG; } 
    /** @return Event */
    const RcuState_t&  EV() const { return fEV; } 
    /** @return Instruction sequencer */
    const RcuState_t& INSSEQ() const { return fINSSEQ; } 

    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      fTST_TRG.Print();
      fTTC_TRG.Print();
      fEV.Print();
      fINSSEQ.Print();
    }
    void Decode(unsigned int dat) { 
      fTST_TRG.Decode(dat);
      fTTC_TRG.Decode(dat);
      fEV.Decode(dat);
      fINSSEQ.Decode(dat);
    }
  protected:
    /** State */
    TST_TRG_t fTST_TRG;
    /** State */
    TTC_TRG_t fTTC_TRG;
    /** State */
    EV_t fEV;
    /** State */
    INSSEQ_t fINSSEQ;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
