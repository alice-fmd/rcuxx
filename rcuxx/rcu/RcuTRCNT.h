//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTRCNT
# define RCU_RCUTRCNT
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @class RcuTRCNT rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache of the TRigger CouNTers.  

      Records the number of triggers seen
      @ingroup rcuxx_rcu 
  */
  struct RcuTRCNT : public RcuRegister
  {

    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory
	@param clear Clear command */
    RcuTRCNT(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "TRCNT", "TRigger (L2a) CouNTer", 
		    clear, false), 
	fReceived(0), 
	fAccepted(0) 
    {}
    /** Decode @a dat as the TRigger CouNTers */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Recieved triggers */
    unsigned int Received() const { return fReceived; }
    /** @return Accepted triggers */
    unsigned int Accepted() const { return fAccepted; }
  protected:
    /** How many triggers we got (L1B) */
    unsigned int fReceived;
    /** How many triggers were validated (L2B) */
    unsigned int fAccepted;
  };
  //====================================================================
  inline void
  RcuTRCNT::Decode(unsigned int dat) 
  { 
    fAccepted = dat & 0xffff;
    fReceived = (dat >> 16) & 0xffff;
  }

  //____________________________________________________________________
  inline void
  RcuTRCNT::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tReceived:\t\t\t" << fReceived << "\n"
	      << "\tAccepted:\t\t\t" << fAccepted << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
//
