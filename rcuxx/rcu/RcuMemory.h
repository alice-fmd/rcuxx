// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/rcu/RcuMemory.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUMEMORY_H
# define RCUXX_RCUMEMORY_H
# include <rcuxx/rcu/RcuCommand.h>
# include <string>
# include <vector>
# include <iostream>
# include <iomanip>
# include <list>

namespace Rcuxx
{
  //____________________________________________________________________
  // Forward declaration 
  class Rcu;
  class RcuCommand;
  
  //====================================================================
  /** @class RcuMemory rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU memory banks     
      @ingroup rcuxx_rcu
  */
  class RcuMemory 
  {
  public:
    /** Type of cache */
    typedef std::vector<unsigned int> Cache_t;
    /** Constructor 
	@param rcu Reference to RCU
	@param name Name of the memory
	@param title  Title of memory 
	@param base Base address
	@param size Number of words in the memory
	@param max Maximum value of words in the memory
	@param commit Whether the memory is writeable  
        @param clear Command to clear 
        @param exec Command to execute 
        @param abort Command to abort execution. */
    RcuMemory(Rcu& rcu, const char* name, const char* title, unsigned int base, 
	      unsigned int size, unsigned int max, bool commit, 
	      RcuCommand* clear=0, RcuCommand* exec=0, 
	      RcuCommand* abort=0);
    /** Destructor */
    virtual ~RcuMemory() { /* delete [] fData;  */ }
    /** Update the cache
	@return 0 on success, error code otherwise */
    virtual unsigned int Update() { return Update(0, fSize); }
    /** Commit to the RCU 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { return Commit(0, fSize); }
    /** Update the cache (restricted)
	@param off Where to start reading
	@param n   How many words to read 
	@return 0 on success, error code otherwise */
    virtual unsigned int Update(size_t off, size_t n);
    /** Write to the hardware (restricted)
	@param off Where to start reading
	@param n   How many words to read 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit(size_t off, size_t n);
    /** Clear memory */ 
    virtual unsigned int Clear();
    /** Clear memory */ 
    virtual unsigned int Exec(unsigned int arg);
    /** Clear memory */ 
    virtual unsigned int Abort();
    /** Load a file into the cache
	@return true on success, false otherwise */
    virtual bool Load(std::istream& in);
    /** Save the cache to file
	@return true on success, false otherwise */      
    virtual bool Save(std::ostream& out);
    /** Zero the memory */
    virtual void Zero();
    /** @return Get the size */
    unsigned int Size() const { return fSize; }
    /** @return maximum value of each entry */
    unsigned int Max() const { return fMax; }
    /** @return Get the data */
    const Cache_t& Data() const { return fData; }
    /** @return Get the data */
    Cache_t& Data() { return fData; }
    /** Set the cache
	@param off Starting offset
	@param n How many words (size of @a d)
	@param d The values */
    /** Set */ 
    virtual void Set(size_t off, size_t n, unsigned int* d);
    /** Get the cache 
	@param off Starting offset
	@param n How many words (size of @a d)
	@param d On return, the values */
    void Get(size_t off, size_t n, unsigned int* d);
    /** Get the base address */ 
    unsigned int Base() const { return fBase; }
    /** Print cache to standard output */
    virtual void Print() const;
    /** Get the name */ 
    const std::string& Name() const { return fName; }
    /** Get the title */ 
    const std::string& Title() const { return fTitle; }
    /** Whether we got Clear */
    virtual bool IsWriteable() const { return fWriteable; }
    /** Whether we got Clear */
    virtual bool IsClearable() const { return (fClear != 0); }
    /** Whether we got Clear */
    virtual bool IsExecutable() const { return (fExec != 0); }
    /** Get reference to mother */
    Rcu& Mother() { return fRcu; } 
    /** Get reference to mother */
    const Rcu& Mother() const { return fRcu; } 
  protected:
    /** Reference to RCU */
    Rcu& fRcu;
    /** Name */
    std::string fName;
    /** Name */
    std::string fTitle;
    /** Base address */ 
    unsigned int fBase;
    /** Data buffer */
    Cache_t fData;
    /** Size of the memory */
    unsigned int fSize;
    /** Max value of each entry in the memory */
    unsigned int fMax;
    /** Whether the memory is writeable */
    bool fWriteable;
    /** Clear command, if any */
    RcuCommand* fClear;
    /** Execute command, if any */
    RcuCommand* fExec;
    /** Abort command, if any */
    RcuCommand* fAbort;
  };

  //====================================================================
  inline
  RcuMemory::RcuMemory(Rcu& rcu, const char* name, const char* title, 
		       unsigned int base, unsigned int size, 
		       unsigned int max,  bool commit, 
		       RcuCommand* clear, RcuCommand* exec, 
		       RcuCommand* abort)
    : fRcu(rcu), 
      fName(name), 
      fTitle(title),
      fBase(base), 
      fData(size), 
      fSize(size), 
      fMax(max), 
      fWriteable(commit), 
      fClear(clear), 
      fExec(exec), 
      fAbort(abort)
  {
    // fData = new unsigned int[fSize];
    Zero();
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuMemory::Commit(size_t off, size_t n)
  {
    unsigned int  m = std::min(unsigned(n), fSize);
    unsigned int  o = std::min(unsigned(off), fSize-1);
    unsigned int ret = fRcu.WriteMemory(fBase, o, m, &(fData[o]));
    return ret;
  }
  
  //____________________________________________________________________
  inline unsigned int 
  RcuMemory::Update(size_t off, size_t n)
  {
    unsigned int m = std::min(unsigned(n), fSize);
    unsigned int o = std::min(unsigned(off), fSize-1);
    unsigned int ret = fRcu.ReadMemory(fBase, o, m, &(fData[o]));
    return ret;
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuMemory::Exec(unsigned int arg) 
  {
    if (!fExec) return 0;
    return fExec->Commit(arg);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuMemory::Abort() 
  {
    if (!fAbort) return 0;
    return fAbort->Commit();
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuMemory::Clear() 
  {
    if (!fClear) return 0;
    return fClear->Commit();
  }

  //____________________________________________________________________
  inline void
  RcuMemory::Zero()
  {
    for (size_t i = 0; i < fData.size(); i++) fData[i] = 0;
  }

  //____________________________________________________________________
  inline void
  RcuMemory::Print() const
  {
    std::cout << "Contents of " << fName << " (0x" 
	      << std::hex << fBase << std::dec << "/" << fSize 
	      << "): " << std::hex << std::setfill('0') << std::endl;
    for (size_t i = 0; i < fData.size(); i++) { 
      // if (i % 6 == 0 && i != 0) std::cout << std::endl;
      std::cout << " 0x" << std::setw(6) << fBase+i
		<< ": 0x" << std::setw(8) << fData[i] << std::endl;
    }
    std::cout << std::dec << std::setfill(' ') << std::flush;
  }

  //____________________________________________________________________
  inline bool
  RcuMemory::Load(std::istream& in) 
  {
    for (size_t i = 0; i < fData.size() && !in.eof(); i++) in >> fData[i];
    return true;
  }
    
  //____________________________________________________________________
  inline bool
  RcuMemory::Save(std::ostream& out) 
  {
    for (size_t i = 0; i < fData.size(); i++) 
      out << "  0x" << std::setfill('0') << std::setw(8) 
	  << std::hex << fData[i] << std::endl;
    out << std::dec;
    return true;
  }

  //____________________________________________________________________
  inline void
  RcuMemory::Set(size_t off, size_t n, unsigned int* d) 
  {
    for (size_t i = 0; i < n && i < fData.size() - off; i++) {
      fData[i + off] = (fMax & d[i]);
    }
  }

  //____________________________________________________________________
  inline void
  RcuMemory::Get(size_t off, size_t n, unsigned int* d) 
  {
    for (size_t i = 0; i < n && i < fData.size() - off; i++) {
      d[i] = (fMax & fData[i + off]);
    }
  }
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
