//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUFMIREG
# define RCU_RCUFMIREG
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuFMIREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the FirMware Input ReGister.
      @ingroup rcuxx_rcu
  */
  struct RcuFMIREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuFMIREG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "FMIREG", "FirMware Input REGister", 0, true), 
	fData(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return Data to write */
    unsigned int Data() const { return fData; }
    /** Set data to write */
    void SetData(unsigned int val) { fData = val; }
  protected:
    /** The data */
    unsigned int fData;

  };

  //====================================================================
  inline void
  RcuFMIREG::Decode(unsigned int dat) 
  { 
    fData = dat & 0xff;
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuFMIREG::Encode() const 
  { 
    return fData & 0xff;
  }

  //____________________________________________________________________
  inline void
  RcuFMIREG::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tData:\t\t\t\t0x" << std::hex << fData 
	      << std::dec << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
