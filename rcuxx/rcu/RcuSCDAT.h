//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUSCDAT
# define RCU_RCUSCDAT
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuSCDAT rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the @f$I^2C @f$ data register 
      @ingroup rcuxx_rcu
  */
  struct RcuSCDAT : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuSCDAT(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "SCDAT", "Slow Control DATa", 0, true), 
	fData(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return Data of data in memory 1 */
    unsigned int Data() const { return fData; }
    /** Set data */
    void SetData(unsigned int addr) { fData = addr; }
  protected:
    /** Data */
    unsigned int fData;
  };
  //====================================================================
  inline void
  RcuSCDAT::Decode(unsigned int dat) 
  { 
    fData = dat;
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuSCDAT::Encode() const 
  { 
    return fData;
  }

  //____________________________________________________________________
  inline void
  RcuSCDAT::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tData :\t\t\t" << std::hex << fData << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
