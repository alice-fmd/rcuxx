// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUCDH_H
# define RCUXX_RCUCDH_H
# include <rcuxx/rcu/RcuMemory.h>
# include <rcuxx/DebugGuard.h>

namespace Rcuxx
{
  //====================================================================
  /** 
   * @struct RcuCDH rcuxx/Rcu.h <rcuxx/Rcu.h>
   * @brief Interface to the RCU instruction memory
   * @ingroup rcuxx_rcu
   */
  struct RcuCDH  : public RcuMemory
  {
    typedef unsigned long long ulong64_t;
    /**
     * Constructor 
     *
     * @param rcu Reference to low-level interface 
     * @param base Base address
     * @param n Number of words  
     * @param mask Value mask
     * @param exec Execute command 
     * @param abort Abort command */
    RcuCDH(Rcu& rcu, size_t base, size_t n);
    bool Debug() const { return fDebug; }
    void SetDebug(bool debug=true) { fDebug = debug; }
    bool Is3() const { return fData[0] == 0xFFFFFFFF; }
    unsigned int   FormatVersion() const;
    unsigned int   L1Message()     const;
    unsigned int   BunchCrossing() const;
    unsigned int   OrbitNumber()   const;
    unsigned int   RcuVersion()    const;
    unsigned int   DetectorMask()  const;
    unsigned int   StatusError()   const;
    unsigned int   MiniID()        const;
    ulong64_t TriggerMask()  const;
    ulong64_t RegionOfInterest() const;
    void Print() const;
  protected:
    /**
     * Current marker 
     */
    bool fDebug;
  };
//====================================================================
  inline unsigned int
  RcuCDH::FormatVersion() const 
  { 
    return (fData[Is3() ? 1 : 0] >> 24) & 0xFF; 
  }
//====================================================================
  inline unsigned int
  RcuCDH::L1Message()     const 
  { 
    return (fData[Is3() ? 1 : 0] >> 14) & 0xFF; 
  }
//====================================================================
  inline unsigned int
  RcuCDH::BunchCrossing() const 
  { 
    return (fData[Is3() ? 1 : 0] >>  0) & 0xFFF; 
  }
//====================================================================
  inline unsigned int
  RcuCDH::OrbitNumber()   const 
  { 
    return (fData[Is3() ? 2 : 1] >>  0) & 0xFFFFFF;
  }
//====================================================================
  inline unsigned int
  RcuCDH::RcuVersion()    const 
  { 
    return (fData[Is3() ? 3 : 2] >> 24) & 0xFF; 
  }
//====================================================================
  inline unsigned int
  RcuCDH::DetectorMask()  const 
  { 
    return (fData[Is3() ? 3 : 2] >>  0) & 0xFFFFFF;
  }
//====================================================================
  inline unsigned int
  RcuCDH::StatusError()   const 
  { 
    return (fData[Is3() ? 4 : 3] >> 12) & 0xFFFF; 
  }
//====================================================================
  inline unsigned int
  RcuCDH::MiniID()        const 
  { 
    return (fData[Is3() ? 4 : 3 ] >>  0) & 0xFFF; 
  }
  //====================================================================
  inline
  RcuCDH::RcuCDH(Rcu& rcu, size_t base, size_t n) 
    : RcuMemory(rcu, "CDH", "Common data header", 
		base, n, 0xFFFFFFFF, false, 0, 0, 0)
  {
  }
  //====================================================================
  inline RcuCDH::ulong64_t
  RcuCDH::TriggerMask() const
  {
    return ulong64_t(fData[Is3() ? 5 : 4]) |
      (ulong64_t(fData[Is3() ? 6 : 5] & 0x3FFFF) << 32);
  }
  //====================================================================
  inline RcuCDH::ulong64_t
  RcuCDH::RegionOfInterest() const
  {
    return (ulong64_t((fData[Is3() ? 8 : 5] >> 28) & 0xF) | 
	    (ulong64_t(fData[Is3() ? 9 : 6]) << 4));
  }
  //====================================================================
  inline void
  RcuCDH::Print() const
  {
    RcuMemory::Print();
    std::cout << std::left
	      << "\tFormat version: "   <<std::setw(2) <<FormatVersion() 
	      << "\n" << std::setfill('0') << std::hex 
	      << "\tL1 message:     0x" <<std::setw(2) <<L1Message()     <<"\n"
	      << std::setfill(' ') << std::dec 
	      << "\tBunch crossing: "   <<std::setw(4) <<BunchCrossing() <<"\n"
	      << "\tOrbit:          "   <<std::setw(8) <<OrbitNumber()   <<"\n"
	      << std::setfill('0') << std::hex 
	      << "\tRCU Version:    0x" <<std::setw(2) << RcuVersion()   <<"\n"
	      << "\tDetectors:      0x" <<std::setw(6) << DetectorMask() <<"\n"
	      << "\tStatus:         0x" <<std::setw(4) << StatusError()  <<"\n"
	      << std::setfill(' ') << std::dec 
	      << "\tMini ID:        "   << std::setw(4)<< MiniID()       <<"\n"
	      << std::setfill('0') << std::hex 
	      << "\tTriggers:       0x" <<std::setw(13)<< TriggerMask()  <<"\n"
	      << "\tROI:            0x" <<std::setw(9) << RegionOfInterest() 
	      << std::right << std::dec << std::setfill(' ') << std::endl;
  }
      
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
