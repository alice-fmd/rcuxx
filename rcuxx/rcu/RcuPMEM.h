// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUPMEM_H
# define RCUXX_RCUPMEM_H
# include <rcuxx/rcu/RcuMemory.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuPMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU pedestal memory
      @ingroup rcuxx_rcu
  */
  struct RcuPMEM  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuPMEM(Rcu& rcu, size_t base, size_t n)
      : RcuMemory(rcu, "PMEM", "Pedestal memory", base, n, 0xffff, true) 
      {}
  };
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
