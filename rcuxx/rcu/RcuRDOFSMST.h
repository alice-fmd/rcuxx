//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURDOFSMST
# define RCU_RCURDOFSMST
# include <rcuxx/rcu/RcuRegister.h>
# include <rcuxx/rcu/RcuState_t.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRDOFSMST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ReaDOut Finite State Machine states
      register

      States of the FSMs for the read-out module
      @ingroup rcuxx_rcu
  */
  struct RcuRDOFSMST : public RcuRegister
  {
    struct SCEVLEN_t : public RcuState_t
    {
      SCEVLEN_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0: return "idle";
	case 1: return "chk_sprs";
	case 3: return "no_bcast";
	case 4: return "bcast";
	case 6: return "wait_fec";
	case 5: return "done";
	case 7: return "clrcnt";
	}
	return "unknown";
      }
    };
    struct EVLEN_t : public RcuState_t
    {
      EVLEN_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	return RcuState_t::AsString();
      }
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuRDOFSMST(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RDOFSMST", "Read-out FSM state", 0, false), 
	fSCEVLEN("SCEVLEN", 12, 0x7), 
	fEVLEN("EVLEN", 8, 0xf), 
	fEVRDO_CTRL("EVRDO_CTRL", 4, 0xf),
	fSEL_BR_B("SEL_BR_B", 2, 0x3), 
	fSEL_BR_A("SEL_BR_A", 0, 0x2)
    {}
    struct SEL_BR_t : public RcuState_t
    {
      SEL_BR_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0: return "idle";
	case 1: return "ckb_br";
	case 3: return "ex";
	}
	return "unknown";
      }
    };
    struct EVRDO_CTRL_t : public RcuState_t
    {
      EVRDO_CTRL_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0:   return "idle";
	case 1:   return "init";
	case 7:   return "chk_rolm";
	case 6:   return "chk_act_brch";
	case 4:   return "send_cmd_brb";
	case 5:   return "send_cmd_bra";
	case 12:  return "inc_rolm_add";
	case 13:  return "delay_2";
	case 3:   return "delay_1";
	case 2:   return "wait_br_free";
	case 15:  return "inc_rp";
	case 14:  return "wait_rp";
	case 10:  return "finish";
	}
	return "unknown";
      }
    };
    /** @return SEL_BR for branch B */
    const RcuState_t& SEL_BR_B() const { return fSEL_BR_B; } 
    /** @return SEL_BR for branch A */
    const RcuState_t& SEL_BR_A() const { return fSEL_BR_A; } 
    /** @return Scan event length */
    const RcuState_t& SCEVLEN() const { return fSCEVLEN; } 
    /** @return event Length */
    const RcuState_t&  EVLEN() const { return fEVLEN; } 
    /** @return Event readout control */
    const RcuState_t&  EVRDO_CTRL() const { return fEVRDO_CTRL; } 

    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      fSCEVLEN.Print();
      fEVLEN.Print();
      fEVRDO_CTRL.Print();
      fSEL_BR_B.Print();
      fSEL_BR_A.Print();
    }
    void Decode(unsigned int dat) { 
      fSCEVLEN.Decode(dat);
      fEVLEN.Decode(dat);
      fEVRDO_CTRL.Decode(dat);
      fSEL_BR_B.Decode(dat);
      fSEL_BR_A.Decode(dat);
    }
  protected:
    /** State */
    SCEVLEN_t fSCEVLEN;
    /** State */
    EVLEN_t fEVLEN;
    /** State */
    EVRDO_CTRL_t fEVRDO_CTRL;
    /** State */
    SEL_BR_t fSEL_BR_B;
    /** State */
    SEL_BR_t fSEL_BR_A;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
