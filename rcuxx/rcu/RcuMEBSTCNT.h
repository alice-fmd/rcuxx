//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUMEBSTCNT
# define RCU_RCUMEBSTCNT
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @class RcuMEBSTCNT rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the Multi-Event-Buffer STates and CouNTers  

      This records the state of the multi-event buffer as well as
      number of free slots. 
      @ingroup rcuxx_rcu 
  */
  struct RcuMEBSTCNT : public RcuRegister
  {

    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory
	@param clear Clear command */
    RcuMEBSTCNT(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "MEBSTCNT", 
		    "Multi Event Buffer STate and CouNTer", clear, false), 
	fCounts(0), 
	fDataReady(0),
	fDM_SEL1(false),
	fDM_SEL2(false)
    {}
    /** Decode @a dat as the TRigger MEBSTCNTs */
    virtual void Decode(unsigned int dat) 
    {
      fCounts    = (dat >> 0) & 0xf;
      fDataReady = (dat >> 4) & 0xf;
      fDM_SEL1   = ((dat >> 8) & 0x1) != 0;
      fDM_SEL2   = ((dat >> 9) & 0x1) != 0;
    }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tCounts:\t\t\t" << fCounts << "\n"
		<< "\tData ready:\t\t" << fDataReady << "\n" 
		<< "\tData memories:\t\t";
      if (fDM_SEL1) std::cout << "1 ";
      if (fDM_SEL2) std::cout << "2 ";
      std::cout << std::endl;
    }
    /** @return Recieved triggers */
    unsigned short Counts() const { return fCounts; }
    /** @return Recieved triggers */
    unsigned short DataReady() const { return fDataReady; }
    /** @return true if data memory 1 selected */
    bool DM1Selected() const { return fDM_SEL1; }
    /** @return true if data memory 2 selected */
    bool DM2Selected() const { return fDM_SEL2; }
  protected:
    /** How many triggers we got */
    unsigned short fCounts;
    /** Data ready ? */
    unsigned short fDataReady;
    /** Data memory 1 selected? */
    bool fDM_SEL1;
    /** Data memory 2 selected? */
    bool fDM_SEL2;

  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
