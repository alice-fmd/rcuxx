//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUALTROCFG1
# define RCU_RCUALTROCFG1
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuALTROCFG1 rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ALTRO configuration 1 register

      The content of this register is written to the RCU trailer for
      every event.  Note, that the register is used in no other way
      than that. That is, it is essentially just 32bits of
      user-definable data that will be appended to each event. 

      @ingroup rcuxx_rcu
  */
  struct RcuALTROCFG1 : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuALTROCFG1(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "ALTROCFG1","ALTRO ConFiGuration 1", 0, true), 
	f1stBMode(0),
	fPolarity(false),
	fZSPre(0),
	fZSPost(0),
	f2ndBEnable(false), 
	fZSGlitch(0),
	f2ndBPre(0),
	f2ndBPost(0), 
	fZSEnable(false)
    {}
    /** @return 1st baseline mode  - 4 bits */
    unsigned short  FirstBMode() const { return f1stBMode; }
    /** @return 2nd baseline mode pre-samples - 2 bits */
    unsigned short  SecondBPre() const { return f2ndBPre; }
    /** @return 2nd baseline mode post-samples - 3 bits */
    unsigned short  SecondBPost() const { return f2ndBPost; }
    /** @return 2nd baseline mode enable - 1 bit*/
    bool            IsSecondBEnable() const { return f2ndBEnable; }
    /** @return zero suppression glitch mode - 2 bit */
    unsigned short  ZSGlitch() const { return fZSGlitch; }
    /** @return Zero suppression pre-samples - 2 bits */
    unsigned short  ZSPre() const { return fZSPre; }
    /** @return Zero suppression post-samples - 4 bits */
    unsigned short  ZSPost() const { return fZSPost; }
    /** @return zero suppression enable - 1 bit*/
    bool            IsZSEnable() const { return fZSEnable; }
    /** @param x Polarity - 1 bit*/
    bool            Polarity() const { return fPolarity; }

    /** @param x 1st baseline mode */
    void SetFirstBMode(unsigned short  x) { f1stBMode = x; }
    /** @param x 2nd baseline mode pre-samples */
    void SetSecondBPre(unsigned short  x) { f2ndBPre = x; }
    /** @param x 2nd baseline mode post-samples */
    void SetSecondBPost(unsigned short  x) { f2ndBPost = x; }
    /** @param x 2nd baseline mode enable */
    void SetSecondBEnable(bool            x) { f2ndBEnable = x; }
    /** @param x 1st baseline mode */
    void SetZSMode(unsigned short  x) { fZSGlitch = x; }
    /** @param x 1st baseline mode */
    void SetZSGlitch(unsigned short  x) { fZSGlitch = x; }
    /** @param x 2nd baseline mode pre-samples */
    void SetZSPre(unsigned short  x) { fZSPre = x; }
    /** @param x 2nd baseline mode post-samples */
    void SetZSPost(unsigned short  x) { fZSPost = x; }
    /** @param x 2nd baseline mode enable */
    void SetZSEnable(bool            x) { fZSEnable = x; }
    /** @param x Polarity */
    void SetPolarity(bool x) { fPolarity = x; }


#if 0
    /** @return # of pre-L1 samples to store */
    unsigned short  PreTrigger() const { return fPreTrigger; }
    /** @return Tail cancellation enable */
    bool            IsTailEnable() const { return fTailEnable; }
    /** @param x # of pre-L1 samples to store */
    void SetPreTrigger(unsigned short  x) { fPreTrigger = x; }
    /** @param x Tail cancellation enable */
    void SetTailEnable(bool            x) { fTailEnable = x; }
#endif
    
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) 
    { 
      f1stBMode     = (dat >>  0) & 0xF;
      fPolarity     = (dat >>  4) & 0x1;
      fZSPre        = (dat >>  5) & 0x3;
      fZSPost       = (dat >>  7) & 0xF;
      f2ndBEnable   = (dat >> 11) & 0x1;
      fZSGlitch     = (dat >> 12) & 0x3;
      f2ndBPost     = (dat >> 14) & 0x7;
      f2ndBPre      = (dat >> 17) & 0x3;
      fZSEnable     = (dat >> 19) & 0x1;
    }
    /** Encode  */
    virtual unsigned int Encode() const 
    { 
      return (((f1stBMode   & 0xF) <<  0) | 
	      ((fPolarity   & 0x1) <<  4) |
	      ((fZSPre      & 0x3) <<  5) | 
	      ((fZSPost     & 0xF) <<  7) | 
	      ((f2ndBEnable & 0x1) << 11) | 
	      ((fZSGlitch   & 0x3) << 12) | 
	      ((f2ndBPost   & 0x7) << 14) | 
	      ((f2ndBPre    & 0x3) << 17) | 
	      ((fZSEnable   & 0x1) << 19));
    }
    /** Print to standard output */
    virtual void Print() const;
    bool Compare(unsigned int v) const
    {
      return Encode() == (v & 0xFFFFF);
    }
  protected:
    /** 1st baseline mode - bits 0 to 3*/
    unsigned short f1stBMode;
    /** Polarity - bit 4*/
    bool           fPolarity;
    /** Zero suppression pre samples - bits 5 to 6 */
    unsigned short fZSPre;
    /** Zero suppression post samples - bits 7 to 10 */
    unsigned short fZSPost;
    /** 2nd baseline mode enable - bit 11 */
    bool           f2ndBEnable;
    /** Zero suppression glitch filter - bits 12 to 13 */ 
    unsigned short fZSGlitch;
    /** 2nd baseline mode pre-samples - bits 14 to 16 */
    unsigned short f2ndBPre;
    /** 2nd baseline mode post-samples - bits 17 to 18 */
    unsigned short f2ndBPost;
    /** Zero suppression pre samples - bit 19 */
    bool fZSEnable;
  };
  //____________________________________________________________________
  inline void
  RcuALTROCFG1::Print() const
  {
    RcuRegister::Print();
    std::cout << std::hex
	      << "\t1st baseline mode:\t\t0x"       << f1stBMode << "\n"
	      << std::dec
	      << "\t2nd baseline mode pre-skip:\t"  << f2ndBPre << "\n"
	      << "\t2nd baseline mode post-skip:\t" << f2ndBPost << "\n"
	      << "\t2nd baseline mode enable:\t"    << f2ndBEnable << "\n"
	      << "\tZS glitch filter:\t\t"          << fZSGlitch << "\n"
	      << "\tZS pre-skip:\t\t\t"             << fZSPre << "\n"
	      << "\tZS post-skip:\t\t\t"            << fZSPost << "\n"
	      << "\tZS enable:\t\t\t"               << fZSEnable << "\n"
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
