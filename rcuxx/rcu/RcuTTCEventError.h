//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTTCEventError
# define RCU_RCUTTCEventError
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuTTCEventError rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the Trigger/Timing/Control error register.
      @ingroup rcuxx_rcu
  */
  struct RcuTTCEventError : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuTTCEventError(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "TTCEventError", 
		    "Trigger/Timing errors in event", 0, false), 
        fSerialBStop(false),	// Serial B stop bit error *
        f1BitAddress(false),	// Single bit hamming address error *
        f2BitAddress(false),	// Double bit hamming address error *
        f1BitBroadcast(false),	// Single bit hamming broadcast error *
        f2BitBroadcast(false),	// Double bit hamming broadcast error *
        fUnknownAddress(false),	// Unknown message address *
        fL1MsgIncomplete(false),	// Incomplete L1 message *
        fL2aMsgIncomplete(false),	// Incomplete L2a message *
        fROIMsgIncomplete(false),	// Incomplete ROI message *
        fTTCAddress(false),	// TTCrx address *
        fSporiusL0(false),	// Spurious L0 *
        fMissingL0(false),	// Missing L0 *
        fSporiusL1(false),	// Spurious L1 *
        fBoundaryL1(false),	// Boundary L1 *
        fMissingL1(false),	// Missing L1 *
        fL1MsgOut(false),	// Late/early L1Msg *
        fL1MsgMissing(false),	// Missing L1Msg *
        fL2MsgOut(false),	// Late/early L2Msg *
        fL2MsgMissing(false),	// Missing L2Msg *
        fROIMsgOut(false),	// Late/early ROIMsg *
        fROIMsgMissing(false),	// Missing ROIMsg *
        fPrePulse(false),	// Prepulse error *
        fL1MsgContent(false),	// L1Msg content error *
        fL2MsgContent(false),	// L2Msg content error *
        fROIMsgContent(false)	// ROIMsg content error *
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;

   /** @return Serial B stop bit error * */
    bool SerialBStop() const { return fSerialBStop; }
   /** @return Single bit hamming address error * */
    bool SingleBitAddress() const { return f1BitAddress; }
   /** @return Double bit hamming address error * */
    bool DoubleBitAddress() const { return f2BitAddress; }
   /** @return Single bit hamming broadcast error * */
    bool SingleBitBroadcast() const { return f1BitBroadcast; }
   /** @return Double bit hamming broadcast error * */
    bool DoubleBitBroadcast() const { return f2BitBroadcast; }
   /** @return Unknown message address * */
    bool UnknownAddress() const { return fUnknownAddress; }
   /** @return Incomplete L1 message * */
    bool L1MsgIncomplete() const { return fL1MsgIncomplete; }
   /** @return Incomplete L2a message * */
    bool L2aMsgIncomplete() const { return fL2aMsgIncomplete; }
   /** @return Incomplete ROI message * */
    bool ROIMsgIncomplete() const { return fROIMsgIncomplete; }
   /** @return TTCrx address * */
    bool TTCAddress() const { return fTTCAddress; }
   /** @return Spurious L0 * */
    bool SporiusL0() const { return fSporiusL0; }
   /** @return Missing L0 * */
    bool MissingL0() const { return fMissingL0; }
   /** @return Spurious L1 * */
    bool SporiusL1() const { return fSporiusL1; }
   /** @return Boundary L1 * */
    bool BoundaryL1() const { return fBoundaryL1; }
   /** @return Missing L1 * */
    bool MissingL1() const { return fMissingL1; }
   /** @return Late/early L1Msg * */
    bool L1MsgOut() const { return fL1MsgOut; }
   /** @return Missing L1Msg * */
    bool L1MsgMissing() const { return fL1MsgMissing; }
   /** @return Late/early L2Msg * */
    bool L2MsgOut() const { return fL2MsgOut; }
   /** @return Missing L2Msg * */
    bool L2MsgMissing() const { return fL2MsgMissing; }
   /** @return Late/early ROIMsg * */
    bool ROIMsgOut() const { return fROIMsgOut; }
   /** @return Missing ROIMsg * */
    bool ROIMsgMissing() const { return fROIMsgMissing; }
   /** @return Prepulse error * */
    bool PrePulse() const { return fPrePulse; }
   /** @return L1Msg content error * */
    bool L1MsgContent() const { return fL1MsgContent; }
   /** @return L2Msg content error * */
    bool L2MsgContent() const { return fL2MsgContent; }
   /** @return ROIMsg content error * */
    bool ROIMsgContent() const { return fROIMsgContent; }
  protected:
    /** Serial B stop bit error */ 
    bool fSerialBStop;
    /** Single bit hamming address error */ 
    bool f1BitAddress;
    /** Double bit hamming address error */ 
    bool f2BitAddress;
    /** Single bit hamming broadcast error */ 
    bool f1BitBroadcast;
    /** Double bit hamming broadcast error */ 
    bool f2BitBroadcast;
    /** Unknown message address */
    bool fUnknownAddress;
    /** Incomplete L1 message */
    bool fL1MsgIncomplete;
    /** Incomplete L2a message */
    bool fL2aMsgIncomplete;
    /** Incomplete ROI message */
    bool fROIMsgIncomplete;
    /** TTCrx address */
    bool fTTCAddress;
    /** Spurious L0 */
    bool fSporiusL0;
    /** Missing L0 */
    bool fMissingL0;
    /** Spurious L1 */
    bool fSporiusL1;
    /** Boundary L1 */
    bool fBoundaryL1;
    /** Missing L1 */
    bool fMissingL1;
    /** Late/early L1Msg */
    bool fL1MsgOut;
    /** Missing L1Msg */
    bool fL1MsgMissing;
    /** Late/early L2Msg */
    bool fL2MsgOut;
    /** Missing L2Msg */
    bool fL2MsgMissing;
    /** Late/early ROIMsg */
    bool fROIMsgOut;
    /** Missing ROIMsg */
    bool fROIMsgMissing;
    /** Prepulse error */
    bool fPrePulse;
    /** L1Msg content error */
    bool fL1MsgContent;
    /** L2Msg content error */
    bool fL2MsgContent;
    /** ROIMsg content error */
    bool fROIMsgContent;
  };
  //====================================================================
  inline void
  RcuTTCEventError::Decode(unsigned int data) 
  { 
    fSerialBStop	= (data & (1 <<  0));	// Serial B stop bit error *
    f1BitAddress	= (data & (1 <<  1));	// Single bit hamming address
    f2BitAddress	= (data & (1 <<  2));	// Double bit hamming address
    f1BitBroadcast	= (data & (1 <<  3));	// Single bit hamming broadcast
    f2BitBroadcast	= (data & (1 <<  4));	// Double bit hamming broadcast
    fUnknownAddress	= (data & (1 <<  5));	// Unknown message address *
    fL1MsgIncomplete	= (data & (1 <<  6));	// Incomplete L1 message *
    fL2aMsgIncomplete	= (data & (1 <<  7));	// Incomplete L2a message *
    fROIMsgIncomplete	= (data & (1 <<  8));	// Incomplete ROI message *
    fTTCAddress		= (data & (1 <<  9));	// TTCrx address *
    fSporiusL0		= (data & (1 << 10));	// Spurious L0 *
    fMissingL0		= (data & (1 << 11));	// Missing L0 *
    fSporiusL1		= (data & (1 << 12));	// Spurious L1 *
    fBoundaryL1		= (data & (1 << 13));	// Boundary L1 *
    fMissingL1		= (data & (1 << 14));	// Missing L1 *
    fL1MsgOut		= (data & (1 << 15));	// Late/early L1Msg *
    fL1MsgMissing	= (data & (1 << 16));	// Missing L1Msg *
    fL2MsgOut		= (data & (1 << 17));	// Late/early L2Msg *
    fL2MsgMissing	= (data & (1 << 18));	// Missing L2Msg *
    fROIMsgOut		= (data & (1 << 19));	// Late/early ROIMsg *
    fROIMsgMissing	= (data & (1 << 20));	// Missing ROIMsg *
    fPrePulse		= (data & (1 << 21));	// Prepulse error *
    fL1MsgContent	= (data & (1 << 22));	// L1Msg content error *
    fL2MsgContent	= (data & (1 << 23));	// L2Msg content error *
    fROIMsgContent	= (data & (1 << 24));	// ROIMsg content error *
  }

  //____________________________________________________________________
  inline void
  RcuTTCEventError::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tSerial B stop bit error *:\t\t" << fSerialBStop << "\n"
	      << "\tSingle bit hamming address error *:\t\t" << f1BitAddress << "\n"
	      << "\tDouble bit hamming address error *:\t\t" << f2BitAddress << "\n"
	      << "\tSingle bit hamming broadcast error *:\t\t" << f1BitBroadcast << "\n"
	      << "\tDouble bit hamming broadcast error *:\t\t" << f2BitBroadcast << "\n"
	      << "\tUnknown message address *:\t\t" << fUnknownAddress << "\n"
	      << "\tIncomplete L1 message *:\t\t" << fL1MsgIncomplete << "\n"
	      << "\tIncomplete L2a message *:\t\t" << fL2aMsgIncomplete << "\n"
	      << "\tIncomplete ROI message *:\t\t" << fROIMsgIncomplete << "\n"
	      << "\tTTCrx address *:\t\t" << fTTCAddress << "\n"
	      << "\tSpurious L0 *:\t\t" << fSporiusL0 << "\n"
	      << "\tMissing L0 *:\t\t" << fMissingL0 << "\n"
	      << "\tSpurious L1 *:\t\t" << fSporiusL1 << "\n"
	      << "\tBoundary L1 *:\t\t" << fBoundaryL1 << "\n"
	      << "\tMissing L1 *:\t\t" << fMissingL1 << "\n"
	      << "\tLate/early L1Msg *:\t\t" << fL1MsgOut << "\n"
	      << "\tMissing L1Msg *:\t\t" << fL1MsgMissing << "\n"
	      << "\tLate/early L2Msg *:\t\t" << fL2MsgOut << "\n"
	      << "\tMissing L2Msg *:\t\t" << fL2MsgMissing << "\n"
	      << "\tLate/early ROIMsg *:\t\t" << fROIMsgOut << "\n"
	      << "\tMissing ROIMsg *:\t\t" << fROIMsgMissing << "\n"
	      << "\tPrepulse error *:\t\t" << fPrePulse << "\n"
	      << "\tL1Msg content error *:\t\t" << fL1MsgContent << "\n"
	      << "\tL2Msg content error *:\t\t" << fL2MsgContent << "\n"
	      << "\tROIMsg content error *:\t\t" << fROIMsgContent << "\n"
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
