// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCURMEM_H
# define RCUXX_RCURMEM_H
# include <rcuxx/rcu/RcuMemory.h>
# include <rcuxx/DebugGuard.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU result memory
      @ingroup rcuxx_rcu
  */
  struct RcuRMEM  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  
	@param mask Bit mask */
    RcuRMEM(Rcu& rcu, size_t base, size_t n, unsigned int mask);
    /** Read ALTRO bus register 
	@param addr Full address of read register 
	@param data Data read 
	@param off  Offset in RMEM 
	@return 0 on success, error code otherwise */
    unsigned int FecRead(unsigned int addr, unsigned int& data, 
			 unsigned int off=0);
    /** Write ALTRO bus register 
	@param addr Full address of read register 
	@param data Data written 
	@param off  Offset in RMEM 
	@return 0 on success, error code otherwise */
    unsigned int FecWrite(unsigned int addr, unsigned int data, 
			 unsigned int off=0);
    /** ALTRO bus command
	@param addr Full address of command 
	@param off  Offset in RMEM 
	@return 0 on success, error code otherwise */
    unsigned int FecCommand(unsigned int addr, unsigned int off=0);
    void SetDebug(bool debug=true) { fDebug = debug; }
  protected:
    unsigned int Check(unsigned int type, unsigned int addr,
		       unsigned int off);
    bool fDebug;
  };
  //====================================================================
  inline RcuRMEM::RcuRMEM(Rcu& rcu, size_t base, size_t n, unsigned int mask) 
    : RcuMemory(rcu, "RMEM", "Result memory", base, n, mask, true),
      fDebug(true)
  {}
  
  //____________________________________________________________________
  inline unsigned int 
  RcuRMEM::FecWrite(unsigned int addr, 
		    unsigned int data, 
		    unsigned int off)
  {
    DebugGuard g(fDebug, "RcuRMEM::FecWrite(%x, %x, %d)", addr, data, off);
    if (fRcu.Layout().kRMEM_FEC_WR == fRcu.Layout().kInvalid) 
      return 0;
    
    unsigned int ret = 0;
    //    if ((ret = Update(off, 2)))   return ret;
    
    if ((ret = Check(fRcu.Layout().kRMEM_FEC_WR, addr, off))) return ret;
    if ((fData[off+1] & 0xFFFFF) != (data & 0xFFFFF)) {
      g.Message(fDebug, "At offset %d in RMEM: "
		"Got data 0x%06x, expected 0x%06x", off,
		(fData[off+1] & 0xFFFFF), (data & 0xFFFFF));
      return Rcu::kDataMismatch;
    }
    return ret;
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuRMEM::FecRead(unsigned int addr, unsigned int& data, unsigned int off)
  {
    DebugGuard g(fDebug, "RcuRMEM::FecRead(%x, %x, %d)", addr, data, off);
    
    unsigned int ret = 0;
    bool rcuII = fRcu.Layout().kRMEM_FEC_RD != fRcu.Layout().kInvalid;
    // if ((ret = Update(off, (rcuII ? 2 : 1))))   return ret;
    
    if (!rcuII) { 
      data = fData[off];
      return ret;
    }
    
    if ((ret = Check(fRcu.Layout().kRMEM_FEC_RD, addr, off))) return ret;
    data = (fData[off+1] & 0xFFFFF);
    
    return ret;
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuRMEM::FecCommand(unsigned int addr, unsigned int off)
  {
    DebugGuard g(fDebug, "RcuRMEM::FecCommand(%x, %d)", addr, off);
    if (fRcu.Layout().kRMEM_FEC_CMD == fRcu.Layout().kInvalid) 
      return 0;
    
    unsigned int ret = 0;
    if ((ret = Update(off, 2))) return ret;
    
    return Check(fRcu.Layout().kRMEM_FEC_CMD, addr, off);
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuRMEM::Check(unsigned int type, unsigned int addr, unsigned int off)
  {
    DebugGuard g(fDebug, "RcuRMEM::Check(%x,%d)", addr, off);
    unsigned int ret = 0;
    if ((fData[off] & (0xf << 21)) != type) { 
      g.Message(fDebug, "Instruction mismatch (Got 0x%x, expected 0x%x)",
		fData[off], type);
      ret = Rcu::kInstructionMismatch;
    }
    if (fData[off] & fRcu.Layout().kRMEM_ERROR) { 
      g.Message(fDebug, "Error in RMEM: 0x%03x", (fData[off+1] & 0x7FF));
      ret = Rcu::kRMemError + (fData[off+1] & 0x7FF);
    }
    else if ((fData[off] & 0xFFFFF) != addr) { 
      g.Message(fDebug, "Address mismatch in RMEM: Got 0x%x expected 0x%x", 
		fData[off] & 0xFFFFF, addr);
      return Rcu::kAddressMismatch;
    }
    return ret;
  }
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
