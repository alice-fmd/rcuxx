// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/rcu/RcuCommand.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUCOMMAND
# define RCUXX_RCUCOMMAND
# include <rcuxx/Command.h>

namespace Rcuxx
{
  //____________________________________________________________________
  // Forward declaration 
  class Rcu;
  
  //====================================================================
  /** @struct RcuCommand rcuxx/rcu/RcuCommand.h <rcuxx/rcu/RcuCommand.h>
      @brief Command interface 
      @ingroup rcuxx_rcu
  */
  struct RcuCommand : public Command
  {
  public:
    /** Constructor 
	@param rcu Rcu interface 
	@param name Name 
	@param title Title of command 
	@param cmd Command 
	@param hasarg Whether it takes arguments.  */
    RcuCommand(Rcu& rcu, const char* name, const char* title, 
	       unsigned int cmd=0, bool hasarg=false) 
      : Command(name, title, cmd), fRcu(rcu), fArg(0), fHasArgument(hasarg)
    {}
    /** Destructor */
    virtual ~RcuCommand(){}
    /** Send command to HW
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { return Commit(fArg);  }
    /** Send command to HW
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit(unsigned int arg)
    {
      fArg = (!fHasArgument ? 0 : arg);
      return fRcu.ExecCommand(fCmd, fArg);
    }
    /** Whether we take an argument or not */
    bool HasArgument() const { return fHasArgument; }
    /** Set argument */ 
    void SetArgument(unsigned int a) { fArg = a; }
  protected:
    /** Interface to RCU */ 
    Rcu&         fRcu;
    /** Name */ 
    // std::string  fName;
    /** Command */
    unsigned int fArg; 
    /** Whether arguments make sense or not */
    bool fHasArgument;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
