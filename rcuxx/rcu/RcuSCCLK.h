//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUSCCLK
# define RCU_RCUSCCLK
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuSCCLK rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief  @brief Interface to the @f$I^2C @f$ address register 
      @ingroup rcuxx_rcu
  */
  struct RcuSCCLK : public RcuRegister
  {
    enum Speed_t { 
      k5MHz, 
      k2500kHz, 
      k1250kHz, 
      k625kHz 
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuSCCLK(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "SCCLK", "Slow Control CLocK", 0, true), 
	fSpeed(k5MHz)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    Speed_t Speed() { return fSpeed; }
    void SetSpeed(Speed_t s) { fSpeed = s; }
    bool Compare(unsigned int v) const;
  protected:
    /** Data */
    Speed_t fSpeed;
  };
  //====================================================================
  inline void
  RcuSCCLK::Decode(unsigned int dat) 
  { 
    switch (dat) { 
    case 0:  fSpeed = k5MHz; break;
    case 1:  fSpeed = k2500kHz; break;
    case 2:  fSpeed = k1250kHz; break;
    case 3:  fSpeed = k625kHz; break;
    default: fSpeed = k5MHz; break;
    }
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuSCCLK::Encode() const 
  { 
    unsigned int ret = 0;
    switch (fSpeed) { 
    case k5MHz:    ret = 0; break;
    case k2500kHz: ret = 1; break;
    case k1250kHz: ret = 2; break;
    case k625kHz:  ret = 3; break;
    }
    return ret;
  }
  //____________________________________________________________________
  inline bool  
  RcuSCCLK::Compare(unsigned int v) const 
  { 
    return Encode() == (v & 0x3);
  }

  //____________________________________________________________________
  inline void
  RcuSCCLK::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tSpeed:\t\t\t";
    switch (fSpeed) { 
    case k5MHz:		std::cout << "5MHz";	break;
    case k2500kHz:	std::cout << "2500kHz";	break;
    case k1250kHz:	std::cout << "1250kHz";	break;
    case k625kHz:	std::cout << "625kHz";	break;
    }
    std::cout << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
