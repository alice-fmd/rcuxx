//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURDOFEC
# define RCU_RCURDOFEC
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRDOFEC rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the ReaDOut FrontEnd Card register.  

      This register is a bit mask of front-end cards to read out.  The
      monitoring and safety module can take out cards from this list
      if the card asserted a hard interrupt. 
      @ingroup rcuxx_rcu
  */
  struct RcuRDOFEC : public RcuRegister
  {
    /** Constructor  
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuRDOFEC(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RDOFEC", "Read-out Front-end card list",
		    0, false), fValue(0x0) {}

    /** Decode @a dat as ACTive Front-End-Cards mask  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return bit mask of active front-end cards */
    unsigned int Value() const { return fValue; }
    /** Check if card @a i is on */ 
    bool IsOn(unsigned int n) { return fValue & (1 << n); }
  protected:
    /** Value */
    unsigned int fValue;
  };
  //====================================================================
  inline void
  RcuRDOFEC::Decode(unsigned int dat) 
  { 
    fValue = dat;
  }

  //____________________________________________________________________
  inline void
  RcuRDOFEC::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tMask:\t\t\t\t0x" << std::hex 
	      << fValue << std::dec << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
