// -*- mode: c++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUEVWORD
# define RCU_RCUEVWORD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  
  //====================================================================
  /** @struct RcuEVWORD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the EVent WORD register.  

      The event word register is specific to the U2F version of the
      RCU.  It records the status of the last event. 
      @note This register is only available with the U2F interface 
      @ingroup rcuxx_rcu
  */
  struct RcuEVWORD : public RcuRegister
  {
    /** Constructor  
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuEVWORD(RcuMemory& mem, size_t offset, RcuCommand* clear);
    /** @return @c true if we have a trigger */ 
    virtual bool HasTrigger(size_t i=2) const;
    /** @return @c true if we are at start of trigger */ 
    virtual bool IsStart(size_t i=2)   const;
    /** @return @c true if we are at end of trigger */ 
    virtual bool IsEnd(size_t i=2)   const;
    /** @return @c true if we have data in DMEM1 or DMEM2 */ 
    virtual bool HasData(size_t i, size_t j=2) const;
    /** Decode @a dat as the EVent WORD register  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Get trigger word from view @a i */
    unsigned int View(size_t i) const { return i == 1 ? fView1 : fView2; }
    /** @return Get trigger word from view 1 */
    unsigned int View1() const { return fView1; }
    /** @return Get trigger word from view 2 */
    unsigned int View2() const { return fView2; }
  protected:
    /** Number of words in DMEM 1 */
    unsigned int fView1;
    /** Number of words in DMEM 2 */
    unsigned int fView2;
  };
  //__________________________________________________________________
  inline bool RcuEVWORD::HasTrigger(size_t j)  const
  {
    return (j == 1 ? bool(fView1 & 0x10) : bool(fView2 & 0x10));
  }
  //__________________________________________________________________
  inline bool RcuEVWORD::IsStart(size_t j) const
  {
    return (j == 1 ? bool(fView1 & 0x8) : bool(fView2 & 0x8));
  }
  //__________________________________________________________________
  inline bool RcuEVWORD::IsEnd(size_t j) const
  {
    return (j == 1 ? bool(fView1 & 0x4) : bool(fView2 & 0x4));
  }
  //__________________________________________________________________
  inline bool RcuEVWORD::HasData(size_t i, size_t j) const
  {
    return (j == 1 
	    ? bool(fView1 & (i == 1 ? 0x2 : 0x1)) 
	    : bool(fView2 & (i == 1 ? 0x2 : 0x1)));
  }
  //__________________________________________________________________
  inline 
  RcuEVWORD::RcuEVWORD(RcuMemory& mem, size_t offset, RcuCommand* clear)
    : RcuRegister(mem, offset, "EVWORD::RcuEVWORD", "EVent WORD", clear, false)
  {
    fView1 = fView2 = 0;
  }
  
  //____________________________________________________________________
  inline void
  RcuEVWORD::Decode(unsigned int dat) 
  { 
    fView1 = (dat >> 16) & 0xffff;
    fView2 = dat & 0xffff;
  }

  //____________________________________________________________________
  inline void
  RcuEVWORD::Print() const
  {
    RcuRegister::Print();
    for (size_t i = 1; i <= 2; i++) {
      std::cout << "\tBranch " << i << ":\t\t\t0x" << std::hex << View(i) 
		<< std::dec << " - ";
      if (HasTrigger(i)) std::cout << "trigger ";
      if (IsStart(i))    std::cout << "start ";
      if (IsEnd(i))      std::cout << "end ";
      if (HasData(1,i))  std::cout << "data1 ";
      if (HasData(1,i))  std::cout << "data2 ";
      std::cout << std::endl;
    }
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
