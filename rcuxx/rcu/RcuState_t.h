//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUState_t
# define RCU_RCUState_t
# include <string>
# include <sstream>
# include <iomanip>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuState_t rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Base class for state machine fields in registers.
      @ingroup rcuxx_rcu
  */
  struct RcuState_t
  {
    /** Constructor 
	@param name Name 
	@param off Offset in memory  
	@param mask Bit mask of valid bits */
    RcuState_t(const char* name, unsigned int off, unsigned int mask)
      : fName(name), fOff(off), fMask(mask), fState(0)
    {}
    /** Destructor */ 
    virtual ~RcuState_t() {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fState = ((dat >> fOff) & fMask); }
    /** GEt the name */
    const std::string& Name() const { return fName; }
    /** Return value of state */ 
    virtual unsigned int State() const { return fState; }
    /** Return state as a string */ 
    virtual const char* AsString() const 
    { 
      std::stringstream s;
      s << "State" << std::setw(2) << fState; 
      return s.str().c_str();
    }
    /** Print to standard output */
    virtual void Print() const
    {
      std::cout << "\t" << fName << ":\t\t" << AsString() << std::endl;
    }
    /** */
    virtual bool Compare(unsigned int v) const { return v == fState; }
  protected:
    /** Name of state */ 
    const std::string fName;
    /** Off set in passed word */
    const unsigned int fOff;
    /** The mask */ 
    const unsigned int fMask;
    /** The current state */ 
    unsigned int fState;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
