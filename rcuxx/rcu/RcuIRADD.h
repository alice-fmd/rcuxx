//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUIRADD
# define RCU_RCUIRADD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuIRADD rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache the last executed ALTRO InstRuction ADDress.  

      Last executed instruction memory address.
      @ingroup rcuxx_rcu
  */
  struct RcuIRADD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuIRADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "IRADD", "Last executed InstrRuction ADDress",
		    0, false), fIRADD(0) {}
    /** Decode @a dat as the last executed ALTRO InstRuction  ADDress  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Last instruction address */
    unsigned int IRADD() const { return fIRADD; }
  protected:
    /** Address of last instruction */
    unsigned int fIRADD;
  };
  //====================================================================
  inline void
  RcuIRADD::Decode(unsigned int dat) 
  { 
    fIRADD = (dat & 0xfffff);
  }

  //____________________________________________________________________
  inline void
  RcuIRADD::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tIRADD:\t\t\t\t0x" << std::hex << fIRADD << std::dec 
	      << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
