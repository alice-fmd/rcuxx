//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUFECERR
# define RCU_RCUFECERR
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuFECERR rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the Front-End Card ERRor register

      Errors in the communication with the front-end cards are
      recorded here. 
      @ingroup rcuxx_rcu
  */
  struct RcuFECERR : public RcuRegister
  {
    enum { 
      kWrPreError      = 0x01, // Error asserted before CSTB
      kWrPreAck        = 0x02, // Ack asserted before CSTB
      kWrWaitAckError  = 0x03, // Error while waiting
      kWrNoAck         = 0x04, // Missing acknowledge 
      kWrWaitDoneError = 0x05, // Error while waiting for data 
      kWrNoRelease     = 0x06, // ACK not released
      kWrBcError       = 0x07, // Error while waiting for BC
      // ---------
      kRdPreError      = 0x09, // Error while asserting bus
      kRdAckError      = 0x0A, // Error while waiting for data
      kRdPreAck        = 0x0B, // Ack asserted before CST
      kRdWaitAckError  = 0x0C, // Error while waiting for ACK
      kRdNoAck         = 0x0D, // No acknowledge
      kRdWaitDoneError = 0x0E, // Error while waiting for rellease 
      kRdNoRelease     = 0x0F, // Ack not releaseed 
      // ---------
      kRdoFSM          = 0x19, // FSM error 
      kRdoPreError     = 0x1A, // Error before TRSF
      kRdoNoTrsf       = 0x1B, // TRSF not asserted 
      kRdoTrsfError    = 0x1C, // Error during transfer 
      kRdoNoRelease    = 0x1D  // Trsf never released
    };
    
      
    /** Constructor 
	@param ab  A or B branch
	@param mem reference to memory 
	@param offset Offset in memory  
	@param clear Clear command */
    RcuFECERR(char ab, RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, 
		    (ab == 'a' || ab == 'A' ? "FECERRA" : "FECERRB"), 
		    "Front-End Card ERRors asserted on branch",
		    clear, false), 
	fData(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fData = dat; }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tData:\t\t\t" << fData << "\n\t";
      if (IsLoop())            std::cout << "Loop error\n";
      if (IsRcuInstrucion())   std::cout << "RCU instruction error\n";
      if (IsBcInstrucion())    std::cout << "BC instruction error\n";
      if (IsBcAddress())       std::cout << "BC address error\n";
      if (IsAltroInstrucion()) std::cout << "ALTRO instruction error\n";
      if (IsAltroAddress())    std::cout << "ALTRO address error\n";
      if (IsWrite())           std::cout << "Is a write error\n";
      if (IsRead())            std::cout << "Is a read error\n";
      if (IsRdo())             std::cout << "Is a read-out error\n";
      // ---------
      if (IsWrPreError())      std::cout << "Error asserted before CSTB\n";
      if (IsWrPreAck())        std::cout << "Ack asserted before CSTB\n";
      if (IsWrWaitAckError())  std::cout << "Error while waiting\n";
      if (IsWrNoAck())         std::cout << "Missing acknowledge \n";
      if (IsWrWaitDoneError()) std::cout << "Error while waiting for data\n";
      if (IsWrNoRelease())     std::cout << "ACK not released\n";
      if (IsWrBcError())       std::cout << "Error while waiting for BC\n";
      // ---------
      if (IsRdPreError())      std::cout << "Error while asserting bus\n";
      if (IsRdAckError())      std::cout << "Error while waiting for data\n";
      if (IsRdPreAck())        std::cout << "Ack asserted before CSTB\n";
      if (IsRdWaitAckError())  std::cout << "Error while waiting for ACK\n";
      if (IsRdNoAck())         std::cout << "No acknowledge\n";
      if (IsRdWaitDoneError()) std::cout << "Error while waiting for release\n";
      if (IsRdNoRelease())     std::cout << "Ack not releaseed \n";
      // ---------
      if (IsRdoFSM())          std::cout << "FSM error \n";
      if (IsRdoPreError())     std::cout << "Error before TRSF\n";
      if (IsRdoNoTrsf())       std::cout << "TRSF not asserted \n";
      if (IsRdoTrsfError())    std::cout << "Error during transfer \n";
      if (IsRdoNoRelease())    std::cout << "Trsf never released\n";
      std::cout << std::endl;
    }
    /** @return Address of data in memory 1 */
    unsigned int Data() const { return fData; }
    /** Whether it's a loop error */ 
    bool IsLoop() const { return fData & 0x1; }
    /** RCU instruction error */ 
    bool IsRcuInstrucion() const { return fData & 0x2; }
    /** BC instruction error */ 
    bool IsBcInstrucion() const { return fData & 0x4; }
    /** BC address error */ 
    bool IsBcAddress() const { return fData & 0x8; }
    /** ALTRO instruction error */ 
    bool IsAltroInstrucion() const { return fData & 0x10; }
    /** ALTRO address error */ 
    bool IsAltroAddress() const { return fData & 0x20; }
    /** Upper bits */ 
    unsigned int Upper() const { return (fData >> 6) & 0x1F; }
    /** Is a write error */
    bool IsWrite() const { return (Upper() >> 3) == 0; }
    /** Is a read error */
    bool IsRead() const { return (Upper() >> 3) == 1; }
    /** Is a read-out error */
    bool IsRdo() const { return (Upper() >> 3) == 3; }
    /** Error asserted before CSTB */
    bool IsWrPreError      () const { return Upper() & kWrPreError      ; }
    /** Ack asserted before CSTB */
    bool IsWrPreAck        () const { return Upper() & kWrPreAck        ; }
    /** Error while waiting */
    bool IsWrWaitAckError  () const { return Upper() & kWrWaitAckError  ; }
    /** Missing acknowledge  */
    bool IsWrNoAck         () const { return Upper() & kWrNoAck         ; }
    /** Error while waiting for data  */
    bool IsWrWaitDoneError () const { return Upper() & kWrWaitDoneError ; }
    /** ACK not released */
    bool IsWrNoRelease     () const { return Upper() & kWrNoRelease     ; }
    /** Error while waiting for BC */
    bool IsWrBcError       () const { return Upper() & kWrBcError       ; }
    // ---------
    /** Error while asserting bus */
    bool IsRdPreError      () const { return Upper() & kRdPreError      ; }
    /** Error while waiting for data */
    bool IsRdAckError      () const { return Upper() & kRdAckError      ; }
    /** Ack asserted before CST */
    bool IsRdPreAck        () const { return Upper() & kRdPreAck        ; }
    /** Error while waiting for ACK */
    bool IsRdWaitAckError  () const { return Upper() & kRdWaitAckError  ; }
    /** No acknowledge */
    bool IsRdNoAck         () const { return Upper() & kRdNoAck         ; }
    /** Error while waiting for rellease  */
    bool IsRdWaitDoneError () const { return Upper() & kRdWaitDoneError ; }
    /** Ack not releaseed  */
    bool IsRdNoRelease     () const { return Upper() & kRdNoRelease     ; }
    // ---------
    /** FSM error  */
    bool IsRdoFSM          () const { return Upper() & kRdoFSM          ; }
    /** Error before TRSF */
    bool IsRdoPreError     () const { return Upper() & kRdoPreError     ; }
    /** TRSF not asserted  */
    bool IsRdoNoTrsf       () const { return Upper() & kRdoNoTrsf       ; }
    /** Error during transfer  */
    bool IsRdoTrsfError    () const { return Upper() & kRdoTrsfError    ; }
    /** Trsf never released */
    bool IsRdoNoRelease    () const { return Upper() & kRdoNoRelease    ; }
    // ---------
  protected:
    /** Whether it's not responding */
    unsigned int fData;
  };
}
  
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
