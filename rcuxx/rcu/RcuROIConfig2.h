//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUROIConfig2
# define RCU_RCUROIConfig2
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuROIConfig2 rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the Eegion Of Interest Configuration register.  
      @ingroup rcuxx_rcu
  */
  struct RcuROIConfig2 : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuROIConfig2(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "ROIConfig2", 
		    "Region If Interest Configuration", 0, true), 
	fRegion(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** Set the region */
    void SetRegion(unsigned int x) { fRegion = x; }
    /** Get the region */
    unsigned int Region() const { return fRegion; }
  protected:
    /** Region */
    unsigned int fRegion;
  };
  //====================================================================
  inline void
  RcuROIConfig2::Decode(unsigned int dat) 
  {
    fRegion = (dat & 0x1FFFF);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuROIConfig2::Encode() const 
  { 
    return fRegion & 0x1FFFF;
  }

  //____________________________________________________________________
  inline void
  RcuROIConfig2::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tRegion:\t\t0x" << std::hex << fRegion 
	      << std::dec << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
