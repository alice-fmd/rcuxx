//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUBusStatus
# define RCU_RCUBusStatus
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @class RcuBusStatus rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the ALTRO bus status register.

      This register records the state of the ALTRO BUS control lines. 
      @ingroup rcuxx_rcu 
  */
  struct RcuBusStatus : public RcuRegister
  {
    /** Constructor 
	@param name Name of BusStatus 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuBusStatus(const char* name, 
		 const char* title, 
		 RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, name, title, 0, false), 
	fCSTB_A(false),
	fACK_A(false),
	fTRSF_A(false),
	fCSTB_B(false),
	fACK_B(false),
	fTRSF_B(false),
	fL1(false),
	fL2(false)
    {}
    /** Decode @a dat as the TRigger BusStatuss */
    virtual void Decode(unsigned int dat) 
    {
      fCSTB_A = (dat >> 0) & 0x1;
      fACK_A  = (dat >> 1) & 0x1;
      fTRSF_A = (dat >> 2) & 0x1;
      fCSTB_B = (dat >> 3) & 0x1;
      fACK_B  = (dat >> 4) & 0x1;
      fTRSF_B = (dat >> 5) & 0x1;
      fL1     = (dat >> 6) & 0x1;
      fL2     = (dat >> 7) & 0x1;
    }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tCSTB_A:\t\t\t"	<< fCSTB_A	<< "\n"
		<< "\tACK_A:\t\t\t"	<< fACK_A	<< "\n"
		<< "\tTRSF_A:\t\t\t"	<< fTRSF_A	<< "\n"
		<< "\tCSTB_B:\t\t\t"	<< fCSTB_B	<< "\n"
		<< "\tACK_B:\t\t\t"	<< fACK_B	<< "\n"
		<< "\tTRSF_B:\t\t\t"	<< fTRSF_B	<< "\n"
		<< "\tL1:\t\t\t"	<< fL1		<< "\n"
		<< "\tL2:\t\t\t"	<< fL2		
		<< std::endl;
    }
    /** @return Value of line CSTB on branch A */
    bool CSTB_A() const { return fCSTB_A; }
    /** @return Value of line ACK on branch A */
    bool ACK_A() const { return fACK_A; }
    /** @return Value of line TRSF on branch A */
    bool TRSF_A() const { return fTRSF_A; }
    /** @return Value of line CSTB on branch B */
    bool CSTB_B() const { return fCSTB_B; }
    /** @return Value of line ACK on branch B */
    bool ACK_B() const { return fACK_B; }
    /** @return Value of line TRSF on branch B */
    bool TRSF_B() const { return fTRSF_B; }
    /** @return Value of line L1 */
    bool L1() const { return fL1; }
    /** @return Value of line L2 */
    bool L2() const { return fL2; }
  protected:
    /** Value of line CSTB on branch A */
    bool fCSTB_A;
    /** Value of line ACK on branch A */
    bool fACK_A;
    /** Value of line TRSF on branch A */
    bool fTRSF_A;
    /** Value of line CSTB on branch B */
    bool fCSTB_B;
    /** Value of line ACK on branch B */
    bool fACK_B;
    /** Value of line TRSF on branch B */
    bool fTRSF_B;
    /** Value of line L1 */
    bool fL1;
    /** Value of line L2 */
    bool fL2;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
