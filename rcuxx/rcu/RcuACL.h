// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUACL_H
# define RCUXX_RCUACL_H
# include <rcuxx/rcu/RcuMemory.h>
# include <rcuxx/DebugGuard.h>
# include <list>
# include <algorithm>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuACL rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU Active Channel List memory

      The memory defines which channels should be read-out. 
      @ingroup rcuxx_rcu
  */
  struct RcuACL  : public RcuMemory
  {
    typedef std::list<unsigned int> ChannelList;
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  
	@param byIndex @c true if chip address set by index 
	@param perBranch  @c true if the memory is split in two */
    RcuACL(Rcu& rcu, size_t base, size_t n, bool byIndex, bool perBranch);
    /** 
     * Enable a channel
     * 
     * @param board   Board address
     * @param chip    ALTRO address
     * @param channel Channel address
     */
    void EnableChannel(unsigned int board, 
		       unsigned int chip, 
		       unsigned int channel);
    /** 
     * Disable a channel
     * 
     * @param board   Board address
     * @param chip    ALTRO address
     * @param channel Channel address
     */
    void DisableChannel(unsigned int board, 
			unsigned int chip, 
			unsigned int channel);
    /** 
     * Check if a channel is enabled
     * 
     * @param board   Board address
     * @param chip    ALTRO address
     * @param channel Channel address
     *
     * @return @c true if the channel is enabled
     */
    bool CheckChannel(unsigned int board, 
		      unsigned int chip, 
		      unsigned int channel) const;
    /** 
     * Enable some channels in a chip
     * 
     * @param board Board address
     * @param chip  ALTRO address
     * @param mask  Mask of channels to enable.
     */
    void EnableChip(unsigned int board, 
		    unsigned int chip, 
		    unsigned int mask=0xFFFF);
    /** 
     * Disable all channels in a chip
     * 
     * @param board Board address
     * @param chip  ALTRO address
     */
    void DisableChip(unsigned int board, unsigned int chip);
    /** 
     * Check if any channels in a chip are enabled
     * 
     * @param board Board address
     * @param chip  Chip address
     * 
     * @return Mask of enabled channels in the addressed chip
     */
    unsigned int CheckChip(unsigned int board, unsigned int chip) const;
    /** 
     * Enable a full board
     * 
     * @param board  Board address
     * @param masks  Array of channel masks of size @a max
     * @param max    Size of @a masks 
     */
    void EnableCard(unsigned int board, 
		    unsigned int* masks, unsigned int max);
    /** 
     * Disable all channels on a board
     * 
     * @param board Board address
     */
    void DisableCard(unsigned int board);
    /** 
     * Get the channels mask for a board
     * 
     * @param board Board address
     * @param mask  An array of size @a max.  On return, an array of
     *        enabled channels 
     * @param max Size of @a mask.
     */
    void CheckBoard(unsigned int board, unsigned int* mask, 
		    unsigned int max) const;
    /** 
     * Enable a board 
     */
    /** 
     * Alias for EnableCard
     * 
     * @param board Board address
     * @param mask  Array of size @a max of masks of enabled channels
     * @param max   Size of @a mask
     */
    void EnableBoard(unsigned int board, unsigned int* mask, unsigned int max);
    /** 
     * Alias for DisableCard.
     * 
     * @param board Board address
     */
    void DisableBoard(unsigned int board);
    
    /** 
     * Update the cache (restricted)
     *
     * @param off Where to start reading
     * @param n   How many words to read 
     *
     * @return 0 on success, error code otherwise 
     */
    virtual unsigned int Update(size_t off, size_t n);
    /** 
     * Write to the hardware (restricted)
     *
     * @param off Where to start writing
     * @param n   How many words to write
     *
     * @return 0 on success, error code otherwise 
     */
    virtual unsigned int Commit(size_t off, size_t n);
    /** 
     * Update the cache
     *
     * @return 0 on success, error code otherwise 
     */
    virtual unsigned int Update() { return Update(0, fSize); }
    /** 
     * Commit to the RCU 
     * 
     * @return 0 on success, error code otherwise 
     */
    virtual unsigned int Commit() { return Commit(0, fSize); }
    /** 
     * Clear the memory
     * 
     * @return 0 on success
     */
    virtual unsigned int Clear() { fChannels.clear(); return 0; }
    /** 
     * Zero (or clear) memory
     * 
     */
    virtual void Zero() { RcuMemory::Zero(); Clear(); }
    /** 
     * Get the list of channels. 
     * 
     * @return Return the list of enabled channels. 
     */
    const ChannelList& Channels() const { return fChannels; }
    /** 
     * Interleave the two branches.  With this order, the speed of the
     * read-out can be increased quite a bit. 
     * 
     */
    void Optimize();
    /** 
     * Print the contents
     * 
     */
    void Print() const;
  protected:
    /** 
     * Get the index (of old-style ACL) corresponding to a board and
     * chip. 
     * 
     * @param board Board address
     * @param chip  ALTRO address.
     * 
     * @return The index corresponding to the address.
     */
    unsigned int Addr2Idx(unsigned int board, unsigned int chip) const;
    /** 
     * Make and address 
     * 
     * @param board   Board number
     * @param chip    Chip number
     * @param channel Channel number 
     * 
     * @return Encoded address
     */
    unsigned int MakeAddr(unsigned int board, unsigned int chip, 
			  unsigned int channel) const;
    /** Whether cards/channels are index by location, or by entries */
    bool fByIndex;
    bool fPerBranch;
    /** Cache */
    ChannelList fChannels;
    /** @{ 
	@name Hide base class member functions */ 
    /** 
     * Set the cache
     * 
     * @param off Starting offset
     * @param n How many words (size of @a d)
     * @param d The values 
     */
    void Set(size_t off, size_t n, unsigned int* d)
    {
      RcuMemory::Set(off, n, d);
    }
    /** 
     * Get the cache 
     *
     * @param off Starting offset
     * @param n How many words (size of @a d)
     * @param d On return, the values 
     */
    void Get(size_t off, size_t n, unsigned int* d)
    {
      RcuMemory::Get(off, n, d);
    }
    /** 
     * @return Get the data 
     */
    Cache_t& Data() { return fData; }
    /** @} */
  };
  //====================================================================
  inline 
  RcuACL::RcuACL(Rcu& rcu, size_t base, size_t n, bool byIndex, bool perBranch) 
    : RcuMemory(rcu, "ACL", "Active Channel List", base, n,
		(byIndex ? 0xffff : 0xfff), true), 
      fByIndex(byIndex),
      fPerBranch(perBranch),
      fChannels(0)
  {}
  //____________________________________________________________________
  inline unsigned int
  RcuACL::Addr2Idx(unsigned int board, unsigned int chip) const
  {
    return board * 8 + chip;
  }
  //____________________________________________________________________
  inline unsigned int
  RcuACL::MakeAddr(unsigned int board, 
		   unsigned int chip, 
		   unsigned int channel) const
  {
    return (((board & 0x1F) << 7) | ((chip & 0x7) << 4) | (channel & 0xF));
  }
  //____________________________________________________________________
  inline void 
  RcuACL::EnableChannel(unsigned int board, unsigned int chip, 
			       unsigned int channel)
  {
    if (CheckChannel(board, chip, channel)) return;
    unsigned int addr = MakeAddr(board, chip, channel);
    // DebugGuard g(true, "Enabling channel 0x%02x/0x%1x/0x%02x (0x%03x)", 
    //              board, chip, channel, addr);
    fChannels.push_back(addr);
  }
  //____________________________________________________________________
  inline void 
  RcuACL::DisableChannel(unsigned int board, unsigned int chip, 
				unsigned int channel)
  {
    unsigned int addr  = MakeAddr(board, chip, channel);
    ChannelList::iterator i = std::find(fChannels.begin(),
				   fChannels.end(),addr);
    if (i == fChannels.end()) return;
    fChannels.erase(i);
  }
  //____________________________________________________________________
  inline bool
  RcuACL::CheckChannel(unsigned int board, unsigned int chip, 
			      unsigned int channel) const
  {
    unsigned int addr  = MakeAddr(board, chip, channel);
    ChannelList::const_iterator i = std::find(fChannels.begin(), 
					 fChannels.end(), addr);
    return i != fChannels.end();
  }
  //____________________________________________________________________
  inline void 
  RcuACL::EnableChip(unsigned int board, unsigned int chip, 
			    unsigned int mask)
  {
    for (size_t i = 0; i < 16; i++) { 
      unsigned int ch   = (1 << i);
      if (!(mask & ch)) 
	DisableChannel(board, chip, i);
      else 
	EnableChannel(board, chip, i);
    }
  }
  //____________________________________________________________________
  inline void 
  RcuACL::DisableChip(unsigned int board, unsigned int chip)
  {
    for (size_t i = 0; i < 16; i++)  
      DisableChannel(board, chip, i);
  }
  //____________________________________________________________________
  inline unsigned int
  RcuACL::CheckChip(unsigned int board, unsigned int chip) const
  {
    unsigned int mask  = 0;
    for (size_t j = 0; j < 16; j++) 
      if (CheckChannel(board, chip, j)) mask |= (1 << j);
    return mask;
  }

  //____________________________________________________________________
  inline void 
  RcuACL::EnableBoard(unsigned int board, unsigned int* mask, 
			     unsigned int max)
  {
    for (size_t i = 0; i < max; i++) EnableChip(board, i, mask[i]);
  }
  //____________________________________________________________________
  inline void 
  RcuACL::DisableBoard(unsigned int board)
  {
    for (size_t i = 0; i < 8; i++) DisableChip(board, i);
  }
  //____________________________________________________________________
  inline void 
  RcuACL::CheckBoard(unsigned int board, unsigned int* mask, 
			    unsigned int max) const
  {
    for (size_t i = 0; i < 8 && i < max; i++) 
      mask[i] = CheckChip(board, i);
  }
  //____________________________________________________________________
  inline unsigned int
  RcuACL::Update(size_t, size_t)
  {
    unsigned int ret;
    fChannels.clear();
    if ((ret = RcuMemory::Update(0, fSize))) { 
      std::cerr << "RcuMemory::Update returned error in RcuACL update: " 
		<< fRcu.ErrorString(ret) << " [" << ret << "]" << std::endl;
    }
    for (size_t i = 0; i < fSize; i++) { 
      if (fByIndex) { 
	unsigned int board = i / 8;
	unsigned int chip  = i % 8;
	for (size_t j = 0; j < 16; j++) 
	  if ((1 << j) & fData[i]) EnableChannel(board,chip, j);
      }
      else { 
	// std::cout << std::hex << fData[i] << std::dec << std::endl;
	if (fData[i] == 0xFFF) { 
	  if (fPerBranch) continue;
	  else            break;
	}
	unsigned int board   = (fData[i] >> 7) & 0x1F;
	unsigned int chip    = (fData[i] >> 4) & 0x7;
	unsigned int channel = (fData[i] >> 0) & 0xF;
	// When split into branch specific, the address does not 
	// contain the branch select bit, so we put it in if the entry
	// read is more than or equal 0x800=2048 (half the memory size)
	// This is a test
	if (fPerBranch && i >= fSize/2) board |= 0x10;
	EnableChannel(board, chip, channel);
      }
    }
    return ret;
  }
  //____________________________________________________________________
  inline unsigned int
  RcuACL::Commit(size_t, size_t)
  {
    size_t j = 0;
    size_t k = fSize/2;
    std::fill(fData.begin(), fData.end(), (fByIndex ? 0 : 0xfff));
    for (ChannelList::const_iterator i = fChannels.begin(); 
	 i != fChannels.end(); ++i) { 
      unsigned int board   = (*i >> 7) & 0x1F;
      if (fByIndex) { 
	unsigned int chip    = (*i >> 4) & 0x7;
	unsigned int channel = (*i >> 0) & 0xF;
	unsigned int idx   = Addr2Idx(board, chip);
	if (idx >= fSize) continue;
	fData[idx] |= (1 << channel);
      }
      else { 
	if (j >= fSize) break;
	if (fPerBranch) { 
	  // If the memory is split, do not put in the branch select 
	  // bit - that is we take only 11 of the bits 
	  if (board >= 0x10) fData[k++] = (*i & 0x7FF);
	  else               fData[j++] = *i;
	}
	else 
	  fData[j++] = *i;
      }
    }
    // Stop word
    if (!fByIndex && !fPerBranch) 
      for (;j < fSize; j++) fData[j] = 0xFFF;
    return RcuMemory::Commit(0, fSize);
  }
  //____________________________________________________________________
  inline void
  RcuACL::Optimize()
  {
    if (fByIndex) { 
      std::cout << "Cannot optimize for old-style ACL" << std::endl;
      return;
    }
    if (fPerBranch) { 
      std::cout << "Split ACL cannot be optimized further" << std::endl;
      return;
    }
    
    ChannelList target;
    ChannelList::iterator aIter = fChannels.begin();
    ChannelList::iterator bIter = fChannels.begin();
    
    while (aIter != fChannels.end()) { 
      unsigned int c1 = *aIter;
      char         b1 = ((c1 >> 11) & 0x1) ? 'B' : 'A';
      ++aIter;
      if (b1 == 'B') continue;
      
      target.push_back(c1);
      
      while (bIter != fChannels.end()) { 
	unsigned int c2 = *bIter;
	char         b2 = ((c2 >> 11) & 0x1) ? 'B' : 'A';
	++bIter;
	if (b1 != b2) { 
	  target.push_back(c2);
	  break;
	}
      }
    }
    while (bIter != fChannels.end()) { 
      target.push_back(*bIter);
      ++bIter;
    }
    fChannels.clear();
    fChannels.insert(fChannels.end(), target.begin(), target.end());
  }
  
  //____________________________________________________________________
  inline void
  RcuACL::Print() const
  {
    std::cout << "Enabled channels:\n  " 
	      << std::hex << std::setfill('0');
    size_t j = 0;
    for (ChannelList::const_iterator i = fChannels.begin(); 
	 i != fChannels.end(); ++i, j++) { 
      char         branch  = ((*i >> 11) & 0x1) ? 'B' : 'A';
      unsigned int board   = (*i >> 7)  & 0x1F;
      unsigned int chip    = (*i >> 4)  & 0x7;
      unsigned int channel = (*i >> 0)  & 0xF;
      std::cout << " "   << branch 
		<< "/0x" << std::setw(2) << board 
		<< "/0x" << std::setw(1) << chip 
		<< "/0x" << std::setw(1) << channel;
      if (j % 4 == 3) std::cout << "\n  ";
    }
    std::cout << std::dec << std::setfill(' ') << std::endl;

    std::cout << "  Per branch: " << (fPerBranch ? "yes" : "no") << "\n"
	      << "  By index:   " << (fByIndex   ? "yes" : "no") << "\n"
	      << "Raw Contents of " << fName << "[0x" << std::hex 
	      << fBase << "-" << fBase+fSize << "]: " << std::endl;
    if (fPerBranch) 
      std::cout << "Branch A" << std::endl;
    const int nPerLine = (80 - 10) / 7;
    j = 0;
    for (size_t i = 0; i < fData.size(); i++) { 
      if (fData[i] == 0xFFF) { 
	if (!fPerBranch) break;
	if (i > fData.size() / 2 - 1) break;
	i = fData.size() / 2 - 1;
	if (j % nPerLine != 0 && j != 0) std::cout << std::endl;
	std::cout << "Branch B" << std::flush;
	j = 0;
	continue;
      }
      if (j % nPerLine == 0 && j != 0) 
	std::cout << "\n  0x"  << std::setw(4) << std::setfill('0') 
		  << std::hex << i + fBase << ": " << std::flush;
      j++;
      std::cout << "  0x" << std::setfill('0') << std::setw(3) 
		<< std::hex << fData[i] << std::flush;
    }
    std::cout << std::dec << std::setfill(' ') << std::endl;

    // RcuMemory::Print();
  }
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
