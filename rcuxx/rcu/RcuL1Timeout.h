//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUL1Timeout
# define RCU_RCUL1Timeout
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuL1Timeout rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the L1 trigger timeout register.  

      The maximum number of 40MHz clock cycles to wait for an L1
      trigger after reception of the L0.
      @ingroup rcuxx_rcu
  */
  struct RcuL1Timeout : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuL1Timeout(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "L1Timeout", "L1 trigger Timeout", 0, true), 
	fTimeout(0), 
	fWindow(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { 
      fTimeout = (dat & 0xFFF);
      fWindow  = (dat >> 12) & 0xF;
    }
    
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const { 
      return (fTimeout & 0xFFF) | ((fWindow & 0xF) << 12);
    }
    bool Compare(unsigned int v) const 
    {
      return Encode() == (v & 0xFFFF);
    }
    /** Print to standard output */
    virtual void Print() const;
    /** @return BC to optimize readout */
    unsigned int Timeout() const { return fTimeout; }
    /** Use BC to optimize readout */
    void SetTimeout(unsigned int timeout) { fTimeout = timeout; } 
    /** @return BC to optimize readout */
    unsigned int Window() const { return fWindow; }
    /** Use BC to optimize readout */
    void SetWindow(unsigned int window) { fWindow = window; } 
  protected:
    /** Trigger source */ 
    unsigned short fTimeout;
    /** L2 latency wrt L1 */ 
    unsigned short fWindow;
  };
  //====================================================================
  //____________________________________________________________________
  inline void
  RcuL1Timeout::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tTimeout:\t\t\t\t" << fTimeout << " [25ns]\n"
	      << "\tWindow:\t\t\t\t" << fWindow << " [25ns]" << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
