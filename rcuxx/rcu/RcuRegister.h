// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/rcu/RcuRegister.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCU_RCUREGISTER
# define RCUXX_RCU_RCUREGISTER
# include <rcuxx/Register.h>
# include <rcuxx/rcu/RcuMemory.h>
# include <stdexcept>

namespace Rcuxx
{
  //____________________________________________________________________
  // Forward declaration 
  class Rcu;
  class RcuCommand;
  class RcuMemory;

  //====================================================================
  /** @class RcuRegister rcuxx/rcu/RcuRegister.h <rcuxx/rcu/RcuRegister.h>
      @brief Register interface 
      @ingroup rcuxx_rcu
  */
  class RcuRegister : public Register
  {
  public:
    /** 
     * A guard for the RCU registers 
     */
    struct Guard : public Register::Guard
    {
      /**
       * Constructor 
       * 
       * @param reg     Register 
       * @param verify  If we should verify/update 
       * @param debug   Debug messages 
       */
      Guard(RcuRegister& reg, bool verify, bool debug)
	: Register::Guard(verify, debug), fRegister(reg)
      {
	if (fVerify) { 
	  unsigned int ret = fRegister.Update();
	  if (ret != 0) { 
	    // If we failed to update the register, ignore it
	    throw std::runtime_error("Failed to update register");
	  }
	}
	// Get the encoded value of the register 
	fCached = fRegister.Encode();
	if (fDebug) {
	  std::cout << "On guard: ";
	  fRegister.Print();
	}
      }
      /** 
       * Destructor 
       */
      ~Guard()
      {
	if (fDebug) { 
	  fRegister.Update();
	  std::cout << "On exit: ";
	  fRegister.Print();
	}
	// Set value on the register 
	fRegister.Decode(fCached);
	
	// And commit to hardware 
	unsigned int ret = fRegister.Commit();
	if (ret != 0) 
	  throw std::runtime_error("Failed to restore register");
	
	if (fVerify && !fRegister.Verify()) 
	  throw std::runtime_error("Failed to validate register");
	if (fDebug) { 
	  fRegister.Update();
	  std::cout << "On restore: ";
	  fRegister.Print();
	}
      }
      /** Reference to register */
      RcuRegister& fRegister;
      /** The cached value */
      unsigned int fCached;
    };
    /** 
     * Create a guard 
     * 
     * @return A newly allocated guard 
     */
    virtual Register::Guard* CreateGuard(bool verify, bool debug)
    {
      return new Guard(*this, verify, debug);
    }
    /** 
     * Constructor 
     * 
     * @param mem     Memory 
     * @param offset  Offset in memory 
     * @param name    NAme of register
     * @param title   Title of register
     * @param clear   Possble clear command 
     * @param submit  Whether this is writeable  
     */
    RcuRegister(RcuMemory& mem, size_t offset, const char* name, 
		const char* title, 
		RcuCommand* clear, bool submit) 
      : Register(name, title, submit), 
	fMemory(mem), 
	fOffset(offset), 
	fClear(clear)
    {}
    /** Destructor */
    virtual ~RcuRegister() {}
    /** Update the cache of the register
	@return 0 on success, error code otherwise */
    virtual unsigned int Update();
    /** Clear the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Clear();
    /** Commit to the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
    /** Whether we got Clear */
    bool IsClearable() const { return fClear != 0; }
    /** Encode set value to register value */ 
    virtual unsigned int Encode() const { return 0; }
    /** Decode register value to set value */
    virtual void Decode(unsigned int dat) {}
    /** Verify that values corresponds to hardware values */
    virtual bool Verify();
    /** Compare passed value to the values in this object */
    virtual bool Compare(unsigned int v) const 
    { 
      return fSubmit ? v == Encode() : true; 
    }
    /** Print */
    virtual void Print() const;
    /** Get reference to mother */
    Rcu& Mother() { return fMemory.Mother(); } 
    /** Get reference to mother */
    const Rcu& Mother() const { return fMemory.Mother(); } 
  protected:
    /** Base memory */
    RcuMemory&  fMemory;
    /** Offset in base memory */
    size_t      fOffset;
    /** Whether we can clear */
    RcuCommand* fClear;
  };

  //====================================================================
  inline unsigned int
  RcuRegister::Update()
  {
    unsigned ret = 0, dat = 0;
    if ((ret = fMemory.Update(fOffset, 1))) return ret;
    fMemory.Get(fOffset, 1, &dat);
    Decode(dat);
    return ret;
  }
  
  //____________________________________________________________________
  inline bool 
  RcuRegister::Verify()
  {
    // unsigned int thisValue = Encode();
    unsigned int ret  = 0;
    bool         same = false;
    if ((ret = fMemory.Update(fOffset, 1)) == 0) {
      unsigned int hwValue = 0;
      fMemory.Get(fOffset, 1, &hwValue);
      same = Compare(hwValue);
    }
    // fMemory.Set(fOffset, 1, &thisValue);
    return same;
  }
  //____________________________________________________________________
  inline unsigned int
  RcuRegister::Clear() 
  { 
    unsigned int ret = 0;
    if (fClear) {
      if ((ret = fClear->Commit())) return ret;
      ret = Update();
    }
    return ret;
  }

  //____________________________________________________________________
  inline unsigned int
  RcuRegister::Commit() 
  {
    if (!fSubmit) return 0;
    unsigned int dat = Encode();
    fMemory.Set(fOffset, 1, &dat);
    return fMemory.Commit(fOffset, 1);
  }
  //____________________________________________________________________
  inline void
  RcuRegister::Print() const
  {
    unsigned int val = Encode();
    std::cout << "Contents of register " << fName << " (0x" 
	      << std::setfill('0') << std::hex 
	      << std::setw(4)      << fMemory.Base() + fOffset << "): 0x"
	      << std::setw(8)      << val << " "
	      << std::dec << std::setfill(' ') << val
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
