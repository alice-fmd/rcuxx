// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUHEADER_H
# define RCUXX_RCUHEADER_H
# include <rcuxx/rcu/RcuMemory.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuHEADER rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU Event Header memory
      @ingroup rcuxx_rcu
      The header has the following format 
      @verbatim 
      32     28   24       18    14    12
      +-----+-----+--------+-----+-----+--------------+
      |              Block lenght                     |
      +-----------+--------------+-----+--------------+
      |  Version  | L1 message   | MBZ | Event Id 1   |
      +-----------+--------------+-----+--------------+
      |   MBZ     |           Event Id 2              |
      +-----------+-----------------------------------+
      | Attribu   |        Sub-detector mask          |
      +-----+-----+--------------------+--------------+
      | MBZ |      Status & Error      | Mini Event   |
      +-----+--------------------------+--------------+
      |         Trigger class (low bits)              |
      +-----+--------------+--------------------------+
      | ROI |     MBZ      |   Trigger class (high)   |
      +-----+--------------+--------------------------+
      |              ROI (high bits)                  |
      +-----------------------------------------------+
      @endverbatim
  */  
  struct RcuHEADER  : public RcuMemory
  {
    typedef unsigned long long ulong64_t;
    
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuHEADER(Rcu& rcu, size_t base, size_t n)
      : RcuMemory(rcu, "HEADER", "Comment evnet HEADER", base, n, 0xffff, true) 
    {
      fData[0] = 0x00ff;
    }

    /** @return block lenght (optional) */
    unsigned int BlockLength() const { return fData[0]; }
    /** @return Format version number */
    unsigned int FormatVersion() const { return (fData[1] >> 24); }
    /** @return  L1 trigger message */
    unsigned int L1Message() const { return (fData[1] >> 14) & 0x3FF; }
    /** @return  Bunch crossing number */
    unsigned int BunchCrossing() const { return (fData[1] & 0xFFF); }
    /** @return  Orbit number */
    unsigned int Orbit() const { return (fData[2] & 0xFFFFFF); }
    /** @return  Block attributes */
    unsigned int Attributes() const { return (fData[3] >> 24); }
    /** @return  Participting sub-detectors */
    unsigned int SubDetectors() const { return (fData[3] & 0xFFFFFF); }
    /** @return  Status and error bits */
    unsigned int StatusErrors() const { return (fData[4] >> 12) & 0xFFFF; } 
    /** @return  Mini event id */
    unsigned int MiniBunchCrossing() const { return fData[4] & 0xFFF; }
    /** @return  Trigger classes */
    unsigned long long TriggerClasses() const 
    { 
      return fData[5] + ((ulong64_t(fData[6]) & 0x3FFFF) << 32);
    }
    /** @return Region of intrest */
    unsigned long long RegionOfIntrest() const 
    { 
      return (fData[6] >> 28) 
	+ ((ulong64_t(fData[7]) & 0xFFFFFFFF) << 4);
    }
    /** @{ 
	@name Status and error bits */
    bool TriggerOverlap() const { return StatusErrors() & 0x1; }
    bool TriggerMissing() const { return StatusErrors() & 0x2; }
    bool DataParityError() const { return StatusErrors() & 0x4; }
    bool ControlParityError() const { return StatusErrors() & 0x8; }
    bool NoTriggerInfo() const { return StatusErrors() & 0x10; }
    bool FeeError() const { return StatusErrors() & 0x20; }
    /** @} */
     

    /** @param x block lenght (optional) */
    void  SetBlockLength(unsigned int x) { fData[0] = x; }
    /** @param x Format version number */
    void  SetFormatVersion(unsigned int x) { fData[1] += (x & 0xFF) << 24; }
    /** @param x  L1 trigger message */
    void  SetL1Message(unsigned int x) { fData[1] += (x  & 0x3FF) << 14; }
    /** @param x  Bunch crossing number */
    void  SetBunchCrossing(unsigned int x) { fData[1] += x & 0xFFF; }
    /** @param x  Orbit number */
    void  SetOrbit(unsigned int x) { fData[2] = x & 0xFFFFFF; }
    /** @param x  Block attributes */
    void  SetAttributes(unsigned int x) { fData[3] += (x & 0xFF) << 24; }
    /** @param x  Participting sub-detectors */
    void  SetSubDetectors(unsigned int x) { fData[3] += x & 0xFFFFFF; }
    /** @param x  Status and error bits */
    void  SetStatusErrors(unsigned int x) { fData[4] += (x & 0xFFFF) << 12; } 
    /** @param x  Mini event id */
    void  SetMiniBunchCrossing(unsigned int x) { fData[4] += x & 0xFFF; }
    /** @param x  Trigger classes */
    void SetTriggerClasses(unsigned long long x)
    { 
      fData[5] =  (x & 0xFFFFFFFF);
      fData[6] += (x >> 32) & 0x3FFFF;
    }
    /** @param x Region of intrest */
    void SetRegionOfIntrest(unsigned long long x) 
    { 
      fData[6] += (x & 0xF) << 28;
      fData[7] =  (x >> 8) & 0xFFFFFFFF;
    }
    
  };
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
