//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUINTREG
# define RCU_RCUINTREG
# include <rcuxx/rcu/RcuRegister.h>

#include <rcuxx/rcu/RcuStatusEntry.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuINTREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the INTerrupt REGister 
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuINTREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuINTREG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "INTREG", "INTerrupt REGister", 0, false), 
	fEntry(true, 0, false, false, false, false, false, false, 
	       false, false, false, false)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fEntry.Decode(dat); }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      fEntry.Print();
    }
    /** @return If the card causing the interrupt is identified */
    bool IsIdentified() const { return fEntry.IsIdentified(); }
    /** @return the hardware address of the card that caused the
	interrupt.  If the 4th bit (counting from 0) is set, then it's
	on branch B */
    unsigned int Where() const { return fEntry.Where(); }
    /** @return @c true if the interrupt was soft */
    bool IsSoft() const { return fEntry.IsSoft(); }
    /** @return @c true if the card is answering */
    bool IsAnswering() const { return fEntry.IsAnswering(); }
    /** @return If a missed sample clocks interrupt is set */
    bool IsMissedSclk() const { return fEntry.IsMissedSclk(); } 
    /** @return If a ALTRO power supply error interrupt is set */
    bool IsAlps() const { return fEntry.IsAlps(); } 
    /** @return If a PASA power supply error interrupt is set */
    bool IsPaps() const { return fEntry.IsPaps(); } 
    /** @return If a digital current over threshold interrupt is set */
    bool IsDcOverTh() const { return fEntry.IsDcOverTh(); } 
    /** @return If a digital voltage over threshold interrupt is set */
    bool IsDvOverTh() const { return fEntry.IsDvOverTh(); } 
    /** @return If a analog current over threshold interrupt is set */
    bool IsAcOverTh() const { return fEntry.IsAcOverTh(); } 
    /** @return If a analog voltage over threshold interrupt is set */
    bool IsAvOverTh() const { return fEntry.IsAvOverTh(); } 
    /** @return If a temperture over threshold interrupt is set */
    bool IsTempOverTh() const { return fEntry.IsTempOverTh(); } 
    /** @return Get the entry */
    const RcuStatusEntry& Entry() const { return fEntry; }
  protected:
    /** Status entry */
    RcuStatusEntry fEntry;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
