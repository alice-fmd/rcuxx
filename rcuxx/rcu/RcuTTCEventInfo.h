//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTTCEventInfo
# define RCU_RCUTTCEventInfo
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuTTCEventInfo rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the Trigger/Timing/Control information register.  
      @ingroup rcuxx_rcu
  */
  struct RcuTTCEventInfo : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuTTCEventInfo(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "TTCEventInfo", 
		    "Trigger/Timing Event information", 
		    0, false), 
	fROIEnabled(false),	// ROI enabled 
	fROIAnnounced(false),	// ROI announced 
	fROIRecieved(false),	// ROI recieved 
	fWithinROI(false),	// Within ROI 
	fSwCalTrgType(0),	// Calibration/software trigger type 
	fSwTrg(false),		// Software trigger 
	fCalTrg(false),		// Calibration trigger 
	fHasL2R(false),		// Has L2 reject trigger 
	fHasL2A(false),		// Has L2 accept trigger 
	fHasPayload(false),	// Has payload 
	fSCLKPhase(0)		// Slow clock phase at L0/L1
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;

    /** @return ROI enabled * */
    bool ROIEnabled() const { return fROIEnabled; }
    /** @return ROI announced * */
    bool ROIAnnounced() const { return fROIAnnounced; }
    /** @return ROI recieved * */
    bool ROIRecieved() const { return fROIRecieved; }
    /** @return Within ROI * */
    bool WithinROI() const { return fWithinROI; }
    /** @return Calibration/software trigger type * */
    unsigned char SwCalTrgType() const { return fSwCalTrgType; }
    /** @return Software trigger * */
    bool SwTrg() const { return fSwTrg; }
    /** @return Calibration trigger * */
    bool CalTrg() const { return fCalTrg; }
    /** @return Has L2 reject trigger * */
    bool HasL2R() const { return fHasL2R; }
    /** @return Has L2 accept trigger * */
    bool HasL2A() const { return fHasL2A; }
    /** @return Has payload * */
    bool HasPayload() const { return fHasPayload; }
    /** @return Slow clock phase at L0/L1 * */
    unsigned char SCLKPhase() const { return fSCLKPhase; }
  protected:
    /** ROI enabled */
    bool fROIEnabled;
    /** ROI announced */
    bool fROIAnnounced;
    /** ROI recieved */
    bool fROIRecieved;
    /** Within ROI */
    bool fWithinROI;
    /** Calibration/software trigger type */
    unsigned char fSwCalTrgType;
    /** Software trigger */
    bool fSwTrg;
    /** Calibration trigger */
    bool fCalTrg;
    /** Has L2 reject trigger */
    bool fHasL2R;
    /** Has L2 accept trigger */
    bool fHasL2A;
    /** Has payload */
    bool fHasPayload;
    /** Slow clock phase at L0/L1 */
    unsigned char fSCLKPhase;
  };
  //====================================================================
  inline void
  RcuTTCEventInfo::Decode(unsigned int data) 
  { 
    fROIEnabled		= (data & (1 <<  0));	// ROI enabled *
    fROIAnnounced	= (data & (1 <<  1));	// ROI announced *
    fROIRecieved	= (data & (1 <<  2));	// ROI recieved *
    fWithinROI		= (data & (1 <<  3));	// Within ROI *
    fSwCalTrgType	= ((data >> 4) & 0xf);	// Calibration/software trigger
    fSwTrg		= (data & (1 <<  8));	// Software trigger *
    fCalTrg		= (data & (1 <<  9));	// Calibration trigger *
    fHasL2R		= (data & (1 << 10));	// Has L2 reject trigger *
    fHasL2A		= (data & (1 << 11));	// Has L2 accept trigger *
    fHasPayload		= (data & (1 << 12));	// Has payload *
    fSCLKPhase		= ((data >> 13) & 0xf);	// Slow clock phase at L0/L1 *
  }
  //____________________________________________________________________
  inline void
  RcuTTCEventInfo::Print() const
  {
    RcuRegister::Print();
    std::cout << "ROI enabled *:\t\t" << fROIEnabled << "\n"
	      << "ROI announced *:\t\t" << fROIAnnounced << "\n"
	      << "ROI recieved *:\t\t" << fROIRecieved << "\n"
	      << "Within ROI *:\t\t" << fWithinROI << "\n"
	      << "Calibration/software trigger type *:" 
	      << unsigned(fSwCalTrgType) <<"\n"
	      << "Software trigger *:\t\t" << fSwTrg << "\n"
	      << "Calibration trigger *:\t\t" << fCalTrg << "\n"
	      << "Has L2 reject trigger *:\t\t" << fHasL2R << "\n"
	      << "Has L2 accept trigger *:\t\t" << fHasL2A << "\n"
	      << "Has payload *:\t\t" << fHasPayload << "\n"
	      << "Slow clock phase at L0/L1 *:\t\t" << unsigned(fSCLKPhase)
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
