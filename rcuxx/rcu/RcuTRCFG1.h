//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTRCFG1
# define RCU_RCUTRCFG1
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuTRCFG1 rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the first TRigger ConFiG register.  
      @ingroup rcuxx_rcu
  */
  struct RcuTRCFG1 : public RcuRegister
  {
    /** Buffer size */
    enum BMD_t {
      /** 4 Buffers */
      k4Buffers,
      /** 8 Buffers  */
      k8Buffers
    };
    /** Mode buttons */
    enum Mode_t {
      /** Software trigger */
      kInternal = 0, 
      /** Ext. L1, internal L2 */
      kDerivedL2 = 2, 
      /** Ext. L1 and L2 */
      kExternal = 3
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory
	@param clear Clear command */
    RcuTRCFG1(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "TRCFG1", "TRigger ConFiGuration", 
		    clear, true), 
	fOpt(false),
	fPop(true),
	fBMD(k4Buffers),
	fMode(kDerivedL2), 
	fTwv(4096),
	fWptr(0),
	fRptr(0),
	fRemb(0),
	fEmpty(false),
	fFull(false)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    bool Compare(unsigned int v) const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return BC to optimize readout */
    bool IsOpt() const { return fOpt; }
    /** Use BC to optimize readout */
    void SetOpt(bool on=true) { fOpt = on; } 
    /** @return Popped readout e.g., via U2F */
    bool IsPop() const { return fPop; }
    /** Popped readout e.g., via U2F */
    void SetPop(bool on=true) { fPop = on; }
    /** Buffer MoDe input/output */
    BMD_t BMD() const { return fBMD; }
    /** Buffer MoDe input/output */
    void SetBMD(BMD_t bmd) { fBMD = bmd; }
    /** Mode buttons */
    Mode_t Mode() const { return fMode; }
    /** Mode buttons */
    void SetMode(Mode_t mode) { fMode = mode; }
    /** @return # clock cycles between L1 and L2 */
    unsigned int Twv() const { return fTwv; }
    /** Set # clock cycles between L1 and L2 */
    void SetTwv(unsigned int val) { fTwv = val; }
    /** Write pointer */
    unsigned int Wptr() const { return fWptr; }
    /** Read pointer */
    unsigned int Rptr() const { return fRptr; }
    /** Free buffers */
    unsigned int Remb() const { return fRemb; }
    /** @return is empty? */
    bool IsEmpty() const { return fEmpty; }
    /** @return is Full? */
    bool IsFull() const { return fFull; }
  protected:
    /** Use BC to optimize readout */
    bool fOpt;
    /** Popped readout e.g., via U2F */
    bool fPop;
    /** Buffer MoDe input/output */
    BMD_t fBMD;
    /** Mode buttons */
    Mode_t fMode;
    /** # clock cycles between L1 and L2 */
    unsigned int fTwv;
    /** Write pointer */
    unsigned int fWptr;
    /** Read pointer */
    unsigned int fRptr;
    /** Free buffers */
    unsigned int fRemb;
    /** Wether buffers are empty */
    bool fEmpty;
    /** Whether buffers are full */
    bool fFull;
  };
  //====================================================================
  inline void
  RcuTRCFG1::Decode(unsigned int dat) 
  { 
    // DebugGuard g(fDebug, "RcuTRCFG1::Decode(0x%08lx)", dat);
    fTwv = dat & 0x3fff;
    fBMD = ((dat >> 14) & 0x1 ? k8Buffers : k4Buffers);
    switch ((dat >> 15) & 0x3) {
    case 0: fMode = kInternal;  break;
    case 2: fMode = kDerivedL2; break;
    case 3: fMode = kExternal;  break;
    }
    fOpt   = (dat >> 17) & 0x1;
    fPop   = (dat >> 18) & 0x1;
    fRemb  = (dat >> 20) & 0xf;
    fEmpty = (dat >> 24) & 0x1;
    fFull  = (dat >> 25) & 0x1;
    fRptr  = (dat >> 26) & 0x3;
    fWptr  = (dat >> 29) & 0x3;
#if 0
    fOpt   = (dat >> 19) & 0x1;
    fPop   = (dat >> 18) & 0x1;
    fRemb  = (dat >> 17) & 0xf;
    fEmpty = (dat >> 21) & 0x1;
    fFull  = (dat >> 22) & 0x1;
    fRptr  = (dat >> 23) & 0x7;
    fWptr  = (dat >> 26) & 0x7;
#endif
  }
  //____________________________________________________________________
  inline bool 
  RcuTRCFG1::Compare(unsigned int v) const
  {
    return Encode() == (v & 0x7FFFF);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuTRCFG1::Encode() const 
  { 
    // DebugGuard g(fDebug, "RcuTRCFG1::Encode()");
    unsigned int dat = 0;
    dat += fTwv & 0x3fff;
    dat += (fBMD == k8Buffers) ? 0x1 << 14 : 0;
    switch (fMode) {
    case kInternal:  dat += (0 & 0x3) << 15; break;
    case kDerivedL2: dat += (2 & 0x3) << 15; break;
    case kExternal:  dat += (3 & 0x3) << 15; break;
    }
    dat += (fOpt  & 0x1) << 17;
    dat += (fPop  & 0x1) << 18;
    // DebugGuard::Message(fDebug, "Returning the value 0x%08lx", dat);
    return dat;
  }

  //____________________________________________________________________
  inline void
  RcuTRCFG1::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tBC optimised:\t\t\t" << fOpt << "\n"
	      << "\tPop mode:\t\t\t"     << fPop << "\n"
	      << "\tEmpty:   \t\t\t"     << fEmpty << "\n"
	      << "\tFull:   \t\t\t"      << fFull << "\n"
	      << "\t# buffers:\t\t\t"    << (fBMD == k4Buffers ? 4 : 8) << "\n"
	      << "\tTrigger mode:\t\t\t" 
	      << (fMode == kInternal ? "Interal only" :
		  (fMode == kDerivedL2 ? 	"Ext. L1, Int. L2" : 
		   "External only")) << "\n"
	      << "\t# clk. between L1 and L2:\t"  << fTwv << "\n"
	      << "\tWrite pointer:\t\t\t0x" << std::hex << fWptr << "\n"
	      << "\tRead pointer:\t\t\t0x" << std::hex << fRptr << "\n"
	      << "\tFree buffers:\t\t\t0x" << std::dec << fRemb << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
