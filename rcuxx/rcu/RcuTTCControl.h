//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTTCControl
# define RCU_RCUTTCControl
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuTTCControl rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the Trigger/Timing Control Control register

      This register configures the TTC interface
      @ingroup rcuxx_rcu
  */
  struct RcuTTCControl : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuTTCControl(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "TTCControl", "Control of trigger/timing", 
		    0, true), 
	fSerialB(true),
	fDisableError(false),
	fEnableRoi(false),
	fL0Support(true),
	fL2aFIFO(true),
	fL2rFIFO(true),
	fL2tFIFO(true),
	fL1MsgMask(true),
	fTrgMask(true),
	fBCOverflow(false),
	fRunActive(false),
	fBusy(false),
	fCDHVersion(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;

    /** @return Get SerialB * */
    bool SerialB() const { return fSerialB; }
    /** @return Get DisableError * */
    bool DisableError() const { return fDisableError; }
    /** @return Get EnableRoi * */
    bool EnableRoi() const { return fEnableRoi; }
    /** @return Get L0Support * */
    bool L0Support() const { return fL0Support; }
    /** @return Get L2aFIFO * */
    bool L2aFIFO() const { return fL2aFIFO; }
    /** @return Get L2rFIFO * */
    bool L2rFIFO() const { return fL2rFIFO; }
    /** @return Get L2tFIFO * */
    bool L2tFIFO() const { return fL2tFIFO; }
    /** @return Get L1MsgMask * */
    bool L1MsgMask() const { return fL1MsgMask; }
    /** @return Get BCOverflow * */
    bool BCOverflow() const { return fBCOverflow; }
    /** @return Get RunActive * */
    bool RunActive() const { return fRunActive; }
    /** @return Get Busy * */
    bool Busy() const { return fBusy; }
    /** @return Get Common data header version * */
    unsigned int CDHVersion() const { return fCDHVersion; }
    /** @return Trigger mask */
    bool TrgMask() const { return fTrgMask; }

    /** @param x Set SerialB * */
    void SetSerialB(bool x) { fSerialB = x; }
    /** @param x Set DisableError * */
    void SetDisableError(bool x) { fDisableError = x; }
    /** @param x Set EnableRoi * */
    void SetEnableRoi(bool x) { fEnableRoi = x; }
    /** @param x Set L0Support * */
    void SetL0Support(bool x) { fL0Support = x; }
    /** @param x Set L2aFIFO * */
    void SetL2aFIFO(bool x) { fL2aFIFO = x; }
    /** @param x Set L2rFIFO * */
    void SetL2rFIFO(bool x) { fL2rFIFO = x; }
    /** @param x Set L2tFIFO * */
    void SetL2tFIFO(bool x) { fL2tFIFO = x; }
    /** @param x Set L1MsgMask * */
    void SetL1MsgMask(bool x) { fL1MsgMask = x; }
    /** @return Trigger mask */
    void SetTrgMask(bool x) { fTrgMask = x; }
    /** @param x Set BCOverflow * */
    void SetBCOverflow(bool x) { fBCOverflow = x; }
    /** @param x Set RunActive * */
    void SetRunActive(bool x) { fRunActive = x; }
    /** @param x Set Busy * */
    void SetBusy(bool x) { fBusy = x; }
    /** @param x Set Common data header version * */
    void SetCDHVersion(unsigned int x) { fCDHVersion = x; }
    bool Compare(unsigned int v) const;
  protected:
    /** SerialB */
    bool fSerialB;
    /** DisableError */
    bool fDisableError;
    /** EnableRoi */
    bool fEnableRoi;
    /** L0Support */
    bool fL0Support;
    /** L2aFIFO */
    bool fL2aFIFO;
    /** L2rFIFO */
    bool fL2rFIFO;
    /** L2tFIFO */
    bool fL2tFIFO;
    /** L1MsgMask */
    bool fL1MsgMask;
    /** Trigger mask */
    bool fTrgMask;
    /** BCOverflow */
    bool fBCOverflow;
    /** RunActive */
    bool fRunActive;
    /** Busy */
    bool fBusy;
    /** Common data header version */
    unsigned int fCDHVersion;
  };
  //====================================================================
  inline void
  RcuTTCControl::Decode(unsigned int data) 
  { 
    fSerialB		= (data & (1 <<  0));
    fDisableError	= (data & (1 <<  1));
    fEnableRoi		= (data & (1 <<  2));
    fL0Support		= (data & (1 <<  3));
    fL2aFIFO		= (data & (1 <<  8));
    fL2rFIFO		= (data & (1 <<  9));
    fL2tFIFO		= (data & (1 << 10));
    fL1MsgMask		= (data & (1 << 11));
    fTrgMask            = (data & (1 << 12));
    fBCOverflow		= (data & (1 << 16));
    fRunActive		= (data & (1 << 17));
    fBusy		= (data & (1 << 18));
    fCDHVersion		= (data >> 20) & 0xF;
  }	

  //____________________________________________________________________
  inline unsigned int 
  RcuTTCControl::Encode() const 
  { 
    unsigned int ret = 0;
    ret |= (fSerialB      ? (1 <<  0) : 0);
    ret |= (fDisableError ? (1 <<  1) : 0);
    ret |= (fEnableRoi    ? (1 <<  2) : 0);
    ret |= (fL0Support    ? (1 <<  3) : 0);
    ret |= (fL2aFIFO      ? (1 <<  8) : 0);
    ret |= (fL2rFIFO      ? (1 <<  9) : 0);
    ret |= (fL2tFIFO      ? (1 << 10) : 0);
    ret |= (fL1MsgMask    ? (1 << 11) : 0);
    ret |= (fTrgMask      ? (1 << 12) : 0);
    return ret;
  }
  //____________________________________________________________________
  inline bool
  RcuTTCControl::Compare(unsigned int v) const 
  {
    return Encode() == (v & 0x1F0F);
  }
  //____________________________________________________________________
  inline void
  RcuTTCControl::Print() const
  {
    RcuRegister::Print();
    std::cout << "SerialB:\t\t" << fSerialB << "\n"
	      << "DisableError:\t\t" << fDisableError << "\n"
	      << "EnableRoi:\t\t" << fEnableRoi << "\n"
	      << "L0Support:\t\t" << fL0Support << "\n"
	      << "L2aFIFO:\t\t" << fL2aFIFO << "\n"
	      << "L2rFIFO:\t\t" << fL2rFIFO << "\n"
	      << "L2tFIFO:\t\t" << fL2tFIFO << "\n"
	      << "L1MsgMask:\t\t" << fL1MsgMask << "\n"
	      << "Trigger mask:\t\t" << fTrgMask << "\n"
	      << "BCOverflow:\t\t" << fBCOverflow << "\n"
	      << "RunActive:\t\t" << fRunActive << "\n"
	      << "Busy:\t\t\t" << fBusy << "\n"
	      << "Common data header version:\t" << fCDHVersion 
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
