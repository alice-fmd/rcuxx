//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURCUID
# define RCU_RCURCUID
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRCUID rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU IDentifier register. 

      The user can put in an identifier here to identify the RCU
      uniquely.  The ID will be stored in every event.
      @ingroup rcuxx_rcu
  */
  struct RcuRCUID : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuRCUID(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RCUID", "RCU IDentifier", 0, true), 
	fId(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fId = (dat & 0x1FF); }
    virtual unsigned int Encode() const { return (fId & 0x1FF); }
    unsigned short Id() const { return fId; }
    void SetId(unsigned short id) { fId = id; }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tID:\t\t\t" << fId << std::endl;
    }
    bool Compare(unsigned int v) const
    {
      return Encode() == (v & 0x1FF);
    }
  protected:
    /** Whether it's not responding */
    unsigned short fId;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
