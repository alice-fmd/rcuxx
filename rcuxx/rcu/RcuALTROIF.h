//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUALTROIF
# define RCU_RCUALTROIF
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuALTROIF rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ALTRO interface register 

      This register defines the settings for the ALTRO BUS interface. 
      @ingroup rcuxx_rcu
  */
  struct RcuALTROIF : public RcuRegister
  {
    enum SampleSpeed_t {
      k20MHz   = 0x0, 
      k10MHz   = 0x1, 
      k5MHz    = 0x2, 
      k2500kHz = 0x3
    };
    enum BcInstructionCheck_t { 
      kNoCheck, 
      kTPCBC, 
      kPHOSBC, 
      kFMDBC
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuALTROIF(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "ALTROIF", "ALTRO InterFace", 0, true), 
	fSamplesPerChannel(0), 
	fSamplingRatio(k10MHz), 
	fCSTBDelay(0), 
	fBcCheck(kNoCheck)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** # samples / channel [9:0] */
    unsigned int SamplesPerChannel() const { return fSamplesPerChannel; }
    /** # samples / channel [9:0] */
    void SetSamplesPerChannel(unsigned int x) { fSamplesPerChannel = x; }
    /** Sampling ratio (T_sample/T_lhc) [13:10] */
    SampleSpeed_t SamplingRatio() const { return fSamplingRatio; }
    /** Sampling ratio (T_sample/T_lhc) [13:10] */
    void SetSamplingRatio(SampleSpeed_t x) { fSamplingRatio = x; }
    /** CSTB programmable delay [15:14] */
    unsigned int CSTBDelay() const { return fCSTBDelay; }
    /** CSTB programmable delay [15:14] */
    void SetCSTBDelay(unsigned int x) { fCSTBDelay = x; }
    /** Whether to check BC instruction, and for which instruction
	set. */
    BcInstructionCheck_t BcCheck() const { return fBcCheck; }
    /** Whether to check BC instruction, and for which instruction
	set. */
    void SetBcCheck(BcInstructionCheck_t x) { fBcCheck = x; }
    bool Compare(unsigned int v) const 
    {
      return Encode() == (v & 0x3FFFF);
    }
  protected:
    /** # samples / channel [9:0] */
    unsigned int fSamplesPerChannel;
    /** Sampling ratio (T_sample/T_lhc) [13:10] */
    SampleSpeed_t fSamplingRatio;
    /** CSTB programmable delay [15:14] */
    unsigned int fCSTBDelay;
    /** Whether to check BC instruction, and for which instruction
	set. [17:16] */
    BcInstructionCheck_t fBcCheck;
  };
  //====================================================================
  inline void
  RcuALTROIF::Decode(unsigned int dat) 
  {
    fSamplesPerChannel   = ((dat >> 0)  & 0x3FF);
    unsigned int speed   = ((dat >> 10) & 0x0F);
    fCSTBDelay           = ((dat >> 14) & 0x03);
    unsigned int bccheck = ((dat >> 16) & 0x03);
    switch (speed) { 
    case 0x0: fSamplingRatio = k20MHz;    break;
    case 0x1: fSamplingRatio = k10MHz;    break;
    case 0x2: fSamplingRatio = k5MHz;     break;
    case 0x3: fSamplingRatio = k2500kHz;  break;
    }
    switch (bccheck) { 
    case 0x0: fBcCheck = kNoCheck; break;
    case 0x1: fBcCheck = kTPCBC;   break;
    case 0x2: fBcCheck = kPHOSBC;  break;
    case 0x3: fBcCheck = kFMDBC;   break;
    }
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuALTROIF::Encode() const 
  {
    unsigned int speed = fSamplingRatio;
    unsigned int bcchk = fBcCheck;
    return ((fSamplesPerChannel & 0x3FF)      |
	    ((speed             & 0xF) << 10) | 
	    ((fCSTBDelay        & 0x3) << 14) |
	    ((bcchk             & 0x3) << 16));
  }

  //____________________________________________________________________
  inline void
  RcuALTROIF::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tSamples/channel:\t" << fSamplesPerChannel << "\n"
	      << "\tSampling ratio:\t\t";
    switch (fSamplingRatio) { 
    case k20MHz:   std::cout << "20MHz\n";   break;
    case k10MHz:   std::cout << "10MHz\n";   break;
    case k5MHz:    std::cout << "5MHz\n";   break;
    case k2500kHz: std::cout << "2.5MHz\n";   break;
    }
    std::cout << "\tCSTB Delay:\t\t" << fCSTBDelay << "\n"
	      << "\tBC check:\t\t" << std::flush;
    switch (fBcCheck) { 
    case kNoCheck: std::cout << "No check";      break;
    case kTPCBC:   std::cout << "TPC (5bits)";   break;
    case kPHOSBC:  std::cout << "PHOS (8bits)";  break;
    case kFMDBC:   std::cout << "FMD (7bits)";   break;
    }
  }
}

#endif
//
// Local Variables:
//   mode: C++
// End:
// 
