// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUDMEM_H
# define RCUXX_RCUDMEM_H
# include <rcuxx/rcu/RcuMemory.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuDMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU data memory
      @ingroup rcuxx_rcu
  */
  struct RcuDMEM  : public RcuMemory
  {
    /** Constructor 
        @param which Which memory (1 or 2)
        @param rcu Reference to RCU
        @param lbase Base address for low-bits
        @param hbase Base address for high-bits
	@param n Size
	@param clear Clear command */
    RcuDMEM(size_t which, Rcu& rcu, size_t lbase, size_t hbase, size_t n, 
	    RcuCommand* clear);
    /** Commit to hard-ware */
    virtual unsigned int Commit() { return RcuMemory::Commit(); }
    /** Read from hard-ware */
    virtual unsigned int Update() { return this->Update(0, fSize); }
    /** Commit to hard-ware 
	@param offset Start address 
	@param n How many words */
    virtual unsigned int Commit(size_t offset, size_t n);
    /** Read from hard-ware 
	@param offset Start address 
	@param n How many words */
    virtual unsigned int Update(size_t offset, size_t n);
  protected:
    /** Which memory */
    size_t fWhich;
    /** Base address of high memory */
    unsigned int fBaseHigh;
  };
  //====================================================================
  inline 
  RcuDMEM::RcuDMEM(size_t which, Rcu& rcu, 
		   size_t lbase, size_t hbase, size_t n, 
		   RcuCommand* clear) 
    : RcuMemory(rcu, (which == 1 ? "DMEM1" : "DMEM2"), 
		"Data memory bank", lbase, n, 0x3fff, 
		false, clear),
      fWhich(which), fBaseHigh(hbase)
  {}

  //____________________________________________________________________
  inline unsigned int 
  RcuDMEM::Commit(size_t off, size_t n)
  {
    unsigned int m     = std::min(unsigned(n), fSize);
    unsigned int o     = std::min(unsigned(off), fSize-1);
    unsigned int ret = fRcu.WriteDataMemory(fBase, fBaseHigh,o,m,&(fData[o]));
    return ret;
  }
  
  //____________________________________________________________________
  inline unsigned int 
  RcuDMEM::Update(size_t off, size_t n)
  {
    // DebugGuard g(fDebug, "Update DMEM%d at 0x%x, 0x%x, read %d words from %d", 
    // fWhich, fBase, fBaseHigh, off, n);
    unsigned int m   = std::min(unsigned(n), fSize);
    unsigned int o   = std::min(unsigned(off), fSize-1);
    unsigned int ret = fRcu.ReadDataMemory(fBase, fBaseHigh, o, m, &(fData[o]));
    return ret;
  }
  
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
