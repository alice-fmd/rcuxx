//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURDOMOD
# define RCU_RCURDOMOD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRDOMOD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ReaDOut MODe register

      This register configures how read-out should be done.
      @ingroup rcuxx_rcu
  */
  struct RcuRDOMOD : public RcuRegister
  {
    enum Buffers_t { 
      k4Buffers, 
      k8Buffers 
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuRDOMOD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RDOMOD", "Read-out mode", 0, true), 
	fBuffers(k4Buffers),
	fSparseReadout(false),
	fExecOnSOD(false),
	fCheckRDYRX(false)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat)
    {
      fBuffers        = (dat & 0x1 ? k8Buffers : k4Buffers);
      fExecOnSOD      = (dat & 0x2);
      fSparseReadout  = (dat & 0x4);
      fCheckRDYRX     = (dat & 0x8);
    }
    /** Encode  */
    virtual unsigned int Encode() const
    {
      return ((fBuffers == k8Buffers ? 0x1 : 0x0) | 
	      (fExecOnSOD            ? 0x2 : 0x0) | 
	      (fSparseReadout        ? 0x4 : 0x0) | 
	      (fCheckRDYRX           ? 0x8 : 0x0));
    }
    bool Compare(unsigned int v) const 
    {
      return Encode() == (v & 0x1f);
    }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\t# of buffers:\t" << NBuffers() << "\n"
		<< "\tExecute IMEM on SOD/EOD:\t" << fExecOnSOD << "\n" 
		<< "\tSparse readout:\t" << fSparseReadout << "\n"
		<< "\tCheck RDYRX:\t" << fCheckRDYRX << std::endl;
    }
    /** Sparse readout (BC optimised) */
    bool SparseReadout() const { return fSparseReadout; }
    /** Sparse readout (BC optimised) */
    void SetSparseReadout(bool x) { fSparseReadout = x; }
    /** Number of buffers */
    unsigned short NBuffers() const { return fBuffers == k8Buffers ? 8 : 4; }
    /** Number of buffers */
    Buffers_t Buffers() const { return fBuffers; }
    /** Meta event block */
    void SetBuffers(Buffers_t x) { fBuffers = x; }
    /** Execute IMEM on SOD/EOD */
    bool ExecOnSOD() const { return fExecOnSOD; }
    /** Execute IMEM on SOD/EOD */
    void SetExecOnSOD(bool x) { fExecOnSOD = x; }
    /** Enable check of RDYRX in TTCrx */
    bool CheckRDYRX() const { return fCheckRDYRX; }
    /** Enable check of RDYRX in TTCrx */
    void SetCheckRDYRX(bool x) { fCheckRDYRX = x; }
  protected:
    /** Number of buffers [0] */ 
    Buffers_t fBuffers;
    /** Sparse readout (BC optimised) [1] */
    bool fSparseReadout;
    /** Execute IMEM on SOD/EOD [2] */
    bool fExecOnSOD; 
    /** Enable check of RDYRX [3] */
    bool fCheckRDYRX;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
