//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURESREG
# define RCU_RCURESREG
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRESREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RESult REGister 

      This is the register written to by the @f$ I^2C@f$ module.
      @ingroup rcuxx_rcu
  */
  struct RcuRESREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuRESREG(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "RESREG", "I2C result register", clear, true), 
	fAddress(0), fResult(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int Address() const { return fAddress; }
    /** @return Address of data in memory 2 */
    unsigned int Result() const { return fResult; }
    /** Set address */
    void SetAddress(unsigned int addr) { fAddress = addr & 0x1F; }
    /** Set result (?) */
    void SetResult(unsigned int res) { fAddress = res & 0xffff; }
  protected:
    /** Address */
    unsigned int fAddress;
    /** Result */
    unsigned int fResult;
  };
  //====================================================================
  inline void
  RcuRESREG::Decode(unsigned int dat) 
  { 
    fAddress  = (dat >> 16) & 0x1f;
    fResult   = (dat & 0xffff);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuRESREG::Encode() const 
  { 
    unsigned int val = 0;
    val += (fAddress & 0x1f) << 16;
    val += (fResult & 0xffff);
    return val;
  }

  //____________________________________________________________________
  inline void
  RcuRESREG::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tAddress :\t\t\t" << std::hex << fAddress << "\n" 
	      << "\tResult  :\t\t\t" << std::hex << fResult << std::dec 
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
