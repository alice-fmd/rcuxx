//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCULWADD
# define RCU_RCULWADD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuLWADD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Cache the Last Written ADDress in the RcuDMEM1 and RcuDMEM2.  

      Record the last addresses of valid data in the data memories.
      Specific to the U2F version of the RCU.
      @ingroup rcuxx_rcu
  */
  struct RcuLWADD : public RcuRegister
  {
    /** Constructor 
	@param mem Memory 
	@param offset Register offset in @a mem */
    RcuLWADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "LWADD", "Last Written ADDress", 0, false), 
	fBank1(0), 
	fBank2(0) 
    {}
    /** Update the cache of the Last Written ADDress in the RcuDMEM1 and
	RcuDMEM2  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Number of 40bit words in DMEM1 */
    unsigned int Bank1() const { return fBank1; }
    /** @return Number of 40bit words in DMEM2 */
    unsigned int Bank2() const { return fBank2; }
  protected:
    /** Number of words in DMEM1 */
    unsigned int fBank1;
    /** Number of words in DMEM2 */
    unsigned int fBank2;
  };
  //====================================================================
  inline void
  RcuLWADD::Decode(unsigned int dat) 
  { 
    fBank1 = (dat & 0xff);
    fBank2 = (dat & 0xff00) >> 8;
  }

  //____________________________________________________________________
  inline void
  RcuLWADD::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tWords in DMEM1:\t\t\t" << fBank1 << "\n"
	      << "\tWords in DMEM2:\t\t\t" << fBank2 << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
