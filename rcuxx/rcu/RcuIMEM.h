// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUIMEM_H
# define RCUXX_RCUIMEM_H
# include <rcuxx/rcu/RcuMemory.h>
# include <rcuxx/DebugGuard.h>

namespace Rcuxx
{
  //====================================================================
  /** 
   * @struct RcuIMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
   * @brief Interface to the RCU instruction memory
   * @ingroup rcuxx_rcu
   */
  struct RcuIMEM  : public RcuMemory
  {
    /**
     * Constructor 
     *
     * @param rcu Reference to low-level interface 
     * @param base Base address
     * @param n Number of words  
     * @param mask Value mask
     * @param exec Execute command 
     * @param abort Abort command */
    RcuIMEM(Rcu& rcu, size_t base, size_t n, unsigned int mask,
	    RcuCommand* exec, RcuCommand* abort);
    /**
     * Append data to the internal buffer 
     *
     * @param data Data to append 
     * @param size Size of buffer 
     * 
     * @return 0 on success, error code otherwise 
     */
    unsigned int Append(unsigned int* data, unsigned int size);
    /** 
     * Append @a data of size @a size to @a cache at @a marker
     * 
     * @param data   Data to append
     * @param size   Size of @a data 
     * @param cache  Cache 
     * @param marker Marker
     * 
     * @return How many words were appended 
     */
    unsigned int Append(unsigned int* data, unsigned int size,
			Cache_t& cache, size_t& marker);
    /**
     * Execute the instruction memory, and clear the marker 
     *
     * 
     * @return 0 on success, error code otherwise 
     */
    unsigned int Execute();
    
    /**
     * Write to a front-end register 
     *
     * @param addr Full address of register 
     * @param data Data to write to the register 
     * 
     * @return 0 on success, error code otherwise 
     */
    unsigned int FecWrite(unsigned int addr, unsigned int data);
    /** 
     * Write a FEC write instruction 
     * 
     * @param addr    Address to write to 
     * @param data    Data to write to that address
     * @param cache   Cache 
     * @param marker  Marker 
     * 
     * @return Number of words written 
     */
    unsigned int FecWrite(unsigned int addr, unsigned int data,
			  Cache_t& cache, size_t& marker);
    /**
     * Read from a front-end register 
     *
     * @param addr Full address of register 
     * 
     * @return 0 on success, error code otherwise 
     */
    unsigned int FecRead(unsigned int addr);
    /** 
     * Write a FEC read instruction 
     * 
     * @param addr   Address to read from 
     * @param cache  Cache 
     * @param marker Marker 
     * 
     * @return Number of words written 
     */
    unsigned int FecRead(unsigned int addr,
			 Cache_t& cache, size_t& marker);
    /**
     * Execute a front-end command 
     *
     * @param addr Full address of the command 
     * 
     * @return 0 on success, error code otherwise 
     */
    unsigned int FecCommand(unsigned int addr);
    /** 
     * Write a FEC command instruction
     * 
     * @param addr   Address of command 
     * @param cache  Cache 
     * @param marker Marker 
     * 
     * @return Number of words written
     */
    unsigned int FecCommand(unsigned int addr,
			    Cache_t& cache, size_t& marker);
    /** 
     * Get the debug level 
     * 
     * @return Debug level
     */
    bool Debug() const { return fDebug; }
    /** 
     * Set the debug level
     * 
     * @param debug Debug level 
     */
    void SetDebug(bool debug=true) { fDebug = debug; }
    /** 
     * Get the marker 
     * 
     * @return Marker value 
     */
    const size_t Marker() const { return fMarker; }
    /** 
     * Get the marker 
     * 
     * @return Marker reference
     */
    size_t& Marker() { return fMarker; }
    /** 
     * Clear the marker 
     * 
     */
    void ClearMarker();
    /**
     * Set the cache
     *
     * @param off Starting offset
     * @param n How many words (size of @a d)
     * @param d The values 
     */
    void Set(size_t off, size_t n, unsigned int* d);
    /**
     * 
     *
     * After updating the cache, use this method to find the
     * stop-word.  This allows us to append to the existing content of
     * the instruction memory.
     *
     * @return The current marker position (found address of stop
     * word).
     */ 
    virtual unsigned int FindStop();
    /** 
     * Fill in stop word at marker, and possibly end words up to the
     * end of the memory,
     */
    void FillTheRest();
    void Print() const;


    struct RestoreGuard 
    {
      RestoreGuard(RcuIMEM* imem, bool enable, bool verbose=false)
	: fIMEM(imem), 
	  fOld(enable ? imem->Size()+1 : 0), 
	  fVerbose(verbose)
      {
	if (!enable) return;

	fIMEM->Update();

	if (fVerbose) fIMEM->Print();

	const Rcuxx::RcuIMEM::Cache_t& current = fIMEM->Data();
	std::copy(current.begin(), current.end(), fOld.begin());
	fIMEM->ClearMarker();
      }
      ~RestoreGuard() 
      {
	if (fOld.empty()) return;

	fIMEM->ClearMarker();
	// const size_t marker = fIMEM->Marker();
	size_t n = fOld.size()-2;
	fIMEM->Set(0, n, &(fOld[0]));
	fIMEM->Commit();

	if (!fVerbose) return;

	fIMEM->Update();
	fIMEM->Print();
      }
      RcuIMEM* fIMEM;
      Cache_t  fOld;
      bool     fVerbose;
    };
  protected:
    /**
     * Current marker 
     */
    size_t fMarker;
    /**
     * Debug level 
     * 
     */
    bool fDebug;
  };
  //====================================================================
  inline
  RcuIMEM::RcuIMEM(Rcu& rcu, size_t base, size_t n, unsigned int mask,
		   RcuCommand* exec, RcuCommand* abort) 
    : RcuMemory(rcu, "IMEM", "Instruction memory", 
		base, n, mask, true, 0, exec, abort),
      fMarker(0), 
      fDebug(true)
  {
  }
  //____________________________________________________________________
  inline void
  RcuIMEM::ClearMarker()
  {
    fMarker = 0;
    FillTheRest();
    fMarker = 0;
  }
  //____________________________________________________________________
  inline void
  RcuIMEM::FillTheRest()
  {
    DebugGuard g(fDebug, "Filling from marker [Marker=%d]",fMarker);
    if (fData.size() <= fMarker) {
      DebugGuard::Message(fDebug, "Marker[=%d] beyond end of data[=%d]",
			  fMarker, fData.size());
      return;
    } 

    fData[fMarker] = fRcu.Layout().kIMEM_STOP;
    fMarker++;
    // unsigned int ret    = 0;
    unsigned int filler = (fRcu.Layout().kIMEM_END != RcuLayout::kInvalid ? 
			   fRcu.Layout().kIMEM_END : 0);
    std::fill(&(fData[fMarker]), &(fData[fData.size()]), filler);
  }
  //____________________________________________________________________
  inline void
  RcuIMEM::Set(size_t off, size_t n, unsigned int* d) 
  {
    DebugGuard g(fDebug, "RcuIMEM::Set(%d,%d,%p)",
		 off, n, d);
    RcuMemory::Set(off, n, d);
    fMarker = off+n;
  }
    
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::Append(unsigned int* data, unsigned int size)
  {
    DebugGuard g(fDebug, "RcuIMEM::Append(%p,%d) [Marker=%d]",
		 data,size,fMarker);
    // Set(fMarker, size, data);
    Append(data, size, fData, fMarker);
    return 0;
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::Append(unsigned int* data, unsigned int size,
		  Cache_t& cache, size_t& marker)
  {
    DebugGuard g(fDebug, "RcuIMEM::Append(%p,%d) [Marker=%d]",
		 data,size,fMarker);
    if (marker+size >= cache.size()) return 0;
    for (size_t i = 0; i < size; i++) 
      cache[marker++] = (data[i] & fMax);
    return 0;
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::FindStop()
  {
    fMarker = 0;
    for (size_t i = 0; i < fSize; i++) { 
      if (fData[i] == fRcu.Layout().kIMEM_STOP) {
	fMarker = i;
	break;
      }
    }
    return fMarker;
  }
      
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::Execute()
  {
    DebugGuard g(fDebug, "RcuIMEM::Exec() [Marker=%d]",fMarker);
    FillTheRest();
    unsigned int ret = 0;
    if (!(ret = Commit(0, fSize /*fMarker*/))) { 
      g.Message(fDebug, "Executing IMEM");
      ret = Exec(0); 
      g.Message(fDebug, "Exec returned %d", ret);
   }
    fMarker = 0;
    return ret;
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::FecWrite(unsigned int addr, unsigned int data,
		    Cache_t& cache, size_t& marker)
  {
    DebugGuard g(fDebug, "RcuIMEM::FecWrite(0x%x, 0x%x, %p, %d)", 
		 addr, data, &(cache[0]), marker);
    // unsigned int size  = 2;
    unsigned int buf[] = { 0x0, 0x0 };
    buf[0] = fRcu.Layout().kIMEM_FEC_WR1 | addr;
    buf[1] = fRcu.Layout().kIMEM_FEC_WR2 | data;
    return Append(buf, 2, cache, marker);
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::FecWrite(unsigned int addr, unsigned int data)
  {
    return FecWrite(addr, data, fData, fMarker);
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::FecRead(unsigned int addr,
		   Cache_t& cache, size_t& marker)
  {
    DebugGuard g(fDebug, "RcuIMEM::FecRead(0x%x,%p,%d)", 
		 addr, &(cache[0]), fMarker);
    unsigned int buf[] = { 0x0, 0x0 };
    // std::cout << std::hex << fRcu.Layout().kIMEM_FEC_RD1 << std::dec 
    //           << std::endl;
    buf[0] = fRcu.Layout().kIMEM_FEC_RD1 | addr;
    buf[1] = fRcu.Layout().kIMEM_FEC_RD2;
    return Append(buf, 
		  fRcu.Layout().kIMEM_FEC_RD2 == fRcu.Layout().kInvalid?1:2,
		  cache, marker);
  }
  //____________________________________________________________________
  inline unsigned int 
  RcuIMEM::FecRead(unsigned int addr)
  {
    return FecRead(addr, fData, fMarker);
  }
  //____________________________________________________________________
  inline unsigned int 
  Rcuxx::RcuIMEM::FecCommand(unsigned int addr,
		   Cache_t& cache, size_t& marker)
  {
    DebugGuard g(fDebug, "RcuIMEM::FecCommand(0x%x,%p,%d)", 
		 addr, &(cache[0]), marker);
    unsigned int buf[] = { 0x0, 0x0 };
    buf[0] = fRcu.Layout().kIMEM_FEC_CMD1 | addr;
    buf[1] = fRcu.Layout().kIMEM_FEC_CMD2;
    size_t n = (fRcu.Layout().kIMEM_FEC_CMD2 == fRcu.Layout().kInvalid ? 1: 2);
    return Append(buf, n, fData, fMarker);
  }
  //____________________________________________________________________
  inline unsigned int 
  Rcuxx::RcuIMEM::FecCommand(unsigned int addr)
  {
    return FecCommand(addr, fData, fMarker);
  }
  //____________________________________________________________________
  inline void
  Rcuxx::RcuIMEM::Print() const
  {
    int nEnd  = 0;
    int nZero = 0;
    std::ostream& o = std::cout;
    o << "Contents of " << fName << ":\n" 
      << "   i | Type        | Address            | Data     | Raw\n"  
      << "-----+-------------+--------------------+----------+----------------"
      << std::endl;
    for (size_t i = 0; i < fData.size(); i++) { 
      unsigned int idx  = i;
      unsigned int word = fData[i];
      unsigned int what = word & 0xffff;
      unsigned int res  = 0;
      char         t    = '\0';
      std::string  n    = "";
      
      if ((word & fRcu.Layout().kIMEM_RCU_CMD) ==
	  fRcu.Layout().kIMEM_RCU_CMD) { 
	// RCU commands 
	if      (word == fRcu.Layout().kIMEM_END)  { t = 'e'; n = "end";  }
	else if (word == fRcu.Layout().kIMEM_STOP) { t = 's'; n = "stop"; }
	else if (word & 0x40000)                   { t = 'l'; n = "loop"; }
	else if (word & 0x20000)                   { t = 'h'; n = "wait"; }
      }
      else if ((word & fRcu.Layout().kIMEM_FEC_WR1)==
	       fRcu.Layout().kIMEM_FEC_WR1) { 
	t = 'w'; 
	n = "FEC write"; 
	res = fData[++i] & 0xfffff;
      }
      else if ((word & fRcu.Layout().kIMEM_FEC_CMD1)==
	       fRcu.Layout().kIMEM_FEC_CMD1) { 
	t = 'c'; 
	n = "FEC command";
	// Skip over empty second part
	if (fRcu.Layout().kIMEM_FEC_CMD2 != fRcu.Layout().kInvalid) i++;
      }
      else if ((word & fRcu.Layout().kIMEM_FEC_RD1)==
	       fRcu.Layout().kIMEM_FEC_RD1) { 
	t = 'r'; 
	n = "FEC read"; 
	// Skip over empty second part
	if (fRcu.Layout().kIMEM_FEC_RD2 != fRcu.Layout().kInvalid) i++;
      }
      else  {
	t = '?'; 
	n = "Unknown";
	nZero++;
      }
      o << std::setw(4) << idx << " | " << std::left << std::setw(11) << n 
		<< " | " << std::hex << std::setfill('0');
      if (t == 'w' || t == 'r' || t == 'c') { 
	// Decode the address 
	bool         bcast = (word & (1 << 18));
	bool         bc    = (word & (1 << 17));
	if (bcast) {
	  o << "broadcast " << (bc ? "BC" : "AL");
	}
	else { 
	  // 2+2+3+1+3+1=12
	  o << "0x" << std::setw(2) << ((word >> 12) & 0x1F);
	  if (!bc) { 
	    o << "/0x" << std::setw(1) << ((word >> 9) & 0x7)
		      << "/0x" << std::setw(1) << ((word >> 5) & 0xF);
	  }
	  else 
	    o << "        ";
	}
	o << " 0x" << std::setw(3) << (word & (bc ? 0xFFF : 0x1F))
		  << " | ";
      }
      else 
	o << "               n/a | ";
      o << std::right;
      if      (t == 'l' || t == 'h') 
	o << "  0x" << std::setw(4) << what << " | ";
      else if (t == 'w')             
	o << "0x" << std::setw(6) << res << " | ";
      else 
	o << "         | ";
      o << "0x" << std::setw(8) << word;
      if (t == 'w') 
	o << " 0x" << std::setw(8) << fData[i];
      o << std::dec << std::setfill(' ') << std::endl;
      if (t == 'e') { 
	nEnd++;
	if (nEnd > 5) break;
      }
      else 
	nEnd = 0;
      if (nZero > 5) break;
    } // for (size_t i ...)
  }
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
