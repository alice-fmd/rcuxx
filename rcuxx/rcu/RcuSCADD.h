//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUSCADD
# define RCU_RCUSCADD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuSCADD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the @f$I^2C @f$ address register 
      @ingroup rcuxx_rcu
  */
  struct RcuSCADD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuSCADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "SCADD", "Slow Control ADDress", 0, true), 
	fRead(false),
	fBCast(false),
	fFec(0),
	fReg(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int Address() const { return Encode(); }
    bool IsRead() const { return fRead; }
    bool IsBroadcast() const { return fBCast; }
    unsigned short Fec() const { return fFec; }
    unsigned short Instruction() const { return fReg; }

    /** Set address */
    void SetAddress(unsigned int addr);
    /** Set the address from fields 
	@param rnw   If true, this is a read-command 
	@param bcast If true, send in broad-cast 
	@param fec   Front-end card (5 bits)
	@param reg   Register address */
    void SetAddress(bool rnw, bool bcast, 
		    unsigned short fec, unsigned short reg);
    /** Set read/write mode */
    void SetRead(bool x=true) { fRead = x; }
    /** Set broadcast mode */
    void SetBroadcast(bool x=true) { fBCast = x; }
    /** Set front-end card address */
    void SetFec(unsigned short x) { fFec = (0x1F & x); }
    /** Set instruction code */
    void SetInstruction(unsigned short x) { fReg = (0xFF & x); }
  protected:
    /** Whether to read or write */
    bool fRead;
    /** Whether write in broadcast  */
    bool fBCast;
    /** Front-end card to address  */
    unsigned short fFec;
    /** Register/command to address  */
    unsigned short fReg;
  };
  //====================================================================
  inline void
  RcuSCADD::Decode(unsigned int dat) 
  { 
    fRead     = (dat >> 14) & 0x1;
    fBCast    = (dat >> 13) & 0x1;
    fFec      = (dat >>  8) & 0x1F;
    fReg      = (dat >>  0) & 0xFF;
  }

  //____________________________________________________________________
  inline void 
  RcuSCADD::SetAddress(unsigned int addr)
  {
    Decode(addr);
  }
  //____________________________________________________________________
  inline void 
  RcuSCADD::SetAddress(bool rnw, bool bcast, 
		       unsigned short fec, unsigned short reg)
  { 
    fRead  = rnw;
    fBCast = bcast;
    fFec   = (fec & 0x1F);
    fReg   = (reg & 0xFF);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuSCADD::Encode() const 
  { 
    unsigned int addr = ((fRead  ? 1 : 0) << 14 | 
			 (fBCast ? 1 : 0) << 13 | 
			 (fFec   & 0x1F)  <<  8 | 
			 (fReg   & 0xFF));
    return addr;
  }

  //____________________________________________________________________
  inline void
  RcuSCADD::Print() const
  {
    RcuRegister::Print();
    std::cout << std::hex << std::setfill('0') 
	      << "\tRead:          " << fRead << "\n"
	      << "\tBCast:         " << fBCast << "\n"
	      << "\tFEC:           " << std::setw(2) << fFec   << "\n"
	      << "\tInstruction:   " << std::setw(2) << fReg   << "\n"
	      << "\tEncoded:       " << std::setw(4) << Encode() 
	      << std::setfill(' ') << std::dec << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
