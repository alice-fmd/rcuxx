//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTRGCONF
# define RCU_RCUTRGCONF
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuTRGCONF rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the TRigger ConFiG register.  

      This register defines how to process triggers
      @ingroup rcuxx_rcu
  */
  struct RcuTRGCONF : public RcuRegister
  {
    /** Buffer size */
    enum Mapping_t {
      /** 4 Buffers */
      kL1ToL1,
      /** 8 Buffers  */
      kL0ToL1
    };
    /** Mode buttons */
    enum Sources_t {
      /** Software trigger */
      kSoftware  = 0x1, 
      /** Ext. L1, internal L2 */
      kHardware = 0x2, 
      /** Ext. L1 and L2 */
      kTTC      = 0x4
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory 
	@param flipped Whether it is flipped */
    RcuTRGCONF(RcuMemory& mem, size_t offset, bool flipped)
      : RcuRegister(mem, offset, "TRGCONF", "TRiGger CONFiguration", 0, true), 
	fSources(0),
	fMapping(kL1ToL1), 
	fL2Latency(4096), 
	fFlipped(flipped)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return BC to optimize readout */
    Mapping_t Mapping() const { return fMapping; }
    /** Whether L0 is mapped into L1 */
    bool IsL0ToL1() const { return fMapping == kL0ToL1; }
    /** Whether L1 is mapped into L1 */
    bool IsL1ToL1() const { return fMapping == kL1ToL1; }
    /** Use BC to optimize readout */
    void SetMapping(Mapping_t map=kL1ToL1) { fMapping = map; } 
    /** @return Popped readout e.g., via U2F */
    unsigned short Sources() const { return fSources; }
    /** Trigger source - bit mask of kSoftware, kHardware, and/or kTTC */
    void SetSources(unsigned short src) { fSources = src; }
    /** Enable software triggers */ 
    void EnableSoftwareTriggers(bool use=true) { 
      if (use) fSources |= kSoftware;
      else     fSources &= ~kSoftware;
    }
    /** Enable Hardware triggers */ 
    void EnableHardwareTriggers(bool use=true) { 
      if (use) fSources |= kHardware;
      else     fSources &= ~kHardware;
    }
    /** Enable TTC triggers */ 
    void EnableTTCTriggers(bool use=true) { 
      if (use) fSources |= kTTC;
      else     fSources &= ~kTTC;
    }
    /** Check if software triggers are enables */
    bool IsSoftwareTriggers() const { return fSources & kSoftware; }
    /** Check if software triggers are enables */
    bool IsHardwareTriggers() const { return fSources & kHardware; }
    /** Check if software triggers are enables */
    bool IsTTCTriggers() const { return fSources & kTTC; }
    /** @return # clock cycles between L1 and L2 */
    unsigned short L2Latency() const { return fL2Latency; }
    /** Set # clock cycles between L1 and L2 */
    void SetL2Latency(unsigned short val) { fL2Latency = val; }
    bool Compare(unsigned int v) const 
    {
      return Encode() == (v & 0x1FFFF);
    }
  protected:
    /** Trigger source */ 
    unsigned short fSources;
    /** Trigger mode */ 
    Mapping_t      fMapping;
    /** L2 latency wrt L1 */ 
    unsigned short fL2Latency;
    /** Flipped? */
    bool fFlipped;
  };
  //====================================================================
  inline void
  RcuTRGCONF::Decode(unsigned int dat) 
  { 
    // DebugGuard g(fDebug, "RcuTRGCONF::Decode(0x%08lx)", dat);
    fSources    = ((dat >> 14) & 0x7);
    fMapping    = ((dat >> 13) & 0x1) ? kL0ToL1 : kL1ToL1;
    fL2Latency  = ((dat >>  0) & 0x1FFF);
    if (fFlipped) 
      fMapping  = (fMapping == kL0ToL1 ? kL1ToL1 : kL0ToL1);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuTRGCONF::Encode() const 
  { 
    // DebugGuard g(fDebug, "RcuTRGCONF::Encode()");
    unsigned mapped = (fMapping == kL0ToL1 ? 1 : 0);
    if (fFlipped) mapped  = (mapped ? kL1ToL1 : kL0ToL1);
    return (((fL2Latency & 0x1FFF) <<  0) | 
	    ((mapped     & 0x1)    << 13) |
	    ((fSources   & 0x7)    << 14));
  }

  //____________________________________________________________________
  inline void
  RcuTRGCONF::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tMapping:\t\t\t\t" << (IsL0ToL1() ? "L0" : "L1") 
	      << "->L1\n"
	      << "\tSources:\t\t\t";
    if (IsSoftwareTriggers()) std::cout << "Software ";
    if (IsHardwareTriggers()) std::cout << "Hardware ";
    if (IsTTCTriggers())      std::cout << "TTC ";
    std::cout << "\n"
	      << "\t# clk. between L1 and L2:\t"  << fL2Latency << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
