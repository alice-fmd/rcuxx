//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUACTFEC
# define RCU_RCUACTFEC
# include <rcuxx/rcu/RcuRegister.h>
# include <rcuxx/rcu/RcuACL.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuACTFEC rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the ACTive FrontEnd Card.  

      This is a bit mask of cards to turn on.   Manipulating this
      register directly effects the card switch line to the cards. 

      @ingroup rcuxx_rcu
  */
  struct RcuACTFEC : public RcuRegister
  {
    /** Constructor  
	@param mem reference to memory 
	@param offset Offset in memory 
	@param scoff Slow control offset */
    RcuACTFEC(RcuMemory& mem, size_t offset, size_t scoff)
      : RcuRegister(mem, offset, "ACTFEC", "ACTive Front-End Cards", 0, true), 
	fValue(0x0),
	fSCOffset(scoff),
	fSCValue(0x0)
    {}

    /** Update the cache of the register
	@return 0 on success, error code otherwise */
    virtual unsigned int Update();
    /** Commit to the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
    /** Decode @a dat as ACTive Front-End-Cards mask  */
    virtual void Decode(unsigned int dat) { fValue = dat; }
    /** Encode as hardware value  */
    virtual unsigned int Encode() const { return fValue; }
    /** Get the SC value */ 
    virtual unsigned int ScValue() const { return fSCValue; }
    /** Print to standard output */
    virtual void Print() const;
    /** @return bit mask of active front-end cards */
    unsigned int Value() const { return fValue; }
    /** Set bit mask of active front-end cards */
    void SetValue(unsigned int val) { fValue = val; }
    /** Turn on card @a i */ 
    void SetOn(unsigned int n, bool on=true) { 
      if (on) fValue |= (1 << n); 
      else    fValue &= ~(1 << n); 
    }
    /** Check if card @a i is on */ 
    bool IsOn(unsigned int n) const { return fValue & (1 << n); }
    /** Check if card @a i is on for slow control */ 
    bool IsScOn(unsigned int n) const { return fSCValue & (1 << n); }
    /** Syncronize this register to the ACL memory 
	@param acl Referenc to ACL memory  */
    void SyncToACL(const RcuACL& acl);
    /** Syncronize ACL memory to this.   Note, that this operation
	turns on all 16 channels of all 8 ALTRO's of the turned on
	cards 
	@param acl  Referenc to ACL memory  
	@param mask Bit mask of ALTRO's to read-out. */
    void ACLSync(RcuACL& acl, unsigned int mask=0xffff) const;
  protected:
    /** Get character representation */
    char AsChar(unsigned int n) const { 
      unsigned v = (IsOn(n) ? 1 << 1 : 0) | (IsScOn(n) ? 1 << 0 : 0);
      switch (v) { 
      case 0: return '-';
      case 1: return '/';
      case 2: return '+';
      case 3: return '*';
      }
      return '-';
    }
    /** Value */
    unsigned int fValue;
    /** SC address */
    unsigned int fSCOffset;
    /** SC value */
    unsigned int fSCValue;
  };
  //====================================================================
  inline unsigned int
  RcuACTFEC::Update()
  {
    unsigned ret = 0;
    if ((ret = fMemory.Update(fOffset, 1))) return ret;
    fMemory.Get(fOffset, 1, &fValue);
    if (fSCOffset == fOffset) return ret;
    if ((ret = fMemory.Update(fSCOffset, 1))) return ret;
    fMemory.Get(fSCOffset, 1, &fSCValue);
    return ret;
  }
  //____________________________________________________________________
  inline unsigned int
  RcuACTFEC::Commit() 
  {
    fMemory.Set(fOffset, 1, &fValue);
    unsigned int ret = fMemory.Commit(fOffset, 1);
    if (ret || fSCOffset == fOffset) return ret;
    fMemory.Set(fSCOffset, 1, &fSCValue);
    return fMemory.Commit(fOffset, 1);
    
  }
  //____________________________________________________________________
  inline void
  RcuACTFEC::SyncToACL(const RcuACL& acl) 
  { 
    // const RcuMemory::Cache_t& mem = acl.Data();
    fValue = 0;
    for (size_t i = 0; i < sizeof(fValue)*8 && i < acl.Size() / 8; i++) {
      for (size_t j = 0; j < 8; j++) {
	unsigned int mask = acl.CheckChip(i, j);
	if (mask != 0) {
	  SetOn(i, true);
	  break;
	}
      }
    }
  }

  //____________________________________________________________________
  inline void
  RcuACTFEC::ACLSync(RcuACL& acl, unsigned int mask) const
  {
    for (size_t i = 0; i < sizeof(fValue)*8 && i < acl.Size() / 8; i++) {
      if (!IsOn(i)) continue;
      for (size_t j = 0; j < 8; j++) 
	acl.EnableChip(i, j);
    }
  }

  
  //____________________________________________________________________
  inline void
  RcuACTFEC::Print() const 
  {
    RcuRegister::Print();
    std::cout << "\tBranch A:\t" << std::flush;
    size_t i = 0;
    for (; i < sizeof(fValue)*8 / 2; i++) 
      std::cout << AsChar(i); 
    std::cout << "\n\tBranch B:\t" << std::flush;
    for (; i < sizeof(fValue)*8; i++) 
      std::cout << AsChar(i);
    std::cout << "\n\t[-: off, +: on, /: off, monitored, *: on, monitored" 
	      << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
