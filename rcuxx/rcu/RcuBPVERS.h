//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUBPVERS
# define RCU_RCUBPVERS
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuBPVERS rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the Back-plane version register

      This register allows to configure for a different back-plane
      type than the usual.   The PHOS has exchange two lines (Cannot
      remember which). 
      @ingroup rcuxx_rcu
  */
  struct RcuBPVERS : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuBPVERS(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "BPVERS", "BackPlane VERSion", 0, true), 
	fPHOS(false)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fPHOS = (dat & 0x1); }
    virtual unsigned int Encode() const { return fPHOS ? 1 : 0; }
    bool IsPHOS() const { return fPHOS; }
    void SetPHOS(bool phos) { fPHOS = phos; }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tPHOS:\t\t\t" << fPHOS << std::endl;
    }
  protected:
    /** Whether it's not responding */
    bool fPHOS;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
