//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUStatusEntry
# define RCU_RCUStatusEntry
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuStatusEntry  rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Class to hold status data from the BC's
  */
  struct RcuStatusEntry 
  {
    /** Destructor */
    virtual ~RcuStatusEntry() {}
    /** Constructor 
	@param data The data */
    RcuStatusEntry(unsigned int data=0);
    /** Constructor 
	@param id Whether we have a valid ID
	@param where Where the interrupt came from
	@param soft If it's a soft interrupt
	@param ans Whether the address is answering 
	@param sclk If it's missed sample clocks
	@param alps If it's an ALTRO power supply error
	@param paps If it's an PASA power supply error
	@param dc If it's a digital current error
	@param dv If it's a digital voltage error
	@param ac If it's a analogue current error
	@param av If it's a analogue voltage error
	@param temp If it's temperature errror  */
    RcuStatusEntry(bool id, unsigned int where, bool soft, bool ans, 
		   bool sclk, bool alps, bool paps, bool dc, bool dv, 
		   bool ac, bool av, bool temp);
    /** Copy constructor 
	@param other Object to copy from */
    RcuStatusEntry(const RcuStatusEntry& other);
    /** Assignment operator 
	@param other Object to assign from 
	@return reference to this object  */
    RcuStatusEntry& operator=(const RcuStatusEntry& other);
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return If the card causing the interrupt is identified */
    bool IsIdentified() const { return fIsIdentified; }
    /** @return the hardware address of the card that caused the
	interrupt.  If the 4th bit (counting from 0) is set, then it's
	on branch B */
    unsigned int Where() const { return fWhere; }
    /** @return @c true if the interrupt was soft */
    bool IsSoft() const { return fIsSoft; }
    /** @return @c true if the card is answering */
    bool IsAnswering() const { return fIsAnswering; }
    /** @return If a missed sample clocks interrupt is set */
    bool IsMissedSclk() const { return fMissedSclk; } 
    /** @return If a ALTRO power supply error interrupt is set */
    bool IsAlps() const { return fAlps; } 
    /** @return If a PASA power supply error interrupt is set */
    bool IsPaps() const { return fPaps; } 
    /** @return If a digital current over threshold interrupt is set */
    bool IsDcOverTh() const { return fDcOverTh; } 
    /** @return If a digital voltage over threshold interrupt is set */
    bool IsDvOverTh() const { return fDvOverTh; } 
    /** @return If a analog current over threshold interrupt is set */
    bool IsAcOverTh() const { return fAcOverTh; } 
    /** @return If a analog voltage over threshold interrupt is set */
    bool IsAvOverTh() const { return fAvOverTh; } 
    /** @return If a temperture over threshold interrupt is set */
    bool IsTempOverTh() const { return fTempOverTh; } 
  protected:
    /** Is the card address known */
    bool fIsIdentified;
    /** Address of the card.  If the top bit is set, then it's on
	branch B */
    unsigned int fWhere;
    /** Whether this is a soft interrupt */
    bool fIsSoft;
    /** Whether the FEC answers. */
    bool fIsAnswering;
    /** SCLK - Missed sample clocks */
    bool fMissedSclk;
    /** Alps - ALTRO power supply error */
    bool fAlps;
    /** Paps - PASA power supply error */
    bool fPaps;
    /** DC - Digital current over threshold */
    bool fDcOverTh;
    /** DV - Digital voltage over threshold */
    bool fDvOverTh;
    /** AC - Analog current over threshold */
    bool fAcOverTh;
    /** AV - Analog voltage over threshold */
    bool fAvOverTh;
    /** T - Temperture over threshold */
    bool fTempOverTh;
  };

  //====================================================================
  inline 
  RcuStatusEntry::RcuStatusEntry(unsigned int dat)
  {
    Decode(dat);
  }

  //____________________________________________________________________
  inline 
  RcuStatusEntry::RcuStatusEntry(bool         id, 
				 unsigned int where, 
				 bool         soft, 
				 bool         ans, 
				 bool         sclk, 
				 bool         alps, 
				 bool         paps, 
				 bool         dc, 
				 bool         dv, 
				 bool         ac, 
				 bool         av, 
				 bool         temp)
    : fIsIdentified(id), 
      fWhere(where), 
      fIsSoft(soft), 
      fIsAnswering(ans),
      fMissedSclk(sclk), 
      fAlps(alps), 
      fPaps(paps), 
      fDcOverTh(dc), 
      fDvOverTh(dv), 
      fAcOverTh(ac), 
      fAvOverTh(av), 
      fTempOverTh(temp)
  {}

  //____________________________________________________________________
  inline 
  RcuStatusEntry::RcuStatusEntry(const RcuStatusEntry& other)
    : fIsIdentified(other.fIsIdentified), 
      fWhere(other.fWhere), 
      fIsSoft(other.fIsSoft), 
      fIsAnswering(other.fIsAnswering),
      fMissedSclk(other.fMissedSclk), 
      fAlps(other.fAlps), 
      fPaps(other.fPaps), 
      fDcOverTh(other.fDcOverTh), 
      fDvOverTh(other.fDvOverTh), 
      fAcOverTh(other.fAcOverTh), 
      fAvOverTh(other.fDvOverTh), 
      fTempOverTh(other.fTempOverTh)
  {}

  //____________________________________________________________________
  inline RcuStatusEntry&
  RcuStatusEntry::operator=(const RcuStatusEntry& other)
  {
    fIsIdentified	= other.fIsIdentified; 
    fWhere	= other.fWhere; 
    fIsSoft	= other.fIsSoft; 
    fIsAnswering	= other.fIsAnswering;
    fMissedSclk	= other.fMissedSclk; 
    fAlps		= other.fAlps; 
    fPaps		= other.fPaps; 
    fDcOverTh	= other.fDcOverTh; 
    fDvOverTh	= other.fDvOverTh; 
    fAcOverTh	= other.fAcOverTh; 
    fAvOverTh	= other.fAvOverTh; 
    fTempOverTh	= other.fTempOverTh;
    return *this;
  }

  //____________________________________________________________________
  inline void
  RcuStatusEntry::Decode(unsigned int dat) 
  { 
    fTempOverTh	= dat & (1 << 0);
    fAvOverTh	= dat & (1 << 1);
    fAcOverTh	= dat & (1 << 2);
    fDvOverTh	= dat & (1 << 3);
    fDcOverTh	= dat & (1 << 4);
    fPaps		= dat & (1 << 5);
    fAlps		= dat & (1 << 6);
    fMissedSclk	= dat & (1 << 7);
    fIsAnswering	= !(dat & (1 << 8));
    fIsSoft	= dat & (1 << 9);
    fWhere	= (dat << 10) & 0x1f;
    fIsIdentified = !(dat & (1 << 15));
  }
  
  //____________________________________________________________________
  inline void
  RcuStatusEntry::Print() const
  {
    std::cout << std::boolalpha 
	      << "\tTemperature over:\t\t" << fTempOverTh << "\n"
	      << "\tAnalog voltage over:\t\t" << fAvOverTh << "\n"
	      << "\tAnalog current over:\t\t" << fAcOverTh << "\n"
	      << "\tDigital voltage over:\t\t" << fDvOverTh << "\n"
	      << "\tDigital current over:\t\t" << fDcOverTh << "\n"
	      << "\tPASA power supply trip:\t\t" << fPaps << "\n"
	      << "\tALTRO power supply trip:\t\t" << fAlps << "\n"
	      << "\tMissed slow clock:\t\t" << fMissedSclk << "\n"
	      << "\tAnswering:\t\t" << fIsAnswering << "\n"
	      << "\tSoft:\t\t" << fIsSoft << "\n"
	      << "\tWhere:\t\t0x" << std::hex << fWhere << std::dec << "\n"
	      << "\tIdentified:\t\t" << fIsIdentified << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
