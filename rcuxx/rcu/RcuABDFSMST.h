//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUABDFSMST
# define RCU_RCUABDFSMST
# include <rcuxx/rcu/RcuRegister.h>
# include <rcuxx/rcu/RcuState_t.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuABDFSMST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ABDFSMST register. 

      ALTRO Bus driver state machine states. 
      @ingroup rcuxx_rcu
  */
  struct RcuABDFSMST : public RcuRegister
  {
    struct RDO_t : public RcuState_t
    {
      RDO_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0:  return "idle";
	case 1:  return "chrdo_ex";
	case 3:  return "sel_dm";
	case 2:  return "load";
	case 6:  return "wait_data";
	case 5:  return "store_data";
	case 7:  return "finish";
	}
	return "unknown";
      }
    };
    struct WR_t : public RcuState_t
    {
      WR_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
        switch (fState) {
        case  0: return               "idle";
        case  1: return             "sv_dat";
        case  3: return          "assertbus";
        case  7: return             "chkack";
        case  4: return            "waitack";
        case 12: return           "waitdone";
        case 13: return              "endwr";
        case  6: return             "waitbc";
        case 15: return           "dly_1cyc";
        case  5: return             "clrcnt";
        case  2: return          "waitevlen";
        }
	return RcuState_t::AsString();
      }
    };
    struct RD_t : public RcuState_t
    {
      RD_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0: return "idle";
	case 1: return "sv_dat";
	case 3: return "assertbus";
	case 6: return "chkack";
	case 7: return "waitack";
	case 5: return "store";
	case 4: return "waitdone";
	case 12: return "endrd";
	case 2: return "clrcnt";
	}
	return "unknown";
      }
    };  
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuABDFSMST(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "ABDFSMST", "Altro Bus D FSM STates", 
		    0, false), 
	fRDO_B("RDO_B", 19, 0x7), 
	fRD_B("RD_B", 15, 0xf), 
	fWR_B("WR_B", 11, 0xf),
	fRDO_A("RDO_A", 8, 0x7), 
	fRD_A("RD_A", 4, 0xf), 
	fWR_A("WR_A", 0, 0xf) 
    {}
    /** @return RDO for branch A */
    const RcuState_t& RDO_A() const { return fRDO_A; } 
    /** @return RD for branch A */
    const RcuState_t& RD_A() const { return fRD_A; } 
    /** @return WR for branch A */
    const RcuState_t&  WR_A() const { return fWR_A; } 
    /** @return RDO for branch B */
    const RcuState_t& RDO_B() const { return fRDO_B; } 
    /** @return RD for branch B */
    const RcuState_t&  RD_B() const { return fRD_B; } 
    /** @return WR for branch B */
    const RcuState_t&  WR_B() const { return fWR_B; } 

    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      fRDO_A.Print();
      fRD_A.Print();
      fWR_A.Print();
      fRDO_B.Print();
      fRD_B.Print();
      fWR_B.Print();
    }
    void Decode(unsigned int dat) { 
      fRDO_B.Decode(dat);
      fRD_B.Decode(dat);
      fWR_B.Decode(dat);
      fRDO_A.Decode(dat);
      fRD_A.Decode(dat);
      fWR_A.Decode(dat);
    }
  protected:
    /** State */
    RDO_t fRDO_B;
    /** State */
    RD_t fRD_B;
    /** State */
    WR_t fWR_B;
    /** State */
    RDO_t fRDO_A;
    /** State */
    RD_t fRD_A;
    /** State */
    WR_t fWR_A;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
