//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUCOUNTER
# define RCU_RCUCOUNTER
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @class RcuCounter rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache of the TRigger CouNTers. 

      This class interfaces to the various types of counters in the
      RCU. 
      @ingroup rcuxx_rcu 
  */
  struct RcuCounter : public RcuRegister
  {

    /** Constructor 
	@param name Name of counter 
	@param title Title of counter 
	@param mem reference to memory 
	@param offset Offset in memory
	@param clear Clear command */
    RcuCounter(const char* name, const char* title, 
	       RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, name, title, clear, false), 
	fCounts(0)
    {}
    /** Decode @a dat as the TRigger CouNTers */
    virtual void Decode(unsigned int dat) 
    {
      fCounts = dat;
    }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tCounts:\t\t\t" << fCounts << std::endl;
    }
    /** @return Recieved triggers */
    unsigned int Counts() const { return fCounts; }
  protected:
    /** How many triggers we got */
    unsigned int fCounts;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
