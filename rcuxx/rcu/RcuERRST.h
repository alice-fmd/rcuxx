//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUERRST
# define RCU_RCUERRST
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @class RcuERRST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ERRor and STatus register.  

      Errors are recorded in this register. 
      @ingroup rcuxx_rcu
  */
  struct RcuERRST : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuERRST(RcuMemory& mem, size_t offset, RcuCommand* clear) 
      : RcuRegister(mem, offset, "ERRST", "ERRor and STatus", clear, false),
	fPattern(false), 
	fAbort(false), 
	fTimeout(false), 
	fAltro(false),
	fHWAdd(false), 
	fBusy(false), 
	fWhere(0)
    {}

    /** @return @c true if Pattern */
    bool IsPattern() const { return fPattern; }
    /** @return @c true if Abort */
    bool IsAbort() const { return fAbort; }
    /** @return @c true if Timeout */
    bool IsTimeout() const { return fTimeout; }
    /** @return @c true if Altro */
    bool IsAltro() const { return fAltro; }
    /** @return @c true if HWAdd */
    bool IsHWAdd() const { return fHWAdd; }
    /** @return @c true if Busy */
    bool IsBusy() const { return fBusy; }
    /** @return where error occured */
    unsigned int Where() const { return fWhere; }
    /** Decode @a dat as the ERRor and STatus register  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
  protected:
    /** Error when comparing ALTRO channel PMEM to RCU PMEM */
    bool fPattern;
    /** user did an abort */
    bool fAbort;
    /** Command timed out */
    bool fTimeout;
    /** Error from ALTRO bus (ALTROs or BCs) */
    bool fAltro;
    /** Error in address read in data read-out */
    bool fHWAdd;
    /** ALTRO bus interface busy */
    bool fBusy;
    /** The address of this chip. */
    unsigned int fWhere;
  };
  //====================================================================
  inline void
  RcuERRST::Decode(unsigned int dat) 
  { 
    fPattern	= (dat & (1 << 0));
    fAbort	= (dat & (1 << 1));
    fTimeout	= (dat & (1 << 2));
    fAltro	= (dat & (1 << 3));
    fHWAdd	= (dat & (1 << 4));
    fBusy		= (dat & (1 << 31));
    fWhere    	= (dat >> 11);
  }

  //____________________________________________________________________
  inline void
  RcuERRST::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha 
	      << "\tPattern:\t\t\t" << fPattern << "\n"
	      << "\tAbort:\t\t\t\t" << fAbort << "\n"
	      << "\tTimeout:\t\t\t" << fTimeout << "\n"
	      << "\tAltro:\t\t\t\t" << fAltro << "\n"
	      << "\tHWAdd:\t\t\t\t" << fHWAdd << "\n"
	      << "\tBusy:\t\t\t\t" << fBusy << "\n"
	      << "\tWhere:\t\t\t\t0x" << std::hex << fWhere << std::dec 
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
