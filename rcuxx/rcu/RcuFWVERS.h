//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUFWVERS
# define RCU_RCUFWVERS
# include <rcuxx/RcuLayout.h>
# include <rcuxx/rcu/RcuRegister.h>
# include <sstream>
# include <iomanip>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuFWVERS rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the FirmWare VERSion register

      This register gives the version of the firmware loaded on the RCU.
      @ingroup rcuxx_rcu
  */
  struct RcuFWVERS : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuFWVERS(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "FWVERS", "FirmWare VERSion", 0, false), 
	fVersion(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int Version() const { return fVersion; }
    /** @return Address of data in memory 1 */
    unsigned int Decoded() const { return fDecoded; }
    /** @return version as a string */
    const std::string& AsString() const { return fStr; }
  protected:
    /** Whether it's not responding */
    unsigned int fVersion;
    /** Whether it's not responding */
    unsigned int fDecoded;
    /** String value */
    std::string fStr;
  };

  //====================================================================
  inline void
  RcuFWVERS::Decode(unsigned int dat) 
  {
    fVersion           = dat;
    fDecoded           = RcuLayout::Hw2Date(dat);
    unsigned int day   = ((fDecoded >>  0) & 0xFF);
    unsigned int month = ((fDecoded >>  8) & 0xFF);
    unsigned int year  = ((fDecoded >> 16) & 0xFF) + 0x2000;

    std::stringstream s(fStr);
    if (fVersion > 0xffffff) { 
#if 0
      unsigned int app   = (fVersion >> 8) & 0xFF;
      unsigned int major = (fVersion >> 4) & 0xF;
      unsigned int minor = (fVersion >> 0) & 0xF;
      s << (app == 0xAE ? "ALICE" : "unknown") 
	<< " " << major << "." << minor << " - ";
#endif
      unsigned int app = fVersion & 0xFF;
      s << (app == 0xAE ? "ALICE" : "unknown") << " - ";
    }

    s << std::hex << std::setfill('0') 
      << std::setw(4) << year << "-"
      << std::setw(2) << month << "-"
      << std::setw(2) << day;
    fStr = s.str();
  }
  //____________________________________________________________________
  inline void
  RcuFWVERS::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tVersion:\t\t\t" << std::hex << std::setfill('0') 
	      << "0x" << std::setw(8) << fVersion << " (yy-mm-dd: " 
	      << fStr << ")" << std::setfill(' ') << std::dec 
	      << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
