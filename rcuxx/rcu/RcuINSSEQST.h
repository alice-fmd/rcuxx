//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUINSSEQST
# define RCU_RCUINSSEQST
# include <rcuxx/rcu/RcuRegister.h>
# include <rcuxx/rcu/RcuState_t.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuINSSEQST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the INStruction SEQuencer STates

      Records the states of the instruction sequencer state machines.
      @ingroup rcuxx_rcu
  */
  struct RcuINSSEQST : public RcuRegister
  {
    struct INSTLOOP_t : public RcuState_t
    {
      INSTLOOP_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
	switch (fState) { 
	case 0:  return "idle";
	case 1:  return "init_cnt";
	case 3:  return "act_mem";
	case 2:  return "wait_dec";
	case 6:  return "chk";
	case 7:  return "wat";
	case 14: return "fnsh_wait";
	case 5:  return "chk_add";
	case 12: return "send_rd";
	case 13: return "send_cmd";
	case 15: return "get_p2";
	case 8:  return "wr_enmem";
	case 24: return "send_wr";
	case 9:  return "wait_done";
	case 11: return "gen_add_1";
	case 4:  return "gen_add_2";
	case 25: return "dly_1cyc";
	case 10: return "finish";
	}
	return "unknown";
      }
    };
    struct INSTSEQ_t : public RcuState_t
    {
      INSTSEQ_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
        switch (fState) {
        case  0: return               "idle";
        case  1: return            "sel_prg";
        case  3: return             "ld_prg";
        case  2: return          "act_mem_1";
        case  6: return           "wait_dec";
        case  7: return                "chk";
        case 11: return              "stlup";
        case  9: return          "wait_loop";
        case  8: return          "fnsh_loop";
        case 24: return            "ld_loop";
        case 25: return           "dly_1cyc";
        case 10: return                "wat";
        case 26: return          "fnsh_wait";
        case 27: return          "gen_add_1";
        case 15: return          "gen_add_2";
        case 14: return          "act_mem_2";
        case 13: return            "send_rd";
        case 12: return           "send_cmd";
        case  4: return             "get_p2";
        case 31: return           "wr_enmem";
        case 29: return            "send_wr";
        case 30: return          "wait_done";
        case  5: return               "fnsh";
        }
	return "unknown";
      }
    };
    struct RU_t : public RcuState_t
    {
      RU_t(const char* name, unsigned int off, unsigned int mask)      
	: RcuState_t(name, off, mask)
      {}
      const char* AsString() const 
      {
        switch (fState) {
        case  0: return               "idle";
        case  1: return           "idle_seq";
        case  3: return                "chk";
        case  4: return         "sv_err_add";
        case 12: return          "wait_done";
        case 13: return         "sv_suc_add";
        case 14: return         "sv_rd_data";
        case 10: return         "sv_wr_data";
        case 15: return        "sv_fail_add";
        case  2: return         "sv_err_cod";
        case  6: return          "wait_loop";
        case 11: return           "loop_suc";
        case  9: return          "loop_fail";
        case 24: return        "ld_loop_err";
        case  8: return            "act_mem";
        case  7: return                "wat";
        case 25: return          "fnsh_wait";
        case  5: return               "fnsh";
        }
	return "unknown";
      }
    };

	  
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuINSSEQST(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "INSSEQST", "INStruction SEQuencer STate", 
		    0, false), 
	fINSTSEQ("INSTSEQ", 10, 0x1f), 
	fINSTLOOP("INSTLOOP", 5, 0x1f), 
	fRU("RU", 0, 0x1f)
    {}
    /** @return FSM_RDO for branch B */
    const RcuState_t& INSTSEQ() const { return fINSTSEQ; } 
    /** @return FSM_RD for branch B */
    const RcuState_t&  INSTLOOP() const { return fINSTLOOP; } 
    /** @return FSM_WR for branch B */
    const RcuState_t&  RU() const { return fRU; } 

    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      fINSTSEQ.Print();
      fINSTLOOP.Print();
      fRU.Print();
    }
    void Decode(unsigned int dat) { 
      fINSTSEQ.Decode(dat);
      fINSTLOOP.Decode(dat);
      fRU.Decode(dat);
    }
  protected:
    /** State */
    INSTSEQ_t fINSTSEQ;
    /** State */
    INSTLOOP_t fINSTLOOP;
    /** State */
    RU_t fRU;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
