//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUPMCFG
# define RCU_RCUPMCFG
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuPMCFG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the Pedestal Memory ConFiGuration

      This sets the first and last timeout to write to when executing
      the pedestal load instruction (not for RCU II and up).
      @ingroup rcuxx_rcu
  */
  struct RcuPMCFG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuPMCFG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "PMCFG","Pedestal Memory range ConFiGuration", 
		    0, true), 
	fBegin(0), fEnd(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return Begin of block address */
    unsigned int Begin() const { return fBegin; }
    /** @return End of block address */
    unsigned int End() const { return fEnd; }
    /** Set start of block address */
    void SetBegin(unsigned int val) { fBegin = (val & 0x3ff); }
    /** Set end of block address */
    void SetEnd(unsigned int val) { fEnd = (val & 0x3ff); }
  protected:
    /** Start */
    unsigned int fBegin;
    /** End */
    unsigned int fEnd;
  };

  //====================================================================
  inline void
  RcuPMCFG::Decode(unsigned int dat) 
  { 
    fBegin = (dat & 0x3ff);
    fEnd   = ((dat >> 10) & 0x3ff);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuPMCFG::Encode() const 
  { 
    unsigned int val = 0;
    val += (fBegin & 0x3ff);
    val += (fEnd & 0x3ff) << 10;
    return val;
  }

  //____________________________________________________________________
  inline void
  RcuPMCFG::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tBegin:\t\t\t\t0x" << std::hex << fBegin << "\n" 
	      << "\tEnd:  \t\t\t\t0x" << std::hex << fEnd << std::dec 
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
