//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUCHADD
# define RCU_RCUCHADD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuCHADD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the CHannel Address register 

      This register records the last channel read-out. 
      @ingroup rcuxx_rcu
  */
  struct RcuCHADD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuCHADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "CHADD", "Last read-out CHannel ADDress", 
		    0, false), 
	fAddress1(0), fAddress2(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int Address1() const { return fAddress1; }
    /** @return Address of data in memory 2 */
    unsigned int Address2() const { return fAddress2; }
  protected:
    /** First address */
    unsigned int fAddress1;
    /** Second address */
    unsigned int fAddress2;
  };
  //====================================================================
  inline void
  RcuCHADD::Decode(unsigned int dat) 
  { 
    fAddress1 = (dat & 0x3ff);
    fAddress2   = ((dat >> 10) & 0x3ff);
  }

  //____________________________________________________________________
  inline void
  RcuCHADD::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tAddress 1:\t\t\t0x" << std::hex << fAddress1 << "\n" 
	      << "\tAddress 2:\t\t\t0x" << std::hex << fAddress2 << std::dec 
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
