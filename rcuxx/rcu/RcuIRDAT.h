//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUIRDAT
# define RCU_RCUIRDAT
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuIRDAT rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache the last executed ALTRO InstRuction DATa.

      Data of last executed instruction memory instruction.
      @ingroup rcuxx_rcu
  */
  struct RcuIRDAT : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuIRDAT(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "IRDAT", "Last executed InstRuction DATa", 
		    0, false), fIRDAT(0) {}
    /** Decode @a dat as the last executed ALTRO InstRuction  ADDress  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Last instruction address */
    unsigned int IRDAT() const { return fIRDAT; }
  protected:
    /** Data of last instruction */
    unsigned int fIRDAT;
  };
  //====================================================================
  inline void
  RcuIRDAT::Decode(unsigned int dat) 
  { 
    fIRDAT = (dat & 0xfffff);
  }

  //____________________________________________________________________
  inline void
  RcuIRDAT::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tIRDAT:\t\t\t\t0x" << std::hex << fIRDAT << std::dec 
	      << std::endl;
  }

}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
