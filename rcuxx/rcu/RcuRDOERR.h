//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURDOERR
# define RCU_RCURDOERR
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRDOERR rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ReaDOut ERRor register 

      Errors during read-out are recorded here.
      @ingroup rcuxx_rcu
  */
  struct RcuRDOERR : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  
	@param clear Clear command */
    RcuRDOERR(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "RDOERR", "ReaD-Out ERRors", clear, false), 
	fSend(false),
	fAltroError(false),
	fNoTrsf(false),
	fMissDstb(false),
	fNoRelease(false),
	fSCEVL(false),
	fRDRX(false),
	fChannelAddress(false),
	fBlockLength(false),
	fWR_ST_B(0), 
	fWR_ST_A(0),
	fScan(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) 
    { 
      
      fSend           = (dat & (1 << 0) ? true : false);
      fAltroError     = (dat & (1 << 1) ? true : false);
      fNoTrsf         = (dat & (1 << 2) ? true : false);
      fMissDstb       = (dat & (1 << 3) ? true : false);
      fNoRelease      = (dat & (1 << 4) ? true : false);
      fSCEVL          = (dat & (1 << 5) ? true : false);
      fRDRX           = (dat & (1 << 6) ? true : false);
      fChannelAddress = (dat & (1 << 7) ? true : false);
      fBlockLength    = (dat & (1 << 8) ? true : false);
      fScan           = (dat >> 20) & 0xf;
      fWR_ST_A        = (dat >> 24) & 0xf; 
      fWR_ST_B        = (dat >> 28) & 0xf; 
    }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tError during send:\t\t\t"		<<fSend	<< "\n"
		<< "\tALTRO error during wait:\t\t"	<<fAltroError	<< "\n"
		<< "\tTrsf not asserted:\t\t\t"		<<fNoTrsf	<< "\n"
		<< "\t< 4 Data strobes:\t\t\t"		<<fMissDstb	<< "\n"
		<< "\tNo release of TRSF:\t\t\t"	<<fNoRelease	<< "\n"
		<< "\tError during scan event length:\t"<<fSCEVL	<< "\n"
		<< "\tRDRX not (de)asserted:\t\t\t"	<<fRDRX	<< "\n"
		<< "\tChannel address mismatch:\t\t\t"	<<fChannelAddress<<"\n"
		<< "\tBlock length mismatch:\t\t\t"	<<fBlockLength	<< "\n"
		<< "\tWrite FSM state branch B:\t\t"	<<fWR_ST_B	<< "\n"
		<< "\tWrite FSM state branch A:\t\t"	<<fWR_ST_A	<< "\n"
		<< "\tScan event length state:\t\t\t"	<<fScan	
		<< std::endl;
    }
    /** @return Error during send command */
    bool IsSend() const { return fSend; }
    /** @return Altro error during wait for transfer */
    bool IsAltroError() const { return fAltroError; }
    /** @return No transfer strobe asserted */
    bool IsNoTrsf() const { return fNoTrsf; }
    /** @return Less than 4 data strobes */
    bool IsMissDstb() const { return fMissDstb; }
    /** @return No release of the transfer line */
    bool IsNoRelease() const { return fNoRelease; }
    /** @return Error during send of scan event length */
    bool IsSCEVL() const { return fSCEVL; }
    /** @return RD_RX not (de)asserted */
    bool IsRDRX() const { return fRDRX; }
    /** @return Channel address mismatch */
    bool IsChannelAddress() const { return fChannelAddress; }
    /** @return Block length mismatch*/
    bool IsBlockLength() const { return fBlockLength; }
    /** @return Write FSM branch B state */
    unsigned short WR_ST_B() const { return fWR_ST_B; } 
    /** @return Write FSM branch A state */
    unsigned short WR_ST_A() const { return fWR_ST_A; }
    /** @return Scan event length FSM state */
    unsigned short Scan() const { return fScan; }

  protected:
    /** Error during send command */
    bool fSend;
    /** Altro error during wait for transfer */
    bool fAltroError;
    /** No transfer strobe asserted */
    bool fNoTrsf;
    /** Less than 4 data strobes */
    bool fMissDstb;
    /** No release of the transfer line */
    bool fNoRelease;
    /** Error during send of scan event length */
    bool fSCEVL;
    /** RD_RX not (de)asserted */
    bool fRDRX;
    /** Channel address mismatch */
    bool fChannelAddress;
    /** Block length mismatch*/
    bool fBlockLength;
    /** Write FSM branch B state */
    unsigned short fWR_ST_B; 
    /** Write FSM branch A state */
    unsigned short fWR_ST_A;
    /** Scan event length FSM state */
    unsigned short fScan;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
