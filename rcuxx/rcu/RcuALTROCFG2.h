//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUALTROCFG2
# define RCU_RCUALTROCFG2
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuALTROCFG2 rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to ALTRO configuration 2 register 

      The content of this register is written to the RCU trailer for
      every event.  Note, that the register is used in no other way
      than that. That is, it is essentially just 32bits of
      user-definable data that will be appended to each event. 
      @ingroup rcuxx_rcu
  */
  struct RcuALTROCFG2 : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuALTROCFG2(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "ALTROCFG2", "ALTRO ConFiGuration 2", 0, true),
	fPreSamples(0), 
	f8Buf(false), 
	fNSamples(0), 
	fSparseRDO(false), 
	fSClkFreq(0), 
	fL1Phase(0)
    {}
    unsigned short PreSamples() const { return fPreSamples; }
    unsigned short NBuffers() const { return f8Buf ? 8 : 4; }
    unsigned short NSamples() const { return fNSamples; }
    bool IsSparseRDO() const { return fSparseRDO; }
    unsigned short SClkFreq() const { return fSClkFreq; }
    unsigned short L1Phase() const { return fL1Phase; }
    void SetPreSamples(unsigned short n) { fPreSamples  = n & 0xF; }
    void SetNBuffers(unsigned short n)   { f8Buf        = (n == 8 ? 8 : 4); }
    void SetNSamples(unsigned short n)   { fNSamples    = n & 0x3FF; }
    void SetSparseRDO(bool sparse)       { fSparseRDO   = sparse; }
    void SetSClkFreq(unsigned short f)   { fSClkFreq    = f & 0xF; }
    unsigned int Encode() const 
    { 
      /* 
       * This is the encoding used in the trailer 
       *
       * return (((f8Buf       & 0x1)   << 24) | 
       *	 ((fPreSamples & 0xF)   << 20) | 
       *	 ((fNSamples   & 0x3FF) << 10) | 
       *	 ((fSparseRDO  & 0x1)   <<  9) | 
       *	 ((fSClkFreq   & 0xF)   <<  5) | 
       *	 ((fL1Phase    & 0x1F)  <<  0));
       */
      return (((f8Buf       & 0x1)   << 4) | 
	      ((fPreSamples & 0xF)   << 0));
    }
    void Decode(unsigned int dat) 
    { 
      f8Buf       = (dat >> 4) & 0x1;
      fPreSamples = (dat >> 0) & 0xF;
      /* 
       * This is the encoding used in the trailer 
       *
       * 
       * f8Buf       = (dat >> 24) & 0x1;
       * fPreSamples = (dat >> 20) & 0xF;
       * fNSamples   = (dat >> 10) & 0x3FF;
       * fSparseRDO  = (dat >>  9) & 0x1;
       * fSClkFreq   = (dat >>  5) & 0xF;
       * fL1Phase    = (dat >>  0) & 0x1F;
       */
    }
    bool Compare(unsigned int v) const 
    {
      return Encode() == (v & 0x1F);
    }
    void Print() const;
    /** Number of pre-L1 samples - user */
    unsigned short fPreSamples;
    /** Whether we're using 8 buffers - user */
    bool           f8Buf;
    /** Number of samples - ALTROIF */
    unsigned short fNSamples;
    /** Wheter sparse read-out is enabled - RDOMOD */
    bool           fSparseRDO; 
    /** Sample frequency - ALTROIF */
    unsigned short fSClkFreq;
    /** Read-only  - TTC interface */
    unsigned short fL1Phase;  
  };
  //____________________________________________________________________
  inline void
  RcuALTROCFG2::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tN presamples:\t\t"   << PreSamples()  << "\n" 
	      << "\tN Buffers:\t\t"      << NBuffers()    << "\n"
	      << "\tN samples:\t\t"      << NSamples()    << "\n"
	      << "\tSparse RDO:\t\t"     << IsSparseRDO() << "\n"
	      << "\tSClk frequency:\t\t" << SClkFreq()    << "\n"
      	      << "\tL1 phase:\t\t"       << L1Phase()     << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
