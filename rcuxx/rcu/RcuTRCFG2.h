//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUTRCFG2
# define RCU_RCUTRCFG2
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuTRCFG2 rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the second TRigger ConFiG register.  
      @note This register is only available with the U2F interface 
      @ingroup rcuxx_rcu
  */
  struct RcuTRCFG2 : public RcuRegister
  {
    /** Read out mode */
    enum Mode_t {
      kPop, 
      kPush
    };

    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuTRCFG2(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "TRCFG2", "TRiGger ConFiGuration", 0, true), 
	fMode(kPop), 
	fEnableHT(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;

    /** Mode */
    Mode_t Mode() const { return fMode; }
    /** Mode */
    void SetMode(Mode_t mode) { fMode = mode; }
    /** @return hardware trigger enabled */
    bool IsEnableHT() const { return fEnableHT; }
    /** Enable hardware trigger */
    void SetEnableHT(bool on=true) { fEnableHT = on; }
  protected:
    /** Mode */
    Mode_t fMode;
    /** Enable hardware trigger */
    bool fEnableHT;
  };

  //====================================================================
  inline void
  RcuTRCFG2::Decode(unsigned int dat) 
  { 
    fMode     = (dat & 0x1 ? kPush : kPop);
    fEnableHT = (dat & 0x2);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuTRCFG2::Encode() const 
  { 
    return (fMode == kPush ? 1 : 0) + (fEnableHT ? 2 : 0);
  }

  //____________________________________________________________________
  inline void
  RcuTRCFG2::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tMode:\t\t\t\t" 
	      << (fMode == kPop ? "Popped" : "Pushed") << "\n"
	      << "\tHW trigger enabled:\t\t" << fEnableHT << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
//
