// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUSTATUS_H
# define RCUXX_RCUSTATUS_H
# include <rcuxx/rcu/RcuMemory.h>
# include <rcuxx/rcu/RcuStatusEntry.h>
# include <vector>
# include <stdexcept>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuSTATUS rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU Event Header memory
      @ingroup rcuxx_rcu
  */
  struct RcuSTATUS  : public RcuMemory
  {
    /** 
     * Constructor 
     * 
     * @param rcu     Reference to low-level interface 
     * @param base    BAse address
     * @param n       Number of words
     * @param noCount Do not count
     * 
     * @return 
     */
    RcuSTATUS(Rcu& rcu, size_t base, size_t n, bool noCount);
    /** Update the memory 
	@return 0 on success, error code otherwise */
    unsigned int Update() { return Update(0, fSize); }
    /** Update @a n words from the memory starting at @a offset 
	@param offset Where to start 
	@param n How many words to read. 
	@return 0 on success, error code otherwise */
    unsigned int Update(size_t offset, size_t n);
    /** Get the @f$ i^{th}@f$ entry. 
	@param i Entry to get
	@return Reference to the @a i entry */
    const RcuStatusEntry& Entry(size_t i) const;
    /** @return number of entries set. */
    size_t Entries() const { return fEntries; }
    /** Print cache to standard output */
    virtual void Print() const;
  protected:
    /** Whether we have a count */
    bool fNoCount;
    /** number of entries */
    unsigned int fEntries;
    /** Type of list of status entries */
    typedef std::vector<RcuStatusEntry> StatusList;
    /** List of status entries */
    StatusList fList;
  };
  //====================================================================
  inline RcuSTATUS::RcuSTATUS(Rcu& rcu, size_t base, size_t n, bool noCount) 
    : RcuMemory(rcu, "STATUS", "Interrupt status memory", 
		base, n, 0xffff, true), 
      fNoCount(noCount),
      fEntries(0), 
      fList(32)
  {}

  //____________________________________________________________________
  inline unsigned int 
  RcuSTATUS::Update(size_t offset, size_t n)
  {
    size_t       m   = (fNoCount ? n : n+1);
    unsigned int ret = RcuMemory::Update(0, m);
    if (ret) return ret;
    fList.clear();
    if (fNoCount) fEntries = n;
    else          fEntries = fData[0];
    if (fEntries > 32) { 
      std::cerr << "Warning, when reading from STATUS memory.\n"
		<< "Apparently, there's " << fEntries << " entries (> 32)" 
		<< std::endl;
      fEntries = 32;
      // return 1;
    }
    m = std::min(n, size_t(fEntries));
    for (size_t i = 0; i < m; i++) 
      fList[i] = RcuStatusEntry(fData[i+1]);
    return 0;
  }

  //____________________________________________________________________
  inline const RcuStatusEntry&
  RcuSTATUS::Entry(size_t i) const
  {
    if (i > fList.size()) throw std::runtime_error("Out of bounds");
    return fList[i];
  }
  
  //____________________________________________________________________
  inline void
  RcuSTATUS::Print() const
  {
    std::cout << "Contents of " << fName << ": " << fEntries 
	      << " cards listed" << std::endl;
    for (size_t i = 0; i < fEntries; ++i) { 
      std::cout << "  Card " << i << std::endl;
      fList[i].Print();
    }
  }

}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
