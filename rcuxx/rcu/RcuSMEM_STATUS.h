//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUSMEM_STATUS
# define RCU_RCUSMEM_STATUS
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuSMEM_STATUS rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief  @brief Interface to the @f$I^2C @f$ address register 
      @ingroup rcuxx_rcu
  */
  struct RcuSMEM_STATUS : public RcuRegister
  {
    enum MaskBit { 
      kT    = 0x01,
      kAV   = 0x02,
      kAC   = 0x04, 
      kDV   = 0x08,
      kDC   = 0x10, 
      kPaps = 0x20, 
      kAlps = 0x40, 
      kSCLK = 0x80
    };
    /** 
     * Constructor 
     *
     * @param mem reference to memory 
     * @param offset Offset in memory 
     * @param clear  Clear command 
     */
    RcuSMEM_STATUS(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "SMEM_STATUS", "Status MEMory STATUS", 
		    clear, false), 
	fData(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fData = dat; }
    /** Encode  */
    virtual unsigned int Encode() const { return fData; }
    /** Print to standard output */
    virtual void Print() const;
    bool AReady() const { return fData & (1 << 4); }
    bool BReady() const { return fData & (1 << 9); }
    unsigned int ALast() const { return (fData >> 0) & 0xf; }
    unsigned int BLast() const { return (fData >> 5) & 0xf; }
  protected:
    unsigned int fData;
  };
  //____________________________________________________________________
  inline void
  RcuSMEM_STATUS::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tA half ready:\t\t" << AReady() << "\n"
	      << "\tB half ready:\t\t" << BReady() << "\n"
	      << "\tLast A offset:\t\t" << ALast() << "\n"
	      << "\tLast B offset:\t\t" << BLast() << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
