//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURCUBUS
# define RCU_RCURCUBUS
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRCUBUS rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the internal RCU BUS state register

      This says how owns the internal bus.
      @ingroup rcuxx_rcu
  */
  struct RcuRCUBUS : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuRCUBUS(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RCUBUS", "RCU BUS status", 0, false), 
	fDDL(false),
	fDCS(false)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat)
    { 
      fDDL = (dat & 0x1);
      fDCS = (dat & 0x2); 
    }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      std::cout << "\tDDL:\t\t\t" << fDDL << "\n"
		<< "\tDCS:\t\t\t" << fDCS << std::endl;
    }
    /** Sparse readout (BC optimised) */
    bool DDL() const { return fDDL; }
    /** Meta event block */
    bool DCS() const { return fDCS; }
  protected:
    /** Sparse readout (BC optimised) [0] */
    bool fDDL;
    /** Meta event block [2] */
    bool fDCS;
  };
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
