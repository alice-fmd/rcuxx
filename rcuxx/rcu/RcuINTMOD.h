//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUINTMOD
# define RCU_RCUINTMOD
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuINTMOD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the INTerrupt MODe register 

      This register defines which branches we should enable interrupt
      handling for. 
      @ingroup rcuxx_rcu
  */
  struct RcuINTMOD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuINTMOD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "INTMOD", "INTerrupt MODe", 0, true), 
	fBranchA(true), fBranchB(true)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode() const;
    /** Print to standard output */
    virtual void Print() const;
    /** @return BranchA of data in memory 1 */
    bool BranchA() const { return fBranchA; }
    /** @return BranchA of data in memory 2 */
    bool BranchB() const { return fBranchB; }
    /** Set BranchA */
    void SetBranchA(bool addr) { fBranchA = addr; }
    /** Set BranchB (?) */
    void SetBranchB(bool res) { fBranchB = res; }
    bool Compare(unsigned int v) const
    {
      return Encode() == (v & 0x3);
    }
  protected:
    /** Whether branch A is on for interrupts */
    bool fBranchA;
    /** Whether branch B is on for interrupts */
    bool fBranchB;
  };
  //====================================================================
  inline void
  RcuINTMOD::Decode(unsigned int dat) 
  { 
    fBranchA  = (dat & 0x1);
    fBranchB  = (dat & 0x2);
  }

  //____________________________________________________________________
  inline unsigned int 
  RcuINTMOD::Encode() const 
  { 
    unsigned int val = (fBranchA ? 1 : 0) + (fBranchB ? 2 : 0);
    return val;
  }

  //____________________________________________________________________
  inline void
  RcuINTMOD::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tBranch A :\t\t\t" << fBranchA << "\n" 
	      << "\tBranch B :\t\t\t" << fBranchB << std::dec 
	      << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
