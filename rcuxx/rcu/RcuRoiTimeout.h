//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCURoiTimeout
# define RCU_RCURoiTimeout
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuRoiTimeout rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the Region Of Interest timeout register.  
      @ingroup rcuxx_rcu
  */
  struct RcuRoiTimeout : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuRoiTimeout(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RoiTimeout", "Region of intrest Timeout", 
		    0, true), 
	fMin(0), 
	fMax(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { 
      fMax = (dat & 0xFFFF);
      fMin = (dat >> 16) & 0xFFFF;
    }
    
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode() const { 
      return (fMax & 0xFFFF) | ((fMin & 0xFFFF) << 16);
    }
    /** Print to standard output */
    virtual void Print() const;
    /** @return BC to optimize readout */
    unsigned int Min() const { return fMin; }
    /** Use BC to optimize readout */
    void SetMin(unsigned int min) { fMin = min; } 
    /** @return BC to optimize readout */
    unsigned int Max() const { return fMax; }
    /** Use BC to optimize readout */
    void SetMax(unsigned int max) { fMax = max; } 
  protected:
    /** Trigger source */ 
    unsigned short fMin;
    /** Roi latency wrt Roi */ 
    unsigned short fMax;
  };
  //====================================================================
  //____________________________________________________________________
  inline void
  RcuRoiTimeout::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tMinimum:\t\t\t\t" << fMin << " [25ns]\n"
	      << "\tMaximum:\t\t\t\t" << fMax << " [25ns]" << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++ 
// End:
//
