// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCUHITLIST_H
# define RCUXX_RCUHITLIST_H
# include <rcuxx/rcu/RcuMemory.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuHITLIST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU hitlist memory

      The hitlist memory is only used during sparse read-out.  When
      sparse read-out is enabled, the RCU will ask each BC on the
      enabled front-end cards to read from the ALTROs which channels
      have data.   After that has finished, the RCU will ask each BC
      for this data.   If a channel has data, then the corresponding
      entry in this memory will be set to one.  Then, the RCU will
      read-out each channel mentioned in this memory and enabled in
      the active channel list. 

      For non-sparse read-out, all entries should be enabled. 
      @ingroup rcuxx_rcu
  */
  struct RcuHITLIST  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuHITLIST(Rcu& rcu, size_t base, size_t n)
      : RcuMemory(rcu, "HITLIST", "List of channels with data", 
		  base, n, 0xffffffff, true) 
      {}
    void Zero()
    {
      for (size_t i = 0; i < fData.size(); i++) fData[i] = 0xffffffff;
    }
  };
}
#endif
// 
// Local Variables:
//   mode: C++
// End:
//
