//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUERRREG
# define RCU_RCUERRREG
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuERRREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ERRor REGister 

      The @f$ I^2C@f$ bus error register. 
      @ingroup rcuxx_rcu
  */
  struct RcuERRREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuERRREG(RcuMemory& mem, size_t offset, RcuCommand* clear=0)
      : RcuRegister(mem, offset, "ERRREG", "I2C ERRor REGister", clear, false), 
	fIsNoAcknowledge(false), fIsNotActive(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int IsNoAcknowledge() const { return fIsNoAcknowledge; }
    /** @return Address of data in memory 2 */
    unsigned int IsNotActive() const { return fIsNotActive; }
  protected:
    /** Whether it's not responding */
    bool fIsNoAcknowledge;
    /** Whether it's not active */
    bool fIsNotActive;
  };
  //====================================================================
  inline void
  RcuERRREG::Decode(unsigned int dat) 
  {
    fIsNoAcknowledge = (dat & 0x2);
    fIsNotActive     = (dat & 0x1);
  }

  //____________________________________________________________________
  inline void
  RcuERRREG::Print() const
  {
    RcuRegister::Print();
    std::cout << std::boolalpha
	      << "\tNo aknowledge:\t\t\t" << fIsNoAcknowledge 
	      << "\n" 
	      << "\tNot active:\t\t\t" << fIsNotActive << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
