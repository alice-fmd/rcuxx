//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifndef RCU_RCUINT_TYPE
# define RCU_RCUINT_TYPE
# include <rcuxx/rcu/RcuRegister.h>

namespace Rcuxx
{
  //====================================================================
  /** @struct RcuINT_TYPE rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief  @brief Interface to the @f$I^2C @f$ address register 
      @ingroup rcuxx_rcu
  */
  struct RcuINT_TYPE : public RcuRegister
  {
    enum MaskBit { 
      kT    = 0x01,
      kAV   = 0x02,
      kAC   = 0x04, 
      kDV   = 0x08,
      kDC   = 0x10, 
      kPaps = 0x20, 
      kAlps = 0x40, 
      kSCLK = 0x80
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuINT_TYPE(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "INT_TYPE", "INTerrupt TYPEs", 0, true), 
	fData(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fData = dat; }
    /** Encode  */
    virtual unsigned int Encode() const { return fData; }
    /** Print to standard output */
    virtual void Print() const;
    unsigned int Mask() { return fData & 0xFF; }
    void SetMask(unsigned int m) { fData = m; }
    bool IsEnabled(MaskBit b) { return (fData & b); }
    void Enable(MaskBit b) { fData = fData | b; }
    void Disable(MaskBit b) { fData = fData ^ b; }
  protected:
    unsigned short fData;
  };
  //____________________________________________________________________
  inline void
  RcuINT_TYPE::Print() const
  {
    RcuRegister::Print();
    std::cout << "\tMask:\t\t\t";
    if (fData & kT)    std::cout << "T ";
    if (fData & kAC)   std::cout << "AC ";
    if (fData & kAV)   std::cout << "AV ";
    if (fData & kDC)   std::cout << "DC ";
    if (fData & kDV)   std::cout << "DV ";
    if (fData & kPaps) std::cout << "PAPS ";
    if (fData & kAlps) std::cout << "ALPS ";
    if (fData & kSCLK) std::cout << "SCLK ";
    std::cout << std::endl;
  }
}
#endif
//
// Local Variables:
//   mode: C++
// End:
// 
