// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USAq
//
/** @file    RcuLayout.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat May 27 23:06:33 2006
    @brief   class that defines the register, command, and memory layout
*/
#ifndef RCUXX_RcuLayout
#define RCUXX_RcuLayout

namespace Rcuxx 
{
  struct RcuLayout 
  {
    /** Constructor 
	@param vers Optional version to use */
    RcuLayout(int vers=0);
    /** 
     * Convert the hardware version number to a date
     * 
     * @param hwvers Hardware version number
     * 
     * @return Date in the format 0xYYMMDD
     */
    static unsigned int Hw2Date(unsigned int hwvers);
    /** Decode version sent from hardware into enum values */
    int Decode(const unsigned int fw_version, unsigned int& decoded) const;
    /** 
     * Setup for a specific version 
     * 
     * @param vers Version number 
     * @param debug Debug flag
     */
    void Setup(int vers=0, bool debug=false);
    /** Print table to standard out */
    void Print() const;

    /** Versions of the firmware */
    enum {
      kDefault, // The default 
      kU2F,     // U2F test card  
      kAncient, // Before May 10, 2006
      k100506,  // Firmware version from May 10, 2006 
      k190606,  // Firmware version from June 19, 2006 
      k270606,  // Firmware version from June 27, 2006 
      k061106,  // Firmware version from November 6, 2006 
      k181206,  // Firmware version from December 12, 2006 
      k000408,  // RCU II firmware (up till and including june 10, 2006)
      k160608,  // Rcu II firmware from June 16, 2008
      k260210,  // RCU II firmware february 26, 2010
      k250510,  // RCU II firmware may 25, 2010
      k191114	// RCU II firmware november 19, 2014 - CDHv3 
    };
    /**
     * Setup for version Default
     *
     * @param debug True if debug messages needed
     */
    void SetupDefault(bool debug);
    /**
     * Setup for version U2F
     *
     * @param debug True if debug messages needed
     */
    void SetupU2F(bool debug);
    /**
     * Setup for version Ancient
     *
     * @param debug True if debug messages needed
     */
    void SetupAncient(bool debug);
    /**
     * Setup for version 20060510
     *
     * @param debug True if debug messages needed
     */
    void Setup20060510(bool debug);
    /**
     * Setup for version 20141119
     *
     * @param debug True if debug messages needed
     */
    void Setup20141119(bool debug);
    /**
     * Setup for version 20100525
     *
     * @param debug True if debug messages needed
     */
    void Setup20100525(bool debug);
    /**
     * Setup for version 20080616
     *
     * @param debug True if debug messages needed
     */
    void Setup20080616(bool debug);
    /**
     * Setup for version 20080400
     *
     * @param debug True if debug messages needed
     */
    void Setup20080400(bool debug);


    
    /** The version */ 
    unsigned int fVersion;
    
    /**@{
       @name Addresses of memories 
    */    
    /** Result memory (0x6000) */
    unsigned int kA_RMEM;
    /** Active Channel List (0x6400) */
    unsigned int kA_ACL;
    /** Pattern memory (0x6800) */
    unsigned int kA_PMEM;
    /** Instruction memory (0x7000) */
    unsigned int kA_IMEM;
    /** Data memory 1 - low bits (0x7400) */
    unsigned int kA_DML1;
    /** Data memory 1 - high bits (0x7500) */
    unsigned int kA_DMH1;
    /** Base address for registers (0x7800) */
    unsigned int kA_REGISTERS;
    /** Data memory 1 - low bits (0x7C00) */
    unsigned int kA_DML2;
    /** Data memory 2 - high bits (0x7D00) */
    unsigned int kA_DMH2;
    /** Data memory 2 - high bits (0xFD00) */
    unsigned int kA_DM2HH;
    /** Base address for monitor registers (0x8000/0x7800 for U2F) */ 
    unsigned int kA_MONITOR;
    /** Status memory (0x8100 - not on U2F) */
    unsigned int kA_STATUS;
    /** Event header words (0x4001 - not on U2F) */
    unsigned int kA_HEADER;
    /** TTCrx registers */
    unsigned int kA_TTCRX;
    /** hit list memory */
    unsigned int kA_HITLIST;

    unsigned int kM_IMEM;
    unsigned int kM_RMEM;
    unsigned int kM_ACL;
    unsigned int kA_CDH;
    /** @} */

    /** @{
	@name Sizes of memories 
    */
    /** Size of instruction memory (256) */
    unsigned int kS_IMEM;
    /** Size of pattern memory (1024) */
    unsigned int kS_PMEM;
    /** Size of pattern memory (256) */
    unsigned int kB_PMEM;
    /** Size of result memory (128) */
    unsigned int kS_RMEM;
    /** Size of active channel memory (256) */
    unsigned int kS_ACL;
    /** Size of partial data memory (256) */
    unsigned int kS_DM;
    /** Size of register memory (8/15 for U2F) */
    unsigned int kS_REGISTERS;
    /** Size of monitor memory (101/15 for U2F) */
    unsigned int kS_MONITOR;
    /** Size of header memory  (8 - not for U2F) */
    unsigned int kS_HEADER;
    /** Size of status memory (32 - not for U2F) */
    unsigned int kS_STATUS;
    /** TTCrx registers */
    unsigned int kS_TTCRX;
    /** hit list memory */
    unsigned int kS_HITLIST;
    unsigned int kS_CDH;
    /** @} */

    /** @{
	@name Registers 
	@ingroup rcuxx
    */
    /** Error and status (0) */
    unsigned int kO_ERRST;
    /** Trigger configuration (1) */
    unsigned int kO_TRCFG1;
    /** Trigger configuration (1) */
    unsigned int kO_TRGCONF;
    /** Trigger counter (2) */
    unsigned int kO_TRCNT;
    /** Last words in data buffers (3) */
    unsigned int kO_LWADD;
    /** Last instruction address (4) */
    unsigned int kO_IRADD;
    /** Last instruction data  (5) */
    unsigned int kO_IRDAT;
    /** Pedestal memory configuration (6 - Not for U2F) */
    unsigned int kO_PMCFG;
    /** Last channel address (7) */
    unsigned int kO_CHADD;
    /** Active FEC bit mask (0 - 7 for U2F) */
    unsigned int kO_ACTFEC;
    /** Read-out FEC bit mask (1 - not for U2F) */
    unsigned int kO_RDOFEC;
    /** Return BC register to DCS (2 - 11 for U2F) */
    unsigned int kO_RESREG;
    /** Error type register (3 - 12 for U2F) */
    unsigned int kO_ERRREG;
    /** Interrupt mask for branches (4 - for U2F) */
    unsigned int kO_INTMOD;
    /** Address to write slow control command to (5 - not u2f) */
    unsigned int kO_SCADD;
    /** Slow control command data (6 - not u2f) */
    unsigned int kO_SCDAT;
    /** Which interrupt bits are counted as hard */ 
    unsigned int kO_INT_TYPE;
    /** Status memory status */
    unsigned int kO_SMEM_STATUS;
    /** Active front-end cards for slow control (RCU II mostly) */
    unsigned int kO_SCAFL;
    /** Firmware version number (8) */
    unsigned int kO_FWVERS;
    /** Slow control I2C clock speed (RCU II only) */
    unsigned int kO_SCCLK;
    /** Interrupt information (100 - 14 for U2F) */
    unsigned int kO_INTREG;
    /** Event word register (6 - only U2F) */
    unsigned int kO_EVWORD;
    /** Firmware input register (8 - only U2F) */
    unsigned int kO_FMIREG;
    /** Firmware output register (9 - only U2F) */
    unsigned int kO_FMOREG;
    /** Trigger config 2 register  (10 - only U2F) */
    unsigned int kO_TRCFG2;

    /** RCU II specific - kO_ALTROIF */
    unsigned int kO_ALTROIF;
    /** RCU II specific - kO_RDOMOD */
    unsigned int kO_RDOMOD;
    /** RCU II specific - kO_ALTROCFG1 */
    unsigned int kO_ALTROCFG1;
    /** RCU II specific - kO_ALTROCFG2 */
    unsigned int kO_ALTROCFG2;
    /** RCU II specific - kO_FECERRA */
    unsigned int kO_FECERRA;
    /** RCU II specific - kO_FECERRB */
    unsigned int kO_FECERRB;
    /** RCU II specific - kO_RDOERR */
    unsigned int kO_RDOERR;
    /** RCU II specific - kO_ALTBUS_STATUS */
    unsigned int kO_ALTBUS_STATUS;
    /** RCU II specific - kO_ALTBUS_TRSF */
    unsigned int kO_ALTBUS_TRSF;
    /** RCU II specific - kO_RCUBUS */
    unsigned int kO_RCUBUS;
    /** RCU II specific - kO_SWTRCNT */
    unsigned int kO_SWTRCNT;
    /** RCU II specific - kO_AUXTRGCNT */
    unsigned int kO_AUXTRGCNT;
    /** RCU II specific - kO_TTCL2ACNT */
    unsigned int kO_TTCL2ACNT;
    /** RCU II specific - kO_TTCL2RCNT */
    unsigned int kO_TTCL2RCNT;
    /** Data strobe counter for branch A - kO_MEBSTCNT */
    unsigned int kO_MEBSTCNT;
    /** Data strobe counter for branch A - kO_DSTBACNT */
    unsigned int kO_DSTBACNT;
    /** Data strobe counter for branch B - kO_DSTBBCNT */
    unsigned int kO_DSTBBCNT;
    /** Transfer assert counter for branch A - kO_TRSFACNT */
    unsigned int kO_TRSFACNT;
    /** Transfer assert counter for branch B - kO_TRSFBCNT */
    unsigned int kO_TRSFBCNT;
    /** Acknowledge assert counter for branch A - kO_ACKACNT */
    unsigned int kO_ACKACNT;
    /** Acknowledge assert counter for branch B - kO_ACKBCNT */
    unsigned int kO_ACKBCNT;
    /** Control strobe counter for branch A - kO_CSTBACNT */
    unsigned int kO_CSTBACNT;
    /** Control strobe counter for branch B - kO_CSTBBCNT */
    unsigned int kO_CSTBBCNT;
    /** Last transfer data strobe counter for branch A - kO_DSTBNUMA */
    unsigned int kO_DSTBNUMA;
    /** Last transfer data strobe counter for branch B - kO_DSTBNUMB */
    unsigned int kO_DSTBNUMB;
    /** RCU II specific - kO_BPVERS */
    unsigned int kO_BPVERS;
    /** RCU II specific - RCU ID */
    unsigned int kO_RCUID;

    /** @{ 
	@name TTC interface */
    /** Trigger control register 0x4000 */
    unsigned int kO_TTCControl;
    /** TTC interface reset command 0x4001 */
    unsigned int kC_TTCReset;
    /** Region of interest configuration 0x4002 */
    unsigned int kO_ROIConfig1;
    /** Region of interest configuration 0x4003 */
    unsigned int kO_ROIConfig2;
    /** TTC interface reset counters command 0x4004 */
    unsigned int kC_TTCResetCnt;
    /** TTC interface test mode command 0x4005 */
    unsigned int kC_TTCTestmode;
    /** L1 timeout and window 0x4006 */
    unsigned int kO_L1Timeout;
    /** L2 time-out 0x4007 */
    unsigned int kO_L2Timeout;
    /** ROI message timeout 0x4009 */
    unsigned int kO_RoiTimeout;
    /** L1 message timeout 0x400A */
    unsigned int kO_L1MsgTimeout;
    /** Prepulser counter 0x400B */
    unsigned int kO_PrePulseCnt;
    /** Bunch-crossing counter 0x400C */
    unsigned int kO_BCId;
    /** L0 counter 0x400D */
    unsigned int kO_L0CNT;
    /** L1 counter 0x400E */
    unsigned int kO_L1CNT;
    /** L1 message counter 0x400F */
    unsigned int kO_L1MsgCNT;
    /** L2A counter 0x4010 */
    unsigned int kO_L2ACNT;
    /** L2R counter 0x4011 */
    unsigned int kO_L2RCNT;
    /** ROI counter 0x4012 */
    unsigned int kO_ROICNT;
    /** Hamming coding errors 0x4016 */
    unsigned int kO_TTCHammingCnt;
    /** Decode errors 0x4017 */
    unsigned int kO_TTCDecodeCnt;
    /** # of buffered events 0x4020 */
    unsigned int kO_BufferedCnt;
    /** Event information 0x4028 */
    unsigned int kO_TTCEventInfo;
    /** Event information 0x4029 */
    unsigned int kO_TTCEventError;
    /** @} */

    /** @{
	@name Commands 
	@ingroup rcuxx
    */
    /** ReSet STATUS command (0x6c01) */
    unsigned int kC_RS_STATUS;
    /** ReSet TRigger ConFiGuration command (0x6c02) */
    unsigned int kC_RS_TRCFG;
    /** ReSet TRigger CouNTer command (0x6c03) */
    unsigned int kC_RS_TRCNT;
    /** EXECute instructions command  (0x0000) */
    unsigned int kC_EXEC;
    /** ABORT instruction execution (0x0800) */
    unsigned int kC_ABORT;
    /** Front-End Card ReSeT command  (0x2001) */
    unsigned int kC_FECRST;
    /** SoftWare TRiGger command (0xD000) */
    unsigned int kC_SWTRG;
    /** ReSet DMEM1 command (0x6C04 - 0xD001 for U2F) */
    unsigned int kC_RS_DMEM1;
    /** ReSet DMEM2 command (0x6C05 - 0xD002 for U2F) */
    unsigned int kC_RS_DMEM2;
    /** DCS OwN bus command  (0xE000 - not U2F) */
    unsigned int kC_DCS_ON;
    /** DDL OwN bus comman (0xF000 - not U2F) */
    unsigned int kC_DDL_ON;
    /** L1-enable via TTC command (0xE800 - not U2F) */
    unsigned int kC_L1_TTC;
    /** L1-enable via I2C command (0xF800 - not U2F) */
    unsigned int kC_L1_I2C;
    /** L1-enable via CoMmanD command (0xD800 - not U2F) */
    unsigned int kC_L1_CMD;
    /** GLoBal RESET command (0x2000 - not U2F) */
    unsigned int kC_GLB_RESET;
    /** RCU RESET command (0x2002 - not U2F) */
    unsigned int kC_RCU_RESET;
    /** ReSet ERRor REGister (0x8010 - not U2F) */
    unsigned int kC_RS_ERRREG;
    /** ReSet RESult REGister (0x8012 - ot U2F) */
    unsigned int kC_RS_RESREG;
    /** Execute slow control command (0x8010 - not U2F)  */
    unsigned int kC_SCEXEC;
    /** Restore active card list */
    unsigned int kC_RST_AFL;
    /** Restore read-out card list */
    unsigned int kC_RST_RDO;
    /** Clear ready bits in status memory */
    unsigned int kC_RS_SMEM;
    /** TRiGger CLeaR command (0xD003 - only U2F) */
    unsigned int kC_TRG_CLR;
    /** WRite FirMware command (0xD004 - only U2F) */
    unsigned int kC_WRFM;
    /** ReaD FirMware command (0xD005 - only U2F) */
    unsigned int kC_RDFM;
    /** ReaD-out ABORT command (0xD006 - only U2F) */
    unsigned int kC_RDABORT;
    /** CLeaR EVent TAG command (0xD007 - only U2F) */
    unsigned int kC_CLR_EVTAG;
    /** Slow Control COMMAND (0xC000 - only U2F) */
    unsigned int kC_SCCOMMAND;
    /** RCU II clear RDRX error register */
    unsigned int kC_CLEARRDRXREG;
    /** RCU II Request interrupt */
    unsigned int kC_ARBITERIRQ;
    /** RCU II align clocks */
    unsigned int kC_ARM_ASYNCH;
    /** RCU II Configure front-end */
    unsigned int kC_CONFFEC;
    /** @} */

    /** @{ 
	@name State machines */
    /** Arbitratoru FSM state */
    unsigned int kO_ABDFSMST;
    /** readout FSM state */
    unsigned int kO_RDOFSMST;
    /** Instruction sequencer FSM state */
    unsigned int kO_INSSEQST;
    /** event mananger FSM state */
    unsigned int kO_EVMNGST;
    /** @} */

    /** RCU II specific - kIMEM_RCU_CMD */
    unsigned int kIMEM_RCU_CMD;
    /** RCU II specific - kIMEM_STOP */
    unsigned int kIMEM_STOP;
    /** RCU II specific - kIMEM_END */
    unsigned int kIMEM_END;
    /** RCU II specific - kIMEM_FEC_RD1 */
    unsigned int kIMEM_FEC_RD1;
    /** RCU II specific - kIMEM_FEC_WR1 */
    unsigned int kIMEM_FEC_WR1;
    /** RCU II specific - kIMEM_FEC_CMD1 */
    unsigned int kIMEM_FEC_CMD1;
    /** RCU II specific - kIMEM_FEC_RD2 */
    unsigned int kIMEM_FEC_RD2;
    /** RCU II specific - kIMEM_FEC_WR2 */
    unsigned int kIMEM_FEC_WR2;
    /** RCU II specific - kIMEM_FEC_CMD2 */
    unsigned int kIMEM_FEC_CMD2;
    /** RCU II specific - kRMEM_ERROR */
    unsigned int kRMEM_ERROR;
    /** RCU II specific - kRMEM_RCU_STOP */
    unsigned int kRMEM_RCU_STOP;
    /** RCU II specific - kRMEM_RCU_END */
    unsigned int kRMEM_RCU_END;
    /** RCU II specific - kRMEM_FEC_WR */
    unsigned int kRMEM_FEC_WR;
    /** RCU II specific - kRMEM_FEC_RD */
    unsigned int kRMEM_FEC_RD;
    /** RCU II specific - kRMEM_FEC_CMD */
    unsigned int kRMEM_FEC_CMD;

    bool fTriggerMapFlipped;
    bool fFullFMDD;

    enum {
      /** Value of address, size, offset, if the entity in question
	  isn't defined in the firmware */
      kInvalid = 0xFFFFFFFF
    };
  };
}

#endif
//
// EOF
//
