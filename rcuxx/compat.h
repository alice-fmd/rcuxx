// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/compat.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of compatibility macros
*/
#ifndef RCUXX_compat
# define RCUXX_compat
# ifdef HAVE_CONFIG_H
#  include "config.h"
# endif
# ifndef _WIN32
#  ifndef __UNISTD_H
#   include <unistd.h>
#  endif
/** @def USLEEP(X) 
    @param X Sleep for X @f$ \mu s@f$ (microseconds) */
#  define USLEEP(X) usleep(X)
/** @def SLEEP(X) 
    @param X Sleep for X seconds */
#  define SLEEP(X)  sleep(X)
/** @def VSNPRINTF(S,L,F,A)
    Print up to @a L characters to @a S based on format @a F and
    arguments @a A
    @param S String to print to. 
    @param L Maximum number of characters to write to @a S 
    @param F Format specifier 
    @param A List of arguments (a @c va_list) 
*/
#  define VSNPRINTF(S,L,F,A) vsnprintf(S,L,F,A)
/** @def SETENV(X,Y) 
    Set environment variable @a X to @a Y (two strings) 
    @param X Environment variable name 
    @param Y Value to set @a X to 
*/
#  define SETENV(X,Y) setenv(X,Y,1)
# else 
#  include <windows.h>
#  define USLEEP(X) Sleep(X*0.001)
#  define SLEEP(X)  Sleep(X*1000)
#  define VSNPRINTF(S,L,F,A) vsprintf(S,F,A)
// SetEnvironmentVariable returns a bool (true == 1 == success), while
// setenv returns an int (0 == success)
#  define SETENV(X,Y) !SetEnvironmentVariable(X,Y)
#  ifdef min 
#   undef min 
#  endif
#  ifdef max
#   undef max
#  endif
#  include <algorithm>
# endif
#endif
//
// EOF
//

