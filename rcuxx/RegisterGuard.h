// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUXX_REGISTER_GUARD
#define RCUXX_REGISTER_GUARD
#ifndef RCUXX_REGISTER
# include <rcuxx/Register.h>
#endif


namespace Rcuxx 
{
  /**
   * Objects of this class stores the content of a register upon
   * construction, and then restore the content of the register on
   * destruction.
   * 
   * A use case is if one needs to change the register temporarily 
   * to do something, and then needs to restore the value afterwards. 
   *  
   * Since both the constructor and destructor throws, it is best
   * executed try-catch block.
   *
   * @code
   try { 
     RegisterGuard g(r);
     ... 
     // do something temporary to r 
   }
   catch (std::exception& e) {
     std::cerr << e.what() << std::endl;
   }
   @endcode 
   * 
   * This will ensure proper roll-back of all registers guarded so
   * far. 
   */
  struct RegisterGuard 
  {
    /**
     * Constructor. Caches the current value of the register.  If @a
     * verify is true, then the register value is first read-back from
     * the hardware.
     *
     * @note: This throws an exception in case of errors
     *
     * @param r Pointer to register 
     * @param debug If true, show some debug information 
     * @param verify If true, update before caching, and on
     * destruction, also verify that the cached value was written to
     * the hardware.
     *
     * @exception std::runtime_error 
     */ 
    RegisterGuard(Register* r, bool verify=true, bool debug=false);
    /** 
     * Destructor.  Restores the cached value to the hardware.  If
     * debugging was turned on at construction, show some messages.
     * If verification was turned on at construction, also verify that
     * the value was written to hardware. 
     * 
     * @note: This throws an exception in case of errors
     *
     * @exception std::runtime_error 
     */
    ~RegisterGuard();
    /** Out register */
    Register::Guard* fGuard; 
  };

}

#endif
// 
// EOF
//
