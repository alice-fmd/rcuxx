//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Bc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:16 2006
    @brief   Implementation of Board Controller interface 
*/
#include <rcuxx/Bc.h>
#include "compat.h"
#include <iostream>
#include <cmath>
#include "DebugGuard.h"
#include "bc/BcAC.h"
#include "bc/BcAC_TH.h"
#include "bc/BcAV.h"
#include "bc/BcAV_TH.h"
#include "bc/BcCommand.h"
#include "bc/BcCSR0.h"
#include "bc/BcCSR1.h"
#include "bc/BcCSR2.h"
#include "bc/BcCSR3.h"
#include "bc/BcDC.h"
#include "bc/BcDC_TH.h"
#include "bc/BcDSTBCNT.h"
#include "bc/BcDV.h"
#include "bc/BcDV_TH.h"
#include "bc/BcL1CNT.h"
#include "bc/BcL2CNT.h"
#include "bc/BcMonitored.h"
#include "bc/BcRegister.h"
#include "bc/BcSCLKCNT.h"
#include "bc/BcTEMP.h"
#include "bc/BcTEMP_TH.h"
#include "bc/BcThreshold.h"
#include "bc/BcTSMWORD.h"
#include "bc/BcUSRATIO.h"
#include "bc/BcVersion.h"

namespace 
{
  bool fDebug = false;
}
//====================================================================
const float Rcuxx::BcAV::fgkCounts2mV   = 4.43;
const float Rcuxx::BcAC::fgkCounts2mA   = 17;
const float Rcuxx::BcDV::fgkCounts2mV   = 4.43;
const float Rcuxx::BcDC::fgkCounts2mA   = 30;

//====================================================================
void
Rcuxx::BcMonitored::USleep(unsigned int usecs)
{
  USLEEP(usecs);
}

//====================================================================
Rcuxx::Bc::Bc(Rcuxx::Rcu& rcu) 
  : fRcu(rcu), 
    fBoardAddress(0), 
    fCNTLAT(0), fCNTCLR(0), fCSR1CLR(0), fALRST(0), fBCRST(0), fSTCNV(0), 
    fSCEVL(0), fEVLRDO(0), fSTTSM(0), fACQRDO(0), 
    fCSR0(0), fCSR1(0), fCSR2(0), fCSR3(0), fVersion(0),
    fTEMP_TH(0), fAV_TH(0), fAC_TH(0), fDV_TH(0), fDC_TH(0), 
    fTEMP(0), fAV(0), fAC(0), fDV(0), fDC(0), 
    fL1CNT(0), fL2CNT(0), fSCLKCNT(0), fDSTBCNT(0), fTSMWORD(0), fUSRATIO(0)
{
  RcuIMEM& imem = *rcu.IMEM();
  RcuRMEM& rmem = *rcu.RMEM();
  
  // Commands 
  fCNTLAT   = new BcCommand("CNTLAT", "Latch counters",         0x16, rcu);
  fCNTCLR   = new BcCommand("CNTCLR", "Clear counters",         0x17, rcu);
  fCSR1CLR  = new BcCommand("CSR1CLR","Clear errors/intr.",     0x18, rcu);
  fALRST    = new BcCommand("ALRST",  "Reset ALTRO's",          0x19, rcu);
  fBCRST    = new BcCommand("BCRST",  "Reset",                  0x1A, rcu);
  fSTCNV    = new BcCommand("STCNV",  "Start conversion",       0x1B, rcu);
  fSCEVL    = new BcCommand("SCEVL",  "Scan event length",      0x1C, rcu);
  fEVLRDO   = new BcCommand("EVLRDO", "Read event length",      0x1D, rcu, 
			    false);
  fSTTSM    = new BcCommand("STTSM",  "Start test mode",        0x1E, rcu);
  fACQRDO   = new BcCommand("ACQRDO", "Read acquisition memory",0x1F, rcu, 
			    false);
  fCommands.push_back(fCNTLAT);
  fCommands.push_back(fCNTCLR);
  fCommands.push_back(fCSR1CLR);
  fCommands.push_back(fALRST);
  fCommands.push_back(fBCRST);
  fCommands.push_back(fSTCNV);
  fCommands.push_back(fSCEVL);
  fCommands.push_back(fEVLRDO);
  fCommands.push_back(fSTTSM);
  fCommands.push_back(fACQRDO);

  // Status 
  fCSR0     = new BcCSR0(rcu);
  fCSR1     = new BcCSR1(rcu, fCSR1CLR);
  fCSR2     = new BcCSR2(rcu);
  fCSR3     = new BcCSR3(rcu);
  fVersion  = new BcVersion(rcu);
  fRegisters.push_back(fCSR0);
  fRegisters.push_back(fCSR1);
  fRegisters.push_back(fCSR2);
  fRegisters.push_back(fCSR3);
  fRegisters.push_back(fVersion);

  // Thresholds
  fTEMP_TH  = new BcTEMP_TH(rcu);
  fAV_TH    = new BcAV_TH(rcu);
  fAC_TH    = new BcAC_TH(rcu);
  fDV_TH    = new BcDV_TH(rcu);
  fDC_TH    = new BcDC_TH(rcu);
  fRegisters.push_back(fTEMP_TH);
  fRegisters.push_back(fAV_TH);
  fRegisters.push_back(fAC_TH);
  fRegisters.push_back(fDV_TH);
  fRegisters.push_back(fDC_TH);

  // Monitor values 
  fTEMP     = new BcTEMP(rcu, *fCSR3, *fSTCNV);
  fAV       = new BcAV(rcu, *fCSR3, *fSTCNV);
  fAC       = new BcAC(rcu, *fCSR3, *fSTCNV);
  fDV       = new BcDV(rcu, *fCSR3, *fSTCNV);
  fDC       = new BcDC(rcu, *fCSR3, *fSTCNV);
  fRegisters.push_back(fTEMP);
  fRegisters.push_back(fAV);
  fRegisters.push_back(fAC);
  fRegisters.push_back(fDV);
  fRegisters.push_back(fDC);

  // Counters 
  fL1CNT    = new BcL1CNT(rcu);
  fL2CNT    = new BcL2CNT(rcu);
  fSCLKCNT  = new BcSCLKCNT(rcu);
  fDSTBCNT  = new BcDSTBCNT(rcu);
  fTSMWORD  = new BcTSMWORD(rcu);
  fUSRATIO  = new BcUSRATIO(rcu);
  fRegisters.push_back(fL1CNT);
  fRegisters.push_back(fL2CNT);
  fRegisters.push_back(fSCLKCNT);
  fRegisters.push_back(fDSTBCNT);
  fRegisters.push_back(fTSMWORD);
  fRegisters.push_back(fUSRATIO);

  // Set initial address 
  SetAddress(0);
}

//____________________________________________________________________
Rcuxx::Bc::~Bc() 
{
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i) 
    if (*i) delete *i;
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i) 
    if (*i) delete *i;
#if 0
  if (fTEMP_TH)	delete fTEMP_TH;
  if (fAV_TH)	delete fAV_TH;
  if (fAC_TH)	delete fAC_TH;
  if (fDV_TH)	delete fDV_TH;
  if (fDC_TH)	delete fDC_TH;
  if (fTEMP)	delete fTEMP;
  if (fAV)	delete fAV;
  if (fAC)	delete fAC;
  if (fDV)	delete fDV;
  if (fDC)	delete fDC;
  if (fL1CNT)	delete fL1CNT;
  if (fL2CNT)	delete fL2CNT;
  if (fSCLKCNT)	delete fSCLKCNT;
  if (fDSTBCNT)	delete fDSTBCNT;
  if (fTSMWORD)	delete fTSMWORD;
  if (fUSRATIO)	delete fUSRATIO;
  if (fCSR0)	delete fCSR0;
  if (fCSR1)	delete fCSR1;
  if (fCSR2)	delete fCSR2;
  if (fCSR3)	delete fCSR3;
  if (fVersion) delete fVersion;

  if (fCNTLAT)	delete fCNTLAT;
  if (fALRST)	delete fALRST;
  if (fBCRST)	delete fBCRST;
  if (fSTCNV)	delete fSTCNV;
  if (fSCEVL)	delete fSCEVL;
  if (fEVLRDO)	delete fEVLRDO;
  if (fSTTSM)	delete fSTTSM;
  if (fACQRDO)	delete fACQRDO;  
#endif
}

//____________________________________________________________________
void 
Rcuxx::Bc::SetBroadcast() 
{
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetBroadcast();
  }
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetBroadcast();
  }
  fBoardAddress = -1;
}

//____________________________________________________________________
void
Rcuxx::Bc::SetAddress(unsigned int board) 
{
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetAddress(board, 0, 0);
  }
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetAddress(board, 0, 0);
  }
  fBoardAddress = board;
}

//____________________________________________________________________
void 
Rcuxx::Bc::SetUseI2C(bool use)
{
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetUseI2C(use);
  }
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i){
    if (!(*i)) continue;
    (*i)->SetUseI2C(use);
  }
  fUseI2C = use;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Bc::Commit()
{
  unsigned int ret;
  size_t j = 0;
  for (size_t i = 0; i < fRegisters.size(); i++) {
    if (!fRegisters[i]) continue;
    fRegisters[i]->Set();
    if (fRegisters[i]->WriteInstructions()) j += 2;
  }
  ret     = fRcu.IMEM()->Execute();
  return ret;
}

//____________________________________________________________________
unsigned int
Rcuxx::Bc::Update() 
{
  unsigned int ret;
  for (Registers::iterator i = fRegisters.begin(); i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    ret = (*i)->Update();
    if (ret) break;
  }
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Bc::Print() const
{
  for (Registers::const_iterator i = fRegisters.begin(); 
       i != fRegisters.end(); ++i){
    if (!(*i)) continue;
    (*i)->Print();
  }
}

//____________________________________________________________________
void
Rcuxx::Bc::SetDebug(bool debug)
{
  fDebug = debug;
}

//____________________________________________________________________
//
// EOF
//
