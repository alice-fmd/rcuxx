// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCTHRESHOLD_H
#define RCUXX_BCTHRESHOLD_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcThreshold rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller threshold registers
   *  @ingroup rcuxx_bc
   */
  struct BcThreshold : public BcRegister
  {
    /** Constructor 
        @param name Name
	@param title Title
        @param addr Address
	@param rcu Reference to RCU interface */
    BcThreshold(const char* name, const char* title, size_t addr, Rcu& rcu) 
      : BcRegister(name, title, addr, rcu, false, true)
    {}
    virtual float Convert2Natural(unsigned int adc) const = 0;
    virtual unsigned int Convert2ADC(float v) const = 0;
    /** @return threshold value in ADC */
    unsigned int Threshold() const { return fCnt; }
    /** @return threshold value in natural units */
    float ThresholdNatural() const { return Convert2Natural(fData); }
    /** Set threshold value in ADC 
	@param val Threshold */
    void SetThreshold(unsigned int val) { fCnt = val; }
    /** Set threshold value in natural units
	@param val Threshold in natural units */
    void SetThreshold(float val) { fCnt = Convert2ADC(val); }
    unsigned int Encode() const { return (fCnt & 0x3FF); }
    void Decode(unsigned int cnt) { fCnt = (cnt & 0x3FF); }
    /** @return Unit string */
    virtual const char* Unit() const { return ""; }
    virtual void Print() const 
    {
      BcRegister::Print();
      std::cout << "\tThreshold:\t\t\t" << ThresholdNatural()
		<< Unit() << std::endl;
    }
    unsigned int fCnt;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
