// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCCSR2_H
#define RCUXX_BCCSR2_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //____________________________________________________________________
  /** @struct BcCSR2 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR2 - test mode,
   *  clock enables, and power supply enables. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR2 : public BcRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    BcCSR2(Rcu& rcu /*RcuImem& imem, RcuRMEM& rmem*/) 
      : BcRegister("CSR2", "Test and enables", 0x13, rcu /*imem, rmem*/, false, true),
	fHADD(0),
	fCardIsolated(false),
	fContinousTSM(false),
	fALTROAddress(0),
	fADCAddress(0),
	fAdcClock(false),
	fRdoClock(false),
	fPASASwitch(false),
	fALTROSwitch(false)
    {}
    /** Decode read data */
    void Decode(unsigned int data)
    {
      fALTROSwitch	= (data & (1 << 0));
      fPASASwitch	= (data & (1 << 1));
      fAdcClock		= (data & (1 << 2));
      fRdoClock		= (data & (1 << 3));
      fADCAddress	= (data >> 4) & 0x3;
      fALTROAddress	= (data >> 6) & 0x7;
      fContinousTSM	= (data & (1 << 9));
      fCardIsolated	= (data & (1 << 10));
      fHADD		= (data >> 11) & 0x1f;
    }

    /** Encode set data before committing */
    unsigned int Encode() const
    {
      unsigned int data = 0;
      data += (fALTROSwitch  ? (1 << 0)	: 0);
      data += (fPASASwitch   ? (1 << 1)	: 0);
      data += (fAdcClock     ? (1 << 2)	: 0);
      data += (fRdoClock     ? (1 << 3)	: 0);
      data += (fADCAddress   & 0x3) << 4;
      data += (fALTROAddress & 0x7) << 6;
      data += (fContinousTSM ? (1 << 9)	: 0);
      data += (fCardIsolated ? (1 << 10): 0);
      return data;
    }  

    /** @return HADD */
    unsigned HADD() const { return fHADD; }
    /** @return if CardIsolated */
    bool IsCardIsolated() const { return fCardIsolated; }
    /** Set CardIsolated on or off */
    void SetCardIsolated(bool on=true) { fCardIsolated = on; }
    /** @return if ContinousTSM */
    bool IsContinousTSM() const { return fContinousTSM; }
    /** Set ContinousTSM on or off */
    void SetContinousTSM(bool on=true) { fContinousTSM = on; }
    /** @return ALTROAddress */
    unsigned ALTROAddress() const { return fALTROAddress; }
    /** Set ALTROAddress to @a val */
    void SetALTROAddress(unsigned int val) { fALTROAddress = val; }
    /** @return ADCAddress */
    unsigned ADCAddress() const { return fADCAddress; }
    /** Set ADCAddress to @a val */
    void SetADCAddress(unsigned int val) { fADCAddress = val; }
    /** @return if AdcClock */
    bool IsAdcClock() const { return fAdcClock; }
    /** Set AdcClock on or off */
    void SetAdcClock(bool on=true) { fAdcClock = on; }
    /** @return if RdoClock */
    bool IsRdoClock() const { return fRdoClock; }
    /** Set RdoClock on or off */
    void SetRdoClock(bool on=true) { fRdoClock = on; }
    /** @return if PASASwitch */
    bool IsPASASwitch() const { return fPASASwitch; }
    /** Set PASASwitch on or off */
    void SetPASASwitch(bool on=true) { fPASASwitch = on; }
    /** @return if ALTROSwitch */
    bool IsALTROSwitch() const { return fALTROSwitch; }
    /** Set ALTROSwitch on or off */
    void SetALTROSwitch(bool on=true) { fALTROSwitch = on; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha 
		<< "\tHardware address:\t\t0x" << std::hex << fHADD << "\n" 
		<< "\tIsolated:\t\t\t" << fCardIsolated << "\n" 
		<< "\tTest mode:\t\t\t" << fContinousTSM << "\n" 
		<< "\tALTRO address:\t\t\t0x" << fADCAddress << std::dec << "\n" 
		<< "\tADC clock enabled:\t\t" << fAdcClock << "\n" 
		<< "\tRead-out clock enabled:\t\t" << fRdoClock << "\n" 
		<< "\tPASA enabled:\t\t\t" << fPASASwitch << "\n" 
		<< "\tALTRO enabled:\t\t\t" << fALTROSwitch << std::endl; 
    }

  protected:
    /** Hard-wired address */
    unsigned int  fHADD;
    /** Whether card is isolated in test mode */
    bool          fCardIsolated;
    /** Whether to do continues tests */
    bool          fContinousTSM;
    /** Altro to read */
    unsigned int  fALTROAddress;
    /** ADC channel to read  */
    unsigned int  fADCAddress;
    /** Enable ADC (monitor) clock */
    bool          fAdcClock;
    /** Enable Read-out clock */
    bool          fRdoClock;
    /** Enable PASA power supply */
    bool          fPASASwitch;
    /** Enable ALTRO power supply */
    bool          fALTROSwitch;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
