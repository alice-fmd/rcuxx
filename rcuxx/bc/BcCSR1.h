// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCCSR1_H
#define RCUXX_BCCSR1_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcCSR1 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR1
   *  CSR1 holds the actual interrupts and errors as well as two bits
   *  signaling if an interrupt and/or error was seen. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR1 : public BcRegister
  {
    /** Constructor 
	@param rcu Reference to RCU interface 
	@param clear Clear registers */
    BcCSR1(Rcu& rcu /*RcuImem& imem, RcuRMEM& rmem*/, AltroCommand* clear) 
      : BcRegister("CSR1", "Errors/Interrupts", 0x12, rcu /*imem, rmem*/, true, false), 
	fClear(clear),
	fParity(false),
	fInstruction(false),
	fMissedSclk(false),
	fAlps(false),
	fPaps(false),
	fDcOverTh(false),
	fDvOverTh(false),
	fAcOverTh(false),
	fAvOverTh(false),
	fTempOverTh(false),
	fAltroError(false),
	fSlowControl(false),
	fError(false),
	fInterrupt(false)
    {}
    /** Decode read data */
    void Decode(unsigned int data)
    {
      fTempOverTh	= data & (1 << 0);
      fAvOverTh		= data & (1 << 1);
      fAcOverTh		= data & (1 << 2);
      fDvOverTh		= data & (1 << 3);
      fDcOverTh		= data & (1 << 4);
      fPaps		= data & (1 << 5);
      fAlps		= data & (1 << 6);
      fMissedSclk	= data & (1 << 7);
      fParity		= data & (1 << 8);
      fInstruction	= data & (1 << 9);
      fAltroError	= data & (1 << 10);
      fSlowControl	= data & (1 << 11);
      fError		= data & (1 << 12);
      fInterrupt	= data & (1 << 13);
    }

    unsigned int Clear() {  return fClear->Commit(); }
    /** @return If a parity error interrupt is set */
    bool IsParity() const { return fParity; } 
    /** @return If a instruction error interrupt is set */
    bool IsInstruction() const { return fInstruction; } 
    /** @return If a missed sample clocks interrupt is set */
    bool IsMissedSclk() const { return fMissedSclk; } 
    /** @return If a ALTRO power supply error interrupt is set */
    bool IsAlps() const { return fAlps; } 
    /** @return If a PASA power supply error interrupt is set */
    bool IsPaps() const { return fPaps; } 
    /** @return If a digital current over threshold interrupt is set */
    bool IsDcOverTh() const { return fDcOverTh; } 
    /** @return If a digital voltage over threshold interrupt is set */
    bool IsDvOverTh() const { return fDvOverTh; } 
    /** @return If a analog current over threshold interrupt is set */
    bool IsAcOverTh() const { return fAcOverTh; } 
    /** @return If a analog voltage over threshold interrupt is set */
    bool IsAvOverTh() const { return fAvOverTh; } 
    /** @return If a temperture over threshold interrupt is set */
    bool IsTempOverTh() const { return fTempOverTh; } 
    /** @return If a ALTRO instruction error  interrupt is set */
    bool IsAltroError() const { return fAltroError; } 
    /** @return If a slow control error  interrupt is set */
    bool IsSlowControl() const { return fSlowControl; } 
    /** @return If an error is set */
    bool IsError() const { return fError; } 
    /** @return If an interrupt is set */
    bool IsInterrupt() const { return fInterrupt; } 
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha 
		<< "\tInstruction error:\t\t" << fAltroError << "\n" 
		<< "\tSlow control error:\t\t" << fSlowControl << "\n" 
		<< "\tError:\t\t\t\t" << fError << std::endl;
      if (fParity)		std::cout << "\t\tParity error\n";
      if (fInstruction)	std::cout << "\t\tInstruction error\n";
      if (fMissedSclk)	std::cout << "\t\tMissed sample clocks\n";
      if (fAlps)		std::cout << "\t\tALTRO power supply error\n";
      if (fPaps)		std::cout << "\t\tPASA power supply error\n";
      std::cout << "\tInterrupt:\t\t\t" << fInterrupt << std::endl;
      if (fDcOverTh)	std::cout << "\t\tDigital current over threshold\n";
      if (fDvOverTh)	std::cout << "\t\tDigital voltage over threshold\n";
      if (fAcOverTh)	std::cout << "\t\tAnalog current over threshold\n";
      if (fAvOverTh)	std::cout << "\t\tAnalog voltage over threshold\n";
      if (fTempOverTh)	std::cout << "\t\tTemperture over threshold\n";
      std::cout << std::flush;
    }

  protected:    
    /** Parity - Parity error */
    bool fParity;
    /** Instr - Instruction error */
    bool fInstruction;
    /** SCLK - Missed sample clocks */
    bool fMissedSclk;
    /** Alps - ALTRO power supply error */
    bool fAlps;
    /** Paps - PASA power supply error */
    bool fPaps;
    /** DC - Digital current over threshold */
    bool fDcOverTh;
    /** DV - Digital voltage over threshold */
    bool fDvOverTh;
    /** AC - Analog current over threshold */
    bool fAcOverTh;
    /** AV - Analog voltage over threshold */
    bool fAvOverTh;
    /** T - Temperture over threshold */
    bool fTempOverTh;
    /** ALTRO instruction error  */
    bool fAltroError;
    /** Slow control error  */
    bool fSlowControl;
    /** Have an error  */
    bool fError;
    /** Have an interrupt */
    bool fInterrupt;
    /** Clear command */ 
    AltroCommand* fClear;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
