// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCCOMMAND_H
#define RCUXX_BCCOMMAND_H
#include <rcuxx/altro/AltroCommand.h>
# include <rcuxx/rcu/RcuSCADD.h>
# include <rcuxx/rcu/RcuSCDAT.h>
# include <rcuxx/rcu/RcuERRREG.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcCommand rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Base class for interfaces to Board Controller commands
   *  @ingroup rcuxx_bc
   */
  struct BcCommand : public AltroCommand
  {
    /** Constructor 
	@param name Name of command 
	@param title Help string
	@param cmd Command number 
	@param rcu Reference to RCU interface 
	@param bcast Whether this command can be broadcasted */
    BcCommand(const char* name, const char* title, unsigned int cmd, 
	      Rcu& rcu, bool bcast=true)
      : AltroCommand(name, title, cmd, rcu, bcast), 
	fUseI2C(false)
    {
      fCmd += (1 << 17);
    }
    /** Set whether to use the @f$ I^2C@f$ bus or not 
	@param use If true, use the I2C bus */
    virtual void SetUseI2C(bool use=true) { fUseI2C = use; }
    /** Whether we're using the  @f$ I^2C@f$ bus 
	@return @c true if we're using the I2C bus. */
    virtual bool UseI2C() const { return fUseI2C; }
    /** Commit stored values to hardware 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { 
      if (fUseI2C) return I2CCommit();
      return AltroCommand::Commit();
    }
    /** Commit stored values to hardware using the @f$ I^2@f$ bus 
	@return 0 on success, error code otherwise */
    virtual unsigned int I2CCommit() 
    {
      unsigned int ret = 0;
      if ((ret = fRcu.RS_ERRREG()->Commit())) return ret;
      unsigned int target = ((fAddress >>  7) & 0x1F);
      unsigned int bcast  = ((fAddress >> 18) & 0x1);
      fRcu.SCADD()->SetAddress((bcast << 13)          | // Broadcast 
			       (target << 8)          | // Fec
			       (fCmd & 0xFF));      // Instr.
      fRcu.SCDAT()->SetData(0);
      if ((ret = fRcu.SCADD()->Commit()))  return ret;
      if ((ret = fRcu.SCDAT()->Commit()))  return ret;
      if ((ret = fRcu.SCEXEC()->Commit())) return ret;
      if ((ret = fRcu.ERRREG()->Update())) return ret;
      if (fRcu.ERRREG()->IsNoAcknowledge()) ret = Rcu::kNoAcknowledge;
      if (fRcu.ERRREG()->IsNotActive())     ret = Rcu::kNotActive;
      if (ret) { 
	fRcu.RS_ERRREG()->Commit();
	return ret;
      }
      return ret;
    }
  protected:
    /** Whether to use the @f$ I^2C@f$ bus */
    bool fUseI2C;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
