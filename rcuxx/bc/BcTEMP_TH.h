// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCTEMP_TH_H
#define RCUXX_BCTEMP_TH_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcTEMP_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register TEMP_TH - the
   *  temperature threshold 
   *  @ingroup rcuxx_bc
   */
  struct BcTEMP_TH : public BcThreshold
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    BcTEMP_TH(Rcu& rcu) 
      : BcThreshold("TEMP_TH", "Temperatur threshold", 0x1, rcu)
    {}
    float Convert2Natural(unsigned int c) const 
    { 
      return 0.25 * c - (c >= 512 ? 256 : 0);
    }
    unsigned int Convert2ADC(float v) const 
    { 
      return unsigned((v < 0 ? v+256 : v) * 4);
    }
    /** Set threshold in mili amps
	@param v Threshold */
    void SetCentigrade(float v) { SetThreshold(v); }
    /** @return Get current human-readable value */
    float Centigrade() const { return ThresholdNatural(); }
    /** @return Unit */
    const char* Unit() const { return "C"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
