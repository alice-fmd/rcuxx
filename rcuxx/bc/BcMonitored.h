// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCMONITORED_H
#define RCUXX_BCMONITORED_H
#include <rcuxx/bc/BcRegister.h>
#include <rcuxx/bc/BcCSR3.h>
#include <rcuxx/altro/AltroCommand.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcMonitored rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Base class for interfaces to Board Controller monitor
   *  register  
   *  @ingroup rcuxx_bc
   */
  struct BcMonitored : public BcRegister
  {
    /** Reference to start command */
    AltroCommand& fStart;
    /** Referene to Control/Status Register 3 */
    BcCSR3&       fCsr3;
    /** Constructor 
	@param name  Name of register 
	@param title Help string 
	@param addr  Address of register 
	@param rcu Reference to RCU interface 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcMonitored(const char* name, const char* title, int addr, 
		Rcu& rcu, BcCSR3& csr3, AltroCommand& start)
      : BcRegister(name, title, addr, rcu, false, false), 
	fStart(start), 
	fCsr3(csr3)
    {}
    /** Start monitoring 
	@param tries Number of times to try (1 @f$ \mu s@f$ sleep
	between each try).
	@return @c true on success, @c false in case of time-out or
	other error  */
    bool Monitor(int tries=10)
    {
      fStart.Commit();
      int i = 0;
      for (i = 0; i < tries; i++) {
	USleep(1);
	fCsr3.Update();
	if (fCsr3.IsCnvEnd()) break;
      }
      if (i == tries) return false;
      Update();
      return true;
    }
    void USleep(unsigned int usecs);
    virtual float Convert2Natural(unsigned int adc) const = 0;
    /** @return Get current value (ADC counts) */
    unsigned int Current() const { return fData; }
    /** @return Get current value (natural units) */
    float CurrentNatural() const { return Convert2Natural(fData); }
    /** @return Unit string */
    virtual const char* Unit() const { return ""; }
    /** Decode read data */
    virtual void Decode(unsigned int data) { fData = data & 0x3ff;  }
    virtual void Print() const
    {
      BcRegister::Print();
      std::cout << "\tValue:\t\t\t\t" << CurrentNatural() << Unit() 
		<< std::endl;
    }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
