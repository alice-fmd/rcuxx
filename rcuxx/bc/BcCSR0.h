// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCCSR0_H
#define RCUXX_BCCSR0_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcCSR0 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR0. 
   *  CSR0 controls the interrupt and error mask - that is, the
   *  interrupts and errors from the BC that signals an error to the
   *  RCU.   Also, the 10th bit or CSR0 controls whether to do
   *  continuous monitoring or voltages, currents, and temperature. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR0 : public BcRegister
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    BcCSR0(Rcu& rcu) 
      : BcRegister("CSR0", "Error/Interrupt mask", 0x11, rcu, false, true),
	fParity(false),
	fInstruction(false),
	fMissedSclk(false),
	fAlps(false),
	fPaps(false),
	fDcOverTh(false),
	fDvOverTh(false),
	fAcOverTh(false),
	fAvOverTh(false),
	fTempOverTh(false),
	fCnv(false)
    {}
    /** Decode read data */
    void Decode(unsigned int data)
    {
      fTempOverTh	= data & (1 << 0);
      fAvOverTh		= data & (1 << 1);
      fAcOverTh		= data & (1 << 2);
      fDvOverTh		= data & (1 << 3);
      fDcOverTh		= data & (1 << 4);
      fPaps		= data & (1 << 5);
      fAlps		= data & (1 << 6);
      fMissedSclk	= data & (1 << 7);
      fParity		= data & (1 << 8);
      fInstruction	= data & (1 << 9);
      fCnv		= data & (1 << 10);
    }

    /** Encode set data before committing */
    unsigned int Encode() const
    {
      unsigned int data =  0;
      data += (fTempOverTh	? (1 << 0)	: 0);
      data += (fAvOverTh	? (1 << 1)	: 0);
      data += (fAcOverTh	? (1 << 2)	: 0);
      data += (fDvOverTh	? (1 << 3)	: 0);
      data += (fDcOverTh	? (1 << 4)	: 0);
      data += (fPaps		? (1 << 5)	: 0);
      data += (fAlps		? (1 << 6)	: 0);
      data += (fMissedSclk	? (1 << 7)	: 0);
      data += (fParity		? (1 << 8)	: 0);
      data += (fInstruction	? (1 << 9)	: 0);
      data += (fCnv		? (1 << 10)	: 0);
      return data;
    }

    /** @return If parity error interrupts are enabled */
    bool IsParity() const { return fParity; } 
    /** Enable/dispable parity error interrupts */
    void SetParity(bool on=true) { fParity = on; }
    /** @return If instruction error interrupts are enabled */
    bool IsInstruction() const { return fInstruction; } 
    /** Enable/dispable instruction error interrupts */
    void SetInstruction(bool on=true) { fInstruction = on; }
    /** @return If missed sample clocks interrupts are enabled */
    bool IsMissedSclk() const { return fMissedSclk; } 
    /** Enable/dispable missed sample clocks interrupts */
    void SetMissedSclk(bool on=true) { fMissedSclk = on; }
    /** @return If ALTRO power supply error interrupts are enabled */
    bool IsAlps() const { return fAlps; } 
    /** Enable/dispable ALTRO power supply error interrupts */
    void SetAlps(bool on=true) { fAlps = on; }
    /** @return If PASA power supply error interrupts are enabled */
    bool IsPaps() const { return fPaps; } 
    /** Enable/dispable PASA power supply error interrupts */
    void SetPaps(bool on=true) { fPaps = on; }
    /** @return If digital current over threshold interrupts are enabled */
    bool IsDcOverTh() const { return fDcOverTh; } 
    /** Enable/dispable digital current over threshold interrupts */
    void SetDcOverTh(bool on=true) { fDcOverTh = on; }
    /** @return If digital voltage over threshold interrupts are enabled */
    bool IsDvOverTh() const { return fDvOverTh; } 
    /** Enable/dispable digital voltage over threshold interrupts */
    void SetDvOverTh(bool on=true) { fDvOverTh = on; }
    /** @return If analog current over threshold interrupts are enabled */
    bool IsAcOverTh() const { return fAcOverTh; } 
    /** Enable/dispable analog current over threshold interrupts */
    void SetAcOverTh(bool on=true) { fAcOverTh = on; }
    /** @return If analog voltage over threshold interrupts are enabled */
    bool IsAvOverTh() const { return fAvOverTh; } 
    /** Enable/dispable analog voltage over threshold interrupts */
    void SetAvOverTh(bool on=true) { fAvOverTh = on; }
    /** @return If temperture over threshold interrupts are enabled */
    bool IsTempOverTh() const { return fTempOverTh; } 
    /** Enable/dispable temperture over threshold interrupts */
    void SetTempOverTh(bool on=true) { fTempOverTh = on; }
    /** @return If continues ADC conversion is enabled */
    bool IsCnv() const { return fCnv; } 
    /** Enable/dispable continues ADC conversion  */
    void SetCnv(bool on=true) { fCnv = on; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha 
		<< "\tContinous conversion:\t\t" << fCnv << "\n"
		<< "\tInterrupts enabled:" << std::endl;
      if (fParity)		std::cout << "\t\tParity error\n";
      if (fInstruction)	std::cout << "\t\tInstruction error\n";
      if (fMissedSclk)	std::cout << "\t\tMissed sample clocks\n";
      if (fAlps)		std::cout << "\t\tALTRO power supply error\n";
      if (fPaps)		std::cout << "\t\tPASA power supply error\n";
      if (fDcOverTh)	std::cout << "\t\tDigital current over threshold\n";
      if (fDvOverTh)	std::cout << "\t\tDigital voltage over threshold\n";
      if (fAcOverTh)	std::cout << "\t\tAnalog current over threshold\n";
      if (fAvOverTh)	std::cout << "\t\tAnalog voltage over threshold\n";
      if (fTempOverTh)	std::cout << "\t\tTemperture over threshold\n";
      std::cout << std::flush;
    }

  protected:
    /** Parity - Parity error */
    bool fParity;
    /** Instr - Instruction error */
    bool fInstruction;
    /** SCLK - Missed sample clocks */
    bool fMissedSclk;
    /** Alps - ALTRO power supply error */
    bool fAlps;
    /** Paps - PASA power supply error */
    bool fPaps;
    /** DC - Digital current over threshold */
    bool fDcOverTh;
    /** DV - Digital voltage over threshold */
    bool fDvOverTh;
    /** AC - Analog current over threshold */
    bool fAcOverTh;
    /** AV - Analog voltage over threshold */
    bool fAvOverTh;
    /** T - Temperture over threshold */
    bool fTempOverTh;
    /** Continues ADC conversion */
    bool fCnv;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
