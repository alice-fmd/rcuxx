// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCTSMWORD_H
#define RCUXX_BCTSMWORD_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcTSMWORD rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register TSMWORD - Number
   *  of words to store/read in test mode.  
   *  @ingroup rcuxx_bc
   */
  struct BcTSMWORD : public BcRegister
  {
    /** Constructor 
	@param rcu Reference to RCU interface */
    BcTSMWORD(Rcu& rcu) 
      : BcRegister("TSMWORD", "Test mode words", 0xF, rcu, false, true)
    {}
    /** Decode read data */
    void Decode(unsigned int data) { fData = data & 0x1ff; }
    /** Encode set data before committing */
    unsigned int Encode() const { return fData & 0x1ff; }
    /** @return number */
    unsigned int Number() const { return fData; }
    /** @return number */
    void SetNumber(unsigned int val) { fData = val; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\t# words:\t\t\t" << fData << std::endl;
    }

  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
