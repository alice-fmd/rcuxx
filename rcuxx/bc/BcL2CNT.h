// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCL2CNT_H
#define RCUXX_BCL2CNT_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcL2CNT rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register L2CNT - Number of
   *  L2 Triggers Received
   *  @ingroup rcuxx_bc
   */
  struct BcL2CNT : public BcRegister
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    BcL2CNT(Rcu& rcu /*RcuImem& imem, RcuRMEM& rmem*/) 
      : BcRegister("L2CNT", "Level 2 counter", 0xC, rcu /*imem, rmem*/, false, false)
    {}
    /** Decode read data */
    void Decode(unsigned int data) { fData = data & 0xffff; }
    /** @return counts */
    unsigned int Counts() const { return fData; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << "\t# L2 triggers:\t\t\t" << fData << std::endl;
    }

  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
