// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCAC_TH_H
#define RCUXX_BCAC_TH_H
#include <rcuxx/bc/BcThreshold.h>
#include <rcuxx/bc/BcAC.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcAC_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register AC_TH
   *  @ingroup rcuxx_bc
   */
  struct BcAC_TH : public BcThreshold
  {
    /** Constructor 
	@param rcu Reference to RCU interface*/
    BcAC_TH(Rcu& rcu) 
      : BcThreshold("AC_TH", "Analog current threshold", 0x3, rcu)
    {}
    float Convert2Natural(unsigned int c) const { return c * BcAC::fgkCounts2mA; }
    unsigned int Convert2ADC(float v) const { return unsigned(v / BcAC::fgkCounts2mA); }
    /** Set threshold in mili amps
	@param v Threshold */
    void SetMiliAmps(float v) { SetThreshold(v); }
    /** @return Get current human-readable value */
    float MiliAmps() const { return ThresholdNatural(); }
    /** @return Unit */
    const char* Unit() const { return "mA"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
