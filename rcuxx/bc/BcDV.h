// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCDV_H
#define RCUXX_BCDV_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcDV rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register DV - current
   *  digital voltage. The conversion is @f$ U = 4.43mV/ADC@f$ 
   *  @ingroup rcuxx_bc
   */
  struct BcDV : public BcMonitored
  {
    /** mV per count */
    static const float fgkCounts2mV;
    /** Constructor 
	@param rcu Reference to RCU interface
	@param csr3 Reference to third Configuration and status
	register 
	@param start Command to start conversion */
    BcDV(Rcu& rcu, BcCSR3& csr3, AltroCommand& start) 
      : BcMonitored("DV", "Current digital voltage", 0x9, rcu, csr3, start)
    {}
    float Convert2Natural(unsigned int c) const { return c * fgkCounts2mV; }
    /** @return Get current value in mili amperes */
    float MiliVolts() const { return CurrentNatural(); }
    /** @return Unit */
    const char* Unit() const { return "mV"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
