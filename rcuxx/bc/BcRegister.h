// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCREGISTER_H
# define RCUXX_BCREGISTER_H
# include <rcuxx/altro/AltroRegister.h>
# include <rcuxx/rcu/RcuSCADD.h>
# include <rcuxx/rcu/RcuSCDAT.h>
# include <rcuxx/rcu/RcuRESREG.h>
# include <rcuxx/rcu/RcuERRREG.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcRegister rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Base class for interfaces to Board Controller registers
   *  @ingroup rcuxx_bc
   */
  struct BcRegister : public AltroRegister 
  {
    /** Constructor 
	@param name    Register name 
	@param title Help string
	@param instr   Instruction number
	@param rcu Reference to RCU interface 
	@param clear   Whether the register is clear-able
	@param submit  Whether the register is write-able
	@param global  Whether the register is global or per channel
	@param bcast   Whether this register can be broadcasted to. */
    BcRegister(const char* name, const char* title, unsigned int instr, 
	       Rcu& rcu, bool clear, bool submit, 
	       bool global=false, bool bcast=true)
      : AltroRegister(name, title, instr, rcu, clear, submit, global, bcast), 
	fUseI2C(false)
    {
      // This is for 7 bit addresses
      fInstr  = (instr & 0x7F);  
    }
    /** Set to broadcast mode */
    virtual void SetBroadcast()
    {
      AltroRegister::SetBroadcast();
      fAddress += (1 << 17);
    }
    /** Set Address
	@param board  Board number 
	@param chip   Chip number (not used)
	@param channel Channel number (not uses). */
    virtual void SetAddress(unsigned int board, unsigned int chip=0, 
			    unsigned int channel=0)
    {
      AltroRegister::SetAddress(board, 0, 0);
      fAddress += (1 << 17);
    }
    /** Set whether to use the @f$ I^2C@f$ bus or not 
	@param use If true, use the I2C bus */
    virtual void SetUseI2C(bool use=true) { fUseI2C = use; }
    /** Whether we're using the  @f$ I^2C@f$ bus 
	@return @c true if we're using the I2C bus. */
    virtual bool UseI2C() const { return fUseI2C; }
    /** Write @a data to register 
	@param data Data to put in register
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(unsigned int data) { 
      if (fUseI2C) return I2CWrite(data);
      return AltroRegister::Write(data);
    }
    /** Read value of register into @a data
	@param data On return, value of register 
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(unsigned int& data) { 
      if (fUseI2C) return I2CRead(data);
      return AltroRegister::Read(data);
    }
    /** Write @a data to register  using the @f$ I^2C@f$ bus
	@param data Data to put in register
	@return 0 on success, error code otherwise */
    virtual unsigned int I2CWrite(unsigned int data)
    {
      if (!IsSubmitable()) return Rcu::kNotWriteable;
      unsigned int ret = 0;
      if ((ret = fRcu.RS_ERRREG()->Commit())) return ret;
      unsigned int target = Board();
      bool         bcast  = IsBroadcast();
      if (bcast && !IsBroadcastable()) return Rcu::kCannotBroadcast;
      fRcu.SCADD()->SetAddress(false, bcast, target, fInstr); 
      fRcu.SCDAT()->SetData(data & 0xFFFF);
      if ((ret = fRcu.SCADD()->Commit()))  return ret;
      if ((ret = fRcu.SCDAT()->Commit()))  return ret;
      if ((ret = fRcu.SCEXEC()->Commit())) return ret;
      if ((ret = fRcu.ERRREG()->Update())) return ret;
      if (fRcu.ERRREG()->IsNoAcknowledge()) ret = Rcu::kNoAcknowledge;
      if (fRcu.ERRREG()->IsNotActive())     ret = Rcu::kNotActive;
      if (ret) { 
	fRcu.RS_ERRREG()->Commit();
	return ret;
      }
      return ret;
    }
    /** Read value of register into @a data using the @f$ I^2C@f$ bus
	@param data On return, value of register 
	@return 0 on success, error code otherwise */
    virtual unsigned int I2CRead(unsigned int& data)
    {
      unsigned int ret = 0;
      if ((ret = fRcu.RS_ERRREG()->Commit())) return ret;
      if ((ret = fRcu.RS_RESREG()->Commit())) return ret;
      unsigned int target = Board();
      bool         bcast  = IsBroadcast();
      if (bcast) return Rcu::kCannotBroadcast;
      fRcu.SCADD()->SetAddress(true, bcast, target, fInstr);
      fRcu.SCDAT()->SetData(0);
      if ((ret = fRcu.SCADD()->Commit()))  return ret;
      if ((ret = fRcu.SCDAT()->Commit()))  return ret;
      if ((ret = fRcu.SCEXEC()->Commit())) return ret;
      if ((ret = fRcu.ERRREG()->Update())) return ret;
      if (fRcu.ERRREG()->IsNoAcknowledge()) ret = Rcu::kNoAcknowledge;
      if (fRcu.ERRREG()->IsNotActive())     ret = Rcu::kNotActive;
      if (ret) { 
	fRcu.RS_ERRREG()->Commit();
	return ret;
      }
      if ((ret = fRcu.RESREG()->Update())) return ret;
      unsigned int addr = fRcu.RESREG()->Address();
      if (addr != target) return Rcu::kAddressMismatch;
      data = fRcu.RESREG()->Result();
      return ret;
    }
  protected:
    /** Whether to use the @f$ I^2C@f$ bus */
    bool fUseI2C;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
