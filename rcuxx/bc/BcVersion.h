// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCVERSION_H
#define RCUXX_BCVERSION_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcVersion rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register Version - watch dog
   *  and missed clock warning ratios. 
   *  @ingroup rcuxx_bc
   */
  struct BcVersion : public BcRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface */
    BcVersion(Rcu& rcu) 
      : BcRegister("Version", "Version", 0x15, rcu, false, false),
	fMajor(0),
	fMinor(0)
    {}
    /** Decode read data */
    void Decode(unsigned int data)
    {
      fMajor = (data >> 8) & 0xFF;
      fMinor = (data >> 0) & 0xFF;
    }

    /** Print contents of this register */
    virtual void Print() const
    {
      std::cout << "\tVersion:\t\t\t" << fMajor << "." << fMinor << std::endl;
    }

    /** Get major number */
    unsigned int Major() const { return fMajor; }
    /** Get major number */
    unsigned int Minor() const { return fMinor; }
    /** AsFloat */
    float AsFloat() const
    {
      float ret = fMajor + (fMinor / pow(10,int(log10(double(fMinor)))+1));
      return ret;
    }

  protected:
    /** Major number */
    unsigned int fMajor;
    /** Minor number */
    unsigned int fMinor;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
