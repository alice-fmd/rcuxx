// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BCCSR3_H
#define RCUXX_BCCSR3_H
#include <rcuxx/bc/BcRegister.h>

namespace Rcuxx 
{  
  //====================================================================
  /** @struct BcCSR3 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR3 - watch dog
   *  and missed clock warning ratios. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR3 : public BcRegister 
  {  
    /** Constructor 
	@param rcu Reference to RCU interface*/
    BcCSR3(Rcu& rcu /*RcuImem& imem, RcuRMEM& rmem*/) 
      : BcRegister("CSR3", "Watch dog", 0x14, rcu /*imem, rmem*/, false, true),
	fWarnRatio(0),
	fWatchDog(0),
	fCnvEnd(false)
    {}
    /** Decode read data */
    void Decode(unsigned int data)
    {
      fWatchDog	 = (data >> 8) & 0x7f;
      fWarnRatio = data & 0xff;
      fCnvEnd	 = (data & (1 << 15));
    }

    /** Encode set data before committing */
    unsigned int Encode() const
    {
      return (((fWatchDog & 0x7f) << 8) 
	      + (fWarnRatio & 0xff) 
	      +(fCnvEnd ? (1 << 15) : 0));
    }

    /** @return WarnRatio */
    unsigned WarnRatio() const { return fWarnRatio; }
    /** Set WarnRatio to @a val */
    void SetWarnRatio(unsigned int val) { fWarnRatio = val; }
    /** @return WatchDog */
    unsigned WatchDog() const { return fWatchDog; }
    /** Set WatchDog to @a val */
    void SetWatchDog(unsigned int val) { fWatchDog = val; }
    /** @return if conversion is ended */
    bool IsCnvEnd() const { return fCnvEnd; }
    /** Print contents of this register */
    virtual void Print() const
    {
      AltroRegister::Print();
      std::cout << std::boolalpha 
		<< "\tWatch dog:\t\t\t0x" << std::hex << fWatchDog << "\n"
		<< "\tWarning ratio:\t\t\t" << std::dec << fWarnRatio << "\n" 
		<< "\tConversion ended:\t\t" << fCnvEnd << std::endl;
    }

  protected:
    /** Warning ratio*/
    unsigned int fWarnRatio;
    /** Watch dog */
    unsigned int fWatchDog;
    /** Whether conversion has ended */
    bool         fCnvEnd;
  };
}
#endif
//
// Local Variables:
//  mode: C++
// End:
//
