// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    ProgressMeter.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:26 2006
    @brief   ProgressMeter implementation
*/
#ifndef RCUXX_PROGRESSMETER_H
# include <rcuxx/ProgressMeter.h>
#endif 
#ifndef __CTIME__
# include <ctime>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

//____________________________________________________________________
Rcuxx::ProgressMeter::ProgressMeter(int n, int freq)
{
  Reset(n);
  fFreq = freq;
}

//____________________________________________________________________
void
Rcuxx::ProgressMeter::Step()
{
  if (fCurrent == 0) fStart = time(NULL);
  fCurrent++;
  if (!(fCurrent % fFreq == 0)) return;
  fNow = time(NULL);
    // Event number output - 17 characters 
  std::cout << fPre << " " << std::setw(8) << fCurrent << " ";
  
  // Bar - (75-10-pre_size-5-14-1) = 45 characters 
  int       m=45-fPre.size()+(fN < 0 ? 5 : 0);
  char      b='=';
  char      a=(fCurrent>=fN ? '=' : '>');
  char      s=' ';
  int       n=int(float(m * fCurrent)/fN)+1;
  int       p=(fCurrent>=fN ? 1 : (m-n+2));
  if (fN < 0) {
    b=' ';
    a='=';
    n=(fCurrent % m+1);
    p=(m-n+2);
  }
  std::cout << std::setw(n) << std::setfill(b) << a;
  std::cout << std::setfill(' ') << std::setw(p) << s;
  
  // Percentage - 5 characters 
  if (fN >= 0)
    std::cout << std::setw(3) << (fCurrent * 100) / fN << "% ";
  
  // Time or ETA - 14 characters
  if (fCurrent == 1) std::cout << "ETA:  --:--:-- ";
  else {
    double elab = difftime(fNow,fStart);
    double full = elab / fCurrent * fN + .5;
    int    rem  = int(fN <= fCurrent ? elab : full-elab);
    int    sec  = rem % 60;
    int    min  = (rem / 60) % 60;
    int    hou  = (rem / 60 / 60) % 24;
    std::cout << (fN < 0 ? "Elap: " : (fN<=fCurrent ? "Done: " : "ETA ~ "))
	      << std::setfill('0')
	      << std::setw(2) << hou << ":"
	      << std::setw(2) << min << ":"
	      << std::setw(2) << sec << " ";
    std::cout << std::setfill(' ');
  }
  
  // Throbber - 1 character 
  switch (fThrobber) {
  case '*':  fThrobber = '-';  break;
  case '-':  fThrobber = '\\'; break;
  case '\\': fThrobber = '|';  break;
  case '|':  fThrobber = '/';  break;
  case '/':  fThrobber = '-';  break;
  }
  if (fN > 0 && fCurrent >= fN) fThrobber = '*';
  std::cout << fThrobber;
  std::cout << "\r" << std::flush;
}

//____________________________________________________________________
//
// EOF
//

