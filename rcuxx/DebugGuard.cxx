//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/DebugGuard.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of DebugGuard
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_DEBUGUARD_H
# include "DebugGuard.h"
#endif
#include "compat.h"
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef __CSTDARG__
# include <cstdarg>
#endif
#ifndef __CSTDIO__
# include <cstdio>
#endif

//====================================================================
// bool Rcuxx::DebugGuard::fDebug = false;
int  Rcuxx::DebugGuard::fLevel = 0;

//____________________________________________________________________
Rcuxx::DebugGuard::DebugGuard(bool debug, const char* fmt, ...) 
{
  if (!debug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap,fmt);
#ifdef WIN32
  vsprintf(buf, fmt, ap);
#else
  vsnprintf(buf, 1024, fmt, ap);
#endif
  va_end(ap);
  fMessage = buf;
  std::cout << std::setw(fLevel+2) << "=>" << fMessage << std::endl;
  fLevel++;
}

//____________________________________________________________________
Rcuxx::DebugGuard::~DebugGuard()
{
  if (fMessage.empty()) return;
  fLevel--;
  std::cout << std::setw(fLevel+2) << "<=" << fMessage << std::endl;
}

//____________________________________________________________________
void
Rcuxx::DebugGuard::Message(bool debug, const char* fmt, ...) 
{
  if (!debug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap,fmt);
#ifdef _WIN32
  vsprintf(buf, fmt, ap);
#else
  vsnprintf(buf, 1024, fmt, ap);
#endif
  va_end(ap);
  std::string sbuf(buf);
  std::stringstream s;
  s << std::setw(fLevel+2) << " ";
  std::string::size_type nl = 0;
  do {
    nl = sbuf.find('\n', nl);
    if (nl == std::string::npos) break;
    sbuf.insert(nl+1, s.str());
    nl += s.str().size()+1;
  } while (nl != std::string::npos && nl < sbuf.size());
  std::cout << std::setw(fLevel+2) << "==" << sbuf << std::endl;
}

//
// EOF
//
