// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Command.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_COMMAND
# define RCUXX_COMMAND
# include <string>

namespace Rcuxx
{
  //__________________________________________________________________
  /** @struct Command rcuxx/Command.h <rcuxx/Command.h> 
      @brief Base class for commands 
      @ingroup rcuxx_rcu
  */
  struct Command 
  {
    /** Destructor */
    virtual ~Command() {}
    /** Constructor 
	@param name name 
	@param title Help string
	@param cmd Command  */
    Command(const char* name, const char* title, 
	    unsigned int cmd) : fName(name), fTitle(title), fCmd(cmd) {}
    /** Get the name */ 
    const std::string& Name() const { return fName; }
    /** Get the name */ 
    const std::string& Title() const { return fTitle; }
    /** Execute command */
    virtual unsigned int Commit() = 0;
    /** Get the cmd */ 
    unsigned int Cmd() const { return fCmd; }
  protected:
    /** Name */
    std::string fName;
    /** Title */
    std::string fTitle;
    /** Command */
    unsigned int fCmd;
  };
}
#endif
//
// EOF
//
