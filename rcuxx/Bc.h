// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Bc.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BC_H
#define RCUXX_BC_H
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif

namespace Rcuxx 
{  
  /** @defgroup rcuxx_bc Board Controller classes 
      @ingroup rcuxx 
  */
  struct BcAC; 			// register AC - current
  struct BcAC_TH; 		// register AC_TH
  struct BcAV; 			// register AV - current
  struct BcAV_TH; 		// register AV_TH
  struct BcCommand; 		//
  struct BcCSR0; 		// register CSR0. 
  struct BcCSR1; 		// register CSR1
  struct BcCSR2; 		// register CSR2 - test mode,
  struct BcCSR3; 		// register CSR3 - watch dog
  struct BcDC; 			// register DC - current
  struct BcDC_TH; 		// register DC_TH
  struct BcDSTBCNT; 		// register DSTBCNT - Number
  struct BcDV; 			// register DV - current
  struct BcDV_TH; 		// register DV_TH
  struct BcL1CNT; 		// register L1CNT - Number of
  struct BcL2CNT; 		// register L2CNT - Number of
  struct BcMonitored; 		// 
  struct BcRegister; 		// 
  struct BcSCLKCNT; 		// register SCLKCNT - Number
  struct BcTEMP; 		// register TEMP - current
  struct BcTEMP_TH; 		// register TEMP_TH - the
  struct BcThreshold; 		// threshold registers
  struct BcTSMWORD; 		// register TSMWORD - Number
  struct BcUSRATIO; 		// register USRATIO - Under
  struct BcVersion; 		// register Version - watch dog

  //====================================================================
  /** @class Bc rcuxx/Bc.h <rcuxx/Bc.h>
      @brief Interface to Board Controller 
      @ingroup rcuxx_bc
  */
  class Bc 
  {
  public:
    /** Type of container of registers */
    typedef std::vector<BcRegister*> Registers;
    /** Type of container of commands */
    typedef std::vector<BcCommand*> Commands;

    /** Destructor */
    virtual ~Bc();
    /** Setup the interface 
	@param rcu Reference to controlling RCU */
    Bc(Rcu& rcu);
    /** Set broadcast */
    virtual void SetBroadcast();
    /** Set recipient/query board 
	@param board Board number on ALTRO bus */
    virtual void SetAddress(unsigned int board);
    /** Set whether to use the @f$ I^2C@f$ bus 
	@param use If true, use the @f$ I^2C@f$ bus */
    virtual void SetUseI2C(bool use=true);
    /** Commit all registers to hardware.  This fills up IMEM with
	ALTRO instructions to write to the front-end for the currently
	selected address.  Note, that only registers are written. 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
    /** Update all */
    virtual unsigned int Update();
    /** Print all */
    virtual void Print() const;

    /** Set the debuging stuff */
    virtual void SetDebug(bool debug);

    /** Board address - only used for informative purposes. 
	@return The board address, or a negative value if 
	broadcasting */
    virtual int  BoardAddress() const { return fBoardAddress; }
    /** Whether we're using the @f$ I^2C@f$ bus or not */ 
    virtual bool UseI2C() const { return fUseI2C; }

    /** Get the list of registers */
    const Registers& RegisterList() const { return fRegisters; }
    /** Get the list of registers */
    const Commands& CommandList()   const { return fCommands; }

    /** Command CNTLAT - Latch counters */
    BcCommand*  CNTLAT() const { return fCNTLAT; }
    /** Command ALRST - Altro reset */
    BcCommand*  CNTCLR() const { return fCNTCLR; }
    /** Command ALRST - Altro reset */
    BcCommand*  CSR1CLR() const { return fCSR1CLR; }
    /** Command ALRST - Altro reset */
    BcCommand*  ALRST() const { return fALRST; }
    /** Command BCRST - Board controller reset */
    BcCommand*  BCRST() const { return fBCRST; }
    /** Command STCNV - Start conversion */
    BcCommand*  STCNV() const { return fSTCNV; }
    /** Command SCEVL - Slow control */
    BcCommand*  SCEVL() const { return fSCEVL; }
    /** Command EVLRDO -  */
    BcCommand*  EVLRDO() const { return fEVLRDO; }
    /** Command STTSM - Start test mode */
    BcCommand*  STTSM() const { return fSTTSM; }
    /** Command ACQRDO - Acquisition read-out */
    BcCommand*  ACQRDO() const { return fACQRDO; }
  
    // 
    /** Register TEMP_TH - temperature threshold */
    BcTEMP_TH*	 TEMP_TH() const { return fTEMP_TH; }
    /** Register AV_TH - Analog voltage threshold */
    BcAV_TH*	 AV_TH() const { return fAV_TH; }
    /** Register AC_TH - Analog current threshold */
    BcAC_TH*	 AC_TH() const { return fAC_TH; }
    /** Register DV_TH - Digital voltage threshold */
    BcDV_TH*	 DV_TH() const { return fDV_TH; }
    /** Register DC_TH - Digitial current threshold */
    BcDC_TH*	 DC_TH() const { return fDC_TH; }
    /** Register TEMP - Current temperatur */
    BcTEMP*	 TEMP() const { return fTEMP; }
    /** Register AV - Current analog voltage */
    BcAV*	 AV() const { return fAV; }
    /** Register AC - Current analog current */
    BcAC*	 AC() const { return fAC; }
    /** Register DV - Current digital voltage */
    BcDV*	 DV() const { return fDV; }
    /** Register DC - Current digital current */
    BcDC*	 DC() const { return fDC; }
    /** Register L1CNT - L1 counter */
    BcL1CNT*	 L1CNT() const { return fL1CNT; }
    /** Register L2CNT - L2 counter */
    BcL2CNT*	 L2CNT() const { return fL2CNT; }
    /** Register SCLKCNT - Sample clock counter */
    BcSCLKCNT*	 SCLKCNT() const { return fSCLKCNT; }
    /** Register DSTBCNT - Data strobe counter */
    BcDSTBCNT*	 DSTBCNT() const { return fDSTBCNT; }
    /** Register TSMWORD - Test mode word */
    BcTSMWORD*	 TSMWORD() const { return fTSMWORD; }
    /** Register USRATIO - Under sampling ratio */
    BcUSRATIO*	 USRATIO() const { return fUSRATIO; }
    /** Register CSR0 - Control and status 0 */
    BcCSR0*	 CSR0() const { return fCSR0; }
    /** Register CSR1 - Control and status 1 */
    BcCSR1*	 CSR1() const { return fCSR1; }
    /** Register CSR2 - Control and status 2 */
    BcCSR2*	 CSR2() const { return fCSR2; }
    /** Register CSR3 - Control and status 3 */
    BcCSR3*	 CSR3() const { return fCSR3; }
    /** Version register */
    BcVersion*   Version() const { return fVersion; }
  protected:
    /** Reference to the controlling RCU */
    Rcu& fRcu;
    /** Board address - mostly used for informative purposes. */
    int fBoardAddress;
    /** Whether to use the I2C bus or not */
    bool fUseI2C;
    
    /** Command CNTLAT - Latch counters */
    BcCommand* fCNTLAT;
    /** Command ALRST - Altro reset */
    BcCommand* fCNTCLR;
    /** Command ALRST - Altro reset */
    BcCommand* fCSR1CLR;
    /** Command ALRST - Altro reset */
    BcCommand* fALRST;
    /** Command BCRST - Board controller reset */
    BcCommand* fBCRST;
    /** Command STCNV - Start conversion */
    BcCommand* fSTCNV;
    /** Command SCEVL - Slow control */
    BcCommand* fSCEVL;
    /** Command EVLRDO -  */
    BcCommand* fEVLRDO;
    /** Command STTSM - Start test mode */
    BcCommand* fSTTSM;
    /** Command ACQRDO - Acquisition read-out */
    BcCommand* fACQRDO;
  
    // 
    /** Register TEMP_TH - temperature threshold */
    BcTEMP_TH*	fTEMP_TH;
    /** Register AV_TH - Analog voltage threshold */
    BcAV_TH*	fAV_TH;
    /** Register AC_TH - Analog current threshold */
    BcAC_TH*	fAC_TH;
    /** Register DV_TH - Digital voltage threshold */
    BcDV_TH*	fDV_TH;
    /** Register DC_TH - Digitial current threshold */
    BcDC_TH*	fDC_TH;
    /** Register TEMP - Current temperatur */
    BcTEMP*	fTEMP;
    /** Register AV - Current analog voltage */
    BcAV*	fAV;
    /** Register AC - Current analog current */
    BcAC*	fAC;
    /** Register DV - Current digital voltage */
    BcDV*	fDV;
    /** Register DC - Current digital current */
    BcDC*	fDC;
    /** Register L1CNT - L1 counter */
    BcL1CNT*	fL1CNT;
    /** Register L2CNT - L2 counter */
    BcL2CNT*	fL2CNT;
    /** Register SCLKCNT - Sample clock counter */
    BcSCLKCNT*	fSCLKCNT;
    /** Register DSTBCNT - Data strobe counter */
    BcDSTBCNT*	fDSTBCNT;
    /** Register TSMWORD - Test mode word */
    BcTSMWORD*	fTSMWORD;
    /** Register USRATIO - Under sampling ratio */
    BcUSRATIO*	fUSRATIO;
    /** Register CSR0 - Control and status 0 */
    BcCSR0*	fCSR0;
    /** Register CSR1 - Control and status 1 */
    BcCSR1*	fCSR1;
    /** Register CSR2 - Control and status 2 */
    BcCSR2*	fCSR2;
    /** Register CSR3 - Control and status 3 */
    BcCSR3*	fCSR3;
    /** Version register */
    BcVersion*   fVersion;
    /** Container of pointers to registers.  This is mostly for
	convenience in this class itself */
    Registers fRegisters;
    /** Container of pointers to commands.  This is mostly for
	convenience in this class itself */
    Commands fCommands;    
  };
}

#endif

//____________________________________________________________________
//
// EOF
//

