// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Altro.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTRO_H
#define RCUXX_ALTRO_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif

namespace Rcuxx 
{
  /** @defgroup rcuxx_altro ALTRO interface classes
      @ingroup rcuxx
   */
  struct AltroRegister;
  struct AltroCommand;
  struct AltroADEVL;	// ADEVL.  The 8bit channel
  struct AltroBCTHR;	// BCTHR - Base line correction
  struct AltroDPCF2;	// AltroDPCF2
  struct AltroDPCFG;	// DPCFG - data path
  struct AltroERSTR;	// ERSTR.  It contains 8 bit for
  struct AltroKLCoeffs;	// K or L registers.  There is a set of 6
  struct AltroPMADD;	// PMADD.  It contains the value
  struct AltroPMDTA;	// PMDTA - The pedestal memory
  struct AltroTRCFG;	// TRCFG - trigger
  struct AltroTRCNT;	// TRCNT.  The 16 lower bits
  struct AltroVFPED;	// VFPED - the variable and
  struct AltroZSTHR;	// ZSTHR - the zero
  
	
  //====================================================================
  /** @class Altro rcuxx/Altro.h <rcuxx/Altro.h>
      @brief Interface to ALTRO's 
      @ingroup rcuxx_altro
   */
  class Altro 
  {
  public:
    /** Type of container of registers */
    typedef std::vector<AltroRegister*> Registers;
    /** Type of container of commands */
    typedef std::vector<AltroCommand*> Commands;
    /** Destructor */
    virtual ~Altro() {}
    /** Consntructor 
	@param rcu Reference to controlling RCU interface */
    Altro(Rcu& rcu);
    /** Commit all registers to hardware.  This fills up IMEM with
	ALTRO instructions to write to the front-end for the currently
	selected address.  Note, that only registers are written. 
	@return 0 on success, error code otherwise */
    unsigned int Commit();
    /** Update values
	@return 0 on success, error code otherwise */
    unsigned int Update();
    /** Set the address to talk to
	@param board Board number
	@param chip  Chip number 
	@param channel Channel number */
    void SetAddress(unsigned int board, unsigned int chip=0, 
		    unsigned int channel=0);
    /** Set to broadcast to all ALTRO's */
    void SetBroadcast();
    /** Print contents */
    void Print() const;
    /** Set the debuging stuff */
    virtual void SetDebug(bool debug);
    /** Board address - only used for informative purposes. 
	@return The board address, or a negative value if 
	broadcasting */
    virtual int  BoardAddress() const { return fBoardAddress; }
    /** Chip address - only used for informative purposes. 
	@return The chip address, or a negative value if 
	broadcasting */
    virtual int  ChipAddress() const { return fChipAddress; }
    /** Channel address - only used for informative purposes. 
	@return The channel address, or a negative value if 
	broadcasting */
    virtual int  ChannelAddress() const { return fChannelAddress; }

    /** Get the list of registers */
    const Registers& RegisterList() const { return fRegisters; }
    /** Get the list of registers */
    const Commands& CommandList()   const { return fCommands; }

    /** Command WPINC - Wrte pointer increment 
	This command is equivalent to the Level 2 Trigger Accept. The
	effect of this command is to freeze in one of the buffers of
	the data memory the data taken after the last Level 1
	Trigger. This is done by increasing the Write Pointer that
	points to the memory position where data is to be written when
	a L1 is received. 

	@note WPINC must be issued only after the acquisition of the
	event is achieved. Data will be corrupted and not retrievable
	if the WPINC is issued while the chip is still  ecording
	data. Refer to Chapter 4 of the ALTRO manual for timing
	specifications.  

	@note If an event is to be kept in memory, the WPINC command
	must be issued before the next L1 trigger arrives.
     */
    AltroCommand* WPINC() const { return fWPINC; }
    /** Command RPINC - Read pointer increment. 
	This command releases a buffer of the Data Memory, making it
	available for writing new data. Buffers are used and released
	on a FIFO basis, therefore this command will free the first
	(read or unread) buffer. 
	
	@note RPINC is intended to be issued after the readout of all
	the channels has been done. Once the command is executed,
	there is no way to recover the data stored in the released
	buffer. 
    */
    AltroCommand* RPINC() const { return fRPINC; }
    /** Command CHRDO - Channel readout. 
	This command produces the readout of the specified
	channel. The readout starts immediately after the command is
	acknowledged. During the readout, the ALTRO becomes the owner
	of the bus. 

	@note After CHRDO is acknowledged, the RCU should not issue
	any further instructions and must wait for the TRSF line to go
	low. 

	@note The readout may be interrupted if a L1 trigger is
	received on its dedicated line.  Therefore, the RCU must wait
	for the completion of the acquisition and then continue to
	store the readout. 
    */
    AltroCommand* CHRDO() const { return fCHRDO; }
    /** Command SWTRG - software trigger.
	This command sends a Level 1 trigger to the processing chain
	of the chip. It is entirely equivalent to the dedicated L1
	line, except that the timing depends on both the readout and
	the sampling clocks. 
	@note This command is provided for testing purposes. In normal
	operation mode, the dedicated L1 line should be used.
    */
    AltroCommand* SWTRG() const { return fSWTRG; }
    /** Command TRCLR - TRigger CLeaR. 
	This command sets the trigger counter (TRCNT) to 0. */
    AltroCommand* TRCLR() const { return fTRCLR; }
    /** Command ERCLR - ERror CLeaR. 
	This command resets the sticky bits of the Status and Error
	Register (ERSTR)  */
    AltroCommand* ERCLR() const { return fERCLR; }
    /** Register K1 - */
    AltroKLCoeffs* K1() const { return fK1; }
    /** Register K2 - */
    AltroKLCoeffs* K2() const { return fK2; }
    /** Register K3 - */
    AltroKLCoeffs* K3() const { return fK3; }
    /** Register L1 - */
    AltroKLCoeffs* L1() const { return fL1; }
    /** Register L2 - */
    AltroKLCoeffs* L2() const { return fL2; }
    /** Register L3 - */
    AltroKLCoeffs* L3() const { return fL3; }
    /** Register VFPED - Variable and fixed pedestal */
    AltroVFPED* VFPED() const { return fVFPED; }
    /** Register PMDTA - Pedestal memory data */
    AltroPMDTA* PMDTA() const { return fPMDTA; }
    /** Register ZSTHR - Zero suppression mode and threshold */
    AltroZSTHR* ZSTHR() const { return fZSTHR; }
    /** Register BCTHR - Baseline correction mode and threshold */
    AltroBCTHR* BCTHR() const { return fBCTHR; }
    /** Register TRCFG - Trigger configuration */
    AltroTRCFG* TRCFG() const { return fTRCFG; }
    /** Register DPCFG - Data path configuration 1 */
    AltroDPCFG* DPCFG() const { return fDPCFG; }
    /** Register DPCF2 - Data path configuration 2 */
    AltroDPCF2* DPCF2() const { return fDPCF2; }
    /** Register PMADD - Pedestal memory address */
    AltroPMADD* PMADD() const { return fPMADD; }
    /** Register ERSTR - Error and status */
    AltroERSTR* ERSTR() const { return fERSTR; }
    /** Register ADEVL - Address */
    AltroADEVL* ADEVL() const { return fADEVL; }
    /** Register TRCNT - Trigger counter */
    AltroTRCNT* TRCNT() const { return fTRCNT; }
  protected:
    /** Reference to controlling RCU */
    Rcu& fRcu;
    /** Board address - mostly used for informative purposes. */
    int fBoardAddress;
    /** Chip address - mostly used for informative purposes. */
    int fChipAddress;
    /** Channel address - mostly used for informative purposes. */
    int fChannelAddress;
    /** Command WPINC - Wrte pointer increment */
    AltroCommand* fWPINC;
    /** Command RPINC - Read pointer increment */
    AltroCommand* fRPINC;
    /** Command CHRDO - Channel readout */
    AltroCommand* fCHRDO;
    /** Command SWTRG - software trigger */
    AltroCommand* fSWTRG;
    /** Command TRCLR - TRigger CLeaR. */
    AltroCommand* fTRCLR;
    /** Command ERCLR - ERror CLeaR. */
    AltroCommand* fERCLR;
    /** Register K1 - */
    AltroKLCoeffs*	fK1;
    /** Register K2 - */
    AltroKLCoeffs*	fK2;
    /** Register K3 - */
    AltroKLCoeffs*	fK3;
    /** Register L1 - */
    AltroKLCoeffs*	fL1;
    /** Register L2 - */
    AltroKLCoeffs*	fL2;
    /** Register L3 - */
    AltroKLCoeffs*	fL3;
    /** Register VFPED - Variable and fixed pedestal */
    AltroVFPED*		fVFPED;
    /** Register PMDTA - Pedestal memory data */
    AltroPMDTA*		fPMDTA;
    /** Register ZSTHR - Zero suppression mode and threshold */
    AltroZSTHR*		fZSTHR;
    /** Register BCTHR - Baseline correction mode and threshold */
    AltroBCTHR*		fBCTHR;
    /** Register TRCFG - Trigger configuration */
    AltroTRCFG*		fTRCFG;
    /** Register DPCFG - Data path configuration 1 */
    AltroDPCFG*		fDPCFG;
    /** Register DPCF2 - Data path configuration 2 */
    AltroDPCF2*		fDPCF2;
    /** Register PMADD - Pedestal memory address */
    AltroPMADD*		fPMADD;
    /** Register ERSTR - Error and status */
    AltroERSTR*		fERSTR;
    /** Register ADEVL - Address */
    AltroADEVL*		fADEVL;
    /** Register TRCNT - Trigger counter */
    AltroTRCNT*		fTRCNT;
    /** Container of pointers to registers.  This is mostly for
	convenience in this class itself */
    Registers fRegisters;
    /** Container of pointers to commands.  This is mostly for
	convenience in this class itself */
    Commands fCommands;    
  };
}
#endif
//____________________________________________________________________
//
// EOF
//
