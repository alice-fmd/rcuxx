// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/DebugGuard.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of DebugGuard
*/
#ifndef RCUXX_DEBUGGUARD
#define RCUXX_DEBUGGUARD
#ifndef __STRING__
# include <string>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace Rcuxx
{
  //====================================================================
  /** @struct DebugGuard rcuxx/DebugGuard.h <rcuxx/DebugGuard.h>
      @ingroup rcuxx_util 
   */
  struct DebugGuard
  {
    /** Static instance of debug flag */
    // static bool fDebug;
    /** Current indention level */
    static int  fLevel;
    /** The message to print */
    std::string fMessage;
    /** Make a debug guard.  Prints the enter message
	@param debug Condition for showing message 
	@param fmt Format of the message. */
    DebugGuard(bool debug, const char* fmt, ...);
    /** Destroy a debug guard.  Prints the exit message */
    ~DebugGuard();
    /** Static member function to print a message 
	@param debug Condition for showing message 
	@param fmt Format of the message */
    static void Message(bool debug, const char* fmt, ...);
  };
}
#endif
//
// EOF
//

