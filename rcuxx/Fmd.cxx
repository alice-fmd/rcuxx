//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Fmd.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:29 2006
    @brief   Implementation of FMD interface 
*/
#ifndef RCUXX_FMD_H
# include <rcuxx/Fmd.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#include "DebugGuard.h"
#include "fmd/FmdAL_ANA_I.h"
#include "fmd/FmdAL_ANA_U.h"
#include "fmd/FmdAL_DIG_I.h"
#include "fmd/FmdAL_DIG_U.h"
#include "fmd/FmdCalIter.h"
#include "fmd/FmdClock.h"
#include "fmd/FmdCommand.h"
#include "fmd/FmdConstants.h"
#include "fmd/FmdFLASH_I.h"
#include "fmd/FmdGTL_U.h"
#include "fmd/FmdHoldWait.h"
#include "fmd/FmdL0Timeout.h"
#include "fmd/FmdL0Triggers.h"
#include "fmd/FmdL1Timeout.h"
#include "fmd/FmdL1Triggers.h"
#include "fmd/FmdMeb.h"
#include "fmd/FmdPulser.h"
#include "fmd/FmdRange.h"
#include "fmd/FmdRegister.h"
#include "fmd/FmdSampleClock.h"
#include "fmd/FmdShapeBias.h"
#include "fmd/FmdShiftClock.h"
#include "fmd/FmdStatus.h"
#include "fmd/FmdT1.h"
#include "fmd/FmdT1SENS.h"
#include "fmd/FmdT2.h"
#include "fmd/FmdT2SENS.h"
#include "fmd/FmdT3.h"
#include "fmd/FmdT4.h"
#include "fmd/FmdTopBottom.h"
#include "fmd/FmdVA_REC_IM.h"
#include "fmd/FmdVA_REC_IP.h"
#include "fmd/FmdVA_REC_UM.h"
#include "fmd/FmdVA_REC_UP.h"
#include "fmd/FmdVA_SUP_IM.h"
#include "fmd/FmdVA_SUP_IP.h"
#include "fmd/FmdVA_SUP_UM.h"
#include "fmd/FmdVA_SUP_UP.h"
#include "fmd/FmdVFP.h"
#include "fmd/FmdVFS.h"

#include "bc/BcAC.h"
#include "bc/BcAC_TH.h"
#include "bc/BcAV.h"
#include "bc/BcAV_TH.h"
#include "bc/BcDC.h"
#include "bc/BcDC_TH.h"
#include "bc/BcDV.h"
#include "bc/BcDV_TH.h"
#include "bc/BcTEMP.h"
#include "bc/BcTEMP_TH.h"


//====================================================================
Rcuxx::Fmd::Fmd(Rcuxx::Rcu& rcu) 
  : Bc(rcu),
    fShapeBias(0),  fVFS(0),          fVFP(0), fPulser(0),    
    fCalIter(0),    fShiftClock(0),   fSampleClock(0), 
    fHoldWait(0),   fL0Timeout(0),    fL1Timeout(0),    
    fRange(0),      fL0Triggers(0),   fL1Triggers(0), fStatus(0), fMeb(0),
    fT1(0),         fT1_TH(0),        fFLASH_I(0),    fFLASH_I_TH(0), 
    fAL_DIG_I(0),   fAL_DIG_I_TH(0),  fAL_ANA_I(0),   fAL_ANA_I_TH(0),
    fVA_REC_IP(0),  fVA_REC_IP_TH(0), fT2(0),         fT2_TH(0), 
    fVA_SUP_IP(0),  fVA_SUP_IP_TH(0), fVA_REC_IM(0),  fVA_REC_IM_TH(0),
    fVA_SUP_IM(0),  fVA_SUP_IM_TH(0), fGTL_U(0),      fGTL_U_TH(0),
    fT3(0),         fT3_TH(0),        fT1SENS(0),     fT1SENS_TH(0),
    fT2SENS(0),     fT2SENS_TH(0),    fAL_DIG_U(0),   fAL_DIG_U_TH(0),
    fAL_ANA_U(0),   fAL_ANA_U_TH(0),  fT4(0),         fT4_TH(0),
    fVA_REC_UP(0),  fVA_REC_UP_TH(0), fVA_SUP_UP(0),  fVA_SUP_UP_TH(0),
    fVA_SUP_UM(0),  fVA_SUP_UM_TH(0), fVA_REC_UM(0),  fVA_REC_UM_TH(0),
    fChangeDacs(0), fFakeTrigger(0),  fPulserOn(0),   fPulserOff(0),
    fTestOn(0),     fTestOff(0),      fSoftReset(0),  fCalibrationRun(0)
{
  RcuIMEM& imem = *rcu.IMEM();
  RcuRMEM& rmem = *rcu.RMEM();
  
  // Commands
  if (fRcu.Layout().fFullFMDD) { 
    fChangeDacs    = new FmdCommand("CHDAC", "Change DAC's",cmd_change_dac,rcu);
    fFakeTrigger   = new FmdCommand("FKTRG", "Fake trigger",cmd_trigger,   rcu);
    fSoftReset     = new FmdCommand("SRST",  "Reset FSMs",  cmd_reset,     rcu);
    fPulserOn      = new FmdCommand("PON",   "Pulser on",   cmd_calib_on,  rcu);
    fPulserOff     = new FmdCommand("POFF",  "Pulser off",  cmd_calib_off, rcu);
    fTestOn        = new FmdCommand("TON",   "Test on",     cmd_test_on,   rcu);
    fTestOff       = new FmdCommand("TOFF",  "Test off",    cmd_test_off,  rcu);
    fCalibrationRun= new FmdCommand("CALRUN","Cal. Run",    cmd_cal_man,   rcu);
    fCommands.push_back(fChangeDacs);
    fCommands.push_back(fFakeTrigger);
    fCommands.push_back(fSoftReset);
    fCommands.push_back(fPulserOn);
    fCommands.push_back(fPulserOff);
    fCommands.push_back(fTestOn);
    fCommands.push_back(fTestOff);
    fCommands.push_back(fCalibrationRun);
  }

  // Remove TPC bc monitors and thresholds 
  BcRegister* tpcRegs[] = { fTEMP_TH, fAV_TH, fAC_TH, fDV_TH, fDC_TH, 
			    fTEMP,    fAV,    fAC,    fDV,    fDC };
  for (size_t i = 0; i < 10; i++) {
    for (Registers::iterator j = fRegisters.begin(); j!=fRegisters.end(); ++j)
      if ((*j) == tpcRegs[i]) *j = 0;
    delete tpcRegs[i];
    tpcRegs[i] = 0;
  }
  if (fTEMP_TH) fTEMP_TH = 0;
  if (fAV_TH)  	fAV_TH = 0;
  if (fAC_TH)   fAC_TH = 0;
  if (fDV_TH)   fDV_TH = 0;
  if (fDC_TH)   fDC_TH = 0;
  if (fTEMP)    fTEMP = 0;
  if (fAV)      fAV = 0;
  if (fAC)      fAC = 0;
  if (fDV)      fDV = 0;
  if (fDC)      fDC = 0;

#if  0  
  // Thresholds
  if (fTEMP_TH)  { delete fTEMP_TH;  fTEMP_TH = 0; }
  if (fAV_TH)    { delete fAV_TH;    fAV_TH = 0; }
  if (fAC_TH)    { delete fAC_TH;    fAC_TH = 0; }
  if (fDV_TH)    { delete fDV_TH;    fDV_TH = 0; }
  if (fDC_TH)    { delete fDC_TH;    fDC_TH = 0; }

  // Monitor values 
  if (fTEMP)     { delete fTEMP;     fTEMP = 0; }
  if (fAV)       { delete fAV;       fAV = 0; }
  if (fAC)       { delete fAC;       fAC = 0; }
  if (fDV)       { delete fDV;       fDV = 0; }
  if (fDC)       { delete fDC;       fDC = 0; }
#endif

  // Status 
  fStatus       = new FmdStatus(rcu);
  fRegisters.push_back(fStatus);

  // Meb
  fMeb          = new FmdMeb(rcu);
  fRegisters.push_back(fMeb);
  
  // Biases 
  fShapeBias    = new FmdShapeBias(rcu);
  fVFP 		= new FmdVFP(rcu);
  fVFS          = new FmdVFS(rcu);
  fRegisters.push_back(fShapeBias);
  fRegisters.push_back(fVFP);
  fRegisters.push_back(fVFS);
  if (fRcu.Layout().fFullFMDD) { 
    fPulser       = new FmdPulser(rcu);
    fCalIter      = new FmdCalIter(rcu);
    fRegisters.push_back(fPulser);
    fRegisters.push_back(fCalIter);
  }

  // Clocks 
  if (fRcu.Layout().fFullFMDD) { 
    fShiftClock   = new FmdShiftClock(rcu);
    fSampleClock  = new FmdSampleClock(rcu);
    fRegisters.push_back(fShiftClock);
    fRegisters.push_back(fSampleClock);
  }

  // Event handling 
  fRange        = new FmdRange(rcu);
  fHoldWait     = new FmdHoldWait(rcu);
  fL0Timeout    = new FmdL0Timeout(rcu);
  fL1Timeout    = new FmdL1Timeout(rcu);
  fRegisters.push_back(fRange);
  fRegisters.push_back(fHoldWait);
  fRegisters.push_back(fL0Timeout);
  fRegisters.push_back(fL1Timeout);

  // Counters 
  fL0Triggers   = new FmdL0Triggers(rcu);
  fRegisters.push_back(fL0Triggers);

  // Montors and thresholds 
  if (fRcu.Layout().fFullFMDD) { 
    fT1			= new FmdT1(rcu, *fCSR3, *fSTCNV);
    fT1_TH		= new FmdT1_TH(rcu);
    fFLASH_I		= new FmdFLASH_I(rcu, *fCSR3, *fSTCNV);
    fFLASH_I_TH		= new FmdFLASH_I_TH(rcu);
    fAL_DIG_I		= new FmdAL_DIG_I(rcu, *fCSR3, *fSTCNV);
    fAL_DIG_I_TH	= new FmdAL_DIG_I_TH(rcu);
    fAL_ANA_I		= new FmdAL_ANA_I(rcu, *fCSR3, *fSTCNV);
    fAL_ANA_I_TH	= new FmdAL_ANA_I_TH(rcu);
    fVA_REC_IP		= new FmdVA_REC_IP(rcu, *fCSR3, *fSTCNV);
    fVA_REC_IP_TH	= new FmdVA_REC_IP_TH(rcu);
    fT2			= new FmdT2(rcu, *fCSR3, *fSTCNV);
    fT2_TH		= new FmdT2_TH(rcu);
    fVA_SUP_IP		= new FmdVA_SUP_IP(rcu, *fCSR3, *fSTCNV);
    fVA_SUP_IP_TH	= new FmdVA_SUP_IP_TH(rcu);
    fVA_REC_IM		= new FmdVA_REC_IM(rcu, *fCSR3, *fSTCNV);
    fVA_REC_IM_TH	= new FmdVA_REC_IM_TH(rcu);
    fVA_SUP_IM		= new FmdVA_SUP_IM(rcu, *fCSR3, *fSTCNV);
    fVA_SUP_IM_TH	= new FmdVA_SUP_IM_TH(rcu);
    fGTL_U		= new FmdGTL_U(rcu, *fCSR3, *fSTCNV);
    fGTL_U_TH		= new FmdGTL_U_TH(rcu);
    fT3			= new FmdT3(rcu, *fCSR3, *fSTCNV);
    fT3_TH		= new FmdT3_TH(rcu);
    fT1SENS		= new FmdT1SENS(rcu, *fCSR3, *fSTCNV);
    fT1SENS_TH		= new FmdT1SENS_TH(rcu);
    fT2SENS		= new FmdT2SENS(rcu, *fCSR3, *fSTCNV);
    fT2SENS_TH		= new FmdT2SENS_TH(rcu);
    fAL_DIG_U		= new FmdAL_DIG_U(rcu, *fCSR3, *fSTCNV);
    fAL_DIG_U_TH	= new FmdAL_DIG_U_TH(rcu);
    fAL_ANA_U		= new FmdAL_ANA_U(rcu, *fCSR3, *fSTCNV);
    fAL_ANA_U_TH	= new FmdAL_ANA_U_TH(rcu);
    fT4			= new FmdT4(rcu, *fCSR3, *fSTCNV);
    fT4_TH		= new FmdT4_TH(rcu);
    fVA_REC_UP		= new FmdVA_REC_UP(rcu, *fCSR3, *fSTCNV);
    fVA_REC_UP_TH	= new FmdVA_REC_UP_TH(rcu);
    fVA_SUP_UP		= new FmdVA_SUP_UP(rcu, *fCSR3, *fSTCNV);
    fVA_SUP_UP_TH	= new FmdVA_SUP_UP_TH(rcu);
    fVA_SUP_UM		= new FmdVA_SUP_UM(rcu, *fCSR3, *fSTCNV);
    fVA_SUP_UM_TH	= new FmdVA_SUP_UM_TH(rcu);
    fVA_REC_UM		= new FmdVA_REC_UM(rcu, *fCSR3, *fSTCNV);
    fVA_REC_UM_TH	= new FmdVA_REC_UM_TH(rcu);
    fRegisters.push_back(fT1);
    fRegisters.push_back(fT1_TH);
    fRegisters.push_back(fFLASH_I);
    fRegisters.push_back(fFLASH_I_TH);
    fRegisters.push_back(fAL_DIG_I);
    fRegisters.push_back(fAL_DIG_I_TH);
    fRegisters.push_back(fAL_ANA_I);
    fRegisters.push_back(fAL_ANA_I_TH);
    fRegisters.push_back(fVA_REC_IP);
    fRegisters.push_back(fVA_REC_IP_TH);
    fRegisters.push_back(fT2);
    fRegisters.push_back(fT2_TH);
    fRegisters.push_back(fVA_SUP_IP);
    fRegisters.push_back(fVA_SUP_IP_TH);
    fRegisters.push_back(fVA_REC_IM);
    fRegisters.push_back(fVA_REC_IM_TH);
    fRegisters.push_back(fVA_SUP_IM);
    fRegisters.push_back(fVA_SUP_IM_TH);
    fRegisters.push_back(fGTL_U);
    fRegisters.push_back(fGTL_U_TH);
    fRegisters.push_back(fT3);
    fRegisters.push_back(fT3_TH);
    fRegisters.push_back(fT1SENS);
    fRegisters.push_back(fT1SENS_TH);
    fRegisters.push_back(fT2SENS);
    fRegisters.push_back(fT2SENS_TH);
    fRegisters.push_back(fAL_DIG_U);
    fRegisters.push_back(fAL_DIG_U_TH);
    fRegisters.push_back(fAL_ANA_U);
    fRegisters.push_back(fAL_ANA_U_TH);
    fRegisters.push_back(fT4);
    fRegisters.push_back(fT4_TH);
    fRegisters.push_back(fVA_REC_UP);
    fRegisters.push_back(fVA_REC_UP_TH);
    fRegisters.push_back(fVA_SUP_UP);
    fRegisters.push_back(fVA_SUP_UP_TH);
    fRegisters.push_back(fVA_SUP_UM);
    fRegisters.push_back(fVA_SUP_UM_TH);
    fRegisters.push_back(fVA_REC_UM);
    fRegisters.push_back(fVA_REC_UM_TH);
  }
  // Set initial address 
  SetAddress(0);
}

#if 0
//____________________________________________________________________
Rcuxx::Fmd::~Fmd()
{
  if (fChangeDacs)	delete fChangeDacs;
  if (fFakeTrigger)	delete fFakeTrigger;
  if (fPulserOn)	delete fPulserOn;
  if (fPulserOff)	delete fPulserOff;
  if (fTestOn)		delete fTestOn;
  if (fTestOff)		delete fTestOff;
  if (fCalibrationRun)	delete fCalibrationRun;
  if (fStatus)		delete fStatus;
  if (fMeb)		delete fMeb;
  if (fShapeBias)	delete fShapeBias;
  if (fVFP)		delete fVFP;
  if (fVFS)		delete fVFS;
  if (fPulser)		delete fPulser;
  if (fCalIter)		delete fCalIter;
  if (fShiftClock)	delete fShiftClock;
  if (fSampleClock)	delete fSampleClock;
  if (fRange)		delete fRange;
  if (fHoldWait)	delete fHoldWait;
  if (fL0Timeout)	delete fL0Timeout;
  if (fL1Timeout)	delete fL1Timeout;
  if (fL0Triggers)	delete fL0Triggers;
  if (fL1Triggers)	delete fL1Triggers;
  if (fT1)		delete fT1;
  if (fT1_TH)		delete fT1_TH;
  if (fFLASH_I)		delete fFLASH_I;
  if (fFLASH_I_TH)	delete fFLASH_I_TH;
  if (fAL_DIG_I)	delete fAL_DIG_I;
  if (fAL_DIG_I_TH)	delete fAL_DIG_I_TH;
  if (fAL_ANA_I)	delete fAL_ANA_I;
  if (fAL_ANA_I_TH)	delete fAL_ANA_I_TH;
  if (fVA_REC_IP)	delete fVA_REC_IP;
  if (fVA_REC_IP_TH)	delete fVA_REC_IP_TH;
  if (fT2)		delete fT2;
  if (fT2_TH)		delete fT2_TH;
  if (fVA_SUP_IP)	delete fVA_SUP_IP;
  if (fVA_SUP_IP_TH)	delete fVA_SUP_IP_TH;
  if (fVA_REC_IM)	delete fVA_REC_IM;
  if (fVA_REC_IM_TH)	delete fVA_REC_IM_TH;
  if (fVA_SUP_IM)	delete fVA_SUP_IM;
  if (fVA_SUP_IM_TH)	delete fVA_SUP_IM_TH;
  if (fGTL_U)		delete fGTL_U;
  if (fGTL_U_TH)	delete fGTL_U_TH;
  if (fT3)		delete fT3;
  if (fT3_TH)		delete fT3_TH;
  if (fT1SENS)		delete fT1SENS;
  if (fT1SENS_TH)	delete fT1SENS_TH;
  if (fT2SENS)		delete fT2SENS;
  if (fT2SENS_TH)	delete fT2SENS_TH;
  if (fAL_DIG_U)	delete fAL_DIG_U;
  if (fAL_DIG_U_TH)	delete fAL_DIG_U_TH;
  if (fAL_ANA_U)	delete fAL_ANA_U;
  if (fAL_ANA_U_TH)	delete fAL_ANA_U_TH;
  if (fT4)		delete fT4;
  if (fT4_TH)		delete fT4_TH;
  if (fVA_REC_UP)	delete fVA_REC_UP;
  if (fVA_REC_UP_TH)	delete fVA_REC_UP_TH;
  if (fVA_SUP_UP)	delete fVA_SUP_UP;
  if (fVA_SUP_UP_TH)	delete fVA_SUP_UP_TH;
  if (fVA_SUP_UM)	delete fVA_SUP_UM;
  if (fVA_SUP_UM_TH)	delete fVA_SUP_UM_TH;
  if (fVA_REC_UM)	delete fVA_REC_UM;
  if (fVA_REC_UM_TH)	delete fVA_REC_UM_TH;
}

//____________________________________________________________________
void 
Rcuxx::Fmd::SetBroadcast() 
{
  Bc::SetBroadcast();
  if (fChangeDacs)	fChangeDacs->SetBroadcast();
  if (fFakeTrigger)	fFakeTrigger->SetBroadcast();
  if (fPulserOn)	fPulserOn->SetBroadcast();
  if (fPulserOff)	fPulserOff->SetBroadcast();
  if (fTestOn)		fTestOn->SetBroadcast();
  if (fTestOff)		fTestOff->SetBroadcast();
  if (fCalibrationRun)  fCalibrationRun->SetBroadcast();
  if (fStatus)		fStatus->SetBroadcast();
  if (fMeb)		fMeb->SetBroadcast();
  if (fShapeBias)	fShapeBias->SetBroadcast();
  if (fVFP)		fVFP->SetBroadcast();
  if (fVFS)		fVFS->SetBroadcast();
  if (fPulser)		fPulser->SetBroadcast();
  if (fCalIter)		fCalIter->SetBroadcast();
  if (fShiftClock)	fShiftClock->SetBroadcast();
  if (fSampleClock)	fSampleClock->SetBroadcast();
  if (fRange)		fRange->SetBroadcast();
  if (fHoldWait)	fHoldWait->SetBroadcast();
  if (fL0Timeout)	fL0Timeout->SetBroadcast();
  if (fL1Timeout)	fL1Timeout->SetBroadcast();
  if (fL0Triggers)	fL0Triggers->SetBroadcast();
  if (fL1Triggers)	fL1Triggers->SetBroadcast();
  if (fT1)		fT1->SetBroadcast();
  if (fT1_TH)		fT1_TH->SetBroadcast();
  if (fFLASH_I)		fFLASH_I->SetBroadcast();
  if (fFLASH_I_TH)	fFLASH_I_TH->SetBroadcast();
  if (fAL_DIG_I)	fAL_DIG_I->SetBroadcast();
  if (fAL_DIG_I_TH)	fAL_DIG_I_TH->SetBroadcast();
  if (fAL_ANA_I)	fAL_ANA_I->SetBroadcast();
  if (fAL_ANA_I_TH)	fAL_ANA_I_TH->SetBroadcast();
  if (fVA_REC_IP)	fVA_REC_IP->SetBroadcast();
  if (fVA_REC_IP_TH)	fVA_REC_IP_TH->SetBroadcast();
  if (fT2)		fT2->SetBroadcast();
  if (fT2_TH)		fT2_TH->SetBroadcast();
  if (fVA_SUP_IP)	fVA_SUP_IP->SetBroadcast();
  if (fVA_SUP_IP_TH)	fVA_SUP_IP_TH->SetBroadcast();
  if (fVA_REC_IM)	fVA_REC_IM->SetBroadcast();
  if (fVA_REC_IM_TH)	fVA_REC_IM_TH->SetBroadcast();
  if (fVA_SUP_IM)	fVA_SUP_IM->SetBroadcast();
  if (fVA_SUP_IM_TH)	fVA_SUP_IM_TH->SetBroadcast();
  if (fGTL_U)		fGTL_U->SetBroadcast();
  if (fGTL_U_TH)	fGTL_U_TH->SetBroadcast();
  if (fT3)		fT3->SetBroadcast();
  if (fT3_TH)		fT3_TH->SetBroadcast();
  if (fT1SENS)		fT1SENS->SetBroadcast();
  if (fT1SENS_TH)	fT1SENS_TH->SetBroadcast();
  if (fT2SENS)		fT2SENS->SetBroadcast();
  if (fT2SENS_TH)	fT2SENS_TH->SetBroadcast();
  if (fAL_DIG_U)	fAL_DIG_U->SetBroadcast();
  if (fAL_DIG_U_TH)	fAL_DIG_U_TH->SetBroadcast();
  if (fAL_ANA_U)	fAL_ANA_U->SetBroadcast();
  if (fAL_ANA_U_TH)	fAL_ANA_U_TH->SetBroadcast();
  if (fT4)		fT4->SetBroadcast();
  if (fT4_TH)		fT4_TH->SetBroadcast();
  if (fVA_REC_UP)	fVA_REC_UP->SetBroadcast();
  if (fVA_REC_UP_TH)	fVA_REC_UP_TH->SetBroadcast();
  if (fVA_SUP_UP)	fVA_SUP_UP->SetBroadcast();
  if (fVA_SUP_UP_TH)	fVA_SUP_UP_TH->SetBroadcast();
  if (fVA_SUP_UM)	fVA_SUP_UM->SetBroadcast();
  if (fVA_SUP_UM_TH)	fVA_SUP_UM_TH->SetBroadcast();
  if (fVA_REC_UM)	fVA_REC_UM->SetBroadcast();
  if (fVA_REC_UM_TH)	fVA_REC_UM_TH->SetBroadcast();
}

//____________________________________________________________________
void
Rcuxx::Fmd::SetAddress(unsigned int board)
{
  Bc::SetAddress(board);
  if (fChangeDacs)	fChangeDacs->SetAddress(board, 0, 0);
  if (fFakeTrigger)	fFakeTrigger->SetAddress(board, 0, 0);
  if (fPulserOn)	fPulserOn->SetAddress(board, 0, 0);
  if (fPulserOff)	fPulserOff->SetAddress(board, 0, 0);
  if (fTestOn)	        fTestOn->SetAddress(board, 0, 0);
  if (fTestOff)	        fTestOff->SetAddress(board, 0, 0);
  if (fCalibrationRun)  fCalibrationRun->SetAddress(board, 0, 0);
  if (fStatus)		fStatus->SetAddress(board, 0, 0);
  if (fMeb)		fMeb->SetAddress(board, 0, 0);
  if (fShapeBias)	fShapeBias->SetAddress(board, 0, 0);
  if (fVFP)		fVFP->SetAddress(board, 0, 0);
  if (fVFS)		fVFS->SetAddress(board, 0, 0);
  if (fPulser)		fPulser->SetAddress(board, 0, 0);
  if (fCalIter)		fCalIter->SetAddress(board, 0, 0);
  if (fShiftClock)	fShiftClock->SetAddress(board, 0, 0);
  if (fSampleClock)	fSampleClock->SetAddress(board, 0, 0);
  if (fRange)		fRange->SetAddress(board, 0, 0);
  if (fHoldWait)	fHoldWait->SetAddress(board, 0, 0);
  if (fL0Timeout)	fL0Timeout->SetAddress(board, 0, 0);
  if (fL1Timeout)	fL1Timeout->SetAddress(board, 0, 0);
  if (fL0Triggers)	fL0Triggers->SetAddress(board, 0, 0);
  if (fL1Triggers)	fL1Triggers->SetAddress(board, 0, 0);
  if (fT1)		fT1->SetAddress(board, 0, 0);
  if (fT1_TH)		fT1_TH->SetAddress(board, 0, 0);
  if (fFLASH_I)		fFLASH_I->SetAddress(board, 0, 0);
  if (fFLASH_I_TH)	fFLASH_I_TH->SetAddress(board, 0, 0);
  if (fAL_DIG_I)	fAL_DIG_I->SetAddress(board, 0, 0);
  if (fAL_DIG_I_TH)	fAL_DIG_I_TH->SetAddress(board, 0, 0);
  if (fAL_ANA_I)	fAL_ANA_I->SetAddress(board, 0, 0);
  if (fAL_ANA_I_TH)	fAL_ANA_I_TH->SetAddress(board, 0, 0);
  if (fVA_REC_IP)	fVA_REC_IP->SetAddress(board, 0, 0);
  if (fVA_REC_IP_TH)	fVA_REC_IP_TH->SetAddress(board, 0, 0);
  if (fT2)		fT2->SetAddress(board, 0, 0);
  if (fT2_TH)		fT2_TH->SetAddress(board, 0, 0);
  if (fVA_SUP_IP)	fVA_SUP_IP->SetAddress(board, 0, 0);
  if (fVA_SUP_IP_TH)	fVA_SUP_IP_TH->SetAddress(board, 0, 0);
  if (fVA_REC_IM)	fVA_REC_IM->SetAddress(board, 0, 0);
  if (fVA_REC_IM_TH)	fVA_REC_IM_TH->SetAddress(board, 0, 0);
  if (fVA_SUP_IM)	fVA_SUP_IM->SetAddress(board, 0, 0);
  if (fVA_SUP_IM_TH)	fVA_SUP_IM_TH->SetAddress(board, 0, 0);
  if (fGTL_U)		fGTL_U->SetAddress(board, 0, 0);
  if (fGTL_U_TH)	fGTL_U_TH->SetAddress(board, 0, 0);
  if (fT3)		fT3->SetAddress(board, 0, 0);
  if (fT3_TH)		fT3_TH->SetAddress(board, 0, 0);
  if (fT1SENS)		fT1SENS->SetAddress(board, 0, 0);
  if (fT1SENS_TH)	fT1SENS_TH->SetAddress(board, 0, 0);
  if (fT2SENS)		fT2SENS->SetAddress(board, 0, 0);
  if (fT2SENS_TH)	fT2SENS_TH->SetAddress(board, 0, 0);
  if (fAL_DIG_U)	fAL_DIG_U->SetAddress(board, 0, 0);
  if (fAL_DIG_U_TH)	fAL_DIG_U_TH->SetAddress(board, 0, 0);
  if (fAL_ANA_U)	fAL_ANA_U->SetAddress(board, 0, 0);
  if (fAL_ANA_U_TH)	fAL_ANA_U_TH->SetAddress(board, 0, 0);
  if (fT4)		fT4->SetAddress(board, 0, 0);
  if (fT4_TH)		fT4_TH->SetAddress(board, 0, 0);
  if (fVA_REC_UP)	fVA_REC_UP->SetAddress(board, 0, 0);
  if (fVA_REC_UP_TH)	fVA_REC_UP_TH->SetAddress(board, 0, 0);
  if (fVA_SUP_UP)	fVA_SUP_UP->SetAddress(board, 0, 0);
  if (fVA_SUP_UP_TH)	fVA_SUP_UP_TH->SetAddress(board, 0, 0);
  if (fVA_SUP_UM)	fVA_SUP_UM->SetAddress(board, 0, 0);
  if (fVA_SUP_UM_TH)	fVA_SUP_UM_TH->SetAddress(board, 0, 0);
  if (fVA_REC_UM)	fVA_REC_UM->SetAddress(board, 0, 0);
  if (fVA_REC_UM_TH)	fVA_REC_UM_TH->SetAddress(board, 0, 0);
}

//____________________________________________________________________
unsigned int
Rcuxx::Fmd::Update() 
{
  unsigned int ret  = Bc::Update();
  if (ret) return ret;
  if (fStatus		&& (ret = fStatus->Update())) 		return ret;
  if (fMeb		&& (ret = fMeb->Update())) 		return ret;
  if (fShapeBias	&& (ret = fShapeBias->Update())) 	return ret;
  if (fVFP		&& (ret = fVFP->Update())) 		return ret;
  if (fVFS		&& (ret = fVFS->Update())) 		return ret;
  if (fPulser		&& (ret = fPulser->Update())) 		return ret;
  if (fCalIter		&& (ret = fCalIter->Update())) 		return ret;
  if (fShiftClock	&& (ret = fShiftClock->Update())) 	return ret;
  if (fSampleClock	&& (ret = fSampleClock->Update())) 	return ret;
  if (fRange		&& (ret = fRange->Update())) 		return ret;
  if (fHoldWait		&& (ret = fHoldWait->Update())) 	return ret;
  if (fL0Timeout	&& (ret = fL0Timeout->Update())) 	return ret;
  if (fL1Timeout	&& (ret = fL1Timeout->Update())) 	return ret;
  if (fL0Triggers	&& (ret = fL0Triggers->Update())) 	return ret;
  if (fT1		&& (ret = fT1->Update()))		return ret;
  if (fT1_TH		&& (ret = fT1_TH->Update()))		return ret;
  if (fFLASH_I		&& (ret = fFLASH_I->Update()))		return ret;
  if (fFLASH_I_TH	&& (ret = fFLASH_I_TH->Update()))	return ret;
  if (fAL_DIG_I		&& (ret = fAL_DIG_I->Update()))		return ret;
  if (fAL_DIG_I_TH	&& (ret = fAL_DIG_I_TH->Update()))	return ret;
  if (fAL_ANA_I		&& (ret = fAL_ANA_I->Update()))		return ret;
  if (fAL_ANA_I_TH	&& (ret = fAL_ANA_I_TH->Update()))	return ret;
  if (fVA_REC_IP	&& (ret = fVA_REC_IP->Update()))	return ret;
  if (fVA_REC_IP_TH	&& (ret = fVA_REC_IP_TH->Update()))	return ret;
  if (fT2		&& (ret = fT2->Update()))		return ret;
  if (fT2_TH		&& (ret = fT2_TH->Update()))		return ret;
  if (fVA_SUP_IP	&& (ret = fVA_SUP_IP->Update()))	return ret;
  if (fVA_SUP_IP_TH	&& (ret = fVA_SUP_IP_TH->Update()))	return ret;
  if (fVA_REC_IM	&& (ret = fVA_REC_IM->Update()))	return ret;
  if (fVA_REC_IM_TH	&& (ret = fVA_REC_IM_TH->Update()))	return ret;
  if (fVA_SUP_IM	&& (ret = fVA_SUP_IM->Update()))	return ret;
  if (fVA_SUP_IM_TH	&& (ret = fVA_SUP_IM_TH->Update()))	return ret;
  if (fGTL_U		&& (ret = fGTL_U->Update()))		return ret;
  if (fGTL_U_TH		&& (ret = fGTL_U_TH->Update()))		return ret;
  if (fT3		&& (ret = fT3->Update()))		return ret;
  if (fT3_TH		&& (ret = fT3_TH->Update()))		return ret;
  if (fT1SENS		&& (ret = fT1SENS->Update()))		return ret;
  if (fT1SENS_TH	&& (ret = fT1SENS_TH->Update()))	return ret;
  if (fT2SENS		&& (ret = fT2SENS->Update()))		return ret;
  if (fT2SENS_TH	&& (ret = fT2SENS_TH->Update()))	return ret;
  if (fAL_DIG_U		&& (ret = fAL_DIG_U->Update()))		return ret;
  if (fAL_DIG_U_TH	&& (ret = fAL_DIG_U_TH->Update()))	return ret;
  if (fAL_ANA_U		&& (ret = fAL_ANA_U->Update()))		return ret;
  if (fAL_ANA_U_TH	&& (ret = fAL_ANA_U_TH->Update()))	return ret;
  if (fT4		&& (ret = fT4->Update()))		return ret;
  if (fT4_TH		&& (ret = fT4_TH->Update()))		return ret;
  if (fVA_REC_UP	&& (ret = fVA_REC_UP->Update()))	return ret;
  if (fVA_REC_UP_TH	&& (ret = fVA_REC_UP_TH->Update()))	return ret;
  if (fVA_SUP_UP	&& (ret = fVA_SUP_UP->Update()))	return ret;
  if (fVA_SUP_UP_TH	&& (ret = fVA_SUP_UP_TH->Update()))	return ret;
  if (fVA_SUP_UM	&& (ret = fVA_SUP_UM->Update()))	return ret;
  if (fVA_SUP_UM_TH	&& (ret = fVA_SUP_UM_TH->Update()))	return ret;
  if (fVA_REC_UM	&& (ret = fVA_REC_UM->Update()))	return ret;
  if (fVA_REC_UM_TH	&& (ret = fVA_REC_UM_TH->Update()))	return ret;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Fmd::Print() const
{
  Bc::Print();
  if (fStatus)		fStatus->Print();
  if (fMeb)		fMeb->Print();
  if (fShapeBias)	fShapeBias->Print();
  if (fVFP)		fVFP->Print();
  if (fVFS)		fVFS->Print();
  if (fPulser)		fPulser->Print();
  if (fCalIter)		fCalIter->Print();
  if (fShiftClock)	fShiftClock->Print();
  if (fSampleClock)	fSampleClock->Print();
  if (fRange)		fRange->Print();
  if (fHoldWait)	fHoldWait->Print();
  if (fL0Timeout)	fL0Timeout->Print();
  if (fL1Timeout)	fL1Timeout->Print();
  if (fL0Triggers)	fL0Triggers->Print();
  if (fL1Triggers)	fL1Triggers->Print();
  if (fT1)		fT1->Print();
  if (fT1_TH)		fT1_TH->Print();
  if (fFLASH_I)		fFLASH_I->Print();
  if (fFLASH_I_TH)	fFLASH_I_TH->Print();
  if (fAL_DIG_I)	fAL_DIG_I->Print();
  if (fAL_DIG_I_TH)	fAL_DIG_I_TH->Print();
  if (fAL_ANA_I)	fAL_ANA_I->Print();
  if (fAL_ANA_I_TH)	fAL_ANA_I_TH->Print();
  if (fVA_REC_IP)	fVA_REC_IP->Print();
  if (fVA_REC_IP_TH)	fVA_REC_IP_TH->Print();
  if (fT2)		fT2->Print();
  if (fT2_TH)		fT2_TH->Print();
  if (fVA_SUP_IP)	fVA_SUP_IP->Print();
  if (fVA_SUP_IP_TH)	fVA_SUP_IP_TH->Print();
  if (fVA_REC_IM)	fVA_REC_IM->Print();
  if (fVA_REC_IM_TH)	fVA_REC_IM_TH->Print();
  if (fVA_SUP_IM)	fVA_SUP_IM->Print();
  if (fVA_SUP_IM_TH)	fVA_SUP_IM_TH->Print();
  if (fGTL_U)		fGTL_U->Print();
  if (fGTL_U_TH)	fGTL_U_TH->Print();
  if (fT3)		fT3->Print();
  if (fT3_TH)		fT3_TH->Print();
  if (fT1SENS)		fT1SENS->Print();
  if (fT1SENS_TH)	fT1SENS_TH->Print();
  if (fT2SENS)		fT2SENS->Print();
  if (fT2SENS_TH)	fT2SENS_TH->Print();
  if (fAL_DIG_U)	fAL_DIG_U->Print();
  if (fAL_DIG_U_TH)	fAL_DIG_U_TH->Print();
  if (fAL_ANA_U)	fAL_ANA_U->Print();
  if (fAL_ANA_U_TH)	fAL_ANA_U_TH->Print();
  if (fT4)		fT4->Print();
  if (fT4_TH)		fT4_TH->Print();
  if (fVA_REC_UP)	fVA_REC_UP->Print();
  if (fVA_REC_UP_TH)	fVA_REC_UP_TH->Print();
  if (fVA_SUP_UP)	fVA_SUP_UP->Print();
  if (fVA_SUP_UP_TH)	fVA_SUP_UP_TH->Print();
  if (fVA_SUP_UM)	fVA_SUP_UM->Print();
  if (fVA_SUP_UM_TH)	fVA_SUP_UM_TH->Print();
  if (fVA_REC_UM)	fVA_REC_UM->Print();
  if (fVA_REC_UM_TH)	fVA_REC_UM_TH->Print();
}
#endif

//____________________________________________________________________
//
// EOF
//
