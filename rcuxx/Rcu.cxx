//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Rcu.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "Rcu.h"
#include "DebugGuard.h"
#include "Url.h"
#include "backends/Coder.h"
#include "backends/Files.h"
#include "backends/Echo.h"
#ifdef HAVE_U2F
# include "backends/U2F.h"
#endif
#ifdef HAVE_FEECLIENT
# include "backends/Fee.h"
#endif
#ifdef HAVE_FED
# include "backends/Fed.h"
#endif
#ifdef HAVE_DATE
# include "backends/Rorc.h"
#endif
#ifdef HAVE_DCSC
# include "backends/Dcsc.h"
#endif
#include "rcu/RcuABDFSMST.h"
#include "rcu/RcuACTFEC.h"
#include "rcu/RcuALTROCFG1.h"
#include "rcu/RcuALTROCFG2.h"
#include "rcu/RcuALTROIF.h"
#include "rcu/RcuBPVERS.h"
#include "rcu/RcuBusStatus.h"
#include "rcu/RcuCDH.h"
#include "rcu/RcuCHADD.h"
#include "rcu/RcuCommand.h"
#include "rcu/RcuCounter.h"
#include "rcu/RcuERRREG.h"
#include "rcu/RcuERRST.h"
#include "rcu/RcuEVMNGST.h"
#include "rcu/RcuEVWORD.h"
#include "rcu/RcuFECERR.h"
#include "rcu/RcuFMIREG.h"
#include "rcu/RcuFMOREG.h"
#include "rcu/RcuFWVERS.h"
#include "rcu/RcuINSSEQST.h"
#include "rcu/RcuINTMOD.h"
#include "rcu/RcuINTREG.h"
#include "rcu/RcuIRADD.h"
#include "rcu/RcuIRDAT.h"
#include "rcu/RcuTTCControl.h"		// Trigger control register (0x4000)
#include "rcu/RcuROIConfig1.h"		// Region of interest configuration 
#include "rcu/RcuROIConfig2.h"		// Region of interest configuration 
#include "rcu/RcuL1Timeout.h"		// L1 timeout and window (0x4006)
#include "rcu/RcuL2Timeout.h"		// L2 time-out (0x4007)
#include "rcu/RcuRoiTimeout.h"		// ROI message timeout (0x4009)
#include "rcu/RcuL1MsgTimeout.h"	// L1 message timeout (0x400A)
#include "rcu/RcuTTCEventInfo.h"	// Event information (0x4028)
#include "rcu/RcuTTCEventError.h"	// Event information (0x4029)
#include "rcu/RcuMEBSTCNT.h"
#include "rcu/RcuLWADD.h"
#include "rcu/RcuPMCFG.h"
#include "rcu/RcuRCUBUS.h"
#include "rcu/RcuRDOFSMST.h"
#include "rcu/RcuRDOERR.h"
#include "rcu/RcuRDOFEC.h"
#include "rcu/RcuRDOMOD.h"
#include "rcu/RcuRegister.h"
#include "rcu/RcuRESREG.h"
#include "rcu/RcuRCUID.h"
#include "rcu/RcuSCADD.h"
#include "rcu/RcuSCDAT.h"
#include "rcu/RcuSCCLK.h"
#include "rcu/RcuINT_TYPE.h"
#include "rcu/RcuSMEM_STATUS.h"
#include "rcu/RcuStatusEntry.h"
#include "rcu/RcuTRCFG1.h"
#include "rcu/RcuTRCFG2.h"
#include "rcu/RcuTRGCONF.h"
#include "rcu/RcuTRCNT.h"
#include "rcu/RcuIMEM.h"
#include "rcu/RcuPMEM.h"
#include "rcu/RcuRMEM.h"
#include "rcu/RcuDMEM.h"
#include "rcu/RcuACL.h"
#include "rcu/RcuHITLIST.h"
#include "rcu/RcuSTATUS.h"
#include "rcu/RcuHEADER.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstdarg>
#include <stdexcept>


#ifndef VERSION
/** String that identifies the package version */
# define VERSION "?.?"
#endif
#ifndef PACKAGE
/** String that identifies the package name */
# define PACKAGE "rcuxx"
#endif
#ifndef PACKAGE_STRING
/** String that identifies the package */
# define PACKAGE_STRING "Rcu++ ?.?"
#endif
#ifndef RELEASE
# define RELEASE "?"
#endif

namespace 
{
  bool        fDebug         = false;
  std::string gVersion       = VERSION;
  std::string gPackage 	     = PACKAGE;
  std::string gPackageString = PACKAGE_STRING;
  std::string gRelease       = RELEASE;

  
}

//====================================================================
Rcuxx::Rcu::Rcu()
  : 
  // Error and status 
  fERRST(0),		// ERRor and STatus register.  
  fTRCNT(0),		// TRigger CouNTers.  
  fRDOFEC(0),		// ReaDOut FrontEnd Card register.  
  fCHADD(0),		// CHannel Address register 
  fFWVERS(0),		// FirmWare VERSion
  fBPVERS(0),		// Back-plane version (RCU II)
  fRCUID(0),		// Back-plane version (RCU II)
  fFECERRA(0),		// ERRors on branch A FEC bus
  fFECERRB(0),		// ERRors on branch B FEC bus
  fRDOERR(0),		// ReaD-Out errors
  fALTBUS_STATUS(0),
  fALTBUS_TRSF(0),
  fRCUBUS(0),		// Bus grand
  fRS_STATUS(0),	// ReSet STATUS command 
  fCLEARRDRXREG(0),	// Clear RDRX error register
  fARM_ASYNCH(0),
  // U2F Specific registers
  fLWADD(0),		// Last Written ADDress in the RcuDMEM1 and RcuDMEM2.  
  fEVWORD(0),		// EVent WORD register.  
  fTRCFG2(0),		// ReaD-Out MODe register.  
  fFMIREG(0),		// FirMware Input ReGister.
  fFMOREG(0),		// FirMware Output ReGister 
  fCLR_EVTAG(0),	// CLeaR EVent TAG command 
  fRDFM(0),		// FirMware command 
  fTRG_CLR(0),		// TRiGger CLeaR command 
  fWRFM(0),		// WRite FirMware command 
  // Instruction coder
  fIRADD(0),		// Last executed ALTRO InstRuction ADDress.  
  fIRDAT(0),		// Last executed ALTRO InstRuction DATa.
  fIMEM(0),		// RCU instruction memory 
  fRMEM(0),		// RCU result memory 
  fABORT(0),		// ABORT instruction execution  
  fEXEC(0),		// EXECute instructions command 
  // Configuration registers
  fACTFEC(0),		// ACTive FrontEnd Card.  
  fPMCFG(0),		// Pedestal Memory ConFiGuration
  fALTROIF(0),		// Altro config (RCU II)
  fALTROCFG1(0),	// ALTRO config (RCU II) 
  fALTROCFG2(0),	// ALTRO config (RCU II) 
  fPMEM(0),		// RCU pedestal memory 
  fDCS_ON(0),		// DCS OwN bus command 
  fDDL_ON(0),		// DDL OwN bus command 
  fFECRST(0),		// Front-End Card ReSeT command 
  fGLB_RESET(0),	// GLoBal RESET command 
  fRCU_RESET(0),	// RCU RESET command 
  fCONFFEC(0),		// Re-program FEC BC
  fARBITERIRQ(0),	// Request DCS interrupt
  // Monitoring and safety registers
  fINTREG(0),		// INTerrupt REGister 
  fRESREG(0),		// RESult REGister 
  fERRREG(0),		// ERRor REGister 
  fINTMOD(0),		// INTerrupt MODe register 
  fSCADD(0),		// FirmWare VERSion
  fSCDAT(0),		// FirmWare VERSion
  fSCCLK(0),		// FirmWare VERSion
  fRS_ERRREG(0),	// ReSet ERRor REGister
  fRS_RESREG(0),	// ReSet ERRor REGister
  fSCEXEC(0),		// ReSet ERRor REGister
  fINT_TYPE(0),		// Interrupt types 
  fSMEM_STATUS(0),	// Status memory status 
  fRST_AFL(0),		// ReSTore Active Front-end card List
  fRST_RDO(0),		// ReSTore ReaD-Out front-end card list
  fRS_SMEM(0),		// Clear ready bits in status memory
  fSTATUS(0),		// Status memory
  fSCCOMMAND(0),	// Slow Control COMMAND command 
  // Trigger & event interfacey
  fTRCFG1(0),		// TRigger ConFiG register.  
  fTRGCONF(0),		// TRigger ConFiG register.  
  fRDOMOD(0),		// ReaD-Out mode (RCU II)
  fSWTRGCNT(0),		// SoftWare TRiGger CouNTer
  fAUXTRGCNT(0),	// AUXilary TRiGger CouNTer
  fTTCL2ACNT(0),	// TTC L2A trigger CouNTer
  fTTCL2RCNT(0),	// TTC L2R trigger CouNTer
  fMEBSTCNT(0),		// Data strobe counter for branch A 
  fDSTBACNT(0),		// Data strobe counter for branch A 
  fDSTBBCNT(0),		// Data strobe counter for branch B 
  fTRSFACNT(0),		// Transfer assert counter for branch A 
  fTRSFBCNT(0),		// Transfer assert counter for branch B 
  fACKACNT(0),		// Acknowledge assert counter for branch A 
  fACKBCNT(0),		// Acknowledge assert counter for branch B 
  fCSTBACNT(0),		// Control strobe counter for branch A 
  fCSTBBCNT(0),		// Control strobe counter for branch B 
  fDSTBNUMA(0),		// Last transfer data strobe counter for branch A 
  fDSTBNUMB(0),		// Last transfer data strobe counter for branch B
  // TTC interface 
  fTTCControl(0),	// Trigger control register (0x4000)
  fTTCReset(0),		// TTC interface reset command (0x4001)
  fROIConfig1(0),	// Region of interest configuration (0x4002)
  fROIConfig2(0),	// Region of interest configuration (0x4003)
  fTTCResetCnt(0),	// TTC interface reset counters command (0x4004)
  fTTCTestmode(0),	// TTC interface test mode command (0x4005)
  fL1Timeout(0),	// L1 timeout and window (0x4006)
  fL2Timeout(0),	// L2 time-out (0x4007)
  fRoiTimeout(0),	// ROI message timeout (0x4009)
  fL1MsgTimeout(0),	// L1 message timeout (0x400A)
  fPrePulseCnt(0),	// Prepulser counter (0x400B)
  fBCId(0),		// Bunch-crossing counter (0x400C)
  fL0CNT(0),		// L0 counter (0x400D)
  fL1CNT(0),		// L1 counter (0x400E)
  fL1MsgCNT(0),		// L1 message counter (0x400F)
  fL2ACNT(0),		// L2A counter (0x4010)
  fL2RCNT(0),		// L2R counter (0x4011)
  fROICNT(0),		// ROI counter (0x4012)
  fTTCHammingCnt(0),	// Hamming coding errors (0x4016)
  fTTCDecodeCnt(0),	// Decode errors (0x4017)
  fBufferedCnt(0),	// # of buffered events (0x4020)
  fTTCEventInfo(0),	// Event information (0x4028)
  fTTCEventError(0),	// Event information (0x4029)
  // Stuff
  fDM1(0),		// RCU data memory 
  fDM2(0),		// RCU data memory 
  fACL(0),		// RCU Active Channel List memory 
  fHITLIST(0),		// RCU Active Channel List memory 
  fHEADER(0),		// RCU event HEADER memory 
  fL1_CMD(0),		// L1-enable via CoMmanD command 
  fL1_I2C(0),		// L1-enable via I2C command 
  fL1_TTC(0),		// L1-enable via TTC command 
  fRDABORT(0),		// ReaD-out ABORT command 
  fRS_DMEM1(0),		// ReSet DMEM1 command 
  fRS_DMEM2(0),		// ReSet DMEM2 command 
  fRS_TRCFG(0),		// ReSet TRigger ConFiGuration command 
  fRS_TRCNT(0),		// ReSet TRigger CouNTer command 
  fSWTRG(0),		// SoftWare TRiGger command 
  fCDH(0),		// Common data header
  // States 
  fABDFSMST(0),		// Arbitrator FSM state
  fRDOFSMST(0),		// read-out FSM state 
  fINSSEQST(0),		// instruction sequencer FSM state 
  fEVMNGST(0),		// event manager FSM state 
  // Misch
  fFirmwareVersion(0),
  fFirmwareVersionOrdered(0),
  fUrl("")
{}

//____________________________________________________________________
Rcuxx::Rcu::Rcu(const Rcu& other)
  : 
  // Error and status 
  fERRST(other.fERRST),		// ERRor and STatus register.  
  fTRCNT(other.fTRCNT),		// TRigger CouNTers.  
  fRDOFEC(other.fRDOFEC),	// ReaDOut FrontEnd Card register.  
  fCHADD(other.fCHADD),		// CHannel Address register 
  fFWVERS(other.fFWVERS),	// FirmWare VERSion
  fBPVERS(other.fBPVERS),	// Back-plane version (RCU II)
  fRCUID(other.fRCUID),		// Back-plane version (RCU II)
  fFECERRA(other.fFECERRA),	// ERRors on branch A FEC bus
  fFECERRB(other.fFECERRB),	// ERRors on branch B FEC bus
  fRDOERR(other.fRDOERR),	// ReaD-Out errors
  fALTBUS_STATUS(other.fALTBUS_STATUS),
  fALTBUS_TRSF(other.fALTBUS_TRSF),
  fRCUBUS(other.fRCUBUS),	// Bus grand
  fRS_STATUS(other.fRS_STATUS),	// ReSet STATUS command 
  fCLEARRDRXREG(other.fCLEARRDRXREG),	// Clear RDRX error register
  fARM_ASYNCH(other.fARM_ASYNCH),
  // U2F Specific registers
  fLWADD(other.fLWADD),		// Last Written ADDress in the DMEM1 and DMEM2.
  fEVWORD(other.fEVWORD),	// EVent WORD register.  
  fTRCFG2(other.fTRCFG2),	// ReaD-Out MODe register.  
  fFMIREG(other.fFMIREG),	// FirMware Input ReGister.
  fFMOREG(other.fFMOREG),	// FirMware Output ReGister 
  fCLR_EVTAG(other.fCLR_EVTAG),	// CLeaR EVent TAG command 
  fRDFM(other.fRDFM),		// FirMware command 
  fTRG_CLR(other.fTRG_CLR),	// TRiGger CLeaR command 
  fWRFM(other.fWRFM),		// WRite FirMware command 
  // Instruction coder
  fIRADD(other.fIRADD),		// Last executed ALTRO InstRuction ADDress.  
  fIRDAT(other.fIRDAT),		// Last executed ALTRO InstRuction DATa.
  fIMEM(other.fIMEM),		// RCU instruction memory 
  fRMEM(other.fRMEM),		// RCU result memory 
  fABORT(other.fABORT),		// ABORT instruction execution  
  fEXEC(other.fEXEC),		// EXECute instructions command 
  // Configuration registers
  fACTFEC(other.fACTFEC),	// ACTive FrontEnd Card.  
  fPMCFG(other.fPMCFG),		// Pedestal Memory ConFiGuration
  fALTROIF(other.fALTROIF),	// Altro config (RCU II)
  fALTROCFG1(other.fALTROCFG1),	// ALTRO config (RCU II) 
  fALTROCFG2(other.fALTROCFG2),	// ALTRO config (RCU II) 
  fPMEM(other.fPMEM),		// RCU pedestal memory 
  fDCS_ON(other.fDCS_ON),	// DCS OwN bus command 
  fDDL_ON(other.fDDL_ON),	// DDL OwN bus command 
  fFECRST(other.fFECRST),	// Front-End Card ReSeT command 
  fGLB_RESET(other.fGLB_RESET),	// GLoBal RESET command 
  fRCU_RESET(other.fRCU_RESET),	// RCU RESET command 
  fCONFFEC(other.fCONFFEC),	// Re-program FEC BC
  fARBITERIRQ(other.fARBITERIRQ),// Request DCS interrupt
  // Monitoring and safety registers
  fINTREG(other.fINTREG),	// INTerrupt REGister 
  fRESREG(other.fRESREG),	// RESult REGister 
  fERRREG(other.fERRREG),	// ERRor REGister 
  fINTMOD(other.fINTMOD),	// INTerrupt MODe register 
  fSCADD(other.fSCADD),		// FirmWare VERSion
  fSCDAT(other.fSCDAT),		// FirmWare VERSion
  fSCCLK(other.fSCCLK),		// FirmWare VERSion
  fRS_ERRREG(other.fRS_ERRREG),	// ReSet ERRor REGister
  fRS_RESREG(other.fRS_RESREG),	// ReSet ERRor REGister
  fSCEXEC(other.fSCEXEC),	// ReSet ERRor REGister
  fINT_TYPE(other.fINT_TYPE),		// Interrupt types 
  fSMEM_STATUS(other.fSMEM_STATUS),	// Status memory status 
  fRST_AFL(other.fRST_AFL),		// ReSTore Active Front-end card List
  fRST_RDO(other.fRST_RDO),		// ReSTore ReaD-Out front-end card list
  fRS_SMEM(other.fRS_SMEM),		// Clear ready bits in status memory
  fSTATUS(other.fSTATUS),	// Status memory
  fSCCOMMAND(other.fSCCOMMAND),	// Slow Control COMMAND command 
  // Trigger & event interfacey
  fTRCFG1(other.fTRCFG1),	// TRigger ConFiG register.  
  fTRGCONF(other.fTRGCONF),	// TRigger ConFiG register.  
  fRDOMOD(other.fRDOMOD),	// ReaD-Out mode (RCU II)
  fSWTRGCNT(other.fSWTRGCNT),	// SoftWare TRiGger CouNTer
  fAUXTRGCNT(other.fAUXTRGCNT),	// AUXilary TRiGger CouNTer
  fTTCL2ACNT(other.fTTCL2ACNT),	// TTC L2A trigger CouNTer
  fTTCL2RCNT(other.fTTCL2RCNT),	// TTC L2R trigger CouNTer
  fMEBSTCNT(other.fMEBSTCNT),	// Multi-event buffer counter 
  fDSTBACNT(other.fDSTBACNT),	// Data strobe counter for branch A 
  fDSTBBCNT(other.fDSTBBCNT),	// Data strobe counter for branch B 
  fTRSFACNT(other.fTRSFACNT),	// Transfer assert counter for branch A 
  fTRSFBCNT(other.fTRSFBCNT),	// Transfer assert counter for branch B 
  fACKACNT(other.fACKACNT),	// Acknowledge assert counter for branch A 
  fACKBCNT(other.fACKBCNT),	// Acknowledge assert counter for branch B 
  fCSTBACNT(other.fCSTBACNT),	// Control strobe counter for branch A 
  fCSTBBCNT(other.fCSTBBCNT),	// Control strobe counter for branch B 
  fDSTBNUMA(other.fDSTBNUMA),	// Last transfer dstb counter for branch A 
  fDSTBNUMB(other.fDSTBNUMB),	// Last transfer dstb counter for branch B
  // TTC interface 
  fTTCControl(other.fTTCControl),	// Trigger control register 
  fTTCReset(other.fTTCReset),		// TTC interface reset command
  fROIConfig1(other.fROIConfig1),	// Region of interest configuration
  fROIConfig2(other.fROIConfig2),	// Region of interest configuration
  fTTCResetCnt(other.fTTCResetCnt),	// TTC interface reset counters command
  fTTCTestmode(other.fTTCTestmode),	// TTC interface test mode command
  fL1Timeout(other.fL1Timeout),		// L1 timeout and window (0x4006)
  fL2Timeout(other.fL2Timeout),		// L2 time-out (0x4007)
  fRoiTimeout(other.fRoiTimeout),	// ROI message timeout (0x4009)
  fL1MsgTimeout(other.fL1MsgTimeout),	// L1 message timeout (0x400A)
  fPrePulseCnt(other.fPrePulseCnt),	// Prepulser counter (0x400B)
  fBCId(other.fBCId),			// Bunch-crossing counter (0x400C)
  fL0CNT(other.fL0CNT),			// L0 counter (0x400D)
  fL1CNT(other.fL1CNT),			// L1 counter (0x400E)
  fL1MsgCNT(other.fL1MsgCNT),		// L1 message counter (0x400F)
  fL2ACNT(other.fL2ACNT),		// L2A counter (0x4010)
  fL2RCNT(other.fL2RCNT),		// L2R counter (0x4011)
  fROICNT(other.fROICNT),		// ROI counter (0x4012)
  fTTCHammingCnt(other.fTTCHammingCnt),	// Hamming coding errors (0x4016)
  fTTCDecodeCnt(other.fTTCDecodeCnt),	// Decode errors (0x4017)
  fBufferedCnt(other.fBufferedCnt),	// # of buffered events (0x4020)
  fTTCEventInfo(other.fTTCEventInfo),	// Event information (0x4028)
  fTTCEventError(other.fTTCEventError),	// Event information (0x4029)
  // Stuff
  fDM1(other.fDM1),		// RCU data memory 
  fDM2(other.fDM2),		// RCU data memory 
  fACL(other.fACL),		// RCU Active Channel List memory 
  fHITLIST(other.fHITLIST),	// RCU Active Channel List memory 
  fHEADER(other.fHEADER),	// RCU event HEADER memory 
  fL1_CMD(other.fL1_CMD),	// L1-enable via CoMmanD command 
  fL1_I2C(other.fL1_I2C),	// L1-enable via I2C command 
  fL1_TTC(other.fL1_TTC),	// L1-enable via TTC command 
  fRDABORT(other.fRDABORT),	// ReaD-out ABORT command 
  fRS_DMEM1(other.fRS_DMEM1),	// ReSet DMEM1 command 
  fRS_DMEM2(other.fRS_DMEM2),	// ReSet DMEM2 command 
  fRS_TRCFG(other.fRS_TRCFG),	// ReSet TRigger ConFiGuration command 
  fRS_TRCNT(other.fRS_TRCNT),	// ReSet TRigger CouNTer command 
  fSWTRG(other.fSWTRG),		// SoftWare TRiGger command 
  fCDH(other.fCDH),		// Common data header
  // States 
  fABDFSMST(other.fABDFSMST),	// Arbitrator FSM state
  fRDOFSMST(other.fRDOFSMST),	// read-out FSM state 
  fINSSEQST(other.fINSSEQST),	// instruction sequencer FSM state 
  fEVMNGST(other.fEVMNGST),	// event manager FSM state 
  // Misc
  fFirmwareVersion(other.fFirmwareVersion),
  fFirmwareVersionOrdered(other.fFirmwareVersionOrdered),
  fUrl(other.fUrl)
{}

//____________________________________________________________________
Rcuxx::Rcu::~Rcu()
{
  DebugGuard g(fDebug, "Desctructor");
  for (Registers::iterator i = fRegisters.begin();i != fRegisters.end();++i){
    if (!*i) continue;
    delete *i;
    *i = 0;
  }
  for (Memories::iterator i = fMemories.begin(); i != fMemories.end(); ++i) {
    if (!*i) continue;
    if (*i == fMONS && fMONS == fREGS) continue; 
    delete *i;
    *i = 0;
  }
  for (Commands::iterator i = fCommands.begin(); i != fCommands.end(); ++i) {
    if (!*i) continue;
    delete *i;
    *i = 0;
  }
}

//____________________________________________________________________
Rcuxx::Rcu& 
Rcuxx::Rcu::operator=(const Rcu& other)
{
  // Error and status 
  fERRST	= other.fERRST;		// ERRor and STatus register.  
  fTRCNT	= other.fTRCNT;		// TRigger CouNTers.  
  fRDOFEC	= other.fRDOFEC;	// ReaDOut FrontEnd Card register.  
  fCHADD	= other.fCHADD;		// CHannel Address register 
  fFWVERS	= other.fFWVERS;	// FirmWare VERSion
  fBPVERS	= other.fBPVERS;	// Back-plane version (RCU II)
  fRCUID	= other.fRCUID;		// Back-plane version (RCU II)
  fFECERRA	= other.fFECERRA;	// ERRors on branch A FEC bus
  fFECERRB	= other.fFECERRB;	// ERRors on branch B FEC bus
  fRDOERR	= other.fRDOERR;	// ReaD-Out errors
  fALTBUS_STATUS= other.fALTBUS_STATUS;
  fALTBUS_TRSF  = other.fALTBUS_TRSF;
  fRCUBUS	= other.fRCUBUS;	// Bus grand
  fRS_STATUS	= other.fRS_STATUS;	// ReSet STATUS command 
  fCLEARRDRXREG	= other.fCLEARRDRXREG;	// Clear RDRX error register
  fARM_ASYNCH   = other.fARM_ASYNCH;    // Align R/O clocks
  // U2F Specific registers
  fLWADD	= other.fLWADD;		// Last Written ADDress in DMEMs
  fEVWORD	= other.fEVWORD;	// EVent WORD register.  
  fTRCFG2	= other.fTRCFG2;	// ReaD-Out MODe register.  
  fFMIREG	= other.fFMIREG;	// FirMware Input ReGister.
  fFMOREG	= other.fFMOREG;	// FirMware Output ReGister 
  fCLR_EVTAG	= other.fCLR_EVTAG;	// CLeaR EVent TAG command 
  fRDFM		= other.fRDFM;		// FirMware command 
  fTRG_CLR	= other.fTRG_CLR;	// TRiGger CLeaR command 
  fWRFM		= other.fWRFM;		// WRite FirMware command 
  // Instruction coder
  fIRADD	= other.fIRADD;		// Last executed InstRuction ADDress.  
  fIRDAT	= other.fIRDAT;		// Last executed InstRuction DATa.
  fIMEM		= other.fIMEM;		// RCU instruction memory 
  fRMEM		= other.fRMEM;		// RCU result memory 
  fABORT	= other.fABORT;		// ABORT instruction execution  
  fEXEC		= other.fEXEC;		// EXECute instructions command 
  // Configuration registers
  fACTFEC	= other.fACTFEC;	// ACTive FrontEnd Card.  
  fPMCFG	= other.fPMCFG;		// Pedestal Memory ConFiGuration
  fALTROIF	= other.fALTROIF;	// Altro config (RCU II)
  fALTROCFG1	= other.fALTROCFG1;	// ALTRO config (RCU II) 
  fALTROCFG2	= other.fALTROCFG2;	// ALTRO config (RCU II) 
  fPMEM		= other.fPMEM;		// RCU pedestal memory 
  fDCS_ON	= other.fDCS_ON;	// DCS OwN bus command 
  fDDL_ON	= other.fDDL_ON;	// DDL OwN bus command 
  fFECRST	= other.fFECRST;	// Front-End Card ReSeT command 
  fGLB_RESET	= other.fGLB_RESET;	// GLoBal RESET command 
  fRCU_RESET	= other.fRCU_RESET;	// RCU RESET command 
  fCONFFEC	= other.fCONFFEC;	// Re-program FEC BC
  fARBITERIRQ	= other.fARBITERIRQ;	// Request DCS interrupt
  // Monitoring and safety registers
  fINTREG	= other.fINTREG;	// INTerrupt REGister 
  fRESREG	= other.fRESREG;	// RESult REGister 
  fERRREG	= other.fERRREG;	// ERRor REGister 
  fINTMOD	= other.fINTMOD;	// INTerrupt MODe register 
  fSCADD	= other.fSCADD;		// FirmWare VERSion
  fSCDAT	= other.fSCDAT;		// FirmWare VERSion
  fSCCLK	= other.fSCCLK;		// FirmWare VERSion
  fRS_ERRREG	= other.fRS_ERRREG;	// ReSet ERRor REGister
  fRS_RESREG	= other.fRS_RESREG;	// ReSet ERRor REGister
  fSCEXEC	= other.fSCEXEC;	// ReSet ERRor REGister
  fINT_TYPE	= other.fINT_TYPE;	// Interrupt types 
  fSMEM_STATUS	= other.fSMEM_STATUS;	// Status memory status 
  fRST_AFL	= other.fRST_AFL;	// ReSTore Active Front-end card List
  fRST_RDO	= other.fRST_RDO;	// ReSTore ReaD-Out front-end card list
  fRS_SMEM	= other.fRS_SMEM;	// Clear ready bits in status memory
  fSTATUS	= other.fSTATUS;	// Status memory
  fSCCOMMAND	= other.fSCCOMMAND;	// Slow Control COMMAND command 
  // Trigger & event interfacey
  fTRCFG1	= other.fTRCFG1;	// TRigger ConFiG register.  
  fTRGCONF	= other.fTRGCONF;	// TRigger ConFiG register.  
  fRDOMOD	= other.fRDOMOD;	// ReaD-Out mode (RCU II)
  fSWTRGCNT	= other.fSWTRGCNT;	// SoftWare TRiGger CouNTer
  fAUXTRGCNT	= other.fAUXTRGCNT;	// AUXilary TRiGger CouNTer
  fTTCL2ACNT	= other.fTTCL2ACNT;	// TTC L2A trigger CouNTer
  fTTCL2RCNT	= other.fTTCL2RCNT;	// TTC L2R trigger CouNTer
  fMEBSTCNT	= other.fMEBSTCNT;	// Multi-event buffer counter 
  fDSTBACNT	= other.fDSTBACNT;	// Data strobe counter for branch A 
  fDSTBBCNT	= other.fDSTBBCNT;	// Data strobe counter for branch B 
  fTRSFACNT	= other.fTRSFACNT;	// Transfer assert counter for branch A
  fTRSFBCNT	= other.fTRSFBCNT;	// Transfer assert counter for branch B
  fACKACNT	= other.fACKACNT;	// Acknowledge counter for branch A 
  fACKBCNT	= other.fACKBCNT;	// Acknowledge counter for branch B 
  fCSTBACNT	= other.fCSTBACNT;	// Control strobe counter for branch A 
  fCSTBBCNT	= other.fCSTBBCNT;	// Control strobe counter for branch B 
  fDSTBNUMA	= other.fDSTBNUMA;	// Last transfer dstb for branch A 
  fDSTBNUMB	= other.fDSTBNUMB;	// Last transfer dstb for branch B
  // TTC interface 
  fTTCControl	= other.fTTCControl;	// Trigger control register 
  fTTCReset	= other.fTTCReset;	// TTC interface reset command
  fROIConfig1	= other.fROIConfig1;	// Region of interest configuration
  fROIConfig2	= other.fROIConfig2;	// Region of interest configuration
  fTTCResetCnt	= other.fTTCResetCnt;	// TTC interface reset counters command
  fTTCTestmode	= other.fTTCTestmode;	// TTC interface test mode command
  fL1Timeout	= other.fL1Timeout;	// L1 timeout and window (0x4006)
  fL2Timeout	= other.fL2Timeout;	// L2 time-out (0x4007)
  fRoiTimeout	= other.fRoiTimeout;	// ROI message timeout (0x4009)
  fL1MsgTimeout	= other.fL1MsgTimeout;	// L1 message timeout (0x400A)
  fPrePulseCnt	= other.fPrePulseCnt;	// Prepulser counter (0x400B)
  fBCId		= other.fBCId;		// Bunch-crossing counter (0x400C)
  fL0CNT	= other.fL0CNT;		// L0 counter (0x400D)
  fL1CNT	= other.fL1CNT;		// L1 counter (0x400E)
  fL1MsgCNT	= other.fL1MsgCNT;	// L1 message counter (0x400F)
  fL2ACNT	= other.fL2ACNT;	// L2A counter (0x4010)
  fL2RCNT	= other.fL2RCNT;	// L2R counter (0x4011)
  fROICNT	= other.fROICNT;	// ROI counter (0x4012)
  fTTCHammingCnt= other.fTTCHammingCnt;	// Hamming coding errors (0x4016)
  fTTCDecodeCnt	= other.fTTCDecodeCnt;	// Decode errors (0x4017)
  fBufferedCnt	= other.fBufferedCnt;	// # of buffered events (0x4020)
  fTTCEventInfo	= other.fTTCEventInfo;	// Event information (0x4028)
  fTTCEventError= other.fTTCEventError;	// Event information (0x4029)
  // Stuff
  fDM1		= other.fDM1;		// RCU data memory 
  fDM2		= other.fDM2;		// RCU data memory 
  fACL		= other.fACL;		// RCU Active Channel List memory 
  fHITLIST	= other.fHITLIST;	// RCU Active Channel List memory 
  fHEADER	= other.fHEADER;	// RCU event HEADER memory 
  fL1_CMD	= other.fL1_CMD;	// L1-enable via CoMmanD command 
  fL1_I2C	= other.fL1_I2C;	// L1-enable via I2C command 
  fL1_TTC	= other.fL1_TTC;	// L1-enable via TTC command 
  fRDABORT	= other.fRDABORT;	// ReaD-out ABORT command 
  fRS_DMEM1	= other.fRS_DMEM1;	// ReSet DMEM1 command 
  fRS_DMEM2	= other.fRS_DMEM2;	// ReSet DMEM2 command 
  fRS_TRCFG	= other.fRS_TRCFG;	// ReSet TRigger ConFiGuration command 
  fRS_TRCNT	= other.fRS_TRCNT;	// ReSet TRigger CouNTer command 
  fSWTRG	= other.fSWTRG;		// SoftWare TRiGger command 
  fCDH		= other.fCDH;		// Common data header
  // States 
  fABDFSMST	= other.fABDFSMST;	// Arbitrator FSM state
  fRDOFSMST	= other.fRDOFSMST;	// read-out FSM state 
  fINSSEQST	= other.fINSSEQST;	// instruction sequencer FSM state 
  fEVMNGST	= other.fEVMNGST;	// event manager FSM state 
  // Misc
  fFirmwareVersion        = other.fFirmwareVersion;
  fFirmwareVersionOrdered = other.fFirmwareVersionOrdered;
  fUrl             = other.fUrl;
  
  return *this;
}

//____________________________________________________________________
int
Rcuxx::Rcu::Init(const Url& url)
{
  fUrl = url;
  int  ret = 0;
  if ((ret = GetFWVersion())) 
    std::cerr << "Failed to get firmware version" << std::endl;

  // const Url::StringVector& options = url.Options();
  for (Url::OptionMap::const_iterator i = url.Map().begin(); 
       i != url.Map().end(); ++i) { 
    std::string opt = i->first;
    std::string arg = i->second;
    DebugGuard::Message(fDebug, "Option %s=%s", opt.c_str(), arg.c_str());
    HandleOption(opt, arg);    
  }
  
  fLayout.Setup(fLayout.Decode(fFirmwareVersion,fFirmwareVersionOrdered), 
		fDebug);
      
  SetupCommands();
  SetupMemories();
  SetupRegisters();
  
  return ret;
}


//____________________________________________________________________
bool
Rcuxx::Rcu::HandleOption(const std::string& opt, const std::string& arg)
{
  int ret = 0;
  if (opt == "emul") 
    std::cout << "Emulation mode" << std::endl;
  else if (opt == "debug") { 
    if (arg.empty()) { 
      Rcu::SetDebug(kRcu, true);
      return true;
    }
    std::stringstream ds(arg);
    while (true) { 
      std::string s;
      std::getline(ds, s, ',');
      if      (s == "backend") SetDebug(kBackend, 1);
      else if (s == "bc")      SetDebug(kBc, 1);
      else if (s == "altro")   SetDebug(kAltro, 1);
      else if (s == "rcu")     SetDebug(kRcu, 1);
      if (ds.eof()) return true;
    }
  } // Debug 
  else if (opt == "fwvers") { 
    if (arg.empty()) return true;
    std::stringstream ds(arg);
    if (arg[0] == '0') { 
      if (arg[1] == 'x' || arg[1] == 'X') ds << std::hex;
      else                                ds << std::oct;
    }
    ds >> fFirmwareVersion;
    ret = 0; // It's ok if the user specified a version 
  } // fwvers
  else { 
    std::cerr << "unknown option: '" << opt << "'" << std::endl;
    return false;
  }
  return true;
}

//____________________________________________________________________
void
Rcuxx::Rcu::SetupRegisters() 
{

  DebugGuard g(fDebug, "Setting up registers");

  //------------------------------------------------------------------
  // Error and status 
  // ERRor and STatus register.  
  if (fLayout.kO_ERRST != fLayout.kInvalid)
    fRegisters.push_back(fERRST = new RcuERRST(*fREGS, fLayout.kO_ERRST, 
					       fRS_STATUS));
  // TRigger CouNTers.  
  if (fLayout.kO_TRCNT != fLayout.kInvalid)
    fRegisters.push_back(fTRCNT = new RcuTRCNT(*fREGS, fLayout.kO_TRCNT, 
					       fRS_TRCNT));

  // ReaDOut FrontEnd Card register.  
  if (fLayout.kO_RDOFEC != fLayout.kInvalid)
    fRegisters.push_back(fRDOFEC = new RcuRDOFEC(*fREGS, fLayout.kO_RDOFEC));

  // CHannel Address register 
  if (fLayout.kO_CHADD != fLayout.kInvalid)
    fRegisters.push_back(fCHADD = new RcuCHADD(*fREGS, fLayout.kO_CHADD));

  // FirmWare VERSion
  if (fLayout.kO_FWVERS != fLayout.kInvalid)
    fRegisters.push_back(fFWVERS = new RcuFWVERS(*fMONS, fLayout.kO_FWVERS));

  // Back-plane version (RCU II)
  if (fLayout.kO_BPVERS != fLayout.kInvalid)
    fRegisters.push_back(fBPVERS = new RcuBPVERS(*fREGS, fLayout.kO_BPVERS));

  // Back-plane version (RCU II)
  if (fLayout.kO_RCUID != fLayout.kInvalid)
    fRegisters.push_back(fRCUID = new RcuRCUID(*fREGS, fLayout.kO_RCUID));

  // ERRors on branch A FEC bus
  if (fLayout.kO_FECERRA != fLayout.kInvalid)
    fRegisters.push_back(fFECERRA = new RcuFECERR('A', *fREGS, 
						  fLayout.kO_FECERRA, 
						  fRS_STATUS));

  // ERRors on branch B FEC bus
  if (fLayout.kO_FECERRB != fLayout.kInvalid)
    fRegisters.push_back(fFECERRB = new RcuFECERR('B', *fREGS, 
						  fLayout.kO_FECERRB, 
						  fRS_STATUS));

  // ReaD-Out errors
  if (fLayout.kO_RDOERR != fLayout.kInvalid)
    fRegisters.push_back(fRDOERR = new RcuRDOERR(*fREGS, fLayout.kO_RDOERR, 
						 fCLEARRDRXREG));

  if (fLayout.kO_ALTBUS_STATUS != fLayout.kInvalid)
    fRegisters.push_back(fALTBUS_STATUS
			 = new RcuBusStatus("ALTBUS_STATUS", "ALTRO bus status",
					    *fREGS, fLayout.kO_ALTBUS_STATUS));
  if (fLayout.kO_ALTBUS_TRSF != fLayout.kInvalid)
    fRegisters.push_back(fALTBUS_TRSF
			 = new RcuBusStatus("ALTBUS_TRSF", 
					    "Altro bus status during transfer",
					    *fREGS, fLayout.kO_ALTBUS_TRSF));

  // Bus grand
  // if (fLayout.kO_RCUBUS != fLayout.kInvalid)
  //   fRegisters.push_back(fRCUBUS = new RcuRCUBUS(*fREGS, fLayout.kO_RCUBUS));

       
    
  //------------------------------------------------------------------
  // U2F Specific registers
  // Last Written ADDress in the RcuDMEM1 and RcuDMEM2.  
  if (fLayout.kO_LWADD != fLayout.kInvalid)
    fRegisters.push_back(fLWADD = new RcuLWADD(*fREGS, fLayout.kO_LWADD));
  // EVent WORD register.  
  if (fLayout.kO_EVWORD != fLayout.kInvalid)
    fRegisters.push_back(fEVWORD = new RcuEVWORD(*fREGS, fLayout.kO_EVWORD, 
						 fTRG_CLR));
  // ReaD-Out MODe register.  
  if (fLayout.kO_TRCFG2 != fLayout.kInvalid)
    fRegisters.push_back(fTRCFG2 = new RcuTRCFG2(*fREGS, fLayout.kO_TRCFG2));
  // FirMware Input ReGister.
  if (fLayout.kO_FMIREG != fLayout.kInvalid)
    fRegisters.push_back(fFMIREG = new RcuFMIREG(*fREGS, fLayout.kO_FMIREG));
  // FirMware Output ReGister 
  if (fLayout.kO_FMOREG != fLayout.kInvalid)
    fRegisters.push_back(fFMOREG = new RcuFMOREG(*fREGS, fLayout.kO_FMOREG));

    
  //------------------------------------------------------------------
  // Instruction coder
  // Last executed ALTRO InstRuction ADDress.  
  if (fLayout.kO_IRADD != fLayout.kInvalid)
    fRegisters.push_back(fIRADD = new RcuIRADD(*fREGS, fLayout.kO_IRADD));
  // Last executed ALTRO InstRuction DATa.
  if (fLayout.kO_IRDAT != fLayout.kInvalid)
    fRegisters.push_back(fIRDAT = new RcuIRDAT(*fREGS, fLayout.kO_IRDAT));

  //------------------------------------------------------------------
  // Configuration registers
  // ACTive FrontEnd Card.  
  if (fLayout.kO_ACTFEC != fLayout.kInvalid)
    fRegisters.push_back(fACTFEC = new RcuACTFEC(*fREGS, 
						 fLayout.kO_ACTFEC,
						 fLayout.kO_SCAFL));
  // Pedestal Memory ConFiGuration
  if (fLayout.kO_PMCFG != fLayout.kInvalid)
    fRegisters.push_back(fPMCFG = new RcuPMCFG(*fREGS, fLayout.kO_PMCFG));
  // Altro config (RCU II)
  if (fLayout.kO_ALTROIF != fLayout.kInvalid)
    fRegisters.push_back(fALTROIF = new RcuALTROIF(*fREGS, fLayout.kO_ALTROIF));
  // ALTRO config (RCU II) 
  if (fLayout.kO_ALTROCFG1 != fLayout.kInvalid)
    fRegisters.push_back(fALTROCFG1 = new RcuALTROCFG1(*fREGS, 
						       fLayout.kO_ALTROCFG1));
  // ALTRO config (RCU II) 
  if (fLayout.kO_ALTROCFG2 != fLayout.kInvalid)
    fRegisters.push_back(fALTROCFG2 = new RcuALTROCFG2(*fREGS, 
						       fLayout.kO_ALTROCFG2));

    
  //------------------------------------------------------------------
  // Monitoring and safety registers
  // INTerrupt REGister 
  if (fLayout.kO_INTREG != fLayout.kInvalid)
    fRegisters.push_back(fINTREG = new RcuINTREG(*fMONS, fLayout.kO_INTREG));
  // RESult REGister 
  if (fLayout.kO_RESREG != fLayout.kInvalid)
    fRegisters.push_back(fRESREG = new RcuRESREG(*fMONS, fLayout.kO_RESREG, 
						 fRS_RESREG));
  // ERRor REGister 
  if (fLayout.kO_ERRREG != fLayout.kInvalid)
    fRegisters.push_back(fERRREG = new RcuERRREG(*fMONS, fLayout.kO_ERRREG, 
						 fRS_ERRREG));
  // INTerrupt MODe register 
  if (fLayout.kO_INTMOD != fLayout.kInvalid)
    fRegisters.push_back(fINTMOD = new RcuINTMOD(*fMONS, fLayout.kO_INTMOD));
  // FirmWare VERSion
  if (fLayout.kO_SCADD != fLayout.kInvalid)
    fRegisters.push_back(fSCADD = new RcuSCADD(*fMONS, fLayout.kO_SCADD));
  // FirmWare VERSion
  if (fLayout.kO_SCDAT != fLayout.kInvalid)
    fRegisters.push_back(fSCDAT = new RcuSCDAT(*fMONS, fLayout.kO_SCDAT));
  // FirmWare VERSion
  if (fLayout.kO_SCCLK != fLayout.kInvalid)
    fRegisters.push_back(fSCCLK = new RcuSCCLK(*fMONS, fLayout.kO_SCCLK));
  if (fLayout.kO_INT_TYPE != fLayout.kInvalid)
    fRegisters.push_back(fINT_TYPE = 
			 new RcuINT_TYPE(*fMONS, fLayout.kO_INT_TYPE));
  if (fLayout.kO_SMEM_STATUS != fLayout.kInvalid)
    fRegisters.push_back(fSMEM_STATUS = 
			 new RcuSMEM_STATUS(*fMONS, fLayout.kO_SMEM_STATUS,
					    fRS_SMEM));
    
  //------------------------------------------------------------------
  // Trigger & event interface
  // TRigger ConFiG register.  
  if (fLayout.kO_TRCFG1 != fLayout.kInvalid)
    fRegisters.push_back(fTRCFG1 = new RcuTRCFG1(*fREGS, fLayout.kO_TRCFG1, 
						 fRS_TRCFG));
  if (fLayout.kO_TRGCONF != fLayout.kInvalid)
    fRegisters.push_back(fTRGCONF = new 
			 RcuTRGCONF(*fREGS, fLayout.kO_TRGCONF,
				    fLayout.fTriggerMapFlipped));
  // ReaD-Out mode (RCU II)
  if (fLayout.kO_RDOMOD != fLayout.kInvalid)
    fRegisters.push_back(fRDOMOD = new RcuRDOMOD(*fREGS, fLayout.kO_RDOMOD));
  // SoftWare TRiGger CouNTer
  if (fLayout.kO_SWTRCNT != fLayout.kInvalid)
    fRegisters.push_back(fSWTRGCNT = new RcuCounter("SWTRCNT", 
						    "Software trigger counter",
						    *fREGS, 
						    fLayout.kO_SWTRCNT,
						    fRS_TRCNT));
  // AUXilary TRiGger CouNTer
  if (fLayout.kO_AUXTRGCNT != fLayout.kInvalid)
    fRegisters.push_back(fAUXTRGCNT = new RcuCounter("AUXTRCNT", 
						     "I2C trigger counter",
						     *fREGS, 
						     fLayout.kO_AUXTRGCNT, 
						     fRS_TRCNT));
  // TTC L2A trigger CouNTer
  if (fLayout.kO_TTCL2ACNT != fLayout.kInvalid)
    fRegisters.push_back(fTTCL2ACNT = new RcuCounter("TTCL2ACNT", 
						     "L2A counter",
						     *fREGS, 
						     fLayout.kO_TTCL2ACNT, 
						     fRS_TRCNT));
  // TTC L2R trigger CouNTer
  if (fLayout.kO_TTCL2RCNT != fLayout.kInvalid)
    fRegisters.push_back(fTTCL2RCNT = new RcuCounter("TTCL2RCNT", 
						     "L2R counter", 
						     *fREGS, 
						     fLayout.kO_TTCL2RCNT, 
						     fRS_TRCNT));

  // Data strobe counter for branch A 
  if (fLayout.kO_MEBSTCNT != fLayout.kInvalid)
    fRegisters.push_back(fMEBSTCNT = new RcuMEBSTCNT(*fREGS, 
						     fLayout.kO_MEBSTCNT, 0));

  // Data strobe counter for branch A 
  if (fLayout.kO_DSTBACNT != fLayout.kInvalid)
    fRegisters.push_back(fDSTBACNT = new RcuCounter("DSTBACNT", 
						    "Data strobe counter", 
						    *fREGS, 
						    fLayout.kO_DSTBACNT, 0));

  // Data strobe counter for branch B
  if (fLayout.kO_DSTBBCNT != fLayout.kInvalid)
    fRegisters.push_back(fDSTBBCNT = new RcuCounter("DSTBBCNT", 
						    "Data strobe counter", 
						    *fREGS, 
						    fLayout.kO_DSTBBCNT, 0));

  // Transfer assert counter for branch A 
  if (fLayout.kO_TRSFACNT != fLayout.kInvalid)
    fRegisters.push_back(fTRSFACNT = new RcuCounter("TRSFACNT", 
						    "Transfer counter", 
						    *fREGS, 
						    fLayout.kO_TRSFACNT, 0));

  // Transfer assert counter for branch B 
  if (fLayout.kO_TRSFBCNT != fLayout.kInvalid)
    fRegisters.push_back(fTRSFBCNT = new RcuCounter("TRSFBCNT", 
						    "Transfer counter", 
						    *fREGS, 
						    fLayout.kO_TRSFBCNT, 0));

  // Acknowledge assert counter for branch A 
  if (fLayout.kO_ACKACNT != fLayout.kInvalid)
    fRegisters.push_back(fACKACNT = new RcuCounter("ACKACNT", 
						   "Acknowledge counter", 
						   *fREGS, 
						   fLayout.kO_ACKACNT, 0));

  // Acknowledge assert counter for branch B 
  if (fLayout.kO_ACKBCNT != fLayout.kInvalid)
    fRegisters.push_back(fACKBCNT = new RcuCounter("ACKBCNT", 
						   "Acknowledge counter", 
						   *fREGS, 
						   fLayout.kO_ACKBCNT, 0));

  // Control strobe counter for branch A
  if (fLayout.kO_CSTBACNT != fLayout.kInvalid)
    fRegisters.push_back(fCSTBACNT = new RcuCounter("CSTBACNT", 
						    "Controi strobe counter", 
						    *fREGS, 
						    fLayout.kO_CSTBACNT, 0));

  // Control strobe counter for branch B 
  if (fLayout.kO_CSTBBCNT != fLayout.kInvalid)
    fRegisters.push_back(fCSTBBCNT = new RcuCounter("CSTBBCNT", 
						    "Control strobe counter", 
						    *fREGS, 
						    fLayout.kO_CSTBBCNT, 0));

  // Last transfer data strobe counter for branch A 
  if (fLayout.kO_DSTBNUMA != fLayout.kInvalid)
    fRegisters.push_back(fDSTBNUMA = new RcuCounter("DSTBNUMA", 
						    "Number of DSTB bursts", 
						    *fREGS, 
						    fLayout.kO_DSTBNUMA, 0));

  // Last transfer data strobe counter for branch B
  if (fLayout.kO_DSTBNUMB != fLayout.kInvalid)
    fRegisters.push_back(fDSTBNUMB = new RcuCounter("DSTBNUMB", 
						    "Number of DSTB bursts", 
						    *fREGS, 
						    fLayout.kO_DSTBNUMB, 0));


  //-----------------------------------------------------------------
  // TTC interface 
  // Trigger control register (0x4000)
  if (fLayout.kO_TTCControl != fLayout.kInvalid)
    fRegisters.push_back(fTTCControl = new 
			 RcuTTCControl(*fREGS, fLayout.kO_TTCControl));
  // Region of interest configuration (0x4002)
  if (fLayout.kO_ROIConfig1 != fLayout.kInvalid)
    fRegisters.push_back(fROIConfig1 = new 
			 RcuROIConfig1(*fREGS, fLayout.kO_ROIConfig1));
  // Region of interest configuration (0x4003)
  if (fLayout.kO_ROIConfig2 != fLayout.kInvalid)
    fRegisters.push_back(fROIConfig2 = new 
			 RcuROIConfig2(*fREGS, fLayout.kO_ROIConfig2));
  // L1 timeout and window (0x4006)
  if (fLayout.kO_L1Timeout != fLayout.kInvalid)
    fRegisters.push_back(fL1Timeout = new 
			 RcuL1Timeout(*fREGS, fLayout.kO_L1Timeout));
  // L2 time-out (0x4007)
  if (fLayout.kO_L2Timeout != fLayout.kInvalid)
    fRegisters.push_back(fL2Timeout = new 
			 RcuL2Timeout(*fREGS, fLayout.kO_L2Timeout));
  // ROI message timeout (0x4009)
  if (fLayout.kO_RoiTimeout != fLayout.kInvalid)
    fRegisters.push_back(fRoiTimeout = new 
			 RcuRoiTimeout(*fREGS, fLayout.kO_RoiTimeout));
  // L1 message timeout (0x400A)
  if (fLayout.kO_L1MsgTimeout != fLayout.kInvalid)
    fRegisters.push_back(fL1MsgTimeout = new 
			 RcuL1MsgTimeout(*fREGS, fLayout.kO_L1MsgTimeout));
  // Prepulser counter (0x400B)
  if (fLayout.kO_PrePulseCnt != fLayout.kInvalid)
    fRegisters.push_back(fPrePulseCnt = new 
			 RcuCounter("PrePulseCnt", "Pre-pulse counter", *fREGS, 
					fLayout.kO_PrePulseCnt, fTTCResetCnt));
  // Bunch-crossing counter (0x400C)
  if (fLayout.kO_BCId != fLayout.kInvalid)
    fRegisters.push_back(fBCId = new RcuCounter("BCId", 
						"Bunch crossing counter",
						*fREGS, 
						fLayout.kO_BCId, 
						fTTCResetCnt));
  // L0 counter (0x400D)
  if (fLayout.kO_L0CNT != fLayout.kInvalid)
    fRegisters.push_back(fL0CNT = new RcuCounter("L0CNT", "L0 counter", 
						 *fREGS, 
						 fLayout.kO_L0CNT, 
						 fTTCResetCnt));
  // L1 counter (0x400E)
  if (fLayout.kO_L1CNT != fLayout.kInvalid)
    fRegisters.push_back(fL1CNT = new RcuCounter("L1CNT", "L1 counter", 
						 *fREGS, 
						 fLayout.kO_L1CNT, 
						 fTTCResetCnt));
  // L1 message counter (0x400F)
  if (fLayout.kO_L1MsgCNT != fLayout.kInvalid)
    fRegisters.push_back(fL1MsgCNT = new RcuCounter("L1MsgCNT", 
						    "L1 message counter", 
						    *fREGS, 
						    fLayout.kO_L1MsgCNT, 
						    fTTCResetCnt));
  // L2A counter (0x4010)
  if (fLayout.kO_L2ACNT != fLayout.kInvalid)
    fRegisters.push_back(fL2ACNT = new RcuCounter("L2ACNT", 
						  "L2 accept counter", 
						  *fREGS, 
						  fLayout.kO_L2ACNT, 
						  fTTCResetCnt));
  // L2R counter (0x4011)
  if (fLayout.kO_L2RCNT != fLayout.kInvalid)
    fRegisters.push_back(fL2RCNT = new RcuCounter("L2RCNT", 
						  "L2 reject counter", 
						  *fREGS, 
						  fLayout.kO_L2RCNT, 
						  fTTCResetCnt));
  // ROI counter (0x4012)
  if (fLayout.kO_ROICNT != fLayout.kInvalid)
    fRegisters.push_back(fROICNT = new RcuCounter("ROICNT", 
						  "Region-of-Interest counter", 
						  *fREGS, 
						  fLayout.kO_ROICNT, 
						  fTTCResetCnt));
  // Hamming coding errors (0x4016)
  if (fLayout.kO_TTCHammingCnt != fLayout.kInvalid)
    fRegisters.push_back(fTTCHammingCnt = new 
			 RcuCounter("TTCHammingCnt", 
				    "Number of Hamming code errors", 
				    *fREGS, 
				    fLayout.kO_TTCHammingCnt, 
				    fTTCResetCnt));
  // Decode errors (0x4017)
  if (fLayout.kO_TTCDecodeCnt != fLayout.kInvalid)
    fRegisters.push_back(fTTCDecodeCnt = new 
			 RcuCounter("TTCDecodeCnt", 
				    "Number of TTC decode errors", 
				    *fREGS, 
				    fLayout.kO_TTCDecodeCnt, 
				    fTTCResetCnt));
  // # of buffered events (0x4020)
  if (fLayout.kO_BufferedCnt != fLayout.kInvalid)
    fRegisters.push_back(fBufferedCnt = new 
			 RcuCounter("BufferedCnt", 
				    "Number of buffered events", 
				    *fREGS, 
				    fLayout.kO_BufferedCnt, fTTCResetCnt));
  // Event information (0x4028)
  if (fLayout.kO_TTCEventInfo != fLayout.kInvalid)
    fRegisters.push_back(fTTCEventInfo = new 
			 RcuTTCEventInfo(*fREGS, fLayout.kO_TTCEventInfo));
  // Event information (0x4029)
  if (fLayout.kO_TTCEventError != fLayout.kInvalid)
    fRegisters.push_back(fTTCEventError = new 
			 RcuTTCEventError(*fREGS,fLayout.kO_TTCEventError));

  // FSM state
  if (fLayout.kO_ABDFSMST != fLayout.kInvalid)
    fRegisters.push_back(fABDFSMST = new
                         RcuABDFSMST(*fREGS,fLayout.kO_ABDFSMST));
  // FSM state
  if (fLayout.kO_RDOFSMST != fLayout.kInvalid)
    fRegisters.push_back(fRDOFSMST = new
                         RcuRDOFSMST(*fREGS,fLayout.kO_RDOFSMST));
  // FSM state
  if (fLayout.kO_INSSEQST != fLayout.kInvalid)
    fRegisters.push_back(fINSSEQST = new
                         RcuINSSEQST(*fREGS,fLayout.kO_INSSEQST));
  // FSM state
  if (fLayout.kO_EVMNGST != fLayout.kInvalid)
    fRegisters.push_back(fEVMNGST = new
                         RcuEVMNGST(*fREGS,fLayout.kO_EVMNGST));

}

//____________________________________________________________________
void
Rcuxx::Rcu::SetupMemories() 
{
  DebugGuard g(fDebug, "Setting up memories");
  // Base address for registers (0x7800)
  if (fLayout.kA_REGISTERS != RcuLayout::kInvalid) 
    fMemories.push_back(fREGS = new RcuMemory(*this, "Registers", 
					      "Register block",
					      fLayout.kA_REGISTERS, 
					      fLayout.kS_REGISTERS, 
					      0xffffffff, false));
  fMONS = fREGS;
  DebugGuard::Message(fDebug, "Register memory at %p of size %d", 
		      fREGS, (fREGS ? fREGS->Size() : 0));
  
  // Base address for monitor registers (0x8000/0x7800 for U2F)
  if (fLayout.kA_MONITOR != RcuLayout::kInvalid) 
    fMemories.push_back(fMONS = new RcuMemory(*this, "Monitor",
					      "Monitor block",
					      fLayout.kA_MONITOR, 
					      fLayout.kS_MONITOR,
					      0xffffffff, false));
    

  //------------------------------------------------------------------
  // Instruction coder
  // RCU instruction memory
  if (fLayout.kA_IMEM != fLayout.kInvalid)
    fMemories.push_back(fIMEM = new RcuIMEM(*this, fLayout.kA_IMEM, 
					    fLayout.kS_IMEM, fLayout.kM_IMEM, 
					    fEXEC, fABORT)); 
  if (fIMEM) fIMEM->SetDebug(fDebug);

  // RCU result memory
  if (fLayout.kA_RMEM != fLayout.kInvalid)
    fMemories.push_back(fRMEM = new RcuRMEM(*this, fLayout.kA_RMEM, 
					    fLayout.kS_RMEM, 
					    fLayout.kM_RMEM)); 
  if (fRMEM) fRMEM->SetDebug(fDebug);

  //------------------------------------------------------------------
  // Configuration registers
  // RCU pedestal memory
  if (fLayout.kA_PMEM != fLayout.kInvalid)
    fMemories.push_back(fPMEM = new RcuPMEM(*this, fLayout.kA_PMEM, 
					    fLayout.kS_PMEM)); 

  //------------------------------------------------------------------
  // Monitoring and safety
  // Status memory
  if (fLayout.kA_STATUS != fLayout.kInvalid)
    fMemories.push_back(fSTATUS = new RcuSTATUS(*this, fLayout.kA_STATUS, 
						fLayout.kS_STATUS, 
						fLayout.kO_SMEM_STATUS 
						!= fLayout.kInvalid));


  //------------------------------------------------------------------
  // Trigger & event interface
  // RCU data memory
  if (fLayout.kA_DML1 != fLayout.kInvalid && 
      fLayout.kA_DMH1 != fLayout.kInvalid)
    fMemories.push_back(fDM1 = new RcuDMEM(1, *this, fLayout.kA_DML1, 
					   fLayout.kA_DMH1,
					   fLayout.kS_DM,
					   fRS_DMEM1)); 
  // RCU data memory
  if (fLayout.kA_DML2 != fLayout.kInvalid && 
      fLayout.kA_DMH2 != fLayout.kInvalid)
    fMemories.push_back(fDM2 = new RcuDMEM(2, *this, fLayout.kA_DML2, 
					   fLayout.kA_DMH2, 
					   fLayout.kS_DM,
					   fRS_DMEM2)); 
  // RCU Active Channel List memory
  if (fLayout.kA_ACL != fLayout.kInvalid)
    fMemories.push_back(fACL = new RcuACL(*this, fLayout.kA_ACL, 
					  fLayout.kS_ACL, 
					  fLayout.kA_ACL != 0x1000,
					  fLayout.fVersion >= 
					  RcuLayout::k250510)); 

  // RCU Active Channel List memory
  if (fLayout.kA_HITLIST != fLayout.kInvalid)
    fMemories.push_back(fHITLIST = new RcuHITLIST(*this, fLayout.kA_HITLIST, 
						  fLayout.kS_HITLIST)); 

  // RCU event HEADER memory
  if (fLayout.kA_HEADER != fLayout.kInvalid)
    fMemories.push_back(fHEADER = new RcuHEADER(*this, fLayout.kA_HEADER, 
						fLayout.kS_HEADER)); 


  // Event header words (0x4001 - not on U2F)
  if (fLayout.kA_HEADER == RcuLayout::kInvalid) 
    fHEADER = 0;
  else {
    fHEADER = new RcuHEADER(*this, fLayout.kA_HEADER, fLayout.kS_HEADER);
    fMemories.push_back(fHEADER);
  }

  // CDH 
  if (fLayout.kA_CDH != RcuLayout::kInvalid) { 
    fCDH = new RcuCDH(*this, fLayout.kA_CDH, fLayout.kS_CDH);
    fMemories.push_back(fCDH);
  }
}

//____________________________________________________________________
void
Rcuxx::Rcu::SetupCommands() 
{
  DebugGuard g(fDebug, "Setting up commands");
  //------------------------------------------------------------------
  // Error and status 
  // ReSet STATUS command
  if (fLayout.kC_RS_STATUS != fLayout.kInvalid)
    fCommands.push_back(fRS_STATUS = new RcuCommand(*this, 
						    "RS_STATUS",
						    "ReSet status",
						    fLayout.kC_RS_STATUS)); 
  // Clear RDRX error register
  if (fLayout.kC_CLEARRDRXREG != fLayout.kInvalid)
    fCommands.push_back(fCLEARRDRXREG = new RcuCommand(*this, 
						       "CLEARRDRXREG", 
						       "CLEAR ReaDy Recieve",
						       fLayout
						       .kC_CLEARRDRXREG));

  //------------------------------------------------------------------
  // U2F Specific registers
  // CLeaR EVent TAG command
  if (fLayout.kC_CLR_EVTAG != fLayout.kInvalid)
    fCommands.push_back(fCLR_EVTAG = new RcuCommand(*this, 
						    "CLR_EVTAG",
						    "CLeaR EVent TAG",
						    fLayout.kC_CLR_EVTAG)); 
  // FirMware command
  if (fLayout.kC_RDFM != fLayout.kInvalid)
    fCommands.push_back(fRDFM = new RcuCommand(*this,"RDFM",
					       "ReaD FirMWare",
					       fLayout.kC_RDFM)); 
  // TRiGger CLeaR command
  if (fLayout.kC_TRG_CLR != fLayout.kInvalid)
    fCommands.push_back(fTRG_CLR = new RcuCommand(*this,"TRG_CLR",
						  "Clear triggers",
						  fLayout.kC_TRG_CLR)); 
  // WRite FirMware command
  if (fLayout.kC_WRFM != fLayout.kInvalid)
    fCommands.push_back(fWRFM = new RcuCommand(*this,"WRFM",
					       "Write FirMware", 
					       fLayout.kC_WRFM)); 


  //------------------------------------------------------------------
  // Instruction coder
  // ABORT instruction execution 
  if (fLayout.kC_ABORT != fLayout.kInvalid)
    fCommands.push_back(fABORT = new RcuCommand(*this, "ABORT", 
						"ABORT instruction execution",
						fLayout.kC_ABORT)); 
  // EXECute instructions command
  if (fLayout.kC_EXEC != fLayout.kInvalid)
    fCommands.push_back(fEXEC = new RcuCommand(*this, "EXEC", 
					       "EXECute instruction memory",
					       fLayout.kC_EXEC)); 

  //------------------------------------------------------------------
  // Configuration registers
  // DCS OwN bus command
  if (fLayout.kC_DCS_ON != fLayout.kInvalid)
    fCommands.push_back(fDCS_ON = new RcuCommand(*this, "DCS_ON", 
						 "DCS interface is master",
						 fLayout.kC_DCS_ON)); 
  // DDL OwN bus command
  if (fLayout.kC_DDL_ON != fLayout.kInvalid)
    fCommands.push_back(fDDL_ON = new RcuCommand(*this, "DDL_ON", 
						 "DDL interface is master",
						 fLayout.kC_DDL_ON)); 
  // Front-End Card ReSeT command
  if (fLayout.kC_FECRST != fLayout.kInvalid)
    fCommands.push_back(fFECRST = new RcuCommand(*this, "FECRST", 
						 "Reset Front-End Cards",
						 fLayout.kC_FECRST)); 
  // GLoBal RESET command
  if (fLayout.kC_GLB_RESET != fLayout.kInvalid)
    fCommands.push_back(fGLB_RESET = new RcuCommand(*this, "GLB_RESET", 
						    "GLoBal RESET",
						    fLayout.kC_GLB_RESET)); 
  // RCU RESET command
  if (fLayout.kC_RCU_RESET != fLayout.kInvalid)
    fCommands.push_back(fRCU_RESET = new RcuCommand(*this, "RCU_RESET", 
						    "Reset RCU",
						    fLayout.kC_RCU_RESET)); 
  // Re-program FEC BC
  if (fLayout.kC_CONFFEC != fLayout.kInvalid) {
    fCommands.push_back(fCONFFEC = new RcuCommand(*this, "CONFFEC", 
						  "CONFigure Front-End Cards",
						  fLayout.kC_CONFFEC, true));
    fCONFFEC->SetArgument(0xFFFFFFFF);
  }

  // Request DCS interrupt
  if (fLayout.kC_ARBITERIRQ != fLayout.kInvalid)
    fCommands.push_back(fARBITERIRQ = new RcuCommand(*this, "ARBITERIRQ", 
						     "Interrupt arbitrator",
						     fLayout.kC_ARBITERIRQ));
  if (fLayout.kC_ARM_ASYNCH != fLayout.kInvalid) 
    fCommands.push_back(fARM_ASYNCH = new RcuCommand(*this, "ARM_ASYNCH", 
						    "Align clocks",
						    fLayout.kC_ARM_ASYNCH));

  //------------------------------------------------------------------
  // Monitoring and safety registers
  if (SCAccess()) {
    // ReSet ERRor REGister
    if (fLayout.kC_RS_ERRREG != fLayout.kInvalid)
      fCommands.push_back(fRS_ERRREG = new RcuCommand(*this, "RS_ERRREG", 
						      "ReSet ERRor REGister",
						      fLayout.kC_RS_ERRREG));
    // ReSet ERRor REGister
    if (fLayout.kC_RS_RESREG != fLayout.kInvalid)
      fCommands.push_back(fRS_RESREG = new RcuCommand(*this, "RS_RESREG", 
						      "ReSet RESult REGister",
						      fLayout.kC_RS_RESREG));
    // ReSet ERRor REGister
    if (fLayout.kC_SCEXEC != fLayout.kInvalid)
      fCommands.push_back(fSCEXEC = new RcuCommand(*this, "SCEXEC", 
						   "Execute slow control",
						   fLayout.kC_SCEXEC));
    // Slow Control COMMAND command
    if (fLayout.kC_SCCOMMAND != fLayout.kInvalid)
      fCommands.push_back(fSCCOMMAND = new RcuCommand(*this, "SCCOMMAND", 
						    "Slow Control COMMAND",
						      fLayout.kC_SCCOMMAND, 
						      true)); 
    if (fLayout.kC_RST_AFL != fLayout.kInvalid)
      fCommands.push_back(fRST_AFL = new RcuCommand(*this, "RST_AFL", 
						    "ReSTore Active FEC List",
						    fLayout.kC_RST_AFL, 
						    true)); 
    if (fLayout.kC_RST_RDO != fLayout.kInvalid)
      fCommands.push_back(fRST_RDO = new RcuCommand(*this, "RST_RDO", 
						    "ReSTore ReaD-Out List",
						    fLayout.kC_RST_RDO, 
						    true)); 
    if (fLayout.kC_RS_SMEM != fLayout.kInvalid)
      fCommands.push_back(fRS_SMEM = new RcuCommand(*this, "RS_SMEM", 
						    "ReSet Status MEMory bits",
						    fLayout.kC_RS_SMEM, 
						    true));
  }

  //------------------------------------------------------------------
  // Trigger & event interface
  // L1-enable via CoMmanD command
  if (fLayout.kC_L1_CMD != fLayout.kInvalid)
    fCommands.push_back(fL1_CMD = new RcuCommand(*this, "L1_CMD", 
						 "Enable L1 from CoMmanD",
                                                 fLayout.kC_L1_CMD)); 
  // L1-enable via I2C command
  if (fLayout.kC_L1_I2C != fLayout.kInvalid)
    fCommands.push_back(fL1_I2C = new RcuCommand(*this, "L1_I2C", 
						 "Enable L1 from I2C bus",
                                                 fLayout.kC_L1_I2C)); 
  // L1-enable via TTC command
  if (fLayout.kC_L1_TTC != fLayout.kInvalid)
    fCommands.push_back(fL1_TTC = new RcuCommand(*this, "L1_TTC", 
					 "Enable L1 from Trigger/Timing",
                                                 fLayout.kC_L1_TTC)); 
  // ReaD-out ABORT command
  if (fLayout.kC_RDABORT != fLayout.kInvalid)
    fCommands.push_back(fRDABORT = new RcuCommand(*this, "RDABORT", 
						  "Abort ReaD-out",
						  fLayout.kC_RDABORT)); 
  // ReSet DMEM1 command
  if (fLayout.kC_RS_DMEM1 != fLayout.kInvalid)
    fCommands.push_back(fRS_DMEM1 = new RcuCommand(*this, "RS_DMEM1", 
						   "Reset 1st Data MEMory",
						   fLayout.kC_RS_DMEM1)); 
  // ReSet DMEM2 command
  if (fLayout.kC_RS_DMEM2 != fLayout.kInvalid)
    fCommands.push_back(fRS_DMEM2 = new RcuCommand(*this, "RS_DMEM2", 
						   "Reset 2nd Data MEMory",
						   fLayout.kC_RS_DMEM2)); 
  // ReSet TRigger ConFiGuration command
  if (fLayout.kC_RS_TRCFG != fLayout.kInvalid)
    fCommands.push_back(fRS_TRCFG = new RcuCommand(*this, "RS_TRCFG", 
					   "ReSet TRigger ConFiGuration",
						   fLayout.kC_RS_TRCFG)); 
  // ReSet TRigger CouNTer command
  if (fLayout.kC_RS_TRCNT != fLayout.kInvalid)
    fCommands.push_back(fRS_TRCNT = new RcuCommand(*this, "RS_TRCNT", 
						   "ReSet TRigger CouNTer",
						   fLayout.kC_RS_TRCNT)); 
  // SoftWare TRiGger command
  if (fLayout.kC_SWTRG != fLayout.kInvalid)
    fCommands.push_back(fSWTRG = new RcuCommand(*this, "SWTRG", 
						"Issue a software trigger",
						fLayout.kC_SWTRG)); 
  // -----------------------------------------------------
  // TTC interface 
  // TTC interface reset command (0x4001)
  if (fLayout.kC_TTCReset != fLayout.kInvalid)
    fCommands.push_back(fTTCReset = new RcuCommand(*this, "TTCReset", 
						   "Reset Trigger/Timing",
						   fLayout.kC_TTCReset));
  // TTC interface reset counters command (0x4004)
  if (fLayout.kC_TTCResetCnt != fLayout.kInvalid)
    fCommands.push_back(fTTCResetCnt = new RcuCommand(*this, "TTCResetCnt", 
					      "Reset Trigger/Timing counters",
						      fLayout.kC_TTCResetCnt));
  // TTC interface test mode command (0x4005)
  if (fLayout.kC_TTCTestmode != fLayout.kInvalid)
    fCommands.push_back(fTTCTestmode = new RcuCommand(*this, "TTCTestmode", 
					      "Put Trigger/Timing in test",
						      fLayout.kC_TTCTestmode));
}

//____________________________________________________________________
const std::string&
Rcuxx::Rcu::Version() 
{
  return gVersion;
}

//____________________________________________________________________
const std::string&
Rcuxx::Rcu::Package() 
{
  return gPackage;
}

//____________________________________________________________________
const std::string&
Rcuxx::Rcu::PackageString() 
{
  return gPackageString;
}

//____________________________________________________________________
void
Rcuxx::Rcu::PrintHelp(std::ostream& o)
{
  o << "Supported protocols and interfaces are\n";
#ifdef HAVE_U2F
  U2F::PrintUrlHelp(o);
#endif
#ifdef HAVE_FEECLIENT
  Fee::PrintUrlHelp(o);
#endif
#ifdef HAVE_FED
  Fed::PrintUrlHelp(o);
#endif
#ifdef HAVE_DATE 
  Rorc::PrintUrlHelp(o);
#endif
#ifdef HAVE_DCSC
  Dcsc::PrintUrlHelp(o);
#endif
  Files::PrintUrlHelp(o);
  Coder::PrintUrlHelp(o);
  Echo::PrintUrlHelp(o);
  o << std::endl;
}


//____________________________________________________________________
bool
Rcuxx::Rcu::ReadFile(const std::string& name, std::vector<unsigned int>& data)
{
  std::ifstream stream(name.c_str());
  if (!stream) return false;
  bool ret = ReadFile(stream, data);
  stream.close();
  return ret;
}

//____________________________________________________________________
bool
Rcuxx::Rcu::ReadFile(std::istream& stream, std::vector<unsigned int>& data)
{
  int n = 0;
  while (!stream.eof()) {
    char c = stream.peek();
    if (c == ' ' || c == '\t') {
      stream.get();
      continue;
    }
    if (stream.peek() == '#') {
      std::string line;
      std::getline(stream, line);
      continue;
    }
    unsigned int i;
    stream >> std::hex >> i;
    data.push_back(i);
    n++;
    if (stream.eof()) break;
    if (stream.fail()) return false;
  }
  return true;
}

    
//____________________________________________________________________
Rcuxx::Rcu*
Rcuxx::Rcu::Open(const char* url, bool, bool debug)
{
  Url u(url);
  return Open(u, false, debug);
}

//____________________________________________________________________
Rcuxx::Rcu*
Rcuxx::Rcu::Open(const Url& url, bool, bool debug)
{
  fDebug = debug;
  DebugGuard g(fDebug, "Rcu::Open(%s)", url.Raw().c_str());
  Rcu* ret = 0;
  try {
    if      (url.Scheme() == "pipe")   ret = new Files(url,debug);
    else if (url.Scheme() == "coder")  ret = new Coder(url,debug);
    else if (url.Scheme() == "echo")   ret = new Echo(url,debug);
#ifdef HAVE_U2F
    else if (url.Scheme() == "usb")    ret = new U2F(url,debug);
    else if (url.Scheme() == "u2f")    ret = new U2F(url,debug);
#endif
#ifdef HAVE_FEECLIENT
    else if (url.Scheme() == "fee")    ret = new Fee(url,debug);
#endif
#ifdef HAVE_FED
    else if (url.Scheme() == "fed")    ret = new Fed(url,debug);
#endif
#ifdef HAVE_DATE 
    else if (url.Scheme() == "rorc")   ret = new Rorc(url,debug);
    else if (url.Scheme() == "ddl")    ret = new Rorc(url,debug);
    else if (url.Scheme() == "date")   ret = new Rorc(url,debug);
    else if (url.Scheme() == "siu")    ret = new Rorc(url,debug);
#endif
#ifdef HAVE_DCSC
    else if (url.Scheme() == "dcsc")   ret = new Dcsc(url,debug);
#endif
    if (!ret) {
      std::cerr << "Unsupported URL: " << url.Raw() << std::endl;
      PrintHelp(std::cerr);
    }
    else {
      if (ret->Init(url)) 
	throw std::runtime_error("Failed in init");
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    if (ret) delete ret;
    ret = 0;
  }
  // fDebug = 0;
  return ret;
}

//____________________________________________________________________
unsigned int
Rcuxx::Rcu::ReadDataMemory(unsigned int  lbase, unsigned int hbase, 
			   unsigned int  off, unsigned int& size, 
			   unsigned int* data) 
{
  std::vector<unsigned int> low;
  std::vector<unsigned int> high;
  size_t o = off;
  unsigned int losize = size;
  unsigned int hosize = size;
  unsigned int r;
  r = ReadMemory(lbase, o, losize, &(low[0]));
  r = ReadMemory(hbase, o, losize, &(high[0]));
  if (losize != hosize) return kInconsistentSizes;
  if (losize != size)   return kNotEnough;
  if (hosize != size)   return kNotEnough;
  size = losize;
  for (size_t i = 0; i < size; i++) {
    data[4*i+0] = low[i]          & 0x3ff;
    data[4*i+1] = (low[i]  >> 10) & 0x3ff;
    data[4*i+2] = high[i]         & 0x3ff;
    data[4*i+3] = (high[i] >> 10) & 0x3ff;
  }
  return r;
}

//____________________________________________________________________
unsigned int
Rcuxx::Rcu::WriteDataMemory(unsigned int  lbase, unsigned int hbase, 
			    unsigned int  off, unsigned int& size, 
			    unsigned int* data) 
{
  std::vector<unsigned int> low;
  std::vector<unsigned int> high;
  size_t o = off;
  unsigned int losize = size;
  unsigned int hosize = size;
  unsigned int r;
  for (size_t i = 0; i < size; i++) {
    low[i]  = (data[4 * i + 0] & 0x3ff) + ((data[4 * i + 1]) << 10);
    high[i] = (data[4 * i + 2] & 0x3ff) + ((data[4 * i + 3]) << 10);
  }
  r = WriteMemory(lbase, o, losize, &(low[0]));
  r = WriteMemory(hbase, o, hosize, &(low[0]));
  return r;
}

  
//____________________________________________________________________
unsigned int
Rcuxx::Rcu::Update() 
{
  DebugGuard g(fDebug, "Rcu::Update");
  unsigned int ret = 0;
  for (Registers::iterator i = fRegisters.begin(); 
       i != fRegisters.end(); ++i) {
    if (!*i) continue;
    if ((ret = (*i)->Update())) return ret;
  }
  for (Memories::iterator i = fMemories.begin(); 
       i != fMemories.end(); ++i) {
    if (!*i) continue;
    if (*i == fDM1  || *i == fDM2)  continue;
    if (*i == fREGS || *i == fMONS) continue;
    if ((ret = (*i)->Update())) return ret;
  }
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Rcu::Print() const
{
  DebugGuard g(fDebug, "Rcu::Print");
  for (Registers::const_iterator i = fRegisters.begin(); 
       i != fRegisters.end(); ++i) {
    if (!*i) continue;
    (*i)->Print();
  }
  for (Memories::const_iterator i = fMemories.begin(); 
       i != fMemories.end(); ++i) {
    if (!*i) continue;
    if (*i == fDM1  || *i == fDM2)  continue;
    if (*i == fREGS || *i == fMONS) continue;
    (*i)->Print();
  }
}

//____________________________________________________________________
void
Rcuxx::Rcu::SetDebug(int id, int lvl) 
{
  // if (id == 0) DebugGuard::fDebug = (lvl > 0);
  switch (id) {
  case kRcu:     
    fDebug = lvl > 0; 
    if (fIMEM) fIMEM->SetDebug(fDebug);
    if (fRMEM) fRMEM->SetDebug(fDebug);
    break;
  }
}

//____________________________________________________________________
std::string
Rcuxx::Rcu::ErrorString(unsigned int code) 
{
  std::cout << "Checking error code " << code << std::endl;
  switch (code) {
  case kNotEnough: 
    return std::string("Not enough data returned");
  case kInconsistentSizes:
    return std::string("Inconsistent size when reading data memory");
  case kUnknownMemory:
    return std::string("Unknown memory bank addressed");
  case kInstructionMismatch:
    return std::string("Got wrong instruction code back");
  case kAddressMismatch:
    return std::string("Got wrong address back");
  case kDataMismatch:
    return std::string("Got wrong data back");
  case kNotWriteable:
    return std::string("Not a writeable register");
  case kCannotBroadcast:
    return std::string("Cannot broadcast this register");
  case kNoAcknowledge:
    return std::string("No acknowledge from FEC");
  case kNotActive:
    return std::string("FEC not active");
  }
  if (code >= kRMemError) { 
    std::string ret("Error(s) from IMEM decoder: ");
    unsigned int sub = code - kRMemError;
    for (size_t i = 0; i < 6; i++) { 
      if ((sub & (0x1 << i)) != 0x0) { 
	switch (i) { 
	case 0: ret += "Loop error, "; break;
	case 1: ret += "Invalid RCU instruction, "; break;
	case 2: ret += "Invalid BC instruction, "; break;
	case 3: ret += "Invalid BC address, "; break;
	case 4: ret += "Invalid ALTRO instruction, "; break;
	case 5: ret += "Invalid ALTRO address "; break;
	}
      }
    }
    unsigned int aim = (code >> 6) & 0x1F;
    switch (aim) { 
    case 0x9: ret += "Error asserted before CSTB";          break;
    case 0xa: ret += "Error asserted before ACK";           break;
    case 0xb: ret += "Premature ACK";                       break;
    case 0xc: ret += "Error asserted during wait for ACK";  break;
    case 0xd: ret += "No ACK - card not on?";               break;
    case 0xe: ret += "Error asserted during wait for !ACK"; break;
    case 0xf: ret += "No release of ACK";                   break;
    }
    return ret;
  }
  return std::string();
}


//====================================================================
//
// EOF
//
