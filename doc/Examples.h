/*
 * $Id: Examples.h,v 1.2 2009-02-09 22:38:13 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Examples.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Arhitecture documentation. */
/** @page exa Examples

    Here are some small examples 

*/
/** @example mainAcq.cxx
    @note Simple Acquisition program 
*/
/** @example rcuexec.cxx
    @note Execute Instruction memory programs 
*/
/** @example rwr_trcfg.cxx
    @note Read/write some registers 
*/
/** @example test.cxx 
    @note A simple test 
*/

#error Not for compilation
//
// EOF
//
