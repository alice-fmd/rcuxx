/* -*- mode: C++ -*- 
 *
 * $Id: Deps.h,v 1.2 2009-02-09 22:38:13 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Deps.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Arhitecture documentation. */
/** @page deps Dependencies

    The core package only requires that you have a C++.  The package
    has been tested with <a href="http://gcc.gnu.org">GCC</a> and 
    MSVC++. 

    When building on Unix (or the like), use the well-known triplet to
    configure, build, and install the library: 

    @verbatim 
    ./configure 
    make 
    make install 
    @endverbatim 

    Using MSVC++, please use the provided project file @c
    rcuxx.vcproj. 

    @section opts Optional Requirements

    If the <a href="http://fmd.nbi.dk/fmd/fee/#altrocc">ALTROCC</a>
    compiler is installed on the system, then this package will in
    addition to the library build a program @c rcuexec, which can
    compile and execute on the fly programs in the RCU IMEM. 

    @section prot Protocol Requirements 

    Each of the communication protocols  have their own set of
    requirements in terms of 3rd party software. 

  - @b Fed  Requires the <a
    href="http://fmd.nbi.dk/fmd/fee/#dim">DIM</a> client library.   
  - @b Fee  Requires the <a
    href="http://fmd.nbi.dk/fmd/fee/#feeclient">FeeClient </a> library
    (this in turn requires the DIM client 
    library). 
  - @b Rorc Requires the <a href="http://cern.ch/ph-dep-aid/">RORC</a>
    library from @b DATE.  
  - @b U2F  Requires the <a href="http://fmd.nbi.dk/fmd/fee">ALTRO</a>
    library 
  - @b Pipe Requires a properly implemented VHDL test-bench (see for
    example <a href="http://fmd.nbi.dk/fmd/fee/#fmdd_bc">the FMDD
    firmware</a>).  
    
    
*/
/*
 * EOF
 */
