/* -*- mode: C++ -*-
 *
 * $Id: Mainpage.h,v 1.6 2009-02-09 22:38:13 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Mainpage.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage ALICE RCU Abstraction layer 

    @section intro Introduction 
    
    This package provides a C++ class library for accessing the 
    <a href="http://cern.ch/ep-ed-alice-tpc/u2f.htm">RCU</a>
    internal structures via an abstract interface.   

    The actual form of communication is hidden to the programmer using
    the C++ class library.  That is, the programmer will always see
    the same interface, no matter what underlying protocol is used.
    The actual protocol used can be selected at run-time by passing a
    valid URL-like string to the static member function
    Rcuxx::Rcu::Open.  In this way, a programmer can design a
    program that can be run in many different environments without
    changing a single line of code.

    Currently, the following access protocols are supported. 

    - @b Fed  Communicate with the RCU via an InterCom Layer broker.  
    - @b Fee  Communicate with the RCU via a FeeServer (with an RCU
      Control Engine) running on a <a href="http://www.kip.uni-heidelberg.de/ti/DCS-Board/current/">DCS daughter board</a>.  
    - @b Rorc Communicate with the RCU via the optical 
      <a href="http://cern.ch/ph-dep-aid/">Detector Data Link</a>
    - @b U2F  Communicate with the test RCU on a test 
      <a href="http://cern.ch/ep-ed-alice-tpc/u2f.htm">U2F</a> card
    - via a USB2 interface.  
    - @b Pipe Communicate with a simulated RCU via pipes. 

    
    - @subpage deps
    - @subpage rpms
    - @subpage arch
    - @subpage links
*/

#error Not for compilation
//
// EOF
//
