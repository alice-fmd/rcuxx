/* -*- mode: C++ -*-
 *
 * $Id: Links.h,v 1.2 2009-02-09 22:38:13 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Links.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Arhitecture documentation. */
/** @page links Related Packages, Projects, and Links 

    @section hehi Other packages by the NBI HEHI group. 

    These packages are all developed by the <a
    href="http://www.nbi.dk">NBI </a> <a
    href="http://www.nbi.dk/hehi">HEHI</a> group. 
    Comments, questions, bugs, etc. should be sent to @c cholm@@nbi.dk 

    - <a href="http://fmd.nbi.dk/fmd/fee/#altrocc">ALTROCC:</a> 
      compiler for RCU insstruction memory programs 
    - <a href="http://fmd.nbi.dk/fmd/fee/#readraw">ReadRaw:</a> 
      Read raw ALTRO data, either from event stream or file.  This
      library provides an abstraction layer to read raw data and
      decode it as needed.  The programmer derives classes from
      various visitors to process the data. 
    - <a href="http://fmd.nbi.dk/fmd/fee/#rcudata">RcuData:</a>
      Structure ALTRO data into <a href="http://root.cern.ch">ROOT</a>
      structures, and provide abstract I/O on these.  Can use the @b
      ReadRaw library to read raw ALTRO data from the event stream or
      from file, and recode it into ROOT 
      <a href="http://root.cern.ch/root/html/TTree.html">TTrees</a>.
    - <a href="http://fmd.nbi.dk/fmd/fee/#rcugui">RcuGui:</a> 
      ROOT based GUI tools to control the RCUs (if this library is
      installed), display data (if RcuData is installed), and other
      such things. 
    - <a href="http://fmd.nbi.dk/fmd/fee/#pvss-rcuxx">Pvss-Rcu++:</a> 
      <a href="http://www.pvss.com">PVSS II</a> control script
      language extension plug-in for calling this library from PVSS II
      scripts. 

    @section third 3rd Party packages used or linked to Rcu++ 

    - <a href="http://ztt.worms.de">Intercom Layer:</a> Provides
      service and command infrastructure for a full detector FEE
      system. 
    - <a href="http://ztt.worms.de">FeeServer:</a> Service and command
      server running on embedded devices close to the Front-End
      Electronics.  Provides a uniform way of communicating with the
      hardware in question. 
    - <a href="http://">RCU Control Engine:</a> driver, driver
      library, and FeeServer implementation specific to the RCU. 
    - <a href="http://cern.ch/ph-dep-aid/">DATE:</a> ALICE Data
      Acquisition suit.  Contains the RORC interface library and
      drivers, and other things related the actual read-out of the
      detectors in ALICE. 
      @note Access to the DATE software and sources is restricted. 
    - <a href="http://cern.ch/ep-ed-alice-tpc/u2f.htm">U2F: </a> 
      Driver and library for the test RCU on a test card via a USB2
      interface.  
    
    @section other Other similar libraries and approaches 

    - Roland Bramms <a
      href="https://subversion.gsi.de/alice/tpc-fee">RCU</a> software
      (subversion repository).  This has some mapping of hardware into
      C++ classes, a CommandCoder to do encoding of configurations
      send to the RCU, and similar.  
    - Ulrich Frankenfelds <a
      href="http://franken.web.cern.ch/franken/">web-pages</a>. Has
      some PVSS code.  
    
    @section info Informative links 

    - <a href="http://cern.ch/ep-ed-alice-tpc/">ALICE TPC FEE</a> 
    - <a href="http://fmd.nbi.dk/fmd/fee/">ALICE FMD FEE</a> 
    - <a href="http://">Bergen DCS Wiki</a>
    - <a href="http://cern.ch/ph-dep-aid/">ALICE DAQ</a> 
    - <a href="http://www.kip.uni-heidelberg.de/ti/DCS-Board/">DCS
      board</a>
    - <a href="http://www.gsi.de/forschung/kp/kp1/experimente/alice/tpc/index.html">GSI TPC</a> pages. 
    - 
*/

#error Not for compilation
//
// EOF
//
