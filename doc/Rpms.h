/* -*- mode: C++ -*- 
 *
 * $Id: Rpms.h,v 1.5 2009-02-09 22:38:13 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Rpms.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Arhitecture documentation. */
/** @page rpms Building RPMs

    The source package contains a valid RPM spec file. This is created
    the configure script.   You can therefor build a set  of RPMS
    directly from the source tar-ball, by doing
    @verbatim 
    rpmbuild -ta rcuxx-0.22.tar.gz 
    @endverbatim 

    A total of 3 binary packages are build 

    - @c rcuxx@e soversion Contains the run-time library.  Note, that
      the interface version (the so-called @e SO @e version) is part
      of the name (@e soversion). This allows you to have several
      versions of the run-time library installed at the same time with
      out conflicts.

    - @c rcuxx-devel Contains the static library, the development
      library symbolic link, the headers, some support utilities (@b
      Autoconf macro, @b PKG-CONFIG file, and a shell script), as well
      as this documentation.  Note, that the SO version is @e not part
      of the name.  That's because it does not make sense to more than
      one version installed, and because it would generate conflicts.

    - @c rcuexec A small program to execute commands in the RCU
      instruction memory. 

    The built package supports all interfaces, as well as the ALTROCC
    instruction compiler. 

    It is possible to built RPMs for specific sub-systems of ALICE,
    which only contain the code needed on that system.  Currently only
    2 such systems is defined: @e DAQ and @e DCS.  

    To build packages for the DCS sub-system (Fed client only), do 

    @verbatim
    rpmbuild -ta -D "variant dcs" rcuxx-0.22.tar.gz 
    @endverbatim 

    The generated packages will be named @c rcuxx-dcs@e soversion, @c
    rcuxx-dcs-devel, and @c rcuexex-dcs.

    To build packages for the DAQ sub-system (Rorc client only), do 

    @verbatim
    rpmbuild -ta -D "variant daq" rcuxx-0.22.tar.gz 
    @endverbatim 
    
    The generated packages will be named @c rcuxx-daq@e soversion, @c
    rcuxx-daq-devel, and @c rcuexex-daq.
*/
/*
 * EOF
 */
