dnl
dnl $Id: ac_feeclient.m4,v 1.1 2012-03-07 09:32:37 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl __________________________________________________________________
AC_DEFUN([AC_FEECLIENT],
[
  # Template for autoheader 
  AH_TEMPLATE([HAVE_FEECLIENT], [Whether we have the FeeClient library])

  # Command line arguments 
  AC_ARG_WITH(feeclient,
              [AC_HELP_STRING([--with-feeclient],
	                      [Top of FeeClient installation])])

  #Let's use he C++ compiler 
  AC_LANG_PUSH(C++)
  have_feeclient=no

  # set some variables we need 
  FEECLIENT_LIBS="-lFeeClient -ldim -pthread"
  if test "x$with_feeclient" = "xyes" || test "x$with_feeclient" = "x" ; then
    FEECLIENT_LDFLAGS=
    FEECLIENT_CPPFLAGS=
  elif test "x$with_feeclient" = "xno" ; then 
    have_feeclient=no
  else
    FEECLIENT_LDFLAGS="-L$with_feeclient/lib -pthread"
    FEECLIENT_CPPFLAGS="-I$with_feeclient/include"
  fi

  # save compiler and linker flags, and set them to new values
  if test ! "x$with_feeclient" = "xno" ; then 
    save_CPPFLAGS=$CPPFLAGS
    save_LDFLAGS=$LDFLAGS
    save_LIBS=$LIBS
    CPPFLAGS="$save_CPPFLAGS $FEECLIENT_CPPFLAGS"
    LDFLAGS="$save_LDFLAGS $FEECLIENT_LDFLAGS"
    LIBS="$save_LIBS $FEECLIENT_LIBS"
  
    # A simple check for a header 
    AC_CHECK_HEADERS([feeclient/FeeClientLibInterface.hpp],
                     [have_feeclient=yes])
  
    # Now check he library 
    if test "x$have_feeclient" = "xyes" ; then
      AC_MSG_CHECKING(for dcs::fee::FeeClientCore in -lFeeClient)
      AC_LINK_IFELSE([
         AC_LANG_PROGRAM([#include <feeclient/FeeClientLogger.hpp>],
  	  	       [dcs::fee::FeeClientLogger::getLogger()])],
  	               [AC_DEFINE(HAVE_FEECLIENT)], 
  	               [have_feeclient=no])
      LIBS=$save_LIBS
      AC_MSG_RESULT($have_feeclient)
    fi
  fi
  # Go back to the old language
  AC_LANG_POP(C++)

  # Set some output variables. 
  if test "x$have_feeclient" = "xno" ; then 
      FEECLIENT_LIBS=
      FEECLIENT_LDFLAGS=
      FEECLIENT_CPPFLAGS=
  fi

  # Restore old values of compiler and linker flags. 
  CPPFLAGS=$save_CPPFLAGS
  LDFLAGS=$save_LDFLAGS
  LIBS=$save_LIBS

    if test "x$have_feeclient" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else 
        ifelse([$2], , :, [$2])
    fi

  # Output the substitution variables. 
  AC_SUBST(FEECLIENT_LIBS)
  AC_SUBST(FEECLIENT_LDFLAGS)
  AC_SUBST(FEECLIENT_CPPFLAGS)
])
dnl
dnl EOF
dnl
