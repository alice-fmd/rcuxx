dnl
dnl $Id: ac_check_u2f.m4,v 1.1 2012-03-07 09:32:37 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_CHECK_U2F],
[
   AC_ARG_ENABLE([u2f],
	         [AC_HELP_STRING([--enable-u2f],
	                         [Enable U2F backend])])
   ac_tmp_have_u2f=no
   AH_TEMPLATE([HAVE_U2F], [Whether we have the U2F library])
   if test ! "x$enable_u2f" = "xno" ; then 
     AC_MSG_CHECKING([for altro-config])
     AC_PATH_PROG([U2F_CONFIG], [altro-config])
     if test "x$U2F_CONFIG" = "x" ; then 
       :
     else
       u2f_min_version=ifelse([$1], ,0.1,$1)
       needed=$(echo $1|awk 'BEGIN {FS="."}{printf "%d",(($''1*1000)+$''2)*1000+$''3}')
       AC_MSG_CHECKING(if libaltro version >= $u2f_min_version)
       U2F_VERSION=$($U2F_CONFIG --version | sed -n 's/ALICE RCU\/U2F //p' )
       found=$(echo $U2F_VERSION | awk 'BEGIN{FS="."}{printf "%d",(($''1*1000)+$''2)*1000+$''3}')
       if test $found -ge $needed ; then 
         ac_tmp_have_u2f=yes
       fi
       AC_MSG_RESULT([$ac_tmp_have_u2f found $U2F_VERSION])
     fi
     if test "x$ac_tmp_have_u2f" = "xyes" ; then 
       save_LDFLAGS=$LDFLAGS
       save_CPPFLAGS="$CPPFLAGS"
       LDFLAGS="$save_LDFLAGS -L`$U2F_CONFIG --libdir`"
       CPPFLAGS="$save_CPPFLAGS `$U2F_CONFIG --cppflags`"
       AC_CHECK_LIB([u2f], [U2F_Open], 
                    [AC_CHECK_HEADERS([altro/u2f.h],
                                      [ac_tmp_have_u2f=yes],
                                      [ac_tmp_have_u2f=no])],
                    [ac_tmp_have_u2f=no],	[$($U2F_CONFIG --libs altro)])
       LDFLAGS="$save_LDFLAGS"
       CPPFLAGS="$save_CPPFLAGS"
     fi
     if test "x$ac_tmp_have_u2f" = "xyes" ; then
       U2F_LIBS=`$U2F_CONFIG --libs u2f`
       U2F_CPPFLAGS=`$U2F_CONFIG --cppflags` 
       U2F_LIBDIR=`$U2F_CONFIG --libdir` 
       U2F_LDFLAGS="-L$U2F_LIBDIR -R $U2F_LIBDIR"
       AC_DEFINE(HAVE_U2F)
     fi
   fi
   AC_SUBST(U2F_LIBS)
   AC_SUBST(U2F_CPPFLAGS)
   AC_SUBST(U2F_LDFLAGS)

  if test "x$ac_tmp_have_u2f" = "xyes" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])
dnl
dnl EOF
dnl
