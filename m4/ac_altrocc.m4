dnl
dnl $Id: ac_altrocc.m4,v 1.1 2012-03-07 09:32:37 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl __________________________________________________________________
dnl AC_ALTROCC([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ALTROCC],
[
    AC_ARG_WITH([altrocc], 
    	        [AC_HELP_STRING([--with-altrocc],
    	                        [Prefix of ALTROCC installation])],
		[with_altrocc=$withval],[with_altrocc=yes])
    save_LDFLAGS=$LDFLAGS
    save_CPPFLAGS=$CPPFLAGS
    if test ! "x$with_altrocc" = "xyes" && \
       test ! "x$with_altrocc" = "xno" ; then 
      ALTROCCLDFLAGS="-L$with_altrocc/lib/altrocc"
      ALTROCPPFLAGS="-I$with_altrocc/include"
      LDFLAGS="$LDFLAGS $ALTROCCLDFLAGS" 
      CPPFLAGS="$CPPFLAGS $ALTROCPPFLAGS"
      with_altrocc=yes
    elif test "x$with_altrocc" = "xyes" ; then 
      with_altrocc=yes
    else
      with_altrocc=no
    fi
    have_altrocc_compiler_h=0
    AH_TEMPLATE(HAVE_ALTROCC_COMPILER_H, [Whether we have ALTROCC header])
    if test "x$with_altrocc" = "xyes" ; then 
      AC_CHECK_HEADER([altrocc/compiler.h], [have_altrocc_compiler_h=1])
      have_libaltrocc=0
      AC_CHECK_LIB([altrocc], [RCUC_compile], [have_libaltrocc=1])
      if test $have_libaltrocc -gt 0 && \
  	 test $have_altrocc_compiler_h -gt 0; then 
        AC_DEFINE(HAVE_ALTROCC_COMPILER_H)
        with_altrocc=yes
        ALTROCCLIB=-laltrocc
      else 
        with_altrocc=no
        ALTROCCLDFLAGS=
        ALTROCCPPFLAGS=
      fi				     
      LDFLAGS="$save_LDFLAGS"
      CPPFLAGS="$save_CPPFLAGS"
    fi 
    if test "x$with_altrocc" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST([ALTROCCLDFLAGS])
    AC_SUBST([ALTROCCCPPFLAGS])
    AC_SUBST([ALTROCCLIB])
])
dnl
dnl EOF
dnl 
