dnl
dnl $Id: ac_date.m4,v 1.1 2012-03-07 09:32:37 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl __________________________________________________________________
AC_DEFUN([AC_DATE],
[
    AC_ARG_ENABLE([date],
	          [AC_HELP_STRING([--enable-date],
	                          [Enable RORC backend])])
    AC_ARG_ENABLE([fec2rorc],
	          [AC_HELP_STRING([--enable-fec2rorc],
	                          [Enable use of fec2rorc lib])])

    AH_TEMPLATE(HAVE_DATE, [Whether date is installed])
    AH_TEMPLATE(DATE_PHYSMEN_FD, [Whether we use an int for the physmen fd])
    AH_TEMPLATE(DATE_USE_FEC2RORC, [Whether to fec2rorc library])
    AH_TEMPLATE(DATE_VERSION, [Encoded version of DATE])
 
    need_physmem=yes
    need_fec2rorc=no
    have_date=no
    if test ! "x$enable_date" = "xno" ; then 
      AC_PATH_PROG(DATE_CONFIG,date-config)
      if test ! "x$DATE_CONFIG" = "x" ; then 
	echo "Executing $DATE_CONFIG --cflags"
        DATE_CPPFLAGS=`/bin/sh $DATE_CONFIG --cflags`
        DATE_LIBSS=`/bin/sh $DATE_CONFIG --libs`
        DATE_RORC=`/bin/sh $DATE_CONFIG --rorclibs`

	date_min_version=ifelse([$1], ,0.1,$1)
	AC_MSG_CHECKING(for DATE version >= $date_min_version)
        date_version=`$DATE_CONFIG --version` 
        date_vers=`echo $date_version | \
          awk 'BEGIN { FS = "."; } \
 	    { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
        date_regu=`echo $date_min_version | \
          awk 'BEGIN { FS = "."; } \
	    { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
	AC_MSG_RESULT([$date_version])
	AC_DEFINE_UNQUOTED(DATE_VERSION, $date_vers)
        if test $date_vers -ge $date_regu ; then 
          have_date=yes 
          need_fec2rorc=yes
	  #for i in $DATE_RORC ; do 
	  #  j=`basename $i .o` 
	  #  AC_LIBOBJ($j)
          #done
	  #AC_LIBOBJ(rorc_lib_p)
	  #AC_LIBOBJ(rorc_aux)
	  #AC_LIBOBJ(rorc_ddl_p)
        fi
	if test $date_vers -lt 7000000 ; then 
	  AC_DEFINE([DATE_PHYSMEN_FD])
          need_physmem=no
	fi
	AC_MSG_CHECKING([whether we need physmem stuff])
	if test "x$need_physmem" = "xyes" ; then 
	  DATE_CPPFLAGS="${DATE_CPPFLAGS} -I${DATE_PHYSMEM_DIR}"
	  DATE_RORC="${DATE_PHYSMEM_BIN}/physmem_lib.o ${DATE_RORC}"
        fi
	AC_MSG_RESULT([$need_physmem])
	AC_MSG_CHECKING([whether we need fec2rorc stuff])
	if test "x$need_fec2rorc" = "xyes" ; then
	  AC_DEFINE([DATE_USE_FEC2RORC])
          DATE_CPPFLAGS="${DATE_CPPFLAGS} -I${DATE_FEC2RORC_DIR}"
    	  DATE_RORC="${DATE_FEC2RORC_BIN}/fec2rorc_lib.o ${DATE_RORC}"
	fi
	AC_MSG_RESULT([$need_fec2rorc])
      fi
    fi
    AC_MSG_CHECKING(for DATE installation)
    AC_MSG_RESULT($have_date) 

    if test "x$have_date" = "xyes" ; then 
        AC_DEFINE(HAVE_DATE)
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi

    #AC_SUBST(LTLIBOBJS)
    AC_SUBST(DATE_CPPFLAGS)
    AC_SUBST(DATE_LIBSS)
    AC_SUBST(DATE_RORC)     
])
dnl
dnl EOF
dnl
