#! /usr/bin/rpm
%define  sovers         			@sovers@
%define _unpackaged_files_terminate_build  	0
%define build_all 				0
%define need_dim				1
%define need_date				1
%define need_fee				1
%define need_arm				0
%define rel_post				%nil 
%define conf_args0				--prefix=%{_prefix} --sysconfdir=/etc --disable-u2f --without-altrocc
%define pkg_prefix				%nil
%define is_variant				0

# Options for build various variants of this package.  
# One should pass the option '-D "macro value"' to rpmbuild.
# For example to build for DCS, do 
#
#   rpmbuild -D "dcs 1" @PACKAGE@-@VERSION@.tar.gz
#
# To build for DAQ, do 
#
#   rpmbuild -D "daq 1" @PACKAGE@-@VERSION@.tar.gz 
#
%if %{?variant:1} %{!?variant:0}
 %define rel_post _%{variant}
 %define is_variant 	1
 %define build_all  	0
 %if %{variant} == "daq"
  %define conf_args 	%{conf_args0} --disable-fed --without-feeclient 
  %define need_dim  	0
  %define need_fee  	0
 %else
  %if %{variant} == "dcs"
   %define conf_args 	%{conf_args0} --disable-date --disable-documentation
   %define need_date 	0
  %else 
   %if %{variant} == "arm" 
    %define _prefix     /home/dcs
    %define _lib        lib
    %define _mandir     %{_prefix}/share/man
    %define conf_args 	--host=arm-linux CC=arm-linux-gcc CXX=arm-linux-g++ CPPFLAGS=-I%{_includedir} LDFLAGS=-L%{_libdir} --prefix=%{_prefix} PATH=/usr/local/bin:%{_prefix}/bin:$PATH --disable-documentation --disable-readline --disable-shared --enable-static 
    %define need_arm        1
    %define need_date	0
    %define need_fee	0
    %define pkg_prefix -arm
    %define __strip    /bin/true
    %define __objdump  /bin/true
    %define debug_package %{nil}
    %define _find_debuginfo_opts -g
   %else
    %{error: Unknown variant %{variant}}
    %quit
   %endif
  %endif
 %endif
%else
 %define conf_args %{conf_args0}
%endif

# --------------------------------------------------------------------
Summary: 	C++ interface to RCU 
Name: 		@PACKAGE@%{?pkg_prefix}%{sovers}
Version: 	@VERSION@
Release: 	@RELEASE@%{?rel_post}
License: 	GPL
Group: 		Development/Libraries
URL: 		http://cern.ch/cholm
Source0: 	@PACKAGE@-%{version}.tar.gz
BuildRoot: 	%{_tmppath}/@PACKAGE@-%{version}-%{release}-buildroot
%if %{build_all}
BuildRequires:	altro-devel
BuildRequires:	altrocc-devel
BuildRequires:	feeclient-devel
Provides:	@PACKAGE@(U2F)
Provides:	@PACKAGE@(ALTROCC)
Provides:	@PACKAGE@(FEE)
%endif
%if %{need_date}
BuildRequires:  date
Provides:	@PACKAGE@(RORC)
%endif
%if %{need_fee}
BuildRequires:  feeclient-devel
Provides:	@PACKAGE@(FEE)
%endif
%if %{need_dim}
BuildRequires:  dim-devel
Provides:	@PACKAGE@(FED)
%endif
%if %{need_arm}
BuildRequires:  arm-uclibc-toolchain
Provides:	@PACKAGE@(DCSC)
%endif
# Conflicts:	rcuxx < 0.17-4

%description -n %{name}
C++ interface to RCU 

%files -n %{name}
%defattr(-,root,root)
%if !%{need_arm}
%{_libdir}/lib*.so.*
%endif

# --------------------------------------------------------------------
%package -n rcuexec%{?pkg_prefix}
Summary:	C++ interface to RCU 
Group:		Development/System
%if %{need_arm}
%else
Requires:	%{name}
%endif
Conflicts:	rcuxx < 0.17-4
%if %{is_variant}
Provides:	rcuexec
#Conflicts:	rcuexec
%endif
%description -n rcuexec%{?pkg_prefix}
C++ interface to RCU 

%files -n rcuexec%{?pkg_prefix}
%{_bindir}/rcuexec
%{_bindir}/rcuxx
%{_mandir}/man1/rcuexec.1*
%{_mandir}/man1/rcuxx.1*

# --------------------------------------------------------------------
%package -n @PACKAGE@%{?pkg_prefix}-devel
Summary:	C++ interface to RCU 
Group:		Development/System
Requires:	%{name} = @VERSION@-%{release}
%if %{build_all}
Provides:	@PACKAGE@-devel(U2F)
Provides:	@PACKAGE@-devel(ALTROCC)
Provides:	@PACKAGE@-devel(FEE)
%endif
%if %{need_date}
BuildRequires:  date
Provides:	@PACKAGE@-devel(RORC)
%endif
%if %{need_dim}
Provides:	@PACKAGE@-devel(FED)
%endif
%if %{need_arm}
Provides:	@PACKAGE@-devel(DCSC)
%endif
%description -n @PACKAGE@%{?pkg_prefix}-devel
C++ interface to RCU 

%files -n @PACKAGE@%{?pkg_prefix}-devel
%defattr(-,root,root)
%{_includedir}/@PACKAGE@
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%if !%{need_arm}
%{_libdir}/lib*.so
%endif
%{_libdir}/pkgconfig/*
%{_bindir}/@PACKAGE@-config
%{_mandir}/man1/@PACKAGE@-config.1*
%{_prefix}/share/aclocal/@PACKAGE@.m4
%{_prefix}/share/doc/@PACKAGE@

# --------------------------------------------------------------------
%prep
%setup -q -n @PACKAGE@-%{version}

%build
%if %{need_date}
. /date/setup.sh
%endif
%if %{need_arm}
./configure %{conf_args} --mandir=%{_mandir}
export PATH=/usr/local/bin:$PATH
%else 
%{configure} %{conf_args} --mandir=%{_mandir} 
%endif

make

%install
%if %{need_arm}
export PATH=%{_bindir}:${PATH}:/usr/local/bin
%endif
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install 
%if %{need_arm}
export STRIP=/bin/true
export OBJDUMP=/bin/true
%endif

%clean
make clean
rm -rf $RPM_BUILD_ROOT

# --------------------------------------------------------------------
%changelog
* Mon Oct 31 2011 Christian Holm Christensen <cholm@himac02.nbi.dk>
- New release

* Thu Mar 10 2011 Christian Holm Christensen <cholm@nbi.dk>
- new release with some bug fixes for RORC access, again

* Thu Mar 10 2011 Christian Holm Christensen <cholm@nbi.dk>
- new release with some bug fixes for RORC access

* Fri Feb 18 2011 Christian Holm Christensen <cholm@nbi.dk>
- Fixes for DATE 7

* Fri Feb 18 2011 Christian Holm Christensen <cholm@nbi.dk>
- Fixes for DATE 7

* Fri Feb 18 2011 Christian Holm Christensen <cholm@nbi.dk>
- New release for newer physmem

* Tue Jul 06 2010 Christian Holm Christensen <cholm@nbi.dk>
- New version number and soversion

* Thu Jun 03 2010 Christian Holm Christensen <cholm@nbi.dk>
- Split of ACL

* Wed Jun 02 2010 Christian Holm Christensen <cholm@nbi.dk>
- New version

* Wed May 26 2010 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Thu Apr 29 2010 Christian Holm Christensen <cholm@nbi.dk>
- Fix SCCLK bug

* Thu Apr 29 2010 Christian Holm Christensen <cholm@nbi.dk>
- Bug fixes in compare methods

* Wed Apr 28 2010 Christian Holm Christensen <cholm@nbi.dk>
- New releae

* Tue Apr 27 2010 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Tue Mar 23 2010 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Tue Mar 23 2010 Christian Holm Christensen <cholm@nbi.dk>
- New version - titles on everything

* Sat Mar 20 2010 Christian Holm Christensen <cholm@nbi.dk>
- New release - support for new MSM interrupt handler interface

* Mon Mar 08 2010 Christian Holm Christensen <cholm@nbi.dk>
- Added DCSC interface

* Mon Jul 27 2009 Christian Holm Christensen <cholm@cholm.priv,nbi.dk>
- New

* Mon Jul 27 2009 Christian Holm Christensen <cholm@cholm.priv,nbi.dk>
- New

* Mon Jul 06 2009 Christian Holm Christensen <cholm@cholm.priv,nbi.dk>
- Can now specify serial, revision, and channel for RORC interface

* Fri Jun 19 2009 Christian Holm Christensen <cholm@cholm.priv,nbi.dk>
- New releae

* Thu Feb 12 2009 Christian Holm Christensen <cholm@nbi.dk>
- Enable optimisation of ACL

* Fri Sep 26 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release - for the heck of it

* Thu Sep 25 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fixes to RORC interface

* Wed Sep 17 2008 Christian Holm Christensen <cholm@nbi.dk>
- Minor bug fix

* Tue Sep 16 2008 Christian Holm Christensen <cholm@nbi.dk>
- New soversion

* Tue Sep 16 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release w/RCU id and proper ZS glitch

* Thu Sep 04 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Wed Sep 03 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Tue Aug 12 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release - merge of changes from alifmdwn02

* Fri Aug 01 2008 Christian Holm Christensen <cholm@nbi.dk>
- Add MEBs

* Thu Jul 10 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Thu Jun 26 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Fri Jun 13 2008 Christian Holm Christensen <cholm@nbi.dk>
- Added BC instr mask

* Fri Jun 06 2008 Christian Holm Christensen <cholm@nbi.dk>
- New version

* Tue Jun 03 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fixes

* Tue Jun 03 2008 Christian Holm Christensen <cholm@nbi.dk>
- I2C access, and I2C AFL fix for RCU II

* Thu May 29 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fix for full result address return - thank you\!

* Thu May 29 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Thu May 29 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Wed Apr 16 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release

* Tue Apr 15 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fixed write access to new registers

* Mon Apr 14 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fix version recognition

* Sun Apr 13 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fix ACTFEC address

* Fri Apr 11 2008 Christian Holm Christensen <cholm@nbi.dk>
- New release - for the heck of it

* Fri Apr 11 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fix version detection

* Fri Apr 11 2008 Christian Holm Christensen <cholm@nbi.dk>
- Fixes for FW version

* Wed Apr 02 2008 Christian Holm Christensen <cholm@nbi.dk>
- Added some functions to ACL

* Tue Apr 01 2008 Christian Holm Christensen <cholm@nbi.dk>
- New version and so version

* Wed Jul 11 2007 Christian Holm Christensen <cholm@nbi.dk>
- New version and so version

* Fri Apr 27 2007 Christian Holm Christensen <cholm@nbi.dk>
- Also install documentation

* Sat Apr 07 2007 Christian Holm Christensen <cholm@nbi.dk>
- New release to mark installation

* Tue Apr 03 2007 Christian Holm Christensen <cholm@nbi.dk>
- Updated soversion

* Tue Mar 20 2007 Christian Holm Christensen <cholm@nbi.dk>
- Fixed a few problems in Fed backend

* Fri Mar 02 2007 Christian Holm Christensen <cholm@nbi.dk>
- New version and soversion update

* Tue Feb 20 2007 Christian Holm Christensen <cholm@nbi.dk>
- New version

* Fri Feb 16 2007 Christian Holm Christensen <cholm@nbi.dk>
- Fix some problems in the code

* Tue Feb 13 2007 Christian Holm Christensen <cholm@nbi.dk>
- New upstream

* Tue Feb 06 2007 Christian Holm Christensen <cholm@nbi.dk>
- Added preliminary Fed client

* Tue Jan 16 2007 Christian Holm Christensen <cholm@nbi.dk>
- Eetter option handling

* Tue Jan 16 2007 Christian Holm Christensen <cholm@nbi.dk>
- Eetter option handling

* Fri Jan 12 2007 Christian Holm Christensen <cholm@nbi.dk>
- Versioned lib RPM

* Thu Jan 11 2007 Christian Holm Christensen <cholm@nbi.dk>
- bumped for fmddaq

* Thu Jan 11 2007 Christian Holm Christensen <cholm@nbi.dk>
- Added calibrations iterations register interface

* Thu Jan 04 2007 Christian Holm Christensen <cholm@nbi.dk>
- New upstream

* Wed Oct 25 2006 Christian Holm Christensen <cholm@nbi.dk>
- Added all flags to FmdStatus

* Wed Oct 18 2006 Christian Holm Christensen <cholm@nbi.dk>
- New release, that introduces test on/off

* Tue Oct 10 2006 Christian Holm Christensen <cholm@nbi.dk>
- Fixes from fmddaq

* Tue Oct 10 2006 Christian Holm Christensen <cholm@nbi.dk>
- Reestablished old setup interface

* Mon Oct 09 2006 Christian Holm Christensen <cholm@nbi.dk>
- Fixes to documentation

* Mon Oct 09 2006 Christian Holm Christensen <cholm@nbi.dk>
- New version, that has new structure

* Fri Oct 06 2006 Christian Holm Christensen <cholm@nbi.dk>
- Make FMD inherit from BC

* Mon Oct 02 2006 Christian Holm Christensen <cholm@nbi.dk>
- New upstream

* Fri Aug 25 2006 Christian Holm Christensen <cholm@nbi.dk>
- Fix units of T

* Mon Aug 14 2006 Christian Holm Christensen <cholm@nbi.dk>
- Uh\!?

* Mon Aug 14 2006 Christian Holm Christensen <cholm@nbi.dk>
- Fixes to status interface

* Tue May 23 2006 Forward Multiplicity Detector <fmd@dhcp61.dhcp.nbi.dk> 
- Initial build.


